<?php
/*
  Plugin Name: One - Time Purchase
  Description: In this page, you can set detail of One - Time Purchase plan
  Version: 1.1
 */


if (!class_exists('WP_List_Table')) {
    require_once(ABSPATH . 'wp-admin/includes/class-wp-list-table.php');
}

function one_time_purchase_admin_menu() {
    add_menu_page(__('One - Time Purchase', ''), __('One - Time Purchase', ''), 'activate_plugins', 'one_time_purchase', 'one_time_purchase_form_page_handler');
}

add_action('admin_menu', 'one_time_purchase_admin_menu');

function one_time_purchase_form_page_handler() {
    global $wpdb;
    $table_name = 'wp_one_time_purchase'; // do not forget about tables prefix

    $message = '';
    $notice = '';

    // this is default $item which will be used for new records
    $default = array(
        'id' => 0,
        'name' => '',
        'description' => '',
        'price_per_word' => ''
    );

    // here we are verifying does this request is post back and have correct nonce
    if (wp_verify_nonce($_REQUEST['nonce'], basename(__FILE__))) {

        // combine our default item with request params
        $item = shortcode_atts($default, $_REQUEST);

        $item = $wpdb->get_row("select * FROM $table_name where id = 1");

        if (!$item) {
            $result = $wpdb->insert($table_name, array(
                'name' => $_REQUEST[one_time_purchase_name],
                'description' => $_REQUEST[one_time_purchase_description],
                'price_per_word' => $_REQUEST[price_per_word]
            ));
        } else {
            $sql = " UPDATE $table_name SET name = '$_REQUEST[one_time_purchase_name]', description = '$_REQUEST[one_time_purchase_description]' , price_per_word = '$_REQUEST[price_per_word]' WHERE id = 1";
            $result = $wpdb->query($sql);
        }
        $message = __('Price was successfully updated', 'custom_table_example');

        echo "<script type='text/javascript'>
			window.location=document.location.href = 'admin.php?page=one_time_purchase';
			</script>";
    } else {
        // if this is not post back we load item to edit or give new one to create
        $item = $wpdb->get_row($wpdb->prepare("SELECT * FROM $table_name WHERE id = %d", 1), ARRAY_A);
        if (!$item) {
            $item = $default;
            $notice = __('Data not found', 'custom_table_example');
        }
    }

    // here we adding our custom meta box
    add_meta_box('myprofile_form_meta_box', 'One - Time Purchase Settings', 'one_time_purchase_meta_box_handler', 'person', 'normal', 'default');
    ?>
    <div class="wrap">
        <div class="icon32 icon32-posts-post" id="icon-edit"><br></div>
        <h2><?php _e('One - Time Purchase', 'custom_table_example') ?></h2>

        <?php if (!empty($notice)): ?>
            <div id="notice" class="error"><p><?php echo $notice ?></p></div>
        <?php endif; ?>
        <?php if (!empty($message)): ?>
            <div id="message" class="updated"><p><?php echo $message ?></p></div>
        <?php endif; ?>

        <form id="form" method="POST">
            <input type="hidden" name="nonce" value="<?php echo wp_create_nonce(basename(__FILE__)) ?>"/>
            <?php /* NOTICE: here we storing id to determine will be item added or updated */ ?>
            <input type="hidden" name="id" value="<?php echo $_REQUEST['id']; ?>"/>

            <div class="metabox-holder" id="poststuff">
                <div id="post-body">
                    <div id="post-body-content">
                        <?php /* And here we call our custom meta box */ ?>
                        <?php do_meta_boxes('person', 'normal', $item); ?>
                        <input type="submit" value="<?php _e('Save', 'custom_table_example') ?>" id="submit" class="button-primary" name="submit">
                    </div>
                </div>
            </div>
        </form>
    </div>
    <?php
}

function one_time_purchase_meta_box_handler($item) {
    ?>
    <script type="text/javascript" src="<?php echo plugins_url('', __FILE__); ?>/js/jquery.min.js"></script>
    <script type="text/javascript">
        jQuery(function ($) {
            $(".only_nums").keypress(function (e) {
                if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57) && (e.which != 46)) {
                    return false;
                }
            });
        });
    </script>
    <table cellspacing="2" cellpadding="5" border="0" style="width: 100%;" class="form-table">
        <tbody>
            <tr class="form-field">
                <th valign="top" scope="row">
                    <label for="one_time_purchase_name"><?php _e('Name', '') ?></label>
                </th>
                <td>
                    <input  id="one_time_purchase_name" name="one_time_purchase_name" type="text" style="width:80%"  value="<?php echo esc_attr($item['name']) ?>" required>
                </td>
            </tr>
            <tr class="form-field">
                <th valign="top" scope="row">
                    <label for="one_time_purchase_description"><?php _e('Description', '') ?></label>
                </th>
                <td>
                    <input  id="one_time_purchase_description" name="one_time_purchase_description" type="text" style="width:80%"  value="<?php echo esc_attr($item['description']) ?>" required>
                </td>
            </tr>
            <tr class="form-field">
                <th valign="top" scope="row">
                    <label for="price_per_word"><?php _e('Price Per Word', '') ?></label>
                </th>
                <td>
                    <input  id="price_per_word" maxlength="6" name="price_per_word" class="only_nums" type="text" style="width:80%"  value="<?php echo esc_attr($item['price_per_word']) ?>" required>
                </td>
            </tr>
        </tbody>
    </table>
    <?php
}
