<?php

/* Ajax call for update Proofreader Status */
add_action('wp_ajax_nopriv_change_proof_status', 'change_proof_status_callback');
add_action('wp_ajax_change_proof_status', 'change_proof_status_callback');

function change_proof_status_callback() {

    $proof_id = $_POST['proof_id'];
    $status = $_POST['status'];
    global $wpdb;

    $update = update_user_meta($proof_id, 'test_status', $status);
    $subject = '[' . get_bloginfo('name') . '] Your application has been ' . $status;
    //send_proof_notification('', $proof_id, 'Your application has been ' . $status, 1, 1, $subject);

    if ($status == 'rejected') {

        $wpdb->delete('tbl_proofreader_general_details', array('fk_proofreader_id' => $proof_id));
        $wpdb->delete('tbl_proofreader_notifications', array('fk_proofreader_id' => $proof_id));
        $wpdb->delete('tbl_proofreader_test', array('fk_proofreader_id' => $proof_id));
        $wpdb->delete('wp_proofreader_notification_setting', array('user_id' => $proof_id));
        $wpdb->delete('job_history', array('proof_id' => $proof_id));
        wp_delete_user($proof_id);
    }
    echo 1;
    die();
}

/* End Ajax call for update Proofreader Status */


add_action('wp_ajax_nopriv_read_notification', 'read_notification_callback');
add_action('wp_ajax_read_notification', 'read_notification_callback');

function read_notification_callback() {
    global $wpdb;
    $noty_out = '';
    $notify_id = $_REQUEST['notify_id'];
    $user_id = get_current_user_id();
    $current_user = wp_get_current_user();
    $user_roles_array = $current_user->roles;
    $user_role = array_shift($user_roles_array);

    if ($user_role == 'customer') {
        $table_name = "tbl_customer_notifications";
        $field = 'fk_customer_id';
        $primary = 'pk_cust_notification_id';
    } elseif ($user_role == 'proofreader') {
        $table_name = "tbl_proofreader_notifications";
        $field = 'fk_proofreader_id';
        $primary = 'pk_proof_notification_id';
    }
    if ($notify_id) {
        $update = $wpdb->update(
                $table_name, array(
            'is_view' => 1,
                ), array(
            $field => $user_id,
            $primary => $notify_id
                )
        );
    } else {
        $update = $wpdb->update(
                $table_name, array(
            'is_view' => 1,
                ), array(
            $field => $user_id
                )
        );
    }

    if ($user_role == 'customer') {
        $notification_list = $wpdb->get_results("SELECT * FROM `tbl_customer_notifications` WHERE fk_customer_id= $user_id AND is_view = 0 ORDER BY pk_cust_notification_id DESC LIMIT 5");
    } else if ($user_role == 'proofreader') {
        $notification_list = $wpdb->get_results("SELECT * FROM `tbl_proofreader_notifications` WHERE fk_proofreader_id= $user_id AND is_view = 0 ORDER BY pk_proof_notification_id DESC LIMIT 3");
    }

    foreach ($notification_list as $notifications) {
        if (count($notification_list) > 0) {
            if ($user_role == 'customer')
                $notify_id = $notifications->pk_cust_notification_id;
            elseif ($user_role == 'proofreader')
                $notify_id = $notifications->pk_proof_notification_id;

            $noty_out .= '<li class="notification">
                <i class="fa fa-bell-o" aria-hidden="true"></i>';
            $noty_out .= '<a data-notify_id="' . $notify_id . '" href="javascript:void(0);">' . substr($notifications->description, 0, 15) . ' ...</a>
                <span> ';

            $date = new DateTime($notifications->notification_date);
            $noty_out .= ago($date->format('U'));

            $noty_out .= '</span>
            </li>';
        }
    }
    if ($user_role == 'customer') {
        $view_notification = get_permalink(1119);
    } else if ($user_role == 'proofreader') {
        $view_notification = get_permalink(1121);
    }
    $noty_out .= '<li class="view_all">
        <a href="' . $view_notification . '">View all notification</a>                                    
    </li> ';
    echo $noty_out;
    die();
}

/* Ajax call for approve Proofreader payment request */
add_action('wp_ajax_nopriv_approve_payment', 'approve_payment_callback');
add_action('wp_ajax_approve_payment', 'approve_payment_callback');

function approve_payment_callback() {
    global $wpdb;
    $payment_id = $_POST['payment_id'];
    echo $wpdb->update(
            'tbl_proofreader_revenue', array('status' => 'Completed', 'approval_date' => date('Y-m-d H:i:s'), "modified_date" => date('Y-m-d H:i:s')), array('pk_proofreader_revenue_id' => $payment_id)
    );

    $doc_revenue = $wpdb->get_row("SELECT * FROM tbl_proofreader_revenue WHERE pk_proofreader_revenue_id= $payment_id");
    if ($doc_revenue) {
        $proof_id = $doc_revenue->fk_proofreader_id;
        $doc_datail_id = $doc_revenue->fk_doc_id;

        $doc_table .= '<table border="1">';
        $doc_table .= '<thead>';
        $doc_table .= '<tr>';
        $doc_table .= ' <th>Doc Title</th>';
        $doc_table .= '<th>Section No.</th>';
        $doc_table .= '<th>Requested Amount</th>';
        $doc_table .= ' <th>Requested Date</th>';
        $doc_table .= ' <th>Edited Word</th>';
        $doc_table .= ' <th>Payment Status</th>';
        $doc_table .= ' <th>Check Status</th>';
        $doc_table .= '</tr>';
        $doc_table .= '</thead>';
        $doc_table .= '<tbody>';


        $check_proof = $wpdb->get_row("SELECT * FROM `tbl_proofreaded_doc_details`  Where fk_doc_details_id =  $doc_datail_id");
        if ($check_proof->fk_proofreader_id == $proof_id) {
            $cstatus = "Single Check";
        } elseif ($check_proof->Fk_DoubleProofReader_Id == $proof_id) {
            $cstatus = "Double Check";
        } else {
            $cstatus = "";
        }
        $main_doc = $wpdb->get_row("SELECT * FROM wp_customer_document_details WHERE pk_doc_details_id= $doc_datail_id AND is_active= 1");

        $result_maindoc = $wpdb->get_row("SELECT * FROM wp_customer_document_main WHERE pk_document_id= $main_doc->fk_doc_main_id AND Status=1");
        $total_docs = $wpdb->get_results("SELECT * FROM wp_customer_document_details WHERE fk_doc_main_id= $result_maindoc->pk_document_id AND is_active= 1 ORDER BY pk_doc_details_id");
        $count = 0;
        foreach ($total_docs as $total_doc) {
            $count++;
            if ($total_doc->pk_doc_details_id == $doc_datail_id) {
                $doc_count = $count;
            }
        }

        $proof_info = get_userdata($user_ID);

        $doc_table .= '<tr>';
        $doc_table .= '<td>' . $result_maindoc->document_title . '</td>';
        $doc_table .= '<td>' . $doc_count . '</td>';
        $doc_table .= '<td>$' . $doc_revenue->requested_amount . '</td>';
        $doc_table .= '<td>' . date("m/d/Y h:i:s A") . '</td>';
        $doc_table .= '<td>' . $doc_revenue->edited_word . '</td>';
        $doc_table .= '<td>' . $doc_revenue->status . '</td>';
        $doc_table .= '<td>' . $cstatus . '</td>';
        $doc_table .= '</tr>';
    }
    $doc_table .= '</tbody>';
    $doc_table .= '</table>';

    $subject = '[' . get_bloginfo('name') . '] Your payment has been approved by admin';
    $desc = "<p>Your payment has been approved.<p>";
    $noti = "<p>Your payment has been approved.<p>";
    $desc .= "<p></br></br>The document details are displayed below:</p>";
    $desc = $desc . ' ' . $doc_table;
    //send_proof_notification('', $proof_id, $desc, 1, 1, $subject, $noti);
    die();
}

/* End Ajax call for approve Proofreader payment request */



/* Ajax call for update Customer Document order */
add_action('wp_ajax_nopriv_change_doc_order_no', 'change_doc_order_no_callback');
add_action('wp_ajax_change_doc_order_no', 'change_doc_order_no_callback');

function change_doc_order_no_callback() {

    global $wpdb;
    $order_no = $_POST['order_no'];
    $doc_id = $_POST['doc_id'];

    $old_order_no = $_POST['old_order_no'];


    $sub_documents = $wpdb->get_results("SELECT * FROM `wp_customer_document_details` WHERE fk_doc_main_id= $doc_id");
    if (count($sub_documents) > 0) {
        $pending_doc = 0;
        foreach ($sub_documents as $sub_document) {
            $status = $sub_document->status;
            if ($status == 'Pending')
                $pending_doc++;
        }
    }
    if (count($sub_documents) != $pending_doc) {
        echo 0;
        die();
    }

    $documents = $wpdb->get_results("SELECT * FROM `wp_customer_document_main` where Status =1 ");
    $com_doc_array = array();
    $com_doc_array[] .= $order_no;
    foreach ($documents as $document) {
        $document_id = $document->pk_document_id;
        $sub_documents = $wpdb->get_results("SELECT * FROM `wp_customer_document_details` WHERE fk_doc_main_id= $document_id");
        if (count($sub_documents) > 0) {
            foreach ($sub_documents as $sub_document) {
                $status = $sub_document->status;
                if ($status == 'In Process' || $status == 'Single Check' || $status == 'Completed') {
                    if (!in_array($document_id, $com_doc_array)) {
                        $com_doc_array[] .= $document->order_no;
                    }
                    if ($document->order_no == $order_no) {
                        echo 1;
                        die();
                    }
                }
            }
        }
    }


    $count = 0;
    if ($old_order_no < $order_no):
        $changable_documents = $wpdb->get_results("SELECT * FROM `wp_customer_document_main` WHERE order_no > $old_order_no AND pk_document_id != $doc_id AND Status =1");
        $order_count = $old_order_no;
    else:
        $changable_documents = $wpdb->get_results("SELECT * FROM `wp_customer_document_main` WHERE order_no >= $order_no AND pk_document_id != $doc_id AND Status =1");
        $order_count = $order_no;
    endif;
    foreach ($changable_documents as $changable_document) {

        $changedocument_id = $changable_document->pk_document_id;

        $change_sub_documents = $wpdb->get_results("SELECT * FROM `wp_customer_document_details` WHERE fk_doc_main_id= $changedocument_id");

        if (count($change_sub_documents) > 0) {
            $pending_docs = 0;

            foreach ($change_sub_documents as $change_sub_document) {
                $status = $change_sub_document->status;
                if ($status == 'Pending')
                    $pending_docs++;
            }

            if (count($change_sub_documents) == $pending_docs) {
                $count++;
                if ($old_order_no < $order_no) {

                    if ($count != 1) {
                        $order_count++;
                    }
                } else {

                    $order_count++;
                }

                while (in_array($order_count, $com_doc_array)) {
                    $order_count++;
                };

                echo $wpdb->update(
                        'wp_customer_document_main', array('order_no' => $order_count), array('pk_document_id' => $changedocument_id)
                );
            }
        }
    }
    echo $wpdb->update(
            'wp_customer_document_main', array('order_no' => $order_no), array('pk_document_id' => $doc_id)
    );

    die();
}

/* End Ajax call for update Customer Document order */

/* Ajax call for unassign proofreader */
add_action('wp_ajax_nopriv_unassign_proofreader', 'unassign_proofreader_callback');
add_action('wp_ajax_unassign_proofreader', 'unassign_proofreader_callback');

function unassign_proofreader_callback() {

    global $wpdb;
    $proof_id = $_POST['proof_id'];
    $doc_id = $_POST['doc_id'];
    $doc_status = trim($_POST['doc_status']);

    if ($doc_status == "In Process") {

        $result = $wpdb->delete('wp_assigned_   document_details', array('fk_doc_details_id' => $doc_id, 'fk_proofreader_id' => $proof_id));
        echo $result = $wpdb->update('wp_customer_document_details', array('status' => 'Pending'), array('pk_doc_details_id' => $doc_id));
    } elseif ($doc_status == "Single Check") {

        $result = $wpdb->delete('wp_assigned_document_details', array('fk_doc_details_id' => $doc_id, 'fk_proofreader_id' => $proof_id));
        echo $result = $wpdb->update('tbl_proofreaded_doc_details', array('Fk_DoubleProofReader_Id' => NULL), array('fk_doc_details_id' => $doc_id, 'Fk_DoubleProofReader_Id' => $proof_id));
    } else {
        echo 0;
    }
    die();
}

/* End Ajax call for unassign proofreader */


/* Ajax call for Delete Document */
add_action('wp_ajax_nopriv_delete_doc', 'delete_doc_callback');
add_action('wp_ajax_delete_doc', 'delete_doc_callback');

function delete_doc_callback() {

    global $wpdb;
    $doc_id = $_POST['doc_id'];
   echo $wpdb->update('wp_customer_document_main', array('Status' => 0, 'order_no' => 0), array('pk_document_id' => $doc_id), array('%d', '%d'), array('%d'));
    $order_no = $wpdb->get_var("SELECT order_no FROM wp_customer_document_main WHERE pk_document_id= $doc_id");
    $documents = $wpdb->get_results("SELECT * FROM `wp_customer_document_main` where Status = 1   AND order_no > $order_no ORDER BY pk_document_id ASC ");
    foreach ($documents as $document) {
        $order_no++;
        $wpdb->update('wp_customer_document_main', array('order_no' => $order_no), array('pk_document_id' => $document->pk_document_id), array('%d'), array('%d'));
    }
    die();
}

/* End Ajax call for Delete Document */

/* Ajax call for update customer credit */
add_action('wp_ajax_nopriv_update_cust_credit', 'update_cust_credit_callback');
add_action('wp_ajax_update_cust_credit', 'update_cust_credit_callback');

function update_cust_credit_callback() {

    global $wpdb;
    $cust_id = $_POST['cust_id'];
    $credits = $_POST['credits'];
    echo $wpdb->update('tbl_customer_general_info', array('remaining_credit_words' => $credits), array('fk_customer_id' => $cust_id), array('%d'), array('%d'));
    die();
}

/* End Ajax call for update customer credit */

function getSubdocuments($id){
	global $wpdb;
	$sub_documents = $wpdb->get_results("SELECT status FROM `wp_customer_document_details` WHERE fk_doc_main_id= $id AND is_active = 1");

	$pending = 0;
	$inprocess = 0;
	$single_check = 0;
	$complete = 0;
	$total = $wpdb->num_rows;

	foreach ($sub_documents as $sub_document) {
		$status = $sub_document->status;
		if ($status == 'Pending')
			$pending++;
		if ($status == 'In Process') {
			$inprocess++;
		}
		if ($status == 'Single Check') {
			$single_check++;
		}
		if ($status == 'Completed') {
			$complete++;
		}
	}

	return json_encode(['total' => $total, 'pending' => $pending, 'inprocess' => $inprocess, 'single_check' => $single_check, 'complete' => $complete]);
}

/* Ajax call for server side processing - all documents */
add_action('wp_ajax_nopriv_all_documents', 'all_documents_callback');
add_action('wp_ajax_all_documents', 'all_documents_callback');

function all_documents_callback() {
	header("Content-Type: application/json");
	// DB table to use
	$table = <<<EOT
 (
    SELECT * FROM admin_all_documents_view_new
 ) temp
EOT;

	// Table's primary key
	$primaryKey = 'pk_document_id';

	// Array of database columns which should be read and sent back to DataTables.
	// The `db` parameter represents the column name in the database, while the `dt`
	// parameter represents the DataTables column identifier. In this case simple
	// indexes
	$columns = array(
		array( 'db' => 'document_title', 'dt' => 0 ),
		array(
			'db' => 'display_name',
			'dt' => 1,
			'formatter' => function( $d, $row ) {
				return '<a class="proofreader_name" href="' . site_url() . '/wp-admin/admin.php?page=view_user&user=' . $row[12] . '">' . $d . '</a>';
			}
		),
		array( 'db' => 'word_count',   'dt' => 2 ),
		array(
			'db'        => 'pk_document_id',
			'dt'        => 3,
			'formatter' => function( $d, $row ) {
				$sub_documents = json_decode(getSubdocuments($d));
				return $sub_documents->total;
			}
		),
		array(
			'db'        => 'pk_document_id',
			'dt'        => 4,
			'formatter' => function( $d, $row ) {
				$sub_documents = json_decode(getSubdocuments($d));
				return $sub_documents->pending;
			}
		),
		array(
			'db'        => 'pk_document_id',
			'dt'        => 5,
			'formatter' => function( $d, $row ) {
				$sub_documents = json_decode(getSubdocuments($d));
				return $sub_documents->inprocess;
			}
		),
		array(
			'db'        => 'pk_document_id',
			'dt'        => 6,
			'formatter' => function( $d, $row ) {
				$sub_documents = json_decode(getSubdocuments($d));
				return $sub_documents->complete;
			}
		),
		array(
			'db'        => 'upload_date',
			'dt'        => 7,
			'formatter' => function( $d, $row ) {
				return ($d) ? date('m/d/Y h:i:s A', strtotime($d)) : '';
			}
		),
		array(
			'db'        => 'Completed_date',
			'dt'        => 8,
			'formatter' => function( $d, $row ) {
				if($d && $row[7]) {
					$diff  = date_diff( date_create($row[7]), date_create($d) );
					return $diff->h . ':' . $diff->i . ':' . $diff->s;
				} else {
					return '';
				}
			}
		),
		array(
			'db'        => 'pk_document_id',
			'dt'        => 9,
			'formatter' => function( $d, $row ) {
				return '<a href="'. site_url() . '/wp-admin/admin.php?page=view_documents&doc_id=' . $d . '" ><i class="fa fa-eye" aria-hidden="true"></i></a>';
			}
		),
		array(
			'db'        => 'order_no',
			'dt'        => 10,
			'formatter' => function( $d, $row ) {
				$html = '<div class="edit_backend">';
				$html .= '<label>' . $d . '</label>';
				$html .= '<input data-order="' . $d . '"  type="text" value="' . $d . '"  class="only_num doc_order" style="display: none;"/>';
				$html .= '<input type="hidden" value="' . $row[9] . '" class="doc_id" />';
				$sub_documents = json_decode(getSubdocuments($row[9]));
				if ($sub_documents->total == $sub_documents->pending){
					$html .= '<a  href="javascript:void(0);" class="edit_order"><i class="fa fa-edit" aria-hidden="true"></i></a>';
				}
				$html .= '</div><div class="doc_msg"></div>';
				return $html;
			}
		),
		array(
			'db'        => 'pk_document_id',
			'dt'        => 11,
			'formatter' => function( $d, $row ) {
				$sub_documents = json_decode(getSubdocuments($d));
				if ($sub_documents->total == $sub_documents->complete || $sub_documents->total == $sub_documents->pending){
					return '<a class="delete_doc unassign_doc" data-doc="' . $d . '" href="javascript:void(0);"><i class="fa fa-close" aria-hidden="true"></i></a>';
				}
			}
		),
		array(
			'db' => 'user_id',
			'dt' => 12
		),
	);

	// SQL server connection information
	$sql_details = array(
		'user' => DB_USER,
		'pass' => DB_PASSWORD,
		'db'   => DB_NAME,
		'host' => DB_HOST
	);

	include_once( plugin_dir_path(__FILE__) . '/ssp.php' );
	echo json_encode(
		SSP::complex( $_GET, $sql_details, $table, $primaryKey, $columns, null, "Status = 1" )
	);

    die();
}

/* End Ajax call for server side processing - all documents */
