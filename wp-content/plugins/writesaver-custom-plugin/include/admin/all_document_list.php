<?php
wp_enqueue_style('admin-custom-bootstrap', WRITESAVER_CUSTOM_PLUGIN_URL . '/css/admin/bootstrap.min.css', '', 'all');
wp_enqueue_style('admin-custom-style', WRITESAVER_CUSTOM_PLUGIN_URL . '/css/admin/style.css', '', '', 'all');
wp_enqueue_style('admin-responsive-style', WRITESAVER_CUSTOM_PLUGIN_URL . '/css/admin/responsive.css', '', '', 'all');
wp_enqueue_style('admin-datatable-style', WRITESAVER_CUSTOM_PLUGIN_URL . '/css/admin/jquery.dataTables.min.css', '', '', 'all');
wp_enqueue_script('admin-custom-js', WRITESAVER_CUSTOM_PLUGIN_URL . '/js/admin/jquery.min.js', array('jquery'), '', 'all');
wp_enqueue_script('admin-jquery-js', WRITESAVER_CUSTOM_PLUGIN_URL . '/js/admin/custom.js', array('jquery'), '', 'all');
wp_enqueue_script('admin-datatable-script', WRITESAVER_CUSTOM_PLUGIN_URL . '/js/admin/jquery.dataTables.min.js', array('jquery'), '', true);
wp_enqueue_script('admin-custom-bootstrap-js', WRITESAVER_CUSTOM_PLUGIN_URL . '/js/admin/bootstrap.min.js', array('jquery'), '', 'all');
?>
<div class="load_overlay" id="loding">
    <img src="<?php echo get_template_directory_uri(); ?>/images/39.gif"/>
</div>
<div class="doc_list" id="doc_list"> 
    <h1>All Document List</h1> 
    <?php
    global $wpdb;
    $orders = $wpdb->get_results("SELECT max(order_no) as max_order FROM `wp_customer_document_main` where Status =1 and order_no != 0 ");
    ?>
    <input type="hidden" id="total_doc" value="<?php echo $orders[0]->max_order; ?>" />
    <table class="table" id="list_table" style="width: 100%; ">
        <thead>
            <tr>
                <th>Doc Title</th>
                <th>Customer Name</th>
                <th>Total Words in Doc</th>
                <th>Total Sub Docs</th>
                <th>Total Pending Sub Docs</th>
                <th>Total In Process Sub Docs</th>
                <th>Total Completed Sub Docs</th>
                <th>Uploaded Date</th>
                <th>Total Time To Completion</th>
                <th>View</th>
                <th>Edit order</th>
                <th>Delete</th>
				<th>Customer ID</th>
            </tr>
        </thead>
    </table>

</div>
<style type="text/css">
	.dataTables_wrapper .dataTables_processing{
		position: absolute !important;
		top: 0 !important;
		left: 0 !important;
		bottom: 0 !important;
		width: 100% !important;
		height: 100% !important;
		margin-left: 0 !important;
		margin-top: 0 !important;
		padding-top: 0 !important;
		text-align: center !important;
		font-size: 1.2em !important;
		background: #1f1a1a0f !important;
	}
</style>
<script>
    jQuery(document).ready(function () {

        jQuery('#list_table').dataTable({
			"pageLength":25,
            "bAutoWidth": false,
            "aoColumns": [
                {sWidth: '10%'},
                {sWidth: '10%'},
                {sWidth: '10%'},
                {sWidth: '10%', "bSortable": false},
                {sWidth: '10%', "bSortable": false},
                {sWidth: '10%', "bSortable": false},
                {sWidth: '10%', "bSortable": false},
                {sWidth: '10%'},
                {sWidth: '10%', "bSortable": false},
                {sWidth: '10%', "bSortable": false},
                {sWidth: '10%'},
                {sWidth: '10%', "bSortable": false}
            ],
			"columnDefs": [
				{
					"targets": [ 12 ],
					"visible": false,
					"searchable": false
				}
			],
            "aaSorting": [[7, 'desc']],
            "oLanguage": {
                "sEmptyTable": "No document available."
            },
			"processing": true,
			"serverSide": true,
			"ajax": '<?php echo admin_url('admin-ajax.php?action=all_documents'); ?>'
        });

    });

    jQuery('.edit_order').live('click', function (e) {
        jQuery(this).parents('td').find('label').hide();
        jQuery(this).parents('td').find('.doc_order').show();
        jQuery(this).after('<a href="javascript:void(0);" class="save_order"><i class="fa fa-check" aria-hidden="true"></i></a><a href="javascript:void(0);" class="cancel_order"><i class="fa fa-remove" aria-hidden="true"></i></a>');
        jQuery(this).hide();
    });

    jQuery('.cancel_order').live('click', function (e) {
        jQuery('.doc_msg').remove();
        var old_order_no = jQuery(this).parents('td').find('.doc_order').attr('data-order');
        jQuery(this).parents('td').find('.doc_order').val(old_order_no);
        jQuery(this).parents('td').find('.edit_order').show();
        jQuery(this).parents('td').find('label').show();
        jQuery(this).parents('td').find('.doc_order').hide();
        jQuery(this).parents('td').find('.save_order').remove();
        jQuery(this).parents('td').find('.cancel_order').remove();
    });

    jQuery('.save_order').live('click', function (e) {

        var total_doc = jQuery('#total_doc').val();
        var button = jQuery(this);
        jQuery('.order_msg').remove();
        jQuery('.order_msg').remove();
        var order_no = jQuery(this).parents('td').find('.doc_order').val();
        var doc_id = jQuery(this).parents('td').find('.doc_id').val();
        var old_order_no = jQuery(this).parents('td').find('.doc_order').attr('data-order');
        if (order_no == '') {
            jQuery(this).parents('td').parent('tr').after('<tr class="order_msg"><td colspan="11" class="doc_msg"><span class="text-danger order_msg">Enter order no..</span></td></tr>');
            return false;
        } else if (order_no == 0) {
            jQuery(this).parents('td').parent('tr').after('<tr  class="order_msg"><td colspan="11" class="doc_msg"><span class="text-danger order_msg">Enter order no greater than 0.</span></td></tr>');
            return false;
        } else if (parseInt(order_no) > parseInt(total_doc)) {
            jQuery(this).parents('td').parent('tr').after('<tr  class="order_msg"><td colspan="11" class="doc_msg"><span class="text-danger order_msg">Enter maximum ' + total_doc + ' order no..</span></td></tr>');
            return false;
        } else if (parseInt(order_no) == parseInt(old_order_no)) {
            jQuery(this).parents('td').parent('tr').after('<tr><td colspan="11" class="doc_msg"><span class="text-danger order_msg">Enter diffrent order no..</span></td></tr>');
            return false;
        }

        if (order_no && doc_id) {
            jQuery('#loding').show();
            jQuery('.order_msg').remove();
            jQuery.ajax({
                url: "<?php echo admin_url('admin-ajax.php'); ?>",
                type: "POST",
                data: {
                    action: 'change_doc_order_no',
                    order_no: order_no,
                    doc_id: doc_id,
                    old_order_no: old_order_no
                },
                success: function (data) {
                    if (data == 1) {
                        button.parents('td').parent('tr').after('<tr class="order_msg"><td colspan="11" class="doc_msg"><span class="text-danger order_msg">Order no already in process... </span></td></tr>');
                        button.parents('td').find('.doc_order').val(old_order_no);
                    } else if (data == 0) {
                        button.parents('td').parent('tr').after('<tr class="order_msg"><td colspan="11" class="doc_msg"><span class="text-danger order_msg">Document is in process...</span></td></tr>');
                        button.parents('td').find('.doc_order').val(old_order_no);
                    } else {
                        button.parents('td').parent('tr').after('<tr class="order_msg"><td colspan="11" class="doc_msg"><span class="text-success order_msg">Order of document changed successfully...</span></td></tr>');
                        button.parents('td').find('.edit_order').show();
                        button.parents('td').find('label').show();
                        button.parents('td').find('.doc_order').hide();
                        button.parents('td').find('.cancel_order').remove();
                        button.parents('td').find('.save_order').remove();

                        window.setTimeout(function () {
                            jQuery(".order_msg").fadeOut(5000);
                            location.reload();
                        }, 2000);
                    }
                    window.setTimeout(function () {
                        jQuery('#loding').hide();
                    }, 500);

                },
                error: function (jqXHR, textStatus, errorThrown) {
                    jQuery('#loding').hide();
                    console.log(jqXHR + " :: " + textStatus + " :: " + errorThrown);
                }
            });

        }
    });

    jQuery('.delete_doc').live('click', function (e) {
        debugger;
        var button = jQuery(this);
        var doc_id = jQuery(this).attr('data-doc');

        if (doc_id) {
            var r = confirm("Are you sure to delete document?");
            if (r == true) {
                jQuery('#loding').show();
                jQuery.ajax({
                    url: "<?php echo admin_url('admin-ajax.php'); ?>",
                    type: "POST",
                    data: {
                        action: 'delete_doc',
                        doc_id: doc_id
                    },
                    success: function (data) {
                        jQuery('#loding').hide();
                        if (data == 0) {
                            button.after('<span class="text-danger statusmsg">Not deleted sucessfully...</span>');
                            jQuery(".statusmsg").fadeOut(5000);
                        } else {
                            button.after('<span class="text-success statusmsg">Deleted sucessfully... </span>');
                            window.setTimeout(function () {
                                jQuery(".statusmsg").fadeOut(7000);
                                location.reload();
                            }, 2000);
                        }
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        jQuery('.pop_loding').hide();
                        console.log(jqXHR + " :: " + textStatus + " :: " + errorThrown);
                    }
                });
            } else {
                $(r).dialog("close");
            }
        }
    });
</script>

