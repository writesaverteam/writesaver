<?php
	//only admins can get this
	if(!function_exists("current_user_can") || (!current_user_can("manage_options") && !current_user_can("pmpro_discountcodes")))
	{
		die(__("You do not have permissions to perform this action.", 'writesaver'));
	}
	
	wp_enqueue_style('admin-custom-bootstrap', WRITESAVER_CUSTOM_PLUGIN_URL . '/css/admin/bootstrap.min.css', '', 'all');
	wp_enqueue_style('admin-font-style', get_template_directory_uri() . '/css/font-awesome.css', '', '', 'all');
	wp_enqueue_style('admin-datatable-style', WRITESAVER_CUSTOM_PLUGIN_URL . '/css/admin/jquery.dataTables.min.css', '', '', 'all');
	wp_enqueue_style('admin-custom-style', WRITESAVER_CUSTOM_PLUGIN_URL . '/css/admin/style.css', '', '', 'all');
	wp_enqueue_script('admin-custom-bootstrap-js', WRITESAVER_CUSTOM_PLUGIN_URL . '/js/admin/bootstrap.min.js', array('jquery'), '', 'all');
	wp_enqueue_script('admin-datatable-script', WRITESAVER_CUSTOM_PLUGIN_URL . '/js/admin/jquery.dataTables.min.js', array('jquery'), '', true);
	wp_enqueue_script('admin-jquery-js', WRITESAVER_CUSTOM_PLUGIN_URL . '/js/admin/custom.js', array('jquery'), '', 'all');

	//vars
	global $wpdb;

	if(isset($_REQUEST['edit']))
		$edit = intval($_REQUEST['edit']);
	else
		$edit = false;

	if(isset($_REQUEST['delete']))
		$delete = intval($_REQUEST['delete']);
	else
		$delete = false;

	if(isset($_REQUEST['saveid']))
		$saveid = intval($_POST['saveid']);
	else
		$saveid = false;			

	if(isset($_REQUEST['s']))
		$s = sanitize_text_field($_REQUEST['s']);
	else
		$s = "";
	
	if($saveid)
	{
		//get vars
		//disallow/strip all non-alphanumeric characters except -
		$code = preg_replace("/[^A-Za-z0-9\-]/", "", sanitize_text_field($_POST['code']));
		$starts_month = intval($_POST['starts_month']);
		$starts_day = intval($_POST['starts_day']);
		$starts_year = intval($_POST['starts_year']);
		$expires_month = intval($_POST['expires_month']);
		$expires_day = intval($_POST['expires_day']);
		$expires_year = intval($_POST['expires_year']);
		$uses = intval($_POST['uses']);
		$discounted_words = intval($_POST['discounted_words']);
		$type = intval($_POST['type']);
		$subscription = intval($_POST['subscription']);
		$minimum_amount = intval($_POST['minimum_amount']);
		
		//fix up dates		
		$starts = date_i18n("Y-m-d", strtotime($starts_month . "/" . $starts_day . "/" . $starts_year, current_time("timestamp")));
		$expires = date_i18n("Y-m-d", strtotime($expires_month . "/" . $expires_day . "/" . $expires_year, current_time("timestamp")));
				
		//insert/update/replace discount code
		$wpdb->replace(
			'tbl_discount_codes',
			array(
				'id'=>max($saveid, 0),
				'code' => $code,
				'starts' => $starts,
				'expires' => $expires,
				'uses' => $uses,
				'discounted_words' => $discounted_words,
				'type' => $type,
				'subscription' => $subscription,
				'minimum_amount' => $minimum_amount,			
			),
			array(
				'%d',
				'%s',
				'%s',
				'%s',
				'%d',
				'%d',
				'%d',
				'%d',
				'%s',
			)
		);
		
		//check for errors and show appropriate message if inserted or updated
		if(empty($wpdb->last_error)) {
			if($saveid < 1) {
				//insert
				$pmpro_msg = __("Discount code added successfully.", 'writesaver');
				$pmpro_msgt = "success";
				$saved = true;
				$edit = $wpdb->insert_id;
			} else {
				//updated
				$pmpro_msg = __("Discount code updated successfully.", 'writesaver');
				$pmpro_msgt = "success";
				$saved = true;
				$edit = $saveid;
			}
		} else {
			if($saveid < 1) {
				//error inserting
				$pmpro_msg = __("Error adding discount code. That code may already be in use.", 'writesaver') . $wpdb->last_error;
				$pmpro_msgt = "error";
			} else {
				//error updating
				$pmpro_msg = __("Error updating discount code. That code may already be in use.", 'writesaver');
				$pmpro_msgt = "error";
			}
		}				

		//now add the membership level rows
		if($saved && $edit > 0)
		{
			//all good. set edit = NULL so we go back to the overview page
			$edit = NULL;
		}
	}

	//are we deleting?
	if(!empty($delete))
	{
		//is this a code?
		$code = $wpdb->get_var( $wpdb->prepare( "SELECT code FROM tbl_discount_codes WHERE id = %d LIMIT 1", $delete ) );
		if(!empty($code))
		{
			//delete the code
			$r2 = $wpdb->delete('tbl_discount_codes', array('id'=>$delete), array('%d'));
			
			if($r2 !== false)
			{
				$pmpro_msg = sprintf(__("Code %s deleted successfully.", 'writesaver'), $code);
				$pmpro_msgt = "success";
			}
			else
			{
				$pmpro_msg = __("Error deleting discount code. Please try again.", 'writesaver');
				$pmpro_msgt = "error";
			}
		}
		else
		{
			$pmpro_msg = __("Code not found.", 'writesaver');
			$pmpro_msgt = "error";
		}
	}
?>

	<?php if($edit) { ?>

		<h1>
			<?php
				if($edit > 0)
					echo __("Edit Discount Code", 'writesaver');
				else
					echo __("Add New Discount Code", 'writesaver');
			?>
		</h1>

		<?php if(!empty($pmpro_msg)) { ?>
			<div id="message" class="<?php if($pmpro_msgt == "success") echo "updated fade"; else echo "error"; ?>"><p><?php echo $pmpro_msg?></p></div>
		<?php } ?>

		<div>
			<?php
				// get the code...
				if($edit > 0)
				{
					$code = $wpdb->get_row(
						$wpdb->prepare("
						SELECT *, UNIX_TIMESTAMP(starts) as starts, UNIX_TIMESTAMP(expires) as expires
						FROM tbl_discount_codes
						WHERE id = %d LIMIT 1",
						$edit ),
						OBJECT
					);

					$uses = $wpdb->get_var( $wpdb->prepare( "SELECT COUNT(*) FROM tbl_discount_codes_uses WHERE code_id = %d", $code->id ) );
					$temp_id = $code->id;
				}

				// didn't find a discount code, let's add a new one...
				if(empty($code->id)) $edit = -1;

				//defaults for new codes
				if($edit == -1)
				{
					$code = new stdClass();
					$code->code = getDiscountCode();
				}
			?>
			<form action="" method="post">
				<input name="saveid" type="hidden" value="<?php echo $edit?>" />
				<table class="form-table">
                <tbody>
                    <tr>
                        <th scope="row" valign="top"><label><?php _e('ID', 'writesaver');?>:</label></th>
                        <td class="pmpro_lite"><?php if(!empty($code->id)) echo $code->id; else echo __("This will be generated when you save.", 'writesaver');?></td>
                    </tr>

                    <tr>
                        <th scope="row" valign="top"><label for="code"><?php _e('Code', 'writesaver');?>:</label></th>
                        <td><input name="code" type="text" required size="20" value="<?php echo str_replace("\"", "&quot;", stripslashes($code->code))?>" /></td>
                    </tr>
					
					<tr>
                        <th scope="row" valign="top"><label for="type"><?php _e('Type of Purchase', 'writesaver');?>:</label></th>
                        <td>
							<select name="type" required onchange="if(jQuery(this).val() == '2'){ jQuery('#subscription_tr').show();jQuery('#subscription').attr('required',true);} else { jQuery('#subscription_tr').hide();jQuery('#subscription').attr('required',false);}">
								<option value="">Select</option>
								<option value="1" <?php if($code->type == 1){ echo 'selected'; }?>>One Time Purchase</option>
								<option value="2" <?php if($code->type == 2){ echo 'selected'; }?>>Subscription</option>
							</select>
						</td>
                    </tr>
					
					<tr id="subscription_tr" style="<?php if(!empty($code->type) && $code->type == 2) { echo 'display: block'; } else { echo 'display: none';} ?>">
                        <th scope="row" valign="top"><label for="subscription"><?php _e('Choose Subscription', 'writesaver');?>:</label></th>
                        <td>
							<select name="subscription" id="subscription">
								<?php
								$levels = $wpdb->get_results("SELECT * FROM $wpdb->pmpro_membership_levels");
								foreach($levels as $level)
								{
									echo '<option value="' . $level->id . '">' . $level->name .  '</option>';
								}
								?>
							</select>
						</td>
                    </tr>

					<?php
						//some vars for the dates
						$current_day = date_i18n("j");
						if(!empty($code->starts))
							$selected_starts_day = date_i18n("j", $code->starts);
						else
							$selected_starts_day = $current_day;
						if(!empty($code->expires))
							$selected_expires_day = date_i18n("j", $code->expires);
						else
							$selected_expires_day = $current_day;

						$current_month = date_i18n("M");
						if(!empty($code->starts))
							$selected_starts_month = date_i18n("m", $code->starts);
						else
							$selected_starts_month = date_i18n("m");
						if(!empty($code->expires))
							$selected_expires_month = date_i18n("m", $code->expires);
						else
							$selected_expires_month = date_i18n("m");							
						
						$current_year = date_i18n("Y");
						if(!empty($code->starts))
							$selected_starts_year = date_i18n("Y", $code->starts);
						else
							$selected_starts_year = $current_year;
						if(!empty($code->expires))
							$selected_expires_year = date_i18n("Y", $code->expires);
						else
							$selected_expires_year = (int)$current_year + 1;
					?>

					<tr>
                        <th scope="row" valign="top"><label for="starts"><?php _e('Start Date', 'writesaver');?>:</label></th>
                        <td>
							<select name="starts_month" required>
								<?php
									for($i = 1; $i < 13; $i++)
									{
									?>
									<option value="<?php echo $i?>" <?php if($i == $selected_starts_month) { ?>selected="selected"<?php } ?>><?php echo date_i18n("M", strtotime($i . "/1/" . $current_year, current_time("timestamp")))?></option>
									<?php
									}
								?>
							</select>
							<input name="starts_day" required type="text" size="2" value="<?php echo $selected_starts_day?>" />
							<input name="starts_year" required type="text" size="4" value="<?php echo $selected_starts_year?>" />
						</td>
                    </tr>

					<tr>
                        <th scope="row" valign="top"><label for="expires"><?php _e('Expiration Date', 'writesaver');?>:</label></th>
                        <td>
							<select name="expires_month" required>
								<?php
									for($i = 1; $i < 13; $i++)
									{
									?>
									<option value="<?php echo $i?>" <?php if($i == $selected_expires_month) { ?>selected="selected"<?php } ?>><?php echo date_i18n("M", strtotime($i . "/1/" . $current_year, current_time("timestamp")))?></option>
									<?php
									}
								?>
							</select>
							<input name="expires_day" required type="text" size="2" value="<?php echo $selected_expires_day?>" />
							<input name="expires_year" required type="text" size="4" value="<?php echo $selected_expires_year?>" />
						</td>
                    </tr>
					
					<tr>
                        <th scope="row" valign="top"><label for="uses"><?php _e('Discounted Words', 'writesaver');?>:</label></th>
                        <td>
							<input name="discounted_words" type="text" size="20" required value="<?php if(!empty($code->discounted_words)) echo str_replace("\"", "&quot;", stripslashes($code->discounted_words));?>" />
						</td>
                    </tr>

					<tr>
                        <th scope="row" valign="top"><label for="minimum_amount"><?php _e('Minimum Amount', 'writesaver');?>:</label></th>
                        <td>
							<input name="minimum_amount" type="text" size="20" required value="<?php if(!empty($code->minimum_amount)) echo str_replace("\"", "&quot;", stripslashes($code->minimum_amount));?>" />
							<small class="pmpro_lite"><?php _e('Minimum before a discount can be applied', 'writesaver');?></small>
						</td>
                    </tr>

					<tr>
                        <th scope="row" valign="top"><label for="uses"><?php _e('Uses', 'writesaver');?>:</label></th>
                        <td>
							<input name="uses" type="text" size="20" value="<?php if(!empty($code->uses)) echo str_replace("\"", "&quot;", stripslashes($code->uses));?>" />
							<small class="pmpro_lite"><?php _e('Leave blank for unlimited uses.', 'writesaver');?></small>
						</td>
                    </tr>
					
				</tbody>
			</table>

			<p class="submit topborder">
				<input name="save" type="submit" class="button button-primary" value="Save Code" />
				<input name="cancel" type="button" class="button button-secondary" value="Cancel" onclick="location.href='<?php echo get_admin_url(NULL, '/admin.php?page=discount_codes')?>';" />
			</p>
			</form>
		</div>

	<?php } else { ?>
		<h1>
			<?php _e('Discount Codes', 'writesaver');?>
			<a href="admin.php?page=discount_codes&edit=-1" class="pull-right add-new-h2 btn btn-info"><?php _e('Add New Discount Code', 'writesaver');?></a>
		</h1>

		<?php if(!empty($pmpro_msg)) { ?>
			<div id="message" class="<?php if($pmpro_msgt == "success") echo "updated fade"; else echo "error"; ?>"><p><?php echo $pmpro_msg?></p></div>
		<?php } ?>

		<!--<form id="posts-filter" method="get" action="">
			<p class="search-box">
				<label class="screen-reader-text" for="post-search-input"><?php _e('Search Discount Codes', 'writesaver');?>:</label>
				<input type="hidden" name="page" value="discount_codes" />
				<input id="post-search-input" type="text" value="<?php if(!empty($s)) echo $s;?>" name="s" size="30" />
				<input class="button" type="submit" value="<?php _e('Search', 'writesaver');?>" id="search-submit "/>
			</p>
		</form>-->

		<br class="clear" />
		<?php
			$sqlQuery = "SELECT *, UNIX_TIMESTAMP(starts) as starts, UNIX_TIMESTAMP(expires) as expires FROM tbl_discount_codes ";
			if(!empty($s)){
				$sqlQuery .= "WHERE code LIKE '%$s%' ";
				$sqlQuery .= "ORDER BY id ASC";
			}

			$codes = $wpdb->get_results($sqlQuery, OBJECT);
		?>
		<table class="widefat" id="list_table">
		<thead>
			<tr>
				<th><?php _e('ID', 'writesaver');?></th>
				<th><?php _e('Code', 'writesaver');?></th>
				<th><?php _e('Starts', 'writesaver');?></th>
				<th><?php _e('Expires', 'writesaver');?></th>
				<th><?php _e('Uses', 'writesaver');?></th>
				<th><?php _e('Status', 'writesaver');?></th>
				<th><?php _e('Type Of Purchase', 'writesaver');?></th>
				<th><?php _e('Level', 'writesaver');?></th>
				<th><?php _e('Discounted Words', 'writesaver');?></th>
				<th></th>
				<th></th>
			</tr>
		</thead>
		<tbody>
			<?php
				if(!$codes)
				{
				?>
					<tr><td colspan="10" class="pmpro_pad20">
						<p style="text-align: center;"><?php _e('Discount codes allow you to offer purchases at discounted prices to select customers.', 'writesaver');?> <a href="admin.php?page=discount_codes&edit=-1"><?php _e('Create your first discount code now', 'writesaver');?></a>.</p>
					</td></tr>
				<?php
				}
				else
				{
					$count = 0;
					foreach($codes as $code)
					{
					?>
					<tr <?php if($count++ % 2 == 1) { ?> class="alternate"<?php } ?>>
						<td><?php echo $code->id?></td>
						<td>
							<a href="?page=discount_codes&edit=<?php echo $code->id?>"><?php echo $code->code?></a>
						</td>
						<td>
							<?php echo date_i18n(get_option('date_format'), $code->starts)?>
						</td>
						<td>
							<?php echo date_i18n(get_option('date_format'), $code->expires)?>
						</td>				
						<td>
							<?php
								$uses = $wpdb->get_var( $wpdb->prepare( "SELECT COUNT(*) FROM tbl_discount_codes_uses WHERE code_id = %d", $code->id ) );
								if($code->uses > 0)
									echo "<strong>" . (int)$uses . "</strong>/" . $code->uses;
								else
									echo "<strong>" . (int)$uses . "</strong>/unlimited";
							?>
						</td>
						<td>
							<?php 
								if($code->starts > strtotime('today')){
									echo "<strong>Not Active</strong>";
								} else if($code->expires < strtotime('today')) {
									echo "<strong>Expired</strong>";
								} else {
									echo "<strong>Active</strong>";
								}
							?>
						</td>
						<td>
							<?php 
								if($code->type == 1){
									echo "One Time Purchase";
								} else {
									echo "Subscription";
								}
							?>
						</td>
						<td>
							<?php 
								if($code->type == 1){
									echo "-";
								} else {
									$level_name = $wpdb->get_col("SELECT name FROM $wpdb->pmpro_membership_levels WHERE id  = '" . $code->subscription . "'");
									echo pmpro_implodeToEnglish($level_name);
								}
							?>
						</td>
						<td><?php echo $code->discounted_words?></td>
						<td>
							<a href="?page=discount_codes&edit=<?php echo $code->id?>"><?php _e('edit', 'writesaver');?></a>
						</td>
						<td>
							<a href="javascript:askfirst('<?php echo str_replace("'", "\'", sprintf(__('Are you sure you want to delete the %s discount code? New users will not be able to use this code anymore.', 'writesaver'), $code->code));?>', '?page=discount_codes&delete=<?php echo $code->id?>'); void(0);"><?php _e('delete', 'writesaver');?></a>
						</td>
					</tr>
					<?php
					}
					?>
					<script>
					jQuery(document).ready(function () {
						jQuery('#list_table').DataTable({
							"oLanguage": {
								"sEmptyTable": "No codes available."
							}
						});
					});
				</script>
				<?php
				}
				?>
		</tbody>
		</table>

	<?php } ?>