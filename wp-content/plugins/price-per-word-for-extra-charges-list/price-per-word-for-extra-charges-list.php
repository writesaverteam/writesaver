<?php
/*
  Plugin Name: Price Per Word For Extra Charges
  Description:  In this page, you can view all payment for extra charges for non-subcribers members
  Plugin URI: http://deb-proj.com/
  Author URI: http://deb-proj.com/
  Author: Debasis Acharya
  License: Public Domain
  Version: 1.1
 */

if (!class_exists('WP_List_Table')) {
    require_once(ABSPATH . 'wp-admin/includes/class-wp-list-table.php');
}

/**
 * Trainers_List_Table class that will display our custom table
 * records in nice table
 */
class Extrachargelist_List_Table extends WP_List_Table {

    /**
     * [REQUIRED] You must declare constructor and give some basic params
     */
    function __construct() {
        global $status, $page;

        parent::__construct(array(
            'singular' => 'extracharge',
            'plural' => 'extracharges',
        ));
    }

    /**
     * [REQUIRED] this is a default column renderer
     *
     * @param $item - row (key, value array)
     * @param $column_name - string (key)
     * @return HTML
     */
    function column_default($item, $column_name) {
        return $item[$column_name];
    }

    /**
     * [OPTIONAL] this is example, how to render column with actions,
     * when you hover row "Edit | Delete" links showed
     *
     * @param $item - row (key, value array)
     * @return HTML
     */
    function column_name($item) {
        // links going to /admin.php?page=[your_plugin_page][&other_params]
        // notice how we used $_REQUEST['page'], so action will be done on curren page
        // also notice how we use $this->_args['singular'] so in this example it will
        // be something like &person=2
        $actions = array(
                /* 'edit' => sprintf('<a href="?page=emailtemplate_form&id=%s">%s</a>', $item['id'], __('Edit', 'custom_table_example')),
                  'delete' => sprintf('<a href="?page=%s&action=delete&id=%s">%s</a>', $_REQUEST['page'], $item['id'], __('Delete', 'custom_table_example')), */
        );

        return sprintf('%s %s', $item['name'], $this->row_actions($actions)
        );
    }

    /**
     * [REQUIRED] this is how checkbox column renders
     *
     * @param $item - row (key, value array)
     * @return HTML
     */
    function column_cb($item) {
        return sprintf('<input type="checkbox" name="id[]" value="%s" />', $item['id']);
    }

    /**
     * [REQUIRED] This method return columns to display in table
     * you can skip columns that you do not want to show
     * like content, or description
     *
     * @return array
     */
    function get_columns() {
        $columns = array(
            'cb' => '<input type="checkbox" />', //Render a checkbox instead of text
            'name' => __('Customer Name', 'custom_table_example'),
            'payment_source' => __('Payment Source', 'custom_table_example'),
            'stripe_reference' => __('Transaction ID#', 'custom_table_example'),
            'payment_date' => __('Payment Date', 'custom_table_example'),
            'descriptions' => __('Descriptions', 'custom_table_example'),
            'price' => __('Amount Paid', 'custom_table_example'),
        );
        return $columns;
    }

    /**
     * [OPTIONAL] This method return columns that may be used to sort table
     * all strings in array - is column names
     * notice that true on name column means that its default sort
     *
     * @return array
     */
    function get_sortable_columns() {
        $sortable_columns = array(
            'name' => array('name', false),
            'payment_source' => array('payment_source', false),
            'stripe_reference' => array('stripe_reference', false),
            'payment_date' => array('payment_date', false),
            'descriptions' => array('descriptions', false),
            'price' => array('price', false)
        );
        return $sortable_columns;
    }

    /**
     * [OPTIONAL] Return array of bult actions if has any
     *
     * @return array
     */
    function get_bulk_actions() {
        $actions = array(
            'delete' => 'Delete'
        );
        return $actions;
    }

    /**
     * [OPTIONAL] This method processes bulk actions
     * it can be outside of class
     * it can not use wp_redirect coz there is output already
     * in this example we are processing delete action
     * message about successful deletion will be shown on page in next part
     */
    function process_bulk_action() {
        global $wpdb;
        $table_name = 'wp_price_per_extra_words'; // do not forget about tables prefix

        if ('delete' === $this->current_action()) {
            $ids = isset($_REQUEST['id']) ? $_REQUEST['id'] : array();
            if (is_array($ids))
                $ids = implode(',', $ids);

            if (!empty($ids)) {
                $wpdb->query("DELETE FROM $table_name WHERE id IN($ids)");
            }
        }
    }

    /**
     * [REQUIRED] This is the most important method
     *
     * It will get rows from database and prepare them to be showed in table
     */
    function prepare_items() {
        global $wpdb;
        $table_name = 'wp_price_per_extra_words'; // do not forget about tables prefix

        $per_page = 20; // constant, how much records will be shown per page

        $columns = $this->get_columns();

        $hidden = array();
        $sortable = $this->get_sortable_columns();

        // here we configure table headers, defined in our methods
        $this->_column_headers = array($columns, $hidden, $sortable);

        // [OPTIONAL] process bulk action if any
        $this->process_bulk_action();

        // will be used in pagination settings
        $total_items = $wpdb->get_var("SELECT COUNT(id) FROM $table_name where status=1");

        // prepare query params, as usual current page, order by and order direction
        $paged = isset($_REQUEST['paged']) ? max(0, intval($_REQUEST['paged'])) : 1;
        //print_r($this->get_sortable_columns());
        $orderby = (isset($_REQUEST['orderby']) && in_array($_REQUEST['orderby'], array_keys($this->get_sortable_columns()))) ? $_REQUEST['orderby'] : 'payment_date';
        $order = (isset($_REQUEST['order']) && in_array($_REQUEST['order'], array('asc', 'desc'))) ? $_REQUEST['order'] : 'desc';

        if ($paged > 1) {
            $OFFSET = $per_page * ($paged - 1);
        } else {
            $OFFSET = 0;
        }
        // [REQUIRED] define $items array
        // notice that last argument is ARRAY_A, so we will retrieve array
        $str = "SELECT e.id,(select display_name from wp_users where ID=e.fk_customer_id) as name,e.payment_source,e.stripe_reference,e.payment_date,e.descriptions,e.price FROM $table_name e where  e.status=1 ORDER BY $orderby $order LIMIT %d OFFSET %d";
        $this->items = $wpdb->get_results($wpdb->prepare($str, $per_page, $paged), ARRAY_A);

        // [REQUIRED] configure pagination
        $this->set_pagination_args(array(
            'total_items' => $total_items, // total items defined above
            'per_page' => $per_page, // per page constant defined at top of method
            'total_pages' => ceil($total_items / $per_page) // calculate pages count
        ));
    }

}

/**
 * PART 3. Admin page
 * ============================================================================
 * In this part you are going to add admin page for custom table
 */

/**
 * admin_menu hook implementation, will add pages to list persons and to add new one
 */
function wp_price_per_extra_words_admin_menu() {
    add_menu_page(__('Payment Received', ''), __('Payment Received', ''), 'activate_plugins', 'wp_price_per_extra_words', 'wp_price_per_extra_words_page_handler', plugin_dir_url(__FILE__) . 'icon.png');

    // add new will be described in next part
    //add_submenu_page('none', __('Add new', 'custom_table_example'), __('Add new', 'custom_table_example'), 'activate_plugins', 'emailtemplate_form', 'emailtemplate_form_page_handler');
}

add_action('admin_menu', 'wp_price_per_extra_words_admin_menu');

/**
 * List page handler
 *
 * This function renders our custom table
 * Notice how we display message about successfull deletion
 * Actualy this is very easy, and you can add as many features
 * as you want.
 */
function wp_price_per_extra_words_page_handler() {
    global $wpdb;

    $table = new Extrachargelist_List_Table();
    $table->prepare_items();

    $message = '';
    if ('delete' === $table->current_action()) {
        $message = '<div class="updated below-h2" id="message"><p>' . sprintf(__('Items deleted: %d', 'custom_table_example'), count($_REQUEST['id'])) . '</p></div>';
    }
    ?>
    <div class="wrap">

        <div class="icon32 icon32-posts-post" id="icon-edit"><br></div>
        <h2><?php _e('Payment Received', 'custom_table_example') ?>
        </h2>
        <?php echo $message; ?>

        <form id="persons-table" method="GET">
            <input type="hidden" name="page" value="<?php echo $_REQUEST['page'] ?>"/>
            <?php $table->display() ?>
        </form>
        <style>tfoot{display:none;}</style>
    </div>
    <?php
}

/**
 * PART 4. Form for adding andor editing row
 * ============================================================================
 *
 * In this part you are going to add admin page for adding andor editing items
 * You cant put all form into this function, but in this example form will
 * be placed into meta box, and if you want you can split your form into
 * as many meta boxes as you want
 */

/**
 * Form page handler checks is there some data posted and tries to save it
 * Also it renders basic wrapper in which we are callin meta box render
 */
function wp_price_per_extra_words_form_page_handler() {
    global $wpdb;
    $table_name = 'wp_price_per_extra_words'; // do not forget about tables prefix

    $message = '';
    $notice = '';

    // this is default $item which will be used for new records
    $default = array(
        'id' => 0,
        'name' => '',
    );

    // here we are verifying does this request is post back and have correct nonce
    if (wp_verify_nonce($_REQUEST['nonce'], basename(__FILE__))) {
        // combine our default item with request params
        $item = shortcode_atts($default, $_REQUEST);

        $item_valid = true;
        if ($item_valid === true) {
            
        } else {
            // if $item_valid not true it contains error message(s)
            $notice = $item_valid;
        }
    } else {
        // if this is not post back we load item to edit or give new one to create
        $item = $default;
        if (isset($_REQUEST['id'])) {
            $item = $wpdb->get_row($wpdb->prepare("SELECT * FROM $table_name WHERE id = %d", $_REQUEST['id']), ARRAY_A);
            if (!$item) {
                $item = $default;
                $notice = __('Item not found', 'custom_table_example');
            }
        }
    }

    // here we adding our custom meta box
    add_meta_box('wp_price_per_extra_words_form_meta_box', 'Payment Received', 'wp_price_per_extra_words_form_meta_box_handler', 'person', 'normal', 'default');
    ?>

    <?php
}

/**
 * This function renders our custom meta box
 * $item is row
 *
 * @param $item
 */
function wp_price_per_extra_words_form_meta_box_handler($item) {
    ?>

    <?php
}
