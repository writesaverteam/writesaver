<?php
/*
Plugin Name: Price Per Word For Non-subscribers
Description: In this page, you can set price per word for non-subcribers members
Plugin URI: http://deb-proj.com/
Author URI: http://deb-proj.com/
Author: Debasis Acharya
License: Public Domain
Version: 1.1
*/

/**
 * PART 1. Defining Table List
 * In this part you are going to define custom table list class,
 * that will display your database records in nice looking table
 */

if (!class_exists('WP_List_Table')) {
    require_once(ABSPATH . 'wp-admin/includes/class-wp-list-table.php');
}

/**
 * PART 3. Admin page
 * ============================================================================
 * In this part you are going to add admin page for custom table
 */

/**
 * admin_menu hook implementation, will add pages to list persons and to add new one
 */
function price_per_word_admin_menu()
{
    add_menu_page(__('Price Per Words', ''), __('Price/Time Per Words', ''), 'activate_plugins', 'price_per_words', 'price_per_words_form_page_handler',plugin_dir_url( __FILE__ ).'icon.png');
}

add_action('admin_menu', 'price_per_word_admin_menu');

/**
 * PART 4. Form for adding andor editing row
 * ============================================================================
 *
 * In this part you are going to add admin page for adding andor editing items
 * You cant put all form into this function, but in this example form will
 * be placed into meta box, and if you want you can split your form into
 * as many meta boxes as you want
 */
/**
 * Form page handler checks is there some data posted and tries to save it
 * Also it renders basic wrapper in which we are callin meta box render
 */
function price_per_words_form_page_handler()
{
    global $wpdb;
    $table_name = 'wp_price_per_words'; // do not forget about tables prefix

    $message = '';
    $notice = '';

    // this is default $item which will be used for new records
    $default = array(
        'id' => 0,
        'name' => '',
    );

    // here we are verifying does this request is post back and have correct nonce
    if (wp_verify_nonce($_REQUEST['nonce'], basename(__FILE__))) {
       
	    // combine our default item with request params
        $item = shortcode_atts($default, $_REQUEST);
		
		$sql = " UPDATE $table_name SET price_per_words = '$_REQUEST[price_per_words]',time_per_words = '$_REQUEST[time_per_words]',atime_per_words = '$_REQUEST[atime_per_words]', timeout = '$_REQUEST[timeout]', inactivity = '$_REQUEST[inactivity]' WHERE id = 1";
		$result = $wpdb->query($sql);
		
		$message = __('Price/Time was successfully updated', 'custom_table_example');
		
		echo "<script type='text/javascript'>
			window.location=document.location.href = 'admin.php?page=price_per_words';
			</script>";
				
    }
    else {
        // if this is not post back we load item to edit or give new one to create
        $item = $wpdb->get_row($wpdb->prepare("SELECT * FROM $table_name WHERE id = %d", 1), ARRAY_A);
		if (!$item) {
			$item = $default;
			$notice = __('Item not found', 'custom_table_example');
		}
    }

    // here we adding our custom meta box
    add_meta_box('myprofile_form_meta_box', 'Per Words Price Settings', 'myprofile_form_meta_box_handler', 'person', 'normal', 'default');
    add_meta_box('timeout_form_meta_box', 'Proofreader Time Settings', 'timeout_form_meta_box_handler', 'person', 'normal', 'default');

    ?>
<div class="wrap">
    <div class="icon32 icon32-posts-post" id="icon-edit"><br></div>
    <h2><?php _e('Price/Time Per Words', 'custom_table_example')?></h2>

    <?php if (!empty($notice)): ?>
    <div id="notice" class="error"><p><?php echo $notice ?></p></div>
    <?php endif;?>
    <?php if (!empty($message)): ?>
    <div id="message" class="updated"><p><?php echo $message ?></p></div>
    <?php endif;?>

    <form id="form" method="POST">
        <input type="hidden" name="nonce" value="<?php echo wp_create_nonce(basename(__FILE__))?>"/>
        <?php /* NOTICE: here we storing id to determine will be item added or updated */ ?>
        <input type="hidden" name="id" value="<?php echo $_REQUEST['id'];?>"/>

        <div class="metabox-holder" id="poststuff">
            <div id="post-body">
                <div id="post-body-content">
                    <?php /* And here we call our custom meta box */ ?>
                    <?php do_meta_boxes('person', 'normal', $item); ?>
                    <input type="submit" value="<?php _e('Save', 'custom_table_example')?>" id="submit" class="button-primary" name="submit">
                </div>
            </div>
        </div>
    </form>
</div>
<?php
}

/**
 * This function renders our custom meta box
 * $item is row
 *
 * @param $item
 */
function myprofile_form_meta_box_handler($item)
{
?>
<table cellspacing="2" cellpadding="5" border="0" style="width: 100%;" class="form-table">
    <tbody>
        <tr class="form-field">
        <th valign="top" scope="row">
        	<label for="price_per_words"><?php _e('Price Per Words', '')?></label>
        </th>
        <td>
        	<input id="price_per_words" name="price_per_words" type="number" style="width:10%" step="any" value="<?php echo esc_attr($item['price_per_words'])?>" required>
        </td>
        </tr>
    </tbody>
</table>
<?php
}

/**
 * This function renders our time meta box
 * $item is row
 *
 * @param $item
 */
function timeout_form_meta_box_handler($item)
{
?>
<table cellspacing="2" cellpadding="5" border="0" style="width: 100%;" class="form-table">
    <tbody>
        <tr class="form-field">
        <th valign="top" scope="row">
        	<label for="time_per_words"><?php _e('Time Per Word(Seconds)', '')?></label>
        </th>
        <td>
        	<input id="time_per_words" name="time_per_words" type="number" style="width:10%" step="any" value="<?php echo esc_attr($item['time_per_words'])?>" required>
        </td>
        </tr>
		<tr class="form-field">
        <th valign="top" scope="row">
        	<label for="atime_per_words"><?php _e('Additional Time Per Word(Seconds)', '')?></label>
        </th>
        <td>
        	<input id="atime_per_words" name="atime_per_words" type="number" style="width:10%" step="any" value="<?php echo esc_attr($item['atime_per_words'])?>" required>
        </td>
        </tr>
		<tr class="form-field">
        <th valign="top" scope="row">
        	<label for="timeout"><?php _e('Time Before Reassignment(Minutes)', '')?></label>
        </th>
        <td>
        	<input id="timeout" name="timeout" type="number" style="width:10%" step="any" value="<?php echo esc_attr($item['timeout'])?>" required>
        </td>
        </tr>
		<tr class="form-field">
        <th valign="top" scope="row">
        	<label for="inactivity"><?php _e('Inactivity Limit(Minutes)', '')?></label>
        </th>
        <td>
        	<input id="inactivity" name="inactivity" type="number" style="width:10%" step="any" value="<?php echo esc_attr($item['inactivity'])?>" required>
        </td>
        </tr>
    </tbody>
</table>
<?php
}