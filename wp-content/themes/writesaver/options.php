<?php

/**

 * A unique identifier is defined to store the options in the database and reference them from the theme.

 * By default it uses the theme name, in lowercase and without spaces, but this can be changed if needed.

 * If the identifier changes, it'll appear as if the options have been reset.

 */
function optionsframework_option_name() {



    // This gets the theme name from the stylesheet

    $themename = wp_get_theme();

    $themename = preg_replace("/\W/", "_", strtolower($themename));



    $optionsframework_settings = get_option('optionsframework');

    $optionsframework_settings['id'] = $themename;

    update_option('optionsframework', $optionsframework_settings);
}

/**

 * Defines an array of options that will be used to generate the settings page and be saved in the database.

 * When creating the 'id' fields, make sure to use all lowercase and no spaces.

 *

 * If you are making your theme translatable, you should replace 'options_framework_theme'

 * with the actual text domain for your theme.  Read more:

 * http://codex.wordpress.org/Function_Reference/load_theme_textdomain

 */
function optionsframework_options() {



    $options = array();
// Pull all the pages into an array
    $options_pages = array();
    $options_pages_obj = get_pages(array('include' => array(6, 8, 12, 16), 'sort_column' => 'post_date', 'sort_order' => 'asc'));
    foreach ($options_pages_obj as $page) {
        $options_pages[$page->ID] = $page->post_title;
    }


    $options[] = array(
        'name' => __('Header Settings', 'options_framework'),
        'type' => 'heading');


    $options[] = array(
        'name' => __('Logo', 'options_framework_theme'),
        'desc' => __('Upload Header Logo.', 'options_framework'),
        'id' => 'header_logo',
        'type' => 'upload');

    $options[] = array(
        'name' => __('Favicon', 'options_framework'),
        'desc' => __('Upload Favicon .ico file (width 16x16)', 'options_framework_theme'),
        'id' => 'favicon_logo',
        'type' => 'upload');



    $options[] = array(
        'name' => __('Footer Setting', 'options_framework'),
        'type' => 'heading');

    $options[] = array(
        'name' => __('Copyright Text', 'options_framework'),
        'desc' => __('', 'options_framework'),
        'id' => 'copyright_text',
        'std' => 'Copyright',
        'type' => 'text');

    $options[] = array(
        'name' => __('Footer Logo', 'options_framework_theme'),
        'desc' => __('Upload Footer Logo.', 'options_framework'),
        'id' => 'footer_logo',
        'type' => 'upload');

    $options[] = array(
        'name' => __('Footer Logo Text', 'options_framework'),
        'desc' => __('', 'options_framework'),
        'id' => 'footer_text',
        'type' => 'textarea');

    $options[] = array(
        'name' => __('Footer Logo Url', 'options_framework'),
        'desc' => __('Footer Logo Url', 'options_framework'),
        'id' => 'footer_logo_url',
        'type' => 'text');



    $options[] = array(
        'name' => __('Social Media Setting', 'options_framework'),
        'type' => 'heading');



    $options[] = array(
        'name' => __('Instagram Link', 'options_framework'),
        'id' => 'instagram_link',
        'type' => 'text');





    $options[] = array(
        'name' => __('Facebook Link', 'options_framework'),
        'id' => 'facebook_link',
        'type' => 'text');


    $options[] = array(
        'name' => __('Twitter Link', 'options_framework'),
        'id' => 'twitter_link',
        'type' => 'text');


    $options[] = array(
        'name' => __('Pinterest Link', 'options_framework'),
        'id' => 'pinterest_link',
        'type' => 'text');

    $options[] = array(
        'name' => __('General Setting', 'options_framework'),
        'type' => 'heading');

    $options[] = array(
        'name' => __('Free words for customer', 'options_framework'),
        'desc' => __('Free words for customer', 'options_framework'),
        'id' => 'free_words_for_customer',
        'std' => '#',
        'type' => 'text');


    $options[] = array(
        'name' => __('Word Price for Single Check', 'options_framework'),
        'desc' => __('Word Price for Single Check', 'options_framework'),
        'id' => 'word_price_for_single_check',
        'type' => 'text');

    $options[] = array(
        'name' => __('Word Price for Double Check', 'options_framework'),
        'desc' => __('Word Price for Double Check', 'options_framework'),
        'id' => 'word_price_for_double_check',
        'type' => 'text');

    $options[] = array(
        'name' => __('Newsletter Popup', 'options_framework'),
        'type' => 'heading');

    $options[] = array(
        'name' => __('Display Popup', 'options_framework'),
        'desc' => __('Check if you want to display Newsletter Popup on site', 'theme-textdomain'),
        'id' => 'display_popup',
        'std' => '1',
        'type' => 'checkbox'
    );

    $options[] = array(
        'name' => __('Select a Page', 'options_framework'),
        'desc' => __('Select a Page to display  Newsletter Popup', 'theme-textdomain'),
        'id' => 'popup_select_pages',
        'type' => 'select',
        'options' => $options_pages
    );

    $options[] = array(
        'name' => __('Enter Time interval to display Newsletter Popup', 'options_framework'),
        'desc' => __('Enter Time interval to display Newsletter Popup in Minutes', 'theme-textdomain'),
        'id' => 'popup_select_time',
        'type' => 'text',
        'std' => 1400,
        'options' => $options_pages
    );


    return $options;
}
