<?php global $post;?>
	<footer class="myFoot">
        <div class="container footIn text-center">
            <p>Copyright &copy; <?php echo date('Y');?> Writesaver</p>
            <ul class="footLinks">
                <!--<li class="firstLink">Social Media</li>
                <li>Privacy Policy</li>-->
            </ul>
        </div>
    </footer>
</div>
<div id="myCourse" class="modal fade" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-body text-center myPopUp">
				<h3>GET THE FREE COURSE!</h3>
				<form method="post" id="landingForm">
					<div class="form-group formHolder">
							<input type="text" name="firstname" id="firstname" class="form-control myInputs myNames" required placeholder="First Name">
							<input type="text" name="lastname" id="lastname" class="form-control myInputs myNames" required placeholder="Last Name">
							<input type="email" name="email" id="email" class="form-control myInputs myEmail" required placeholder="E-mail Address">
					</div>
					<button type="submit" class="btn btn-info startBtn2">GET STARTED</button>
					<input type="hidden" name="message" value="yes"/>
					<input type="hidden" name="course" id="course" value="<?php echo $post->post_title ?>"/>
					<input type="hidden" name="course_id" id="course_id" value="<?php echo $post->ID ?>"/>
					<div class="message" style="display: none;"></div>
				</form>
			</div>
		</div>
	</div>
</div>
<style>
div.message {
    margin: 2em 0.5em 1em;
    padding: 0.2em 1em
}
.overlay-loading {
    display:    none;
    position:   fixed;
    z-index:    1100;
    top:        0;
    left:       0;
    height:     100%;
    width:      100%;
    background: rgba( 255, 255, 255, .8 );
}
body .overlay-loading {
    display: block;
}
.overlay-loading > .fa {
    position: absolute;
    top: 50%;
    left: 50%;
    margin-left: -15px;
    margin-top: -15px;
    color: #000;
    font-size: 30px;
}
</style>
<script>(function(){var w=window;var ic=w.Intercom;if(typeof ic==="function"){ic('reattach_activator');ic('update',intercomSettings);}else{var d=document;var i=function(){i.c(arguments)};i.q=[];i.c=function(args){i.q.push(args)};w.Intercom=i;function l(){var s=d.createElement('script');s.type='text/javascript';s.async=true;s.src='https://widget.intercom.io/widget/f5fyv8vq';var x=d.getElementsByTagName('script')[0];x.parentNode.insertBefore(s,x);}if(w.attachEvent){w.attachEvent('onload',l);}else{w.addEventListener('load',l,false);}}})()</script>

<script type="text/javascript">
	window.Intercom('boot', {
		app_id: "f5fyv8vq"
	});
	$(document).on({
		ajaxStart: function () {
			$("body").append('<div class="overlay-loading"><i class="fa fa-refresh fa-spin"></i></div>');
			$("body").css('overflow', 'hidden');
		},
		ajaxStop: function () {
			$("body").css('overflow', 'visible');
			$(".overlay-loading").fadeOut("slow");
			$(".overlay-loading").remove();
		}
	});
	$('#landingForm').on( "submit", function(e){
		e.preventDefault();
		$('.message').hide().html('');
		var form_data = $(this).serialize();
		$.ajax({
			type: "POST",
			 url: '<?php echo get_template_directory_uri(); ?>/saveEmails.php',
			data:  form_data,
			dataType:'json',
			success: function(data) {
				if(data.success == true){
				Intercom('trackEvent', 'registercourse1');
					window.Intercom('update', {
						app_id: "f5fyv8vq",
						email: $('#email').val(),
						name: $('#firstname').val() + ' ' + $('#lastname').val(),
						coursefirstname: $('#firstname').val(),
						courselastname: $('#lastname').val(),
						courseemail: $('#email').val(),
						courseregistered: data.courses
					});
					
					$('.message').html(data.message);
					$('.form-control').val('');
					$('.message').css( "color", "#fff" );
					$('.message').css( "border", "2px solid #fff" );
					$('.message').show();
				}
				else{				    
					$('.message').html(data.message);
					$('.message').css( "color", "red" );
					$('.message').css( "border", "2px solid #ff0000" );
					$('.message').show();
				}
		   },
		   error: function(){
				$('.message').html("An error has occurred and the request was not received.");
				$('.message').css( "color", "red" );
				$('.message').css( "border", "2px solid #ff0000" );
				$('.message').show();
		   }
		});
	});	
</script>
<?php wp_footer(); ?>
<script type="text/javascript">
_linkedin_data_partner_id = "251042";
</script><script type="text/javascript">
(function(){var s = document.getElementsByTagName("script")[0];
var b = document.createElement("script");
b.type = "text/javascript";b.async = true;
b.src = "https://snap.licdn.com/li.lms-analytics/insight.min.js";
s.parentNode.insertBefore(b, s);})();
</script>
<noscript>
<img height="1" width="1" style="display:none;" alt="" src="https://dc.ads.linkedin.com/collect/?pid=251042&fmt=gif" />
</noscript>
</body>
</html>