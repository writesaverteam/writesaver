<?php
/*
  Theme Name: writesaver
  Theme URI: 192.168.0.87/wp_content/themes/writesaver
  Description: A brief description.
  Version: 1.0
  Author: Admin
  Author URI: http://192.168.0.87/writesaver
 */
?>

<html lang="en">
    <html <?php language_attributes(); ?> class="no-js"> 

        <head>
            <title>
                <?php if (is_front_page() || is_home()) { ?>
                    <?php bloginfo('name'); ?> | <?php bloginfo('description'); ?>
                <?php } else { ?>
                    <?php wp_title(''); ?> - <?php bloginfo('name'); ?> | <?php bloginfo('description'); ?>
                <?php } ?>
            </title>
            <meta charset="UTF-8">
            <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
            <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
            <link href="<?php echo get_template_directory_uri() ?>/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
            <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/fileupload.css" />
            <link href="<?php echo get_template_directory_uri() ?>/css/responsive-tab.css" rel="stylesheet" type="text/css"/>
            <link href="<?php echo get_template_directory_uri() ?>/css/main.css" rel="stylesheet" type="text/css"/>
            <link href="<?php echo get_template_directory_uri() ?>/css/flexslider.css" rel="stylesheet" type="text/css"/>
            <link href="<?php echo get_template_directory_uri() ?>/css/jquery.fullpage.min.css" rel="stylesheet" type="text/css"/>
            <link href="<?php echo get_template_directory_uri() ?>/css/font-awesome.css" rel="stylesheet" type="text/css"/>
            <link href="<?php echo get_template_directory_uri() ?>/css/style_uv.css" rel="stylesheet" type="text/css"/>
            <link href="<?php echo get_template_directory_uri() ?>/css/style.css" rel="stylesheet" type="text/css"/>
            <link href="<?php echo get_template_directory_uri() ?>/css/responsive.css" rel="stylesheet" type="text/css"/>
            <link rel="icon" type="image/x-icon" href="<?php echo of_get_option('favicon_logo'); ?>">
            <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>  
            <script src="<?php echo get_template_directory_uri() ?>/js/jquery.flexslider.js" type="text/javascript"></script>

            <?php wp_head(); ?>
        </head>   

<!--        <div id="wptime-plugin-preloader"></div>-->

        <body <?php body_class(); ?>>
            <?php date_default_timezone_set(get_option('timezone_string')); ?>
            <div class="load_overlay" id="loding">
                <img src="<?php echo get_template_directory_uri(); ?>/images/39.gif"/>
            </div>
            <!--Top Section Start-->
            <?php
			if(!isset($_REQUEST['level'])){
            ?>
            <header class="header <?php echo (is_user_logged_in() ? "with_login" : "") ?>">
                <div class="logo">
                    <a href="<?php echo home_url(); ?>"><img src="<?php echo of_get_option('header_logo') ?>" alt="logo" class="img-responsive"></a>
                </div>
                <div class="nav_manu">
                    <nav class="navbar navbar-inverse">
                        <div class="navbar-header">
                            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-controls="navbar">
                                <span class="sr-only">Toggle navigation</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>                            
                            <div id="navbar" class="navbar-collapse collapse">
                                <ul class="nav-menu">
                                    <?php
                                    $current_user = wp_get_current_user();
                                    $user_roles_array = $current_user->roles;
                                    $user_role = array_shift($user_roles_array);
                                    $menu_name = 'Header Menu';
                                    $menu = wp_get_nav_menu_object($menu_name);

                                    $menuitems = wp_get_nav_menu_items($menu->term_id, array('order' => 'DESC'));
                                    _wp_menu_item_classes_by_context($menuitems);
                                    foreach ($menuitems as $item):

                                        $classes = $item->classes;
                                        if (in_array('current-menu-item', $classes) || in_array('current-menu-parent', $classes)) {
                                            $classes[] = 'active ';
                                        }
                                        apply_filters('nav_menu_css_class', $classes, $menu_item, $args);
                                        $classes = implode(" ", $classes);

                                        $id = get_post_meta($item->ID, '_menu_item_object_id', true);
                                        $page = get_page($id);
                                        $link = get_page_link($id);
                                        if (($id == 719 && is_user_logged_in()) || ($id == 14 && $user_role != 'customer' && $user_role != '')) {
                                            
                                        } else {
                                            ?>
                                            <li class="<?php echo $classes; ?>">
                                                <a href="<?php echo $link; ?>" class="title">
                                                    <?php echo $page->post_title; ?>
                                                </a>
                                            </li>
                                            <?php
                                        }
                                    endforeach;
                                    ?>
                                </ul>
                            </div>
                        </div>
                    </nav>
                </div>
                <?php if (!is_user_logged_in()) { ?>
                    <div class="login_home">
                        <ul class="desktop_login">                    
                            <li><a href="<?php echo get_page_link(545); ?>">Login</a></li>
                            <li class="blue_bg"><a href="<?php echo get_page_link(540); ?>">Register today!</a></li>
                        </ul> 
                    </div>
                    <?php
                } else {
                    $user_id = get_current_user_id();
                    $key1 = 'first_name';
                    $key2 = 'last_name';
                    $single = true;
                    $user_first = ucwords(get_user_meta($user_id, $key1, $single));
                    $current_user = wp_get_current_user();
                    $user_roles_array = $current_user->roles;
                    $user_role = array_shift($user_roles_array);
                    $page_id = get_the_ID();

                    $profile_pic = get_user_meta($user_id, 'profile_pic_url', true);

                    if ($profile_pic == '')
                        $profile_pic = get_template_directory_uri() . '/images/customer.png';
                    ?>
                    <div class="login_home">
                        <ul class="desktop_login"> 
                            <?php
                            if ($page_id == 762 || $page_id == 810)
                                $class = 'active';
                            else
                                $class = '';
                            if ($user_role == 'customer') {
                                echo ' <li class=' . $class . '><a  href="' . get_the_permalink(762) . '">Dashboard</a></li>';
                            } else if ($user_role == 'proofreader') {
                                echo ' <li class=' . $class . '><a href="' . get_the_permalink(810) . '">Dashboard</a></li>';
                            }

                            if ($user_role == 'customer') {
                                $notification_list = $wpdb->get_results("SELECT * FROM `tbl_customer_notifications` WHERE fk_customer_id= $user_id AND is_view = 0 ORDER BY pk_cust_notification_id DESC LIMIT 5");
                            } else if ($user_role == 'proofreader') {
                                $notification_list = $wpdb->get_results("SELECT * FROM `tbl_proofreader_notifications` WHERE fk_proofreader_id= $user_id AND is_view = 0 ORDER BY pk_proof_notification_id DESC LIMIT 3");
                            }
                            if ($user_role == 'customer' || $user_role == 'proofreader') {
                                /* echo "<pre>";
                                  print_r($notification_list);
                                  echo "</pre>"; */
                                ?>                   
                                <li class="notrify">
                                    <a class="open_noti" href="javascript:void(0);"></a>
                                    <div class="notify_content" style="display: none">
                                        <ul class="notify_list">
                                            <?php
                                            foreach ($notification_list as $notifications) {
                                                if (count($notification_list) > 0) {
                                                    if ($user_role == 'customer')
                                                        $notify_id = $notifications->pk_cust_notification_id;
                                                    elseif ($user_role == 'proofreader')
                                                        $notify_id = $notifications->pk_proof_notification_id;
                                                    ?>
                                                    <li class="notification">
                                                        <i class="fa fa-bell-o" aria-hidden="true"></i>
                                                        <a data-notify_id="<?php echo $notify_id; ?>" href="javascript:void(0);"><?php echo substr($notifications->description, 0, 15); ?>...</a>
                                                        <span> 
                                                            <?php
                                                            $date = new DateTime($notifications->notification_date);
                                                            echo ago($date->format('U'));
                                                            ?> 
                                                        </span>
                                                    </li>
                                                    <?php
                                                }
                                            }
                                            if ($user_role == 'customer') {
                                                $view_notification = get_permalink(1119);
                                            } else if ($user_role == 'proofreader') {
                                                $view_notification = get_permalink(1121);
                                            }
                                            ?>
                                            <li class="view_all">
                                                <a href="<?php echo $view_notification; ?>">View all notification</a>                                    
                                            </li>

                                        </ul>
                                    </div>
                                </li>
                            <?php } ?>
                            <li class="user">
                                <a href="javascript:void(0);">
                                    <img src="<?php echo $profile_pic; ?>" alt="img" class="img-circle" /> 
                                    <span><strong>Welcome</strong><span class="user_fname"><?php echo $user_first; ?></span></span>

                                    <i class="fa fa-angle-down" aria-hidden="true"></i>
                                </a>
                                <div class="notify_content" style="display:none">
                                    <?php
                                    if ($user_role == 'customer') {
                                        $defaults1 = array(
                                            'echo' => true,
                                            'theme_location' => 'customer-dropdown-sub-menu'
                                        );
                                        wp_nav_menu($defaults1);
                                    } else if ($user_role == 'proofreader') {
                                        $defaults1 = array(
                                            'echo' => true,
                                            'theme_location' => 'proofreader-dropdown-sub-menu'
                                        );
                                        wp_nav_menu($defaults1);
                                    } else {
                                        ?>
                                        <ul>
                                            <li>  <a href="<?php echo wp_logout_url(home_url()); ?>">Log out</a></li>
                                        </ul>                                  
                                    <?php } ?> 
                                </div>
                            </li>
                        </ul>                  
                    </div>
                <?php } ?>
                <?php if (!is_user_logged_in()) { ?>
                    <div class="login_home_mobile">
                        <ul class="mobile_icon">
                            <li class="login_mobile"><a href="<?php echo get_permalink(545); ?>"></a></li>
                            <li class="register_mobile"><a href="<?php echo get_permalink(540); ?>"></a></li>
                        </ul>
                    </div>
                <?php } ?>
            </header>
            <?php
			}
            if (is_front_page()): echo '<div id="fullpage">';

            endif;
            ?>