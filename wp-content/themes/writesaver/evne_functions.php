<?php
/**
 * Evne, DM
 * We'll use separated functions file to prevent
 * basic functions.php code populating.
 * Reuired in ./functions.php:3735
 * Ref. functions.php:3735
 * Redmine EVNE ref.: #7229
 * TODO: Disable all TEST/DEV functions in production env. Important!
 */

class Evne_DocxReader {
    private $fileData = false;
    private $errors = array();
    private $styles = array();
    var $list_tag_started = false;
    public function __construct() {

    }
    private function load($file) {
        if (file_exists($file)) {
            $zip = new ZipArchive();
            $openedZip = $zip->open($file);
            if ($openedZip === true) {
                //attempt to load styles:
                if (($styleIndex = $zip->locateName('word/styles.xml')) !== false) {
                    $stylesXml = $zip->getFromIndex($styleIndex);
                    $xml = simplexml_load_string($stylesXml);
                    $namespaces = $xml->getNamespaces(true);
                    $children = $xml->children($namespaces['w']);
                    foreach ($children->style as $s) {
                        $attr = $s->attributes('w', true);
                        if (isset($attr['styleId'])) {
                            $tags = array();
                            $attrs = array();
                            foreach (get_object_vars($s->rPr) as $tag => $style) {
                                $att = $style->attributes('w', true);
                                switch ($tag) {
                                    case "b":
                                        $tags[] = 'strong';
                                        break;
                                    case "i":
                                        $tags[] = 'em';
                                        break;
                                    case "color":
                                        //echo (String) $att['val'];
                                        $attrs[] = 'color:#' . $att['val'];
                                        break;
                                    case "sz":
                                        $attrs[] = 'font-size:' . $att['val'] . 'px';
                                        break;
                                }
                            }
                            $styles[(String)$attr['styleId']] = array('tags' => $tags, 'attrs' => $attrs);
                        }
                    }
                    $this->styles = $styles;
                }
                if (($index = $zip->locateName('word/document.xml')) !== false) {
                    // If found, read it to the string
                    $data = $zip->getFromIndex($index);
                    // Close archive file
                    $zip->close();
                    return $data;
                }
                $zip->close();
            } else {
                switch($openedZip) {
                    case ZipArchive::ER_EXISTS:
                        $this->errors[] = 'File exists.';
                        break;
                    case ZipArchive::ER_INCONS:
                        $this->errors[] = 'Inconsistent zip file.';
                        break;
                    case ZipArchive::ER_MEMORY:
                        $this->errors[] = 'Malloc failure.';
                        break;
                    case ZipArchive::ER_NOENT:
                        $this->errors[] = 'No such file.';
                        break;
                    case ZipArchive::ER_NOZIP:
                        $this->errors[] = 'File is not a zip archive.';
                        break;
                    case ZipArchive::ER_OPEN:
                        $this->errors[] = 'Could not open file.';
                        break;
                    case ZipArchive::ER_READ:
                        $this->errors[] = 'Read error.';
                        break;
                    case ZipArchive::ER_SEEK:
                        $this->errors[] = 'Seek error.';
                        break;
                }
            }
        } else {
            $this->errors[] = 'File does not exist.';
        }
    }
    public function setFile($path) {
        $this->fileData = $this->load($path);
    }
    public function to_plain_text() {
        if ($this->fileData) {
            return strip_tags($this->fileData);
        } else {
            return false;
        }
    }
    public function xml_attribute($object, $attribute) {
        if(isset($object[$attribute]))
            return (string) $object[$attribute];
    }
    public function to_html() {
        if ($this->fileData) {

            $xml = simplexml_load_string($this->fileData);
            $namespaces = $xml->getNamespaces(true);
            $children = $xml->children($namespaces['w']);
            $html = '';
            foreach ($children->body->p as $p) {
                $attri = $this->xml_attribute($p->pPr->numPr->numId, 'val');
                $startTags = array();
                if ($p->pPr->numPr && !$this->list_tag_started) {
                    $html .= '<'.($attri == 1 ? 'o' : 'u').'l>';
                    $this->list_tag_started = true;
                    $this->list_type = $attri;
                }else {
                    $html .= '<p>';
                }

                $li = false;
                if ($p->pPr->numPr) {
                    $li = true;
                    $html .= '<li>';
                }else{
                    $html .= '</'.($this->list_type == 1 ? 'o' : 'u').'l>';
                    $this->list_tag_started = false;
                }
                foreach ($p->r as $part) {
                    $tags = $startTags;
                    foreach ((array)$part->rPr as $tag => $key) {
                        $tag = (array)$key;
                        reset($tag);
                        $tag = key($tag);
                        switch ($tag) {
                            case "b":
                                $tags[] = 'strong';
                                break;
                            case "i":
                                $tags[] = 'em';
                                break;
                        }
                    }
                    $openTags = '';
                    $closeTags = '';
                    foreach ($tags as $tag) {
                        $openTags.='<' . $tag . '>';
                        $closeTags.='</' . $tag . '>';
                    }
                    $html .= $openTags . $part->t . $closeTags;
                }
                if ($li) {
                    $html.='</li>';
                }
                $html .= '</p>';
            }
            //Trying to weed out non-utf8 stuff from the file:
            $regex = <<<'END'
/
  (
    (?: [\x00-\x7F]                 # single-byte sequences   0xxxxxxx
    |   [\xC0-\xDF][\x80-\xBF]      # double-byte sequences   110xxxxx 10xxxxxx
    |   [\xE0-\xEF][\x80-\xBF]{2}   # triple-byte sequences   1110xxxx 10xxxxxx * 2
    |   [\xF0-\xF7][\x80-\xBF]{3}   # quadruple-byte sequence 11110xxx 10xxxxxx * 3 
    ){1,100}                        # ...one or more times
  )
| .                                 # anything else
/x
END;
            preg_replace($regex, '$1', $html);
            return $html;
            exit();
        }
    }
    public function get_errors() {
        return $this->errors;
    }
}

add_action('wp_ajax_nopriv_get_Diff', 'get_Diff');
add_action('wp_ajax_get_Diff', 'get_Diff');

function get_Diff(){
    $diff = $_REQUEST['diff'];
    $customer = $_REQUEST['customer'];
    $docid = $_REQUEST['docid'];
    $diff = stripslashes($diff);

    $diff = mb_convert_encoding($diff, 'HTML-ENTITIES', "UTF-8");

    $filename = get_template_directory().'/documents/doc-u'.$customer.'d'.$docid.'.doc';
    $filenameHTML = get_template_directory().'/documents/doc-u'.$customer.'d'.$docid.'.html';
    $filenameHTMLC = get_template_directory().'/documents/doc-u'.$customer.'d'.$docid.'c.html';

    //$diff = highlight_string($diff, true);

    $inserts = preg_match_all('/<difftag class="diffmod diff ins">(.*?)<\/difftag>/', $diff, $matchINS);
    foreach($matchINS[0] as $k => $v){
            $diff = str_replace_first($v, strip_tags($v) . ' <!-- Writesaver Added: ' . html_entity_decode(strip_tags($matchINS[0][$k])) . '--> ', $diff);
    }

    $deletions = preg_match_all('/<difftag class="diffmod diff del">(.*?)<\/difftag>/', $diff, $matchDEL);
    foreach($matchDEL[0] as $k2 => $v2){
            $diff = str_replace_first($v2, ' <!-- Writesaver Deleted: '.html_entity_decode(strip_tags($matchDEL[0][$k2])).'--> ', $diff);
    }

    //$diff = preg_replace('/(<difftag(.*?)>)(.*?)(<\/difftag>)/', '', $diff);

    // HTML 2 DOCX
    file_put_contents($filenameHTML, $diff);
    $converter = new \NcJoes\OfficeConverter\OfficeConverter($filenameHTML);
    $converter->convertTo($filename, 'html');
    die();

    /**
     * CODE FOR PHPDOCX LIBRARY
     * NO USED MORE
     */
    /*$docx = new CreateDocx();
    $docx->embedHTML($diff);
    $docx->createDocx($filename);*/

    // Add Comments INS

    
    /*foreach($matchINS[0] as $k => $v){
        $docx = new CreateDocxFromTemplate($filename);
        $comment = new WordFragment($docx, 'document');

        $comment->addComment(
            array(
                'textDocument' => $matchINS[1][$k],
                'textComment' => 'Added: '. $matchINS[1][$k],
                'initials' => 'WS',
                'author' => 'Writesaver',
                'date' => date("j F Y")
            )
        );

        $docx->replaceVariableByWordFragment(array('TGP'.$k.'BEG' => $comment), array('type' => 'inline'));
        $docx->createDocx($filename);
    }

    foreach($matchDEL[0] as $k => $v){
        $docx = new CreateDocxFromTemplate($filename);
        $comment = new WordFragment($docx, 'document');

        $comment->addComment(
            array(
                'textDocument' => '',
                'textComment' => 'Deleted: '. $matchDEL[1][$k],
                'initials' => 'WS',
                'author' => 'Writesaver',
                'date' => date("j F Y")
            )
        );
        $docx->replaceVariableByWordFragment(array('TGP'.$k.'DEL' => $comment), array('type' => 'inline'));
        //$docx->removeTemplateVariable('TGP'.$k.'DEL', 'inline');
        $docx->createDocx($filename);
    }

    foreach($matchLinks[0] as $k => $v){
        $docx = new CreateDocxFromTemplate($filename);
        $comment = new WordFragment($docx, 'document');

        $comment->addComment(
            array(
                'textDocument' => '',
                'textComment' => 'Deleted ' . $matchLinksChanged[$k] . "\n" .' Added ' . strip_tags($matchLinks[0][$k]),
                'initials' => 'WS',
                'author' => 'Writesaver',
                'date' => date("j F Y")
            )
        );
        $docx->replaceVariableByHTML('TGP'.$k.'LINKS', 'inline', $matchLinks[0][$k], array('strictWordStyles' => false));
        $docx->replaceVariableByWordFragment(array('TGP'.$k.'LINKSDESC' => $comment), array('type' => 'inline'));
        $docx->createDocx($filename);
    }*/

    die();
}

function str_replace_first($from, $to, $subject){
    $from = '/'.preg_quote($from, '/').'/';
    //var_dump($from);
    return preg_replace($from, $to, $subject, 1);
}

function diffHtml($oldText, $newText) {
    //$newText = stripslashes($newText);
    //$newText = preg_replace('/(\s){2,}/', '', $newText);
    //$oldText = preg_replace('/(\s){2,}/', '', $oldText);
    require_once __DIR__."/evne/HtmlDiff.php";
    $diff = new HtmlDiff( $oldText, $newText );
    $diff->build();
    return $diff->getDifference();
}
