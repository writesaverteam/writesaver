<?php

global $wpdb, $pmpro_msg, $pmpro_msgt, $current_user, $pmpro_currency, $pmpro_currency_symbol, $pmpro_currencies;



$sqlQuery = "SELECT * FROM $wpdb->pmpro_membership_levels order by most_popular desc";

$raw_levels = $wpdb->get_results($sqlQuery);



//lets put them into an array where the key is the id of the level

$pmpro_levels = array();

foreach ($raw_levels as $raw_level) {

	$pmpro_levels[$raw_level->id] = $raw_level;

}

//$pmpro_levels = pmpro_getAllLevels(false, true);

//echo '<pre>';

//print_r($pmpro_levels);exit;

$pmpro_level_order = pmpro_getOption('level_order');



if (!empty($pmpro_level_order)) {

    $order = explode(',', $pmpro_level_order);



    //reorder array

    $reordered_levels = array();

    foreach ($order as $level_id) {

        foreach ($pmpro_levels as $key => $level) {

            if ($level_id == $level->id)

                $reordered_levels[] = $pmpro_levels[$key];

        }

    }



    $pmpro_levels = $reordered_levels;

}



$pmpro_levels = apply_filters("pmpro_levels_array", $pmpro_levels);

$currency_code = get_option('pmpro_currency');



$user_id = get_current_user_id();

$paypal_result = $wpdb->get_results("SELECT * FROM tbl_customer_general_info WHERE fk_customer_id = " . $user_id . " LIMIT 1 ");

$wp_one_time_purchase = $wpdb->get_row("SELECT * FROM wp_one_time_purchase WHERE id = 1");

if ($pmpro_msg) {

    ?>

    <div class="pmpro_message <?php echo $pmpro_msgt ?>"><?php echo $pmpro_msg ?></div>

    <?php

}

?>

<?php

if (isset($_REQUEST['add_paypal']) || isset($_REQUEST['add_paypal1']) || isset($_REQUEST['paypal_id'])) {

	$datetime = date('Y-m-d H:i:s');

	$timestamp = strtotime($datetime);

	

	if(isset($_SESSION['sess_id'])){

		$sess_id = $_SESSION['sess_id'];

	} else {

		$_SESSION['sess_id'] = $timestamp;

		$sess_id = $timestamp;

	}

	

    $paypal_id = $_REQUEST['paypal_id'];

    $paypal_pay_amount = round($_REQUEST['paypal_pay_amount'], 2);

    $paypal_extra_words = $_REQUEST['paypal_extra_words'];



    $date = date('Y-m-d h:i:s');

    $descr = $wp_one_time_purchase->name . " charges for " . $paypal_extra_words . " words";

    $wpdb->insert('wp_price_per_extra_words', array('fk_customer_id' => $user_id, 'payment_date' => $date, 'price' => $paypal_pay_amount, 'descriptions' => $descr, 'payment_source' => 'PayPal', 'status' => 0, 'words' => $paypal_extra_words));

    $ins_id = $wpdb->insert_id;

	

	$paypal_discount_amount = $_REQUEST['paypal_discount_amount'];



    $wpdb->update("tbl_customer_general_info", array('paypal_id' => $paypal_id), array('fk_customer_id' => $user_id));



    $business_email = get_option('pmpro_gateway_email');

    $gateway_environment = get_option('pmpro_gateway_environment');

    if ($gateway_environment == 'sandbox') {

        $URL = "https://www.sandbox.paypal.com/cgi-bin/webscr";

    } elseif ($gateway_environment == 'live') {

        $URL = "https://www.paypal.com/cgi-bin/webscr";

    }



    $currency_code = get_option('pmpro_currency');



    $notify = get_theme_root_uri() . '/' . get_template() . '/notify-paypal.php'; //get_site_url()."/notify-paypal.php";

    if($paypal_discount_amount > 0){

		$success = get_the_permalink(762) . "/?token=xs00u8c9cxd&level=success&ins_id=$ins_id&discount=yes";

	} else {

		$success = get_the_permalink(762) . "/?token=xs00u8c9cxd&level=success&ins_id=$ins_id";

	}

    $cancel = get_the_permalink(762) . "/?token=xd0eu8c9cdf&level=cancel";

    $i = 1;

    ?>



    <div class="wrapper">

        <div style="text-align:center; margin-top:30px; font-size:24px;">Please wait...Loading PayPal...<br/><br/>Don't Refresh the page.</div>

        <div style="text-align:center; margin-top:20px;">

        </div>



        <form action="<?php echo $URL; ?>" method="post" name="ckout" id="ckout">

            <input type="hidden" name="cmd" value="_cart">

            <input type="hidden" name="upload" value="1">

            <input type="hidden" name="business" value="<?php echo $business_email; ?>">

            <input type="hidden" name="item_number_<?php echo $i; ?>" value="<?php echo $ins_id; ?>">

            <input type="hidden" name="item_name_<?php echo $i; ?>" value="<?php echo $descr; ?>">

            <input type="hidden" name="quantity_<?php echo $i; ?>" value="1">

            <input type="hidden" name="amount_<?php echo $i; ?>" value="<?php echo $paypal_pay_amount; ?>">

            <input type="hidden" name="shipping_1" value="0">

            <input type="hidden" name="discount_amount_cart" value="0">

			<input type="hidden" name="discount_amount_cart" value="<?php echo $paypal_discount_amount; ?>">

            <input type="hidden" name="currency_code" value="<?php echo $currency_code; ?>">

            <input type="hidden" name="custom" value="<?php echo $ins_id; ?>">

            <input type="hidden" name="notify_url" value="<?php echo $notify; ?>">

            <input type="hidden" name="return" value="<?php echo $success; ?>">

            <input type="hidden" name="cancel_return" value="<?php echo $cancel; ?>">

        </form>

        <script type="text/javascript">

            document.ckout.submit();

        </script>

        <?php

    }



    include 'stripe/Stripe.php';



    Stripe::setApiKey(pmpro_getOption("stripe_secretkey"));



    if (isset($_POST['stripeToken'])) {

		$datetime = date('Y-m-d H:i:s');

		$timestamp = strtotime($datetime);



		if(isset($_SESSION['sess_id'])){

			$sess_id = $_SESSION['sess_id'];

		} else {

			$_SESSION['sess_id'] = $timestamp;

			$sess_id = $timestamp;

		}

		

        $user_id = get_current_user_id();

        $cardno = encrypt_string($_REQUEST['cardno']);

        $expdate = $_REQUEST['expdate'];

        $expyear = $_REQUEST['expyear'];

        $securitycode = encrypt_string($_REQUEST['securitycode']);



        $country = $_REQUEST['country'];

        $address = $_REQUEST['address'];

        $address1 = $_REQUEST['address1'];

        $zipcode = $_REQUEST['zipcode'];

        $state = $_REQUEST['state'];



        $prefix = $wpdb->prefix;

        $table_name = $prefix . 'creditdebit_card_details';

        $user = $wpdb->get_results("SELECT * FROM $table_name WHERE customer_id = $user_id");

        $user_count = count($user);

        if ($user_count > 0) {

            $insert = $wpdb->update(

                    $table_name, array(

                'cardnumber' => $cardno,

                'expirymonth' => $expdate,

                'expyear' => $expyear,

                'securitycode' => $securitycode,

                'country' => $country,

                'address' => $address,

                'address1' => $address1,

                'zipcode' => $zipcode,

                'state' => $state

                    ), array('customer_id' => $user_id)

            );

        } else {

            $insert = $wpdb->insert(

                    $table_name, array(

                'customer_id' => $user_id,

                'cardnumber' => $cardno,

                'expirymonth' => $expdate,

                'expyear' => $expyear,

                'securitycode' => $securitycode,

                'country' => $country,

                'address' => $address,

                'address1' => $address1,

                'zipcode' => $zipcode,

                'state' => $state,

                'createddate' => date('Y-m-d')

                    )

            );

        }



        $amount_cents = str_replace(".", "", $_POST['card_price']);

        $price = $_POST['card_price'];

        $txt_extra_words = $_POST['txt_extra_words'];

        $dashboard_url = get_the_permalink(762);

        try {

            if($_POST['level'] == "custom") {

                /* In case of One-time payment */

				$date = date('Y-m-d h:i:s');

				$description = $wp_one_time_purchase->name . " charges for " . $txt_extra_words . " words";

				$wpdb->insert('wp_price_per_extra_words', array('fk_customer_id' => $user_id, 'stripe_reference' => '', 'payment_date' => $date, 'price' => $price, 'descriptions' => $description, 'payment_source' => 'Stripe', 'words' => $txt_extra_words, 'status' => 0));

				$ins_id = $wpdb->insert_id;





				$table_name = $wpdb->prefix . 'creditdebit_card_details';

				$user_card_details = $wpdb->get_results("SELECT * FROM $table_name WHERE customer_id = $user_id");



				$result_countries = $wpdb->get_results("SELECT * from countries");



				foreach ($result_countries as $value) {

					if($value->id == $user_card_details[0]->country) {

						$country = $value->name;



						$result_states = $wpdb->get_results("SELECT * from states where country_id=" . $value->id . "");



						foreach ($result_states as $state_val) {

							if( $state_val->id == $user_card_details[0]->state ) {

								$state = $state_val->name;

								break;

							}

						}



						break;

					}

				}



				$metadata = array();



				if( $country ) {

					$metadata['country'] = $country;

				}

				if( $user_card_details[0]->address ) {

					$metadata['address'] = $user_card_details[0]->address;

				}

				if( $user_card_details[0]->address1 ) {

					$metadata['address1'] = $user_card_details[0]->address1;

				}

				if( $state ) {

					$metadata['state'] = $state;

				}

				if( $user_card_details[0]->zipcode ) {

					$metadata['zipcode'] = $user_card_details[0]->zipcode;

				}



				$userdata = get_userdata( $user_id );

				$exist_customer = Stripe_Customer::all(array("email" => $userdata->user_email));



				if (isset($_REQUEST['stripeDiscount'])) {

					$discount = Stripe_Coupon::create(array(

						'duration' => "once",

						"currency" => "usd",

						'id' => "discount-coupon-" . $_POST['stripeToken'],

						'amount_off' => str_replace(".", "", $_POST['stripeDiscount'])

					));

				}

				

				if( !empty($exist_customer['data']) ) {

					$stripe_user_id = $exist_customer['data'][0]['id'];



					$cus_retrieve = Stripe_Customer::retrieve($stripe_user_id);

					$cus_retrieve->source = $_POST['stripeToken'];

					$cus_retrieve->metadata = $metadata;



					if (isset($_REQUEST['stripeDiscount'])) {

						$cus_retrieve->coupon = "discount-coupon-" . $_POST['stripeToken'];

					}

					

					$cus_retrieve->save();



				} else {

					if (isset($_REQUEST['stripeDiscount'])) {					

						$customer = Stripe_Customer::create(array(

							"email" => $userdata->user_email,

							"source" => $_POST['stripeToken'],

							"metadata" => $metadata,

							"coupon" => "discount-coupon-" . $_POST['stripeToken']

						));

					} else {

						$customer = Stripe_Customer::create(array(

							"email" => $userdata->user_email,

							"source" => $_POST['stripeToken'],

							"metadata" => $metadata

						));

					}

					$stripe_user_id = $customer['id'];

				}



				if (isset($_REQUEST['stripeDiscount'])) {

					$charge_metadata = array(

						"coupon_code" => "discount-coupon-" . $_POST['stripeToken'],

						"coupon_discount" => str_replace(".", "", $_POST['stripeDiscount'])

					);

				} else {

					$charge_metadata = array();

				}



				$charge = Stripe_Charge::create(array(

					"amount" => $amount_cents,

					"currency" => "usd",

					"customer" => $stripe_user_id,

					"description" => $description,

					"metadata" => $charge_metadata

					)

				);



				$chargeArray = $charge->__toArray(true);

				$stripe_reference = $chargeArray['id'];



				$wpdb->update("wp_price_per_extra_words", array('stripe_reference' => $stripe_reference), array('id' => $ins_id));

				if (isset($_REQUEST['stripeDiscount'])) {

					echo "<script type='text/javascript'>window.location.href='$dashboard_url/?token=xs00u8c9cxd&level=success&ins_id=$ins_id&discount=yes';</script>";

				} else {

					echo "<script type='text/javascript'>window.location.href='$dashboard_url/?token=xs00u8c9cxd&level=success&ins_id=$ins_id';</script>";

				}

            } else {

                /* In case of Membership payment */

                $pmpro_level = pmpro_getLevel($_POST['level']);

                $description = "Subscription for ".$pmpro_level->name;



                

                $table_name = $wpdb->prefix . 'creditdebit_card_details';

                $user_card_details = $wpdb->get_results("SELECT * FROM $table_name WHERE customer_id = $user_id");



                $result_countries = $wpdb->get_results("SELECT * from countries");



                foreach ($result_countries as $value) {

                    if($value->id == $user_card_details[0]->country) {

                        $country = $value->name;



                        $result_states = $wpdb->get_results("SELECT * from states where country_id=" . $value->id . "");



                        foreach ($result_states as $state_val) {

                            if( $state_val->id == $user_card_details[0]->state ) {

                                $state = $state_val->name;

                                break;

                            }

                        }



                        break;

                    }

                }

				

				$discountCode = false;

				if (isset($_REQUEST['stripeDiscount'])) {

					$sess_id = $_SESSION['sess_id'];

					$code = $wpdb->get_row($wpdb->prepare("SELECT * FROM tbl_discount_codes c, tbl_discount_codes_applications a WHERE a.user_id = %d AND c.code = a.code_id AND a.sess_id = %d AND subscription = %d AND type = 2 LIMIT 1", $user_id, $sess_id, $_POST['level']));

					

					if($code){

						$counts = $wpdb->get_row($wpdb->prepare("SELECT COUNT(*) as usedcounts FROM tbl_discount_codes_uses WHERE code_id = %s AND user_id = %d", $code->code_id, $user_id));

					

						if($code->uses > 0){

							$remaining = max(0,(int)$code->uses - (int)$counts->usedcounts);

						} else {

							$remaining = 0;

						}

						if($remaining == 1){

							$uses = 'once';

							$duration_in_months = null;

						} else if($remaining == 0){

							$uses = 'forever';

							$duration_in_months = null;

						} else {

							$uses = 'repeating';

							$duration_in_months = $remaining;

						}

						

						$discount = Stripe_Coupon::create(array(

							'duration' => $uses,

							'duration_in_months' => $duration_in_months,

							"currency" => "usd",

							'id' => "discount-coupon-" . $_POST['stripeToken'],

							'amount_off' => str_replace(".", "", $_POST['stripeDiscount'])

						));

						$discountCode = true;

					}

				}



                $metadata = array();



                if( $country ) {

                    $metadata['country'] = $country;

                }

                if( $user_card_details[0]->address ) {

                    $metadata['address'] = $user_card_details[0]->address;

                }

                if( $user_card_details[0]->address1 ) {

                    $metadata['address1'] = $user_card_details[0]->address1;

                }

                if( $state ) {

                    $metadata['state'] = $state;

                }

                if( $user_card_details[0]->zipcode ) {

                    $metadata['zipcode'] = $user_card_details[0]->zipcode;

                }



                $userdata = get_userdata( $user_id );

                $exist_customer = Stripe_Customer::all(array("email" => $userdata->user_email));



                if( !empty($exist_customer['data']) ) {

                    $stripe_user_id = $exist_customer['data'][0]['id'];



                    $cus_retrieve = Stripe_Customer::retrieve($stripe_user_id);

                    $cus_retrieve->source = $_POST['stripeToken'];

                    $cus_retrieve->metadata = $metadata;

					

					if (isset($_REQUEST['stripeDiscount']) && $discountCode == true) {

						$cus_retrieve->coupon = "discount-coupon-" . $_POST['stripeToken'];

					}



                    $cus_retrieve->save();



                } else {

					if (isset($_REQUEST['stripeDiscount']) && $discountCode == true) {					

						$customer = Stripe_Customer::create(array(

							"email" => $userdata->user_email,

							"source" => $_POST['stripeToken'],

							"metadata" => $metadata,

							"coupon" => "discount-coupon-" . $_POST['stripeToken']

						));

					} else {

						$customer = Stripe_Customer::create(array(

							"email" => $userdata->user_email,

							"source" => $_POST['stripeToken'],

							"metadata" => $metadata

						));

					}

                    $stripe_user_id = $customer['id'];

                }



				if (isset($_REQUEST['stripeDiscount']) && $discountCode == true) {					

					$subscr = Stripe_Subscription::create(array(

						"customer" => $stripe_user_id,

						"items" => array(

							array(

								"plan" => ucfirst($pmpro_level->name),

							),

						),

						"coupon" => "discount-coupon-" . $_POST['stripeToken']

					));

				} else {

					$subscr = Stripe_Subscription::create(array(

						"customer" => $stripe_user_id,

						"items" => array(

							array(

								"plan" => ucfirst($pmpro_level->name),

							),

						)

					));

				}

				

                $subscrArray = $subscr->__toArray(true);

                $stripe_reference = $subscrArray['id'];

				

                if($stripe_reference){					

					$morder = new MemberOrder();

					$morder->user_id = $user_id;

					$morder->membership_id = $pmpro_level->id;



					$morder->subtotal = $price;

					$morder->tax = 0;

					$morder->total = $price;



					$morder->payment_transaction_id = '';

					$morder->subscription_transaction_id = $stripe_reference;

					$morder->payment_type = 'Stripe';



					$morder->gateway = 'stripe';

					$morder->gateway_environment = get_option('pmpro_gateway_environment');



					$morder->FirstName = $user_card_details[0]->firstname;

					$morder->LastName = $user_card_details[0]->lastname;

					$morder->Email = $wpdb->get_var("SELECT user_email FROM $wpdb->users WHERE ID = '" . $user_id . "' LIMIT 1");

					$morder->Address1 = $user_card_details[0]->address;

					$morder->City = $user_card_details[0]->city;

					$morder->State = $user_card_details[0]->state;

					$morder->Zip = $user_card_details[0]->zipcode;

					$morder->PhoneNumber = $user_card_details[0]->phone;



					$morder->billing = new stdClass();

					

					$morder->billing_name = $user_card_details[0]->firstname ." ". $user_card_details[0]->lastname;

					$morder->billing_street = $user_card_details[0]->address;

					$morder->billing->city = $user_card_details[0]->city;

					$morder->billing->state = $user_card_details[0]->state;

					$morder->billing->zip = $user_card_details[0]->zipcode;

					$morder->billing->country = $country;

					$morder->billing->phone = $user_card_details[0]->phone;



					$morder->cardtype = '';

					$card = $wpdb->get_row("SELECT * FROM $table_name WHERE customer_id = $user_id");

					$morder->accountnumber = $card->cardnumber;

					$morder->expirationmonth = $card->expirymonth;

					$morder->expirationyear = $card->expyear;



					//save

					$morder->status = "pending";

					$morder->saveOrder();

					//update the current user

					global $current_user;

					if (!$current_user->ID && $user->ID) {

						$current_user = $user;

					} //in case the user just signed up

						pmpro_set_current_user();

					//}

					if (isset($_REQUEST['stripeDiscount'])) {

						echo "<script type='text/javascript'>window.location.href='$dashboard_url/?token=xs00u8c9frd&level=success&discount=yes';</script>";

					} else {

						echo "<script type='text/javascript'>window.location.href='$dashboard_url/?token=xs00u8c9frd&level=success';</script>";

					}

				} else {

					echo "<script type='text/javascript'>window.location.href='$dashboard_url/?token=xd0eu8c9cdf&level=cancel';</script>";

				}

            }



        } catch (Stripe_CardError $e) {



            echo "<script type='text/javascript'>

			window.location.href='$dashboard_url/?token=xd0eu8c9cdf&level=declined';

			</script>";



            //$error = $e->getMessage();

        } catch (Stripe_InvalidRequestError $e) {



            //print_r($e->getMessage());exit;

            echo "<script type='text/javascript'>

			window.location.href='$dashboard_url/?token=xd0eu8c9cdf&level=declined';

			</script>";

        } catch (Stripe_AuthenticationError $e) {



            //print_r($e->getMessage());exit;

            echo "<script type='text/javascript'>

			window.location.href='$dashboard_url/?token=xd0eu8c9cdf&level=declined';

			</script>";

        } catch (Stripe_ApiConnectionError $e) {



            //print_r($e->getMessage());exit;

            echo "<script type='text/javascript'>

			window.location.href='$dashboard_url/?token=xd0eu8c9cdf&level=declined';

			</script>";

        } catch (Stripe_Error $e) {



            //print_r($e->getMessage());exit;

            echo "<script type='text/javascript'>

			window.location.href='$dashboard_url/?token=xd0eu8c9cdf&level=declined';

			</script>";

        } catch (Exception $e) {



        }

    }

    ?>





    <div class="all_plans pmpro_checkout"  id="pmpro_levels_table">

        <div>

			<?php

            $count = 0;

            if ($pmpro_levels) { ?>

			<?php $trans=array();

				foreach ($pmpro_levels as $level) {

					if (isset($current_user->membership_level->ID))

						$current_level = ($current_user->membership_level->ID == $level->id);

					else

						$current_level = false;

					if ($level->most_popular == 1) {

						$class = "priceTableInnTopSpec";

						$planclass = 'priceTablesInnSpec';

					} else {

						$class = "priceTableInnTop";

						$planclass = "";

					}

					?>

					<div class="col-xs-12 col-sm-12 col-md-4 priceTables monthly_plan">

						<div class="priceTablesInn <?php echo $planclass; ?> plan_box">

							<div class="<?php echo $class; ?> text-center">

								<?php if ($level->most_popular == 1): ?>

									<div class="businessBest">

										<h5>Most popular</h5>

									</div>

								<?php endif; ?>

								<h3><?php echo $level->name; ?></h3>

							</div>

							<div class="priceTableInnMid text-center">

								<div class="pricingElipse text-center">

									<?php

									if (pmpro_isLevelFree($level)){

										echo "<h3>" . __("Free", "pmpro") . "</h3>";

									} else {

										echo "<h3>" . pmpro_formatPrice($level->billing_amount) . "</h3><div class='elipseUnder'></div><p>/ " . pmpro_translate_billing_period($level->cycle_period) . "</p>";

									}?>

								</div>

								<h4 class="prSpecH"><?php echo $level->plan_words; ?></span> words / <?php echo pmpro_translate_billing_period($level->cycle_period); ?></h4>

								<p><?php echo $pmpro_currency_symbol . $level->price_per_additional_word; ?> /additional word*</p>

								<p style="margin-bottom: 0px;"><a href="javascript:void(0);" class="applyDiscount" style="margin-top: 10px;">Apply Discount Code</a></p>

								<p style="margin-bottom: 0px;" class="discounts">

									<input type="text" autocomplete="off" class="contact_block discountCode" name="discountCode" placeholder="Enter Discount Code" style="display: none;width: 80%;margin-bottom: 5px !important;">

									<span class="edit_link">

										<a href="javascript:void(0);" class="btnSaveDiscount save_pro" style="display: none;"></a>

										<a href="javascript:void(0);" class="btnCancelDiscount cancel_pro" style="display: none;"></a>

									</span>

								</p>

							</div>

							<div class="priceTableInnBtm text-center">

								<h3>

									<?php if (empty($current_user->membership_level->ID)) {

										$price=str_replace('&#36;', '',str_replace('/Month', '', $cost_text));						

										$trans []= array('id'=>$level->id, 'name'=>$level->name,    'category'=>'Monthly Subscription Plans', 'list'=>'Plan', 'position'=>$level->id ,'price'=>$level->billing_amount); 

									?>

									<a class=" pmpro_btn-select btn_sky class_bill_method" 

									data-name="<?=$level->name?>" 

									data-price="<?=$level->billing_amount?>" 

									data-sku="<?= $level->id?>" 

									data-per-words="<?= $level->price_per_additional_word?>" 

									data-id_sku="<?= $level->id?>" 

									data-list="Plan" 

									data-category="Monthly Subscription Plans" 

									id="<?php echo $level->id; ?>" data-price="<?=$level->billing_amount?>" href="javascript:void(0);"><?php _e('Purchase now', 'pmpro'); ?></a>

									<?php } elseif (!$current_level) { ?>                	

										<a class=" pmpro_btn-select btn_sky class_bill_method" data-price="<?=$level->billing_amount?>" data-per-words="<?= $level->price_per_additional_word?>" data-words="<?=$level->plan_words?>" id="<?php echo $level->id; ?>" href="javascript:void(0);"><?php _e('Purchase now', 'pmpro'); ?></a>

									<?php } elseif ($current_level) { 

										//if it's a one-time-payment level, offer a link to renew				

										if (pmpro_isLevelExpiringSoon($current_user->membership_level) && $current_user->membership_level->allow_signups) {

											?>

											<a class=" pmpro_btn-select btn_sky class_bill_method" id="<?php echo $level->id; ?>" data-price="<?=$level->billing_amount?>" data-words="<?=$level->plan_words?>" data-per-words="<?= $level->price_per_additional_word?>" href="javascript:void(0);"><?php _e('Upgrade Plan', 'pmpro'); ?></a>

											<?php

										} else {

											?>

											<a class=" disabled btn_sky class_bill_method" id="<?php echo $level->id; ?>" data-price="<?=$level->billing_amount?>" data-words="<?=$level->plan_words?>" data-per-words="<?= $level->price_per_additional_word?>" href="javascript:void(0);"><?php _e('Purchase now', 'pmpro'); ?></a>

											<?php

										}

									} ?>    

								</h3>

							</div>

						</div> 

					</div> 

				<?php

				}

				?>

				<?php

				if ($wp_one_time_purchase):



					$current_level = pmpro_getMembershipLevelForUser($current_user->ID);

					if($current_level){

						$plan_id = $current_level->id;

					} else {

						$plan_id = 0;

					}



					if ($plan_id > 0) {

						$user_is_subscribed = 'yes';



						$per_words_price = $wpdb->get_row("SELECT price_per_additional_word FROM wp_pmpro_membership_levels WHERE id = " . $plan_id);



						$price_per_words = $per_words_price->price_per_additional_word;

						if ($price_per_words == "") {

							$price_per_words = 0;

						}

					} else {



						$user_is_subscribed = 'no';

						$price_per_words = $wp_one_time_purchase->price_per_word;

					}

					?>

					<div class="col-xs-12 col-sm-12 col-md-4 priceTables one_time">

						<div class="priceTablesInn">

							<div class="priceTableInnTop text-center">

								<h3><?php echo $wp_one_time_purchase->name; ?></h3>

							</div>

							<div class="priceTableInnMid text-center enter_word">

								<div class="pricingElipse text-center">

									<h3 id="dis_price">$0.00</h3>

									<div class="elipseUnder"></div>

									<p><?php echo $pmpro_currency_symbol . $price_per_words; ?>/word</p>

								</div>

								<div class="input-group subs">

									<input type="text" placeholder="Number of words" id="hdntotalwords" name="hdntotalwords" class="only_num form-control subEmail" maxlength="6">

									<div class="input-group-btn">

										<button class="btn btn-info subbtn" id="save_words">Calculate</button>

									</div>

									<input type="hidden" id="total_words_price" name="total_words_price" />

									<input type="hidden" id="hid_price_per_words" value="<?php echo $price_per_words; ?>" />

								</div>

								<p style="margin-bottom: 0px;"><a href="javascript:void(0);" class="applyDiscount" style="margin-top: 10px;">Apply Discount Code</a></p>

								<p style="margin-bottom: 0px;" class="discounts">

									<input type="text" autocomplete="off" class="contact_block discountCode" name="discountCode" placeholder="Enter Discount Code" style="display: none;width: 80%;margin-bottom: 5px !important;">

									<span class="edit_link">

										<a href="javascript:void(0);" class="btnSaveDiscount save_pro" style="display: none;"></a>

										<a href="javascript:void(0);" class="btnCancelDiscount cancel_pro" style="display: none;"></a>

									</span>

								</p>

							</div>

							<div class="priceTableInnBtm text-center">

								<h3 id="id_SaveProofRead" class="btn_sky" disabled="" data-level_id="custom" data-name="<?=$wp_one_time_purchase->name?>" 

									data-sku="0" 

									data-per-words="<?= $price_per_words ?>" 

									data-id_sku="0" 

									data-list="Plan" 

									data-category="One-time Purchase">Purchase now</h3>

							</div>

						</div>

					</div>

				<?php endif; ?>

                <?php

            }			

// Function to return the JavaScript representation of a TransactionData object.

function getItemJs(&$trans) {

  return <<<HTML

ga('ec:addImpression', {

  'id': '{$trans['id']}',

  'name': '{$trans['name']}',

  'price':'{$trans['price']}'

});

HTML;

}



/*

  'category': '{$trans['category']}',

  'list': '{$trans['list']}',

  'position': '{$trans['position']}',

*/



// Function to return the JavaScript representation of an ItemData object.

            ?>

			<script type="text/javascript" id="">(function(a,e,f,g,b,c,d){a.GoogleAnalyticsObject=b;a[b]=a[b]||function(){(a[b].q=a[b].q||[]).push(arguments)};a[b].l=1*new Date;c=e.createElement(f);d=e.getElementsByTagName(f)[0];c.async=1;c.src=g;d.parentNode.insertBefore(c,d)})(window,document,"script","https://www.google-analytics.com/analytics.js","ga");ga("create","UA-80475559-1","auto");ga("send","pageview");</script>

			<script>

			//Google Ecommerce



			ga('require', 'ec');

			<?php

			foreach ($trans as $item) {

			  echo getItemJs($item);

			}

			?>

			ga('send', 'pageview'); 

			</script>

			

            <script>

                // new code

                jQuery(document).ready(function ($) {

                    $("#country").change(function () {

                        $('#state').find('option:not(:first)').remove();

                        $.ajax({

                            url: '<?php echo admin_url('admin-ajax.php'); ?>',

                            data: {

                                'action': 'getStates',

                                'countryId': $("#country").val()



                            },

                            dataType: 'text',

                            success: function (data) {

                                // This outputs the result of the ajax request

                                $("#state").append(data);

                            },

                            error: function (errorThrown) {

                                console.log(errorThrown);

                            }

                        });

                    });



					$('.class_bill_method').click(function () {

                      //  debugger;

                        var level_id = $(this).attr('id');

                        $('#hid_mamu_level_id').val(level_id);

                        var price = parseInt($(this).attr('data-price'));

                        $('#hid_mamu_price').val(price.toFixed(2));

                        $('#hid_mamu_words').val($(this).attr('data-words'));

                        var url = '<?php echo home_url(); ?>/paypal-process/?level=' + level_id;

                        $('#lbl_paypal_submit_link').attr("href", url);

                        $('#lbl_paypal_submit_link').removeAttr("data-toggle");

                        $('#lbl_paypal_submit_link').removeAttr("data-target");

                        $('#add_paypal').prop('type', 'button');

                        $('#add_paypal').val('Update');

                        if ($('#current_user_ID').val() == '') {

                            $('#hid_level_id').val(level_id);

                            open_loginpop();

                            return false;

                        } else {

                             $('#payment_option_modal .paypal_billing').hide();

                            $('#payment_option_modal').fadeIn();

                            $('#payment_option_modal').removeClass('open');

                            return false;

                        }

                    });

                    $('#openBtn1').click(function () {

                        //debugger;

                        var pay_type = $(this).attr('data-card-type');

                        var level_id = $('#hid_mamu_level_id').val();

                        if (level_id == 'custom') {

                            $('#Paypal').fadeIn();

                            $('#Paypal').addClass('open');

                            return false;

                        }

                        var mamu_url = $('#hid_mamu_url').val();

                        var iframe_url = mamu_url + "level=" + level_id + "&i=i&pay_type=" + pay_type;

                        $('#frame_plan1').attr('src', iframe_url);

                        setTimeout(function () {

                            $('#myModal1').fadeOut();

                            $('#myModal1').addClass('open');

                            $('#myModal1').css('display', 'block');

                        }, 3000);



                        return false;

                    });



                    $('#openBtn2').click(function () {



                        var pay_type = $(this).attr('data-card-type');

                        var level_id = $('#hid_mamu_level_id').val();

                        var mamu_url = $('#hid_mamu_url').val();



                        $('#frame_plan1').attr('src', mamu_url + "level=" + level_id + "&i=i&pay_type=" + pay_type)



                        $('#myModal1').fadeOut();

                        $('#myModal1').addClass('open');

                        $('#myModal1').css('display', 'block');



                        return false;

                    });



                    $('#payment_option_modal a.ppmodal .fa-remove').click(function (e) {

                        e.stopPropagation();

                        $('#payment_option_modal').fadeOut();

                        $('#payment_option_modal').removeClass('open');

                    });



                    $('#myModal1 a.ppmodal .fa-remove').click(function (e) {

                        $('#myModal1').fadeIn();

                        $('#myModal1').removeClass('open');

                        $('#myModal1').css('display', 'none');

                    });

                });

                function goToPaypal() {

                   // debugger;

                    var paypal_id = $('#paypal_id').val().trim();

                    if (paypal_id == "") {

                        $('#paypal_id').focus();

                        return false;

                    }

                    $.post("<?php echo get_template_directory_uri(); ?>/ajax.php", {"choice": "update_paypal", "paypal_id": paypal_id}, function (result) {



                        var tmp = result.split('~');

                        if (tmp[0] == "yes") {



                            var level_id = $('#hid_mamu_level_id').val();

                            if (level_id == 'custom') {

                                return false;

                            }

                            var url = '<?php echo home_url(); ?>/paypal-process/?level=' + level_id;

                            window.location = url;

                        }

                    });

                }

            </script>







            <!--Billing Method-->

            <input type="hidden" id="hid_mamu_level_id" value="<?php ($_REQUEST['plan']) ? $_REQUEST['plan'] : ''; ?>" />

            <input type="hidden" id="hid_mamu_price" value="" />

            <input type="hidden" id="hid_mamu_words" value="" />

            <input type="hidden" id="hid_mamu_url" value="<?php echo get_site_url(); ?>/membership-account/membership-checkout/?" />

            <input type="hidden" id="hid_mamu_pay_type" value="" />

            <div id="payment_option_modal" class="pop_overlay" style="display: none;">

                <div class="pop_main" style="padding: 0px; height: 310px; margin: auto;">

                    <div class="pop_head" style="min-height:0;">

                        <a href="javascript:void(0);" data-dismiss="modal" type="button" class="ppmodal" ><i class="fa fa-remove"  aria-hidden="true"></i></a>

                    </div>

                    <div class="pop_body">

                        <div class="setting_right">

                            <div class="field_title">

                                <h4>Billing Methods</h4>

                            </div>

                            <div class="">

                                <div class="billing_table_responsive">

                                    <div class="billing_table">

                                        <div class="customer_billing_methods paypal_billing">

                                            <div class="billing_image">

                                                <img src="<?php echo get_template_directory_uri() ?>/images/paypal.png" alt="paypal">

                                            </div>

                                            <div class="billing_type">

                                                <p>Paypal</p>

                                            </div>

                                            <div class="bill_popup" style="width:50%;">

                                                <?php if ($paypal_result[0]->paypal_id == "") { ?>

                                                    <a href="#" role="button" data-toggle="modal" data-target="#paypal_modal" onclick="myga('Paypal')" data-card-type="paypal" class="paypal_pop btn_sky">Set up</a>

                                                <?php } else { ?>

                                                    <a href="#" role="button" id="lbl_paypal_submit_link" onclick="myga('Proceed')" class="paypal_pop btn_sky">Proceed</a>

                                                <?php } ?>

                                            </div>

                                        </div>

                                        <div class="customer_billing_methods">

                                            <div class="billing_image">

                                                <img src="<?php echo get_template_directory_uri() ?>/images/credit_debit.png" alt="paypal">

                                            </div>

                                            <div class="billing_type">

                                                <p>Credit or debit card</p>

                                            </div>

                                            <div class="bill_popup" style="width:50%;">

                                                <a href="javascript:void(0);" class="btn_sky pop_btn" data-toggle="modal" onclick="myga('Credit card')" data-card-type="card" id="openBtn1">Set up</a>

                                            </div>

                                        </div>

                                    </div>

                                </div>

                            </div>

                        </div>

                    </div>

                </div>

            </div>



            <!--Paypal Method-->

            <div id="paypal_modal" class="pop_overlay" style="display: none;">

                <div class="pop_main" style="bottom:30%;">

                    <div class="pop_head">

                        <a href="javascript:void(0);" data-dismiss="modal" type="button" ><i class="fa fa-remove"  aria-hidden="true"></i></a>

                    </div>

                    <div class="pop_body">

                        <div class="page_title">

                            <h2>Add a Paypal</h2>

                            <h4>Payment information</h4>

                        </div>

                        <div class="pop_content">



                            <form method="post" action="" id="cust_paypal" autocomplete="off">

                                <div class="row">

                                    <div class="col-sm-offset-2 col-sm-8">

                                        <input type="email" class="contact_block" name="paypal_id" id="paypal_id" value="<?php echo $paypal_result[0]->paypal_id; ?>" maxlength="100"  placeholder="Paypal Id*">

                                    </div>

                                    <div class="buttons col-sm-12" style="margin-top:2%;">

                                        <input type="button" name="add_paypal" id="add_paypal" value="Update" class="btn_sky" style="float:none;" onclick="goToPaypal();">

                                        <input data-dismiss="modal" type="button" id="close_paypal_model" value="Cancel" class="btn_sky">

                                        <input type="hidden" name="paypal_extra_words" id="paypal_extra_words" />

                                        <input type="hidden" name="paypal_pay_amount" id="paypal_pay_amount" />

										<input type="hidden" value="0" name="paypal_discount_amount" id="paypal_discount_amount" />

                                    </div>

                                    <div class="paypal_msg"></div>

                                </div>

                            </form>



                        </div>

                    </div>

                </div>

            </div>

            <div id="Paypal" class="pop_overlay open" style="display: none;">

                <div class="pop_main">

                    <div class="pop_head">

                        <a href="javascript:void(0);"><i class="fa fa-remove" aria-hidden="true"></i></a>

                    </div>

                    <div class="pop_body">

                        <div class="page_title">

                            <h2>Add a credit or debit card</h2>

                            <h4>Payment information</h4>

                        </div>

                        <div class="pop_content">            	

                            <form action="" method="POST" id="payment-form" autocomplete="off"  >

                                <?php

                                $user_id = get_current_user_id();



                                $prefix = $wpdb->prefix;

                                $table_name = $prefix . 'creditdebit_card_details';

                                $user_infio = $wpdb->get_row("SELECT * FROM $table_name WHERE customer_id = $user_id");



                                $result_countries = $wpdb->get_results("SELECT * from countries");

                                $credit_result = $wpdb->get_results("SELECT * FROM wp_creditdebit_card_details WHERE customer_id = $user_id LIMIT 1 ");

                                $credit_result = $credit_result[0];

                                ?>

                                <div class="row">

                                    <div class="col-sm-12 text-center">

                                        <span class="payment-errors" style="color:#F00;"></span>

                                    </div>

                                    <div class="col-sm-12">

                                        <input type="text" value="<?php echo decrypt_string($user_infio->cardnumber); ?>" name="cardno" id="cardno" placeholder="Card number" value="" data-stripe="number" class="only_num contact_block" maxlength="16">

                                        <div><span id="errorcardno" style="color:red;"></span></div>

                                    </div>

                                    <div class="col-sm-6">

                                        <select class="contact_block" id="expdate" name="expdate" data-stripe="exp_month">

                                            <option>Expiration Month</option>

                                            <option value="01" <?php echo ($user_infio->expirymonth == '01') ? "selected" : ''; ?>>01</option>

                                            <option value="02" <?php echo ($user_infio->expirymonth == '02' ) ? "selected" : ''; ?>>02</option>

                                            <option value="03" <?php echo ($user_infio->expirymonth == '03') ? "selected" : ''; ?>>03</option>

                                            <option value="04" <?php echo ($user_infio->expirymonth == '04' ) ? "selected" : ''; ?>>04</option>

                                            <option value="05" <?php echo ($user_infio->expirymonth == '05') ? "selected" : ''; ?>>05</option>

                                            <option value="06" <?php echo ($user_infio->expirymonth == '06' ) ? "selected" : ''; ?>>06</option>

                                            <option value="07" <?php echo ($user_infio->expirymonth == '07') ? "selected" : ''; ?>>07</option>

                                            <option value="08" <?php echo ($user_infio->expirymonth == '08' ) ? "selected" : ''; ?>>08</option>

                                            <option value="09" <?php echo ($user_infio->expirymonth == '09') ? "selected" : ''; ?>>09</option>

                                            <option value="10" <?php echo ($user_infio->expirymonth == '10' ) ? "selected" : ''; ?>>10</option>

                                            <option value="11" <?php echo ($user_infio->expirymonth == '11') ? "selected" : ''; ?>>11</option>

                                            <option value="12" <?php echo ($user_infio->expirymonth == '12' ) ? "selected" : ''; ?>>12</option>

                                        </select>

                                        <div><span id="errorexpdate" style="color:red;"></span></div> 

                                    </div>

                                    <div class="col-sm-6">

                                        <select class="contact_block" id="expyear" name="expyear" data-stripe="exp_year">

                                            <option>Year</option>

                                            <?php

                                            $next_yr = date('Y') + 12;

                                            for ($i = date('Y'); $i < $next_yr; $i++) {

                                                ?>

                                                <option value="<?php echo $i; ?>" <?php echo ($user_infio->expyear == $i) ? "selected" : ''; ?>><?php echo $i; ?></option>

                                            <?php } ?>

                                        </select>

                                        <div><span id="errorexpyear" style="color:red;"></span></div> 

                                    </div>

                                    <div class="col-sm-6">

                                        <input type="text" value="<?php echo $user_infio->securitycode; ?>" maxlength="3" placeholder="Security Code*" id="securitycode" name="securitycode" class="only_num contact_block" data-stripe="cvc" />

                                        <div><span id="errorsecuritycode" style="color:red;"></span></div> 

                                    </div>

                                    <div class="col-sm-6">

                                        <label class="contact_block">What's this?</label>

                                    </div>



                                    <div class="col-sm-12">

                                        <select class="contact_block" id="country" name="country">

                                            <option  value="0">Select Country</option>

                                            <?php 

                                                foreach ($result_countries as $value): ?>

                                                    <?php if ($value->name != "") : ?>

                                                            <option <?php echo ($credit_result->country == $value->id) ? "selected" : ''; ?> value="<?php echo $value->id ?>"> <?php echo $value->name ?> </option>

                                                    <?php endif; ?>

                                                <?php endforeach; ?>

                                        </select>

                                    </div>



                                    <div class="col-sm-12">

                                        <input  value="<?php echo $credit_result->address; ?>"  type="text" placeholder="Address" class="contact_block" id="address" name="address" maxlength="200"/>

                                    </div>



                                    <div class="col-sm-12">

                                        <input  value="<?php echo $credit_result->address1; ?>"  type="text" placeholder="Address (optional)" class="contact_block" id="address1" name="address1" maxlength="200"/>

                                    </div>



                                    <div class="col-sm-6">

                                        <input  value="<?php echo $credit_result->zipcode; ?>"  maxlength="6" type="text" placeholder="Zip Code*" class="only_num contact_block" id="zipcode" name="zipcode"/>

                                        <div><span id="errorzipcode" style="color:red;"></span></div>

                                    </div>



                                    <div class="col-sm-6">

                                        <select class="contact_block" id="state" name="state">

                                            <option value="0">Select State</option>



                                            <?php

                                                $country_id = $credit_result->country;

                                                if ($country_id):

                                                    $state_id = $credit_result->state;

                                                    $result_states = $wpdb->get_results("SELECT * from states where country_id=" . $country_id . "");



                                                    foreach ($result_states as $value) {

                                                        if ($value->name != "") : ?>

                                                            <option <?php echo ($state_id == $value->id) ? "selected" : ''; ?> value="<?php echo $value->id; ?>"> <?php echo $value->name; ?> </option>

                                                            <?php

                                                        endif;

                                                    }

                                                endif;

                                            ?>



                                        </select>

                                    </div>



                                </div>

                                <div class="buttons">

                                    <input type="submit" value="Make Payment" class="btn_sky" id="btnAddCart"  />

                                    <input type="reset" value="Cancel" id="close_paypal"  data-dismiss="modal" class="btn_sky" />

                                </div>

                                <div class="card_msg"></div>

                            </form>

                        </div>

                    </div>

                </div>

            </div>



            <!-- Plan Details Modal -->

            <div id="myModal1" class="pop_overlay open" style="display: none;">

                <div class="pop_main" style="height:550px;bottom:30%; width:430px;">

                    <div class="pop_head" style="min-height:0;">

                        <a href="javascript:void(0);" data-dismiss="modal" type="button" class="ppmodal" ><i class="fa fa-remove"  aria-hidden="true"></i></a>

                    </div>

                    <div class="pop_body" style="margin-left:-30px">

                        <iframe id="frame_plan1" src="<?php echo get_site_url(); ?>/membership-account/membership-checkout/?level=1&pay_type=card" style="width:430px; height:100%; border:none;"></iframe>

                    </div>

                </div>

            </div>

            <!-- /Plan Details #myModal -->

        </div>

    </div>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>

    <script type="text/javascript" src="https://js.stripe.com/v2/"></script>

    <script type="text/javascript">

		Stripe.setPublishableKey('<?php echo pmpro_getOption("stripe_publishablekey"); ?>');



		$(function () {

			var $form = $('#payment-form');



			$form.submit(function (event) {

				if ($("#cardno").val() == "") {

					$("#errorcardno").html("Please enter card number");

					$("#cardno").focus();

					return false;

				} else {

					$("#errorcardno").html("");

					if ($("#cardno").val().length < 16) {

						$("#errorcardno").html("Please enter valid card number");

						$("#cardno").focus();

						return false;

					}

				}

				if ($("#expdate").val() == "Expiration Month") {

					$("#errorexpdate").html("Please select expiry month");

					$("#expdate").focus();

					return false;

				} else {

					$("#errorexpdate").html("");

				}

				if ($("#expyear").val() == "Year") {

					$("#errorexpyear").html("Please select expiry year");

					$("#expyear").focus();

					return false;

				} else {

					$("#errorexpyear").html("");

				}

				if ($("#securitycode").val() == "") {

					$("#errorsecuritycode").html("Please enter cvv no");

					$("#securitycode").focus();

					return false;

				} else {

					$("#errorsecuritycode").html("");

				}

				if ($("#zipcode").val() == "") {

					$("#errorzipcode").html("Please enter zipcode");

					$("#zipcode").focus();

					return false;

				} else {

					$("#errorzipcode").html("");

				}



				// Disable the submit button to prevent repeated clicks:

				$form.find('#btnAddCart').prop('disabled', true);



				// Request a token from Stripe:

				Stripe.card.createToken($form, stripeResponseHandler);



				// Prevent the form from being submitted:

				return false;

			});

		});



		function stripeResponseHandler(status, response) {

			// Grab the form:

			var $form = $('#payment-form');



			if (response.error) { // Problem!



				// Show the errors on the form:

				$form.find('.payment-errors').text(response.error.message);

				$form.find('#btnAddCart').prop('disabled', false); // Re-enable submission



			} else { // Token was created!



				// Get the token ID:

				var token = response.id;



				// Price

				if($('#hid_mamu_level_id').val()== "custom") {

					var price = $('#total_words_price').val();

					var totalwords = $("#hdntotalwords").val().trim();

					var txt_extra_words = parseInt(totalwords);

				} else {

					var price = $('#hid_mamu_price').val();

					var txt_extra_words = $('#hid_mamu_words').val();

				}

				if($('#card_price').length){

					//$('#card_price').val(price);

				} else {

					$form.append($('<input type="hidden" name="card_price" id="card_price">').val(price));

				}

				

				$form.append($('<input type="hidden" name="txt_extra_words">').val(txt_extra_words));

				$form.append($('<input type="hidden" name="level">').val($('#hid_mamu_level_id').val()));



				// Insert the token ID into the form so it gets submitted to the server:

				$form.append($('<input type="hidden" name="stripeToken">').val(token));



				// Submit the form:

				$form.get(0).submit();

			}

		}

    </script>

    <script>

        $('#hdntotalwords').change(function () {

            $('#id_SaveProofRead').prop('disabled', true);

        });

        $('#save_words').click(function () {

            $('.pay_msg').remove();

            if ($("#hdntotalwords").val() == "") {

                $('#hdntotalwords').after('<span class="pay_msg text-danger">Enter No. of words</span>');

                return false;

            } else if ($("#hdntotalwords").val() <= 0) {

                $('#hdntotalwords').after('<span class="pay_msg text-danger">Enter word greater than 0.</span>');

                return false;

            } else if (isNaN($("#hdntotalwords").val())) {

                $('#hdntotalwords').after('<span class="pay_msg text-danger">Enter only numbers.</span>');

                return false;

            } else {

                var totalwords = $("#hdntotalwords").val().trim();

                total_words = parseInt(totalwords);

                var per_words = $('#hid_price_per_words').val();

                var lbl_priceperwords = parseInt(total_words) * parseFloat(per_words);

                $('#dis_price').html('$' + lbl_priceperwords.toFixed(2));

                $('#save_words').text('Recalculate');

                $('#id_SaveProofRead').prop('disabled', false);

				$('#id_SaveProofRead').data("price", lbl_priceperwords.toFixed(2));

				$('#id_SaveProofRead').attr("data-price",lbl_priceperwords.toFixed(2));

						

				ga('require', 'ec');		  

				ga('ec:addImpression', {

				  'id': '0',

				  'name': $('#id_SaveProofRead').data('name'),

				  'price':lbl_priceperwords.toFixed(2),

				  'variant':total_words

				});  

				ga('send', 'pageview'); 

            }

        });



		$("#id_SaveProofRead").click(function ()

        {

            $('.pay_msg').remove();

            if ($('#current_user_ID').val() == '') {

                $('#hid_mamu_level_id').val('custom');

                open_loginpop();

                return false;

            } else {



                var totalwords = $("#hdntotalwords").val().trim();

                total_words = parseInt(totalwords);



                var per_words = $('#hid_price_per_words').val();



                //alert(per_words)

                var lbl_priceperwords = parseInt(total_words) * parseFloat(per_words);

                $('#total_words_price').val(lbl_priceperwords.toFixed(2));



                $('#paypal_extra_words').val(total_words);

                $('#paypal_pay_amount').val(lbl_priceperwords);



                var level_id = $(this).attr('data-level_id');

                $('#hid_mamu_level_id').val(level_id);

                $('#lbl_paypal_submit_link').attr("href", '#');

                $('#lbl_paypal_submit_link').attr("data-toggle", 'modal');

                $('#lbl_paypal_submit_link').attr("data-target", '#paypal_modal');

                $('#lbl_paypal_submit_link').text('Set up');

                $('#add_paypal').prop('type', 'submit');

                $('#add_paypal').val('Make Payment');



                window.dataLayer = window.dataLayer || [];

                window.dataLayer.push({

                    'event': 'Virtual Doc Submit Payment View',

                    'virtualPageURL': '/extra/pay',

                    'virtualPageTitle': 'Modal pay for document extra words',

                    'price_of_doc': lbl_priceperwords.toFixed(2)

                });

                $('#payment_option_modal .paypal_billing').show();

                $('#payment_option_modal').fadeIn();

                $('#payment_option_modal').addClass('open');

                $('#payment_option_modal').css('display', 'block');



                return false;

            }



        });

		

		//One Time Purchase		

		$(document).on("click", ".one_time .applyDiscount", function () {

			$(".one_time .applyDiscount").hide();

			$(".one_time .discountCode").show();

			$(".one_time .btnSaveDiscount").show();

			$(".one_time .btnCancelDiscount").show();

		});

		

		$(document).on("click", ".one_time .btnSaveDiscount", function () {

			if ($(".one_time .discountCode").val().trim() == "")

			{

				$('.one_time .discounts').append('<span class="text-danger" style="width: 100%; display: inline-block; margin-bottom: 0px;">Discount Code is required...</span>');

				$(".one_time .discounts span.text-danger").fadeOut(5000);

				return false;

			}

			

			$('#loding').show();

			

			$.ajax({

				url: '<?php echo admin_url('admin-ajax.php'); ?>',

				type: 'post',

				data: {

					'action': 'saveDiscount',

					'code': $(".one_time .discountCode").val()

				},

				dataType: 'text',

				success: function (data) {

					if(data == 1){

						$('.one_time .discounts').append('<span class="text-success" style="width: 100%; display: inline-block; margin-bottom: 0px;">Discount code successfully saved.</span>');

						$(".one_time .discounts span.text-success").fadeOut(5000);

						$(".one_time .discountCode").hide();

						$(".one_time .btnSaveDiscount").hide();

						$(".one_time .btnCancelDiscount").hide();

						$(".one_time .applyDiscount").show();

						

						var totalwords = $("#hdntotalwords").val().trim();

						total_words = parseInt(totalwords);



						var per_words = $('#hid_price_per_words').val();

						$.ajax({

							url: '<?php echo admin_url('admin-ajax.php'); ?>',

							type: 'post',

							data: {

								'action': 'checkDiscount',

								'words': total_words,

								'type': 'onetime'

							},

							dataType: 'text',

							success: function (data) {

								if(data == 0){

									//Discount Applied and there are no words to pay for

									$('#dis_price').html('$0.00');

									window.location.href = "<?php echo get_the_permalink(762); ?>" + "/?token=xs00u8c9cxd&level=success&discount=yes";

								} else {

									if(data != total_words){

										//Paypal

										var discount_words = parseInt(total_words)-parseInt(data)

										var disc_price = parseFloat(parseInt(discount_words) * parseFloat(per_words)).toFixed(2);

										var new_price = parseFloat(parseInt(data) * parseFloat(per_words)).toFixed(2);



										$('#paypal_discount_amount').val(disc_price);

										$('#dis_price').html('$' + new_price);

										

										//Stripe

										var $form = $('#payment-form');

										if($("#stripeDiscount").length){

											$('#stripeDiscount').val(disc_price);

										} else {

											$form.append($('<input type="hidden" name="stripeDiscount">').val(disc_price));

										}

										

										if($('#card_price').length){

											$('#card_price').val(new_price);

										} else {

											$form.append($('<input type="hidden" name="card_price" id="card_price">').val(new_price));

										}

									} else {

										var lbl_priceperwords = parseInt(total_words) * parseFloat(per_words);

										$('#dis_price').html('$' + lbl_priceperwords.toFixed(2));

										//reset any previous holding inputs

										$('#paypal_discount_amount').val('0');

										if($("#stripeDiscount").length){

											$('#stripeDiscount').remove();

										} 

										

										if($('#card_price').length){

											$('#card_price').remove();

										}

									}



									$('#loding').hide();

									return false;

								}

							},

							error: function (jqXHR, textStatus, errorThrown) {

								$('#loding').hide();

								return false;

							}

						});

					} else {

						if(data == 2){

							$('.one_time .discounts').append('<span class="text-danger" style="width: 100%; display: inline-block; margin-bottom: 0px;">You have alreaady appplied this discount code.</span>');

							$(".one_time .discounts span.text-danger").fadeOut(5000);

						} else if(data == 3){

							$('.one_time .discounts').append('<span class="text-danger" style="width: 100%; display: inline-block; margin-bottom: 0px;">You have already applied another discount code.</span>');

							$(".one_time .discounts span.text-danger").fadeOut(5000);

						} else {

							$('.one_time .discounts').append('<span class="text-danger" style="width: 100%; display: inline-block; margin-bottom: 0px;">Discount code is invalid. Try again.</span>');

							$(".one_time .discounts span.text-danger").fadeOut(5000);

						}

					}

					$('#loding').hide();

				},

				error: function (jqXHR, textStatus, errorThrown) {

					$('.one_time .discounts').append('<span class="text-danger" style="width: 100%; display: inline-block; margin-bottom: 0px;">An error has occurred.</span>');

					$(".one_time .discounts span.text-danger").fadeOut(5000);

					$('#loding').hide();

				}

			});

		});

		

		$(document).on("click", ".one_time .btnCancelDiscount", function () {

			$(".one_time .discountCode").hide();

			$(".one_time .btnSaveDiscount").hide();

			$(".one_time .btnCancelDiscount").hide();

			$(".one_time .applyDiscount").show();

		});

		

		//Subscription

		$(document).on("click", ".monthly_plan .applyDiscount", function () {

			$(this).closest('.plan_box').find(".applyDiscount").hide();

			$(this).closest('.plan_box').find(".discountCode").show();

			$(this).closest('.plan_box').find(".btnSaveDiscount").show();

			$(this).closest('.plan_box').find(".btnCancelDiscount").show();

		});

		

		$(document).on("click", ".monthly_plan .btnCancelDiscount", function () {

			$(this).closest('.plan_box').find(".discountCode").hide();

			$(this).closest('.plan_box').find(".btnSaveDiscount").hide();

			$(this).closest('.plan_box').find(".btnCancelDiscount").hide();

			$(this).closest('.plan_box').find(".applyDiscount").show();

		});

		

		$(document).on("click", ".monthly_plan .btnSaveDiscount", function () {

			var that = $(this).closest('.plan_box');

			if (that.find(".discountCode").val().trim() == "")

			{

				that.find('.discounts').append('<span class="text-danger" style="width: 100%; display: inline-block; margin-bottom: 0px;">Discount Code is required...</span>');

				that.find(".discounts span.text-danger").fadeOut(5000);

				return false;

			}

			

			$('#loding').show();

			

			$.ajax({

				url: '<?php echo admin_url('admin-ajax.php'); ?>',

				type: 'post',

				data: {

					'action': 'saveDiscount',

					'code': that.find(".discountCode").val()

				},

				dataType: 'text',

				success: function (data) {

					if(data == 1){

						that.find('.discounts').append('<span class="text-success" style="width: 100%; display: inline-block; margin-bottom: 0px;">Discount code successfully saved.</span>');

						that.find(".discounts span.text-success").fadeOut(5000);

						that.find(".discountCode").hide();

						that.find(".btnSaveDiscount").hide();

						that.find(".btnCancelDiscount").hide();

						that.find(".applyDiscount").show();

						

						var level_id = that.find('.class_bill_method').attr('id');

                        var price = parseInt(that.find('.class_bill_method').attr('data-price'));

						var url = '<?php echo home_url(); ?>/paypal-process/?level=' + level_id;

						var words = that.find('.class_bill_method').attr('data-words');

						var per_words = that.find('.class_bill_method').attr('data-per-words');

						

						$.ajax({

							url: '<?php echo admin_url('admin-ajax.php'); ?>',

							type: 'post',

							data: {

								'action': 'checkDiscount',

								'words': words,

								'type': 'subscription',

								'level': level_id

							},

							dataType: 'text',

							success: function (data) {

								if(data == 0){

									//Discount Applied and there are no words to pay for

									//window.location.href = "<?php echo get_the_permalink(762); ?>" + 					"/?token=xs00u8c9cxd&level=success&discount=yes";

									//Paypal

									var disc_price = parseFloat(price).toFixed(2);

									var new_price = parseFloat(parseInt(data) * parseFloat(per_words)).toFixed(2);



									$('#paypal_discount_amount').val(disc_price);

									

									//Stripe

									var $form = $('#payment-form');

									if($("#stripeDiscount").length){

										$('#stripeDiscount').val(disc_price);

									} else {

										$form.append($('<input type="hidden" name="stripeDiscount">').val(disc_price));

									}

									

									if($('#card_price').length){

										$('#card_price').val(new_price);

									} else {

										$form.append($('<input type="hidden" name="card_price" id="card_price">').val(new_price));

									}

								} else {

									if(data != words){

										//Paypal

										var discount_words = parseInt(words)-parseInt(data);

										var disc_price = parseFloat(parseInt(discount_words) * parseFloat(per_words)).toFixed(2);

										var new_price = parseFloat(parseFloat(price) - parseFloat(disc_price)).toFixed(2);



										$('#paypal_discount_amount').val(disc_price);

										

										//Stripe

										var $form = $('#payment-form');

										if($("#stripeDiscount").length){

											$('#stripeDiscount').val(disc_price);

										} else {

											$form.append($('<input type="hidden" name="stripeDiscount">').val(disc_price));

										}

										

										if($('#card_price').length){

											$('#card_price').val(new_price);

										} else {

											$form.append($('<input type="hidden" name="card_price" id="card_price">').val(new_price));

										}										

									} else {

										//reset any previous holding inputs

										$('#paypal_discount_amount').val('0');

										if($("#stripeDiscount").length){

											$('#stripeDiscount').remove();

										} 

										

										if($('#card_price').length){

											$('#card_price').remove();

										}

									}

								}

								$('#loding').hide();

							},

							error: function (data) {

								that.find('.discounts').append('<span class="text-danger" style="width: 100%; display: inline-block; margin-bottom: 0px;">An error has occurred.</span>');

								that.find(".discounts span.text-danger").fadeOut(5000);

								$('#loding').hide();

							}

						});

					} else {

						if(data == 2){

							that.find('.discounts').append('<span class="text-danger" style="width: 100%; display: inline-block; margin-bottom: 0px;">You have alreaady appplied this discount code.</span>');

							that.find(".discounts span.text-danger").fadeOut(5000);

						} else if(data == 3){

							that.find('.discounts').append('<span class="text-danger" style="width: 100%; display: inline-block; margin-bottom: 0px;">You have already applied another discount code.</span>');

							that.find(".discounts span.text-danger").fadeOut(5000);

						} else {

							that.find('.discounts').append('<span class="text-danger" style="width: 100%; display: inline-block; margin-bottom: 0px;">Discount code is invalid. Try again.</span>');

							that.find(".discounts span.text-danger").fadeOut(5000);

						}

					}

					$('#loding').hide();

				},

				error: function (jqXHR, textStatus, errorThrown) {

					that.find('.discounts').append('<span class="text-danger" style="width: 100%; display: inline-block; margin-bottom: 0px;">An error has occurred.</span>');

					that.find(".discounts span.text-danger").fadeOut(5000);

					$('#loding').hide();

				}

			});

		});

    </script>