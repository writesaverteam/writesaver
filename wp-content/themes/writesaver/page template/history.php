<?php
/*
 * Template Name: Proofreader History
 */

$current_user = wp_get_current_user();
$user_roles_array = $current_user->roles;
$user_role = array_shift($user_roles_array);
if (!is_user_logged_in() || $user_role != "proofreader") {
    echo '<script>window.location.href="' . get_site_url() . '"</script>';
    exit;
}
get_header();
global $wpdb;
$user_id = get_current_user_id();
?>
<section>
    <div class="breadcum">
        <div class="container">
            <div class="page_title">
                <h1><?php the_title(); ?></h1>
            </div>
        </div>
    </div>
</section>        
<section>
    <div class="container">
        <div class="all_page_proofer">
            <div class="proofer_section_title">
                <h4>Edited documents</h4>
            </div>
            <div class="history_table_main">
                <div class="history_table">
                    <div class="history_row">
                        <div class="history_col">Date</div>
                        <div class="history_col">Doc.  </div>
                        <!--<div class="history_col">Customer  </div>-->
                        <div class="history_col">Total words  </div>
                        <div class="history_col">Edited words   </div>
                        <div class="history_col">Earned </div>
                    </div>
                    <?php
                    $paged = ( get_query_var('paged') ) ? get_query_var('paged') : 1;
                    $query = "SELECT COUNT(fk_proofreader_id) FROM `tbl_proofreader_history` WHERE fk_proofreader_id= $user_id ";
                    $total = $wpdb->get_var($query);
                    $limit = 10;
                    $offset = ( $paged - 1 ) * $limit;
                    $num_of_pages = ceil($total / $limit);
                    $history_list = $wpdb->get_results("SELECT * FROM `tbl_proofreader_history` WHERE fk_proofreader_id= $user_id ORDER BY date DESC LIMIT $offset, $limit");
                    if (count($history_list) > 0) {
                        foreach ($history_list as $history) {
                            ?>
                            <div class="history_row">
                                <div class="history_col"><?php echo date('m/d/Y h:i:s A', strtotime($history->date)); ?></div>
                                <div class="history_col"><?php echo $history->doc_name; ?></div>
                                <!--<div class="history_col"><?php echo $history->customer; ?></div>-->
                                <div class="history_col"><?php echo $history->total_words; ?></div>
                                <div class="history_col"><?php echo $history->edited_words; ?></div>
                                <div class="history_col"><?php echo "$" . $history->earned; ?></div>
                            </div>
                            <?php
                        }
                    }
                    ?>
                </div>
                <?php
                if (count($history_list) <= 0) {
                    _e('<div class="history_row"><p>You have no history yet..</p></div>');
                }
                ?>       
            </div>
            <?php
            if (count($history_list) > 0) {
                ?>
                <div class="pagination_contain">
                    <p>Showing <?php
                        $entry = $limit + $offset;
                        if ($offset == 0) {
                            echo '1';
                        } else {
                            echo $offset + 1;
                        }
                        ?> to <?php
                        if ($entry > $total) {
                            echo $total;
                        } else {
                            echo $entry;
                        }
                        ?> of <?php echo $total; ?> entries</p>
                    <div class="pagination_main">
                        <?php
                        if (function_exists(custom_pagination)) {
                            custom_pagination($num_of_pages, "", $paged);
                        }
                        ?>
                    </div>
                </div>
            <?php }
            ?>
        </div>
    </div>
</section>
<?php
get_footer();
