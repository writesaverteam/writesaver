<?php
/*
Template Name: Thank You For Payment
*/

if (!is_user_logged_in()) {
    ob_start();
    ob_get_clean();
    wp_redirect(get_site_url());
    exit;
}
get_header();

if($_REQUEST['tx']!=""){

	global $wpdb;

	//$id = $_REQUEST['custom'];
	$id = $_REQUEST['cm'];

	$wpdb->update("wp_price_per_extra_words", array('stripe_reference' => $_REQUEST['tx']), array('id' => $id));

}
//print_r($wpdb);
?>
<section class="login">
    <div class="breadcum">
        <div class="container">
            <div class="page_title">
                <?php the_title('<h1>', '</h1>'); ?>
            </div>
            <div class="confirmation_thank_you registration_thank_you">
                <div class="row">
                    <div class="col-md-offset-3 col-sm-6">
                        <div class="thank-you">
                            <div class="thank_msg">
                                <div class="thank_img">
                                    <img src="<?php echo get_template_directory_uri() ?>/images/check_thanku.png" alt="images">
                                </div>
                                <h2>Thank You For Payment</h2>
                                <p>Your request has been received.
                                    We've also sent you a confirmation e-mail of your package.
                                    Didn't receive the e-mail? Check your spam folder just in case.</p>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<script>
console.log(localStorage);

if (localStorage['id_sku']!==undefined) {
	

ga('require', 'ec');

ga('ec:addProduct', {
  'id': localStorage['id_sku'],
  'name': localStorage['name'],
 // 'category': localStorage['name'],
  'price': localStorage['price'],
 // 'quantity': '1'
});


ga('ec:setAction', 'purchase', {
  'id': localStorage['id_sku'],
  'name': localStorage['name'],
 // 'category': localStorage['name'],
  'revenue': localStorage['price'],
  
});

ga('send', 'pageview');  



delete localStorage['id_sku'];
delete localStorage['name'];
delete localStorage['sku'];
delete localStorage['price'];


}
</script>
<?php
get_footer();

