<?php

/*

 * Template Name: Customer Dashboard

 */



$current_user = wp_get_current_user();



$user_roles_array = $current_user->roles;

$user_role = array_shift($user_roles_array);

if (!is_user_logged_in() || $user_role != "customer") {

    echo '<script>window.location.href="' . get_site_url() . '"</script>';

    exit;

}

/* echo get_theme_root_uri().'/'.get_template().'/notify-paypal.php';

  echo '<br>';

  echo get_template();

  echo '<br>';

  echo template_directory_uri;

  echo '<br>';

  exit; */

get_header();

?>



<?php

//prevent duplicate submission

if(isset($_SESSION['submitted'])){

    unset($_SESSION['submitted']);

}

global $wpdb;


$user_id = get_current_user_id();



$datetime = date('Y-m-d H:i:s');



$usersDtl = array(226, "proofreader");

array_push($usersDtl, 225, "customer");



$current_user->membership_level = pmpro_getMembershipLevelForUser($user_id);

?>

<script>

    $(document).ready(function () {

        if (localStorage.getItem('proof_doc'))

            $('.changeable > textarea').val(localStorage.getItem('proof_doc'));

    });



</script>

<?php

if (!empty($current_user->membership_level)) {

    $user_is_subscribed = 'yes';



    $plan_id = $current_user->membership_level->id ? $current_user->membership_level->id : 0;



    $per_words_price = $wpdb->get_results("SELECT price_per_additional_word FROM wp_pmpro_membership_levels WHERE id = " . $plan_id);

    $price_per_words = $per_words_price[0]->price_per_additional_word;

    if ($price_per_words == "") {

        $price_per_words = 0;

    }

} else {

    $user_is_subscribed = 'no';



    $per_words_price = $wpdb->get_results("SELECT * FROM wp_price_per_words WHERE id = 1");

    $price_per_words = $per_words_price[0]->price_per_words;

}

//echo $user_is_subscribed;exit;

?>





<input type="hidden" id="user_is_subscribed" value="<?php echo $user_is_subscribed; ?>" />

<input type="hidden" id="hid_price_per_words" value="<?php echo $price_per_words; ?>" />

<?php

$wp_one_time_purchase = $wpdb->get_row("SELECT * FROM wp_one_time_purchase WHERE id = 1");

//Error Modal for One - Time Purchase

if (isset($_REQUEST['token']) && $_REQUEST['token'] == "xd0eu8c9cxd" && isset($_SESSION['sess_id'])) {

    $sess_id = $_SESSION['sess_id'];

    $wpdb->delete('wp_customer_document_main_temp', array('sess_id' => $sess_id));

    $wpdb->delete('wp_customer_document_details_temp', array('sess_id' => $sess_id));

    $wpdb->delete('tbl_discount_codes_uses_temp', array('sess_id' => $sess_id));

    unset($_SESSION['sess_id']);

    ?>

    <div id="error_modal" class="pop_overlay open" >

        <div class="pop_main" style="height:355px;">

            <div class="pop_head">

                <a href="javascript:void(0);" data-dismiss="modal" type="button" ><i class="fa fa-remove"  aria-hidden="true"></i></a>

            </div>

            <div class="pop_body">

                <div class="confirmation_thank_you registration_thank_you">

                    <div class="row">

                        <div class="col-md-offset-2 col-sm-8">

                            <div class="">

                                <div class="thank_msg" style="border-top:solid 5px #F00;">

                                    <div class="thank_img">

                                        <img src="<?php echo get_template_directory_uri() ?>/images/warning.png" alt="images">

                                    </div>

                                    <h3 style="text-align:center;">Payment Error</h3>

                                    <p style="text-align:center;">Unfortunately we are unable to process your payment at this time. If the price of your document was less that $0.50, please contact us to have your document proofread.</p>

                                </div>

                            </div>

                        </div>



                    </div>

                </div>

            </div>

        </div>

    </div>

    <?php

}

//Thankyou Modal for One - Time Purchase

if (isset($_REQUEST['token']) && $_REQUEST['token'] == "xs00u8c9cxd" && isset($_SESSION['sess_id'])) {

    if (isset($_REQUEST['discount']) && $_REQUEST['discount'] == 'yes' && !isset($_REQUEST['ins_id'])) {

        $sess_id = $_SESSION['sess_id'];

        if(isset($sess_id)){

            $user_info = $wpdb->get_results("SELECT * FROM tbl_customer_general_info WHERE fk_customer_id = $user_id LIMIT 1");

            $disTemp = $wpdb->get_row("SELECT * FROM tbl_discount_codes_uses_temp WHERE sess_id = '" . $sess_id . "'");

            if (!empty($user_info)) {

                $txt_extra_words = $disTemp->words;

                $remaining_credit_words = $user_info[0]->remaining_credit_words;

                if ($remaining_credit_words < 0)

                    $remaining_credit_words = 0;

                $total_worls = $remaining_credit_words + $txt_extra_words;

                $info_id = $user_info[0]->pk_customer_general_id;

                $wpdb->update(

                        'tbl_customer_general_info', array('remaining_credit_words' => $total_worls), array('pk_customer_general_id' => $info_id), array('%d'), array('%d')

                );

            } else {

                $total_worls = $pay_info->words;

            }

            

            $per_words_price = $wpdb->get_results("SELECT * FROM wp_price_per_words WHERE id = 1");

            $price_per_words = $per_words_price[0]->price_per_words;

            $discountamount = (int)$disTemp->words*(float)$price_per_words;

                

            $wpdb->insert(

                'tbl_discount_codes_uses', 

                array(

                    'code_id' => $disTemp->code_id,

                    'user_id' => $disTemp->user_id,

                    'words' => $disTemp->words,

                    'order_no' => 1

                ),

                array(

                    '%s',

                    '%d',

                    '%d',

                    '%d'

                ));

            

            $subject = 'You have new words on Writesaver.';

            $descs = 'You have new words in your Writesaver account! Our proofreaders are standing by and are ready to help you write with perfect, native English on your emails, papers, documents, proposals, and any other writing you can think of. If you have any questions about your account, feel free to shoot us an email at contact@writesaver.co, we\'ll get back to you as soon as we can, and we\'re always happy to help.





            To your success in writing,

            The Writesaver Team';

                        $noti = 'You have new words available.';

                        $descs .= '

            <p>Below is some more information about your order:</p>

            <p>Membership Level: ' . $wp_one_time_purchase->name . '</p>

            <p>Membership Fee: $' . $discountamount . '</p>

            <p>Discount Amount: $' . $discountamount . '</p>

            <p>Words added: ' . $disTemp->words . '</p>

            <p>Total Words in your Account: ' . $total_worls . '</p>';



            send_cust_notification($user_id, '', $descs, 1, 1, $subject, $noti);

        }

    }

    

    if (isset($_REQUEST['ins_id'])) {

        $ins_id = $_REQUEST['ins_id'];

        $user_info = $wpdb->get_results("SELECT * FROM tbl_customer_general_info WHERE fk_customer_id = $user_id LIMIT 1");

        $pay_info = $wpdb->get_row("SELECT * FROM wp_price_per_extra_words WHERE id = $ins_id");

        if ($pay_info->status == 0) {

            if (!empty($user_info)) {

                $txt_extra_words = $pay_info->words;

                $remaining_credit_words = $user_info[0]->remaining_credit_words;

                if ($remaining_credit_words < 0)

                    $remaining_credit_words = 0;

                $total_worls = $remaining_credit_words + $txt_extra_words;

                $info_id = $user_info[0]->pk_customer_general_id;

                $wpdb->update(

                        'tbl_customer_general_info', array('remaining_credit_words' => $total_worls), array('pk_customer_general_id' => $info_id), array('%d'), array('%d')

                );

            } else {

                $total_worls = $pay_info->words;

            }

            

            $wpdb->update("wp_price_per_extra_words", array('status' => 1), array('id' => $ins_id));

            $subject = 'You have new words on Writesaver.';

            $descs = 'You have new words in your Writesaver account! Our proofreaders are standing by and are ready to help you write with perfect, native English on your emails, papers, documents, proposals, and any other writing you can think of. If you have any questions about your account, feel free to shoot us an email at contact@writesaver.co, we\'ll get back to you as soon as we can, and we\'re always happy to help.





            To your success in writing,

            The Writesaver Team';

                        $noti = 'You have new words available.';

                        $descs .= '

            <p>Below is some more information about your order:</p>

            <p>Membership Level: ' . $wp_one_time_purchase->name . '</p>

            <p>Membership Fee: $' . $pay_info->price . '</p>

            <p>Words added: ' . $pay_info->words . '</p>

            <p>Total Words in your Account: ' . $total_worls . '</p>';



            send_cust_notification($user_id, '', $descs, 1, 1, $subject, $noti);

        }

    }

    if(isset($_SESSION['sess_id'])){

        $sess_id = $_SESSION['sess_id'];

        $wpdb->delete('tbl_discount_codes_uses_temp', array('sess_id' => $sess_id));

        unset($_SESSION['sess_id']);

    }

    ?>

    <div id="thank_modal" class="pop_overlay open" >

        <div class="pop_main" style="height:380px;">

            <div class="pop_head">

                <a href="javascript:void(0);" data-dismiss="modal" type="button" ><i class="fa fa-remove"  aria-hidden="true"></i></a>

            </div>

            <div class="pop_body">

                <div class="confirmation_thank_you registration_thank_you">

                    <div class="row">

                        <div class="col-md-offset-2 col-sm-8">

                            <div class="">

                                <div class="thank_msg">

                                    <div class="thank_img">

                                        <img src="<?php echo get_template_directory_uri() ?>/images/check_thanku.png" alt="images">

                                    </div>

                                    <?php if (isset($_REQUEST['discount']) && $_REQUEST['discount'] == 'yes') { ?>

                                        <h3 style="text-align:center;">Thanks for your purchase!</h3>

                                        <p>Discount was successfully applied to your purchase and your new words are now added to your account. Our editors are ready and waiting to help you write with perfect English!</p>

                                    <?php } else { ?>

                                        <h3 style="text-align:center;">Thanks for your purchase!</h3>

                                        <p>Your new words are now added to your account, and our editors are ready and waiting to help you write with perfect English!</p>

                                    <?php } ?>

                                </div>

                            </div>

                        </div>

                    </div>

                </div>

            </div>

        </div>

    </div>

    <?php

}

//Thankyou Modal for Subscription Purchase

if (isset($_REQUEST['token']) && $_REQUEST['token'] == "xs00u8c9frd" && isset($_SESSION['sess_id'])) {

    if (isset($_REQUEST['discount']) && $_REQUEST['discount'] == 'yes') {

        $sess_id = $_SESSION['sess_id'];

        if(isset($sess_id)){

            $user_info = $wpdb->get_results("SELECT * FROM tbl_customer_general_info WHERE fk_customer_id = $user_id LIMIT 1");

            $disTemp = $wpdb->get_row("SELECT * FROM tbl_discount_codes_uses_temp WHERE sess_id = '" . $sess_id . "'");

            

            $wpdb->insert(

                'tbl_discount_codes_uses', 

                array(

                    'code_id' => $disTemp->code_id,

                    'user_id' => $disTemp->user_id,

                    'words' => $disTemp->words,

                    'order_no' => 1

                ),

                array(

                    '%s',

                    '%d',

                    '%d',

                    '%d'

                ));

        }

    }

    if(isset($_SESSION['sess_id'])){

        $sess_id = $_SESSION['sess_id'];

        $wpdb->delete('tbl_discount_codes_uses_temp', array('sess_id' => $sess_id));

        unset($_SESSION['sess_id']);

    }

    ?>

    <div id="thank_modal" class="pop_overlay open" >

        <div class="pop_main" style="height:380px;">

            <div class="pop_head">

                <a href="javascript:void(0);" data-dismiss="modal" type="button" ><i class="fa fa-remove"  aria-hidden="true"></i></a>

            </div>

            <div class="pop_body">

                <div class="confirmation_thank_you registration_thank_you">

                    <div class="row">

                        <div class="col-md-offset-2 col-sm-8">

                            <div class="">

                                <div class="thank_msg">

                                    <div class="thank_img">

                                        <img src="<?php echo get_template_directory_uri() ?>/images/check_thanku.png" alt="images">

                                    </div>

                                    <?php if (isset($_REQUEST['discount']) && $_REQUEST['discount'] == 'yes') { ?>

                                        <h3 style="text-align:center;">Thanks for your purchase!</h3>

                                        <p>Discount was successfully applied to your purchase and your new words will be added as soon as payment is confirmed. Our editors are ready and waiting to help you write with perfect English!</p>

                                    <?php } else { ?>

                                        <h3 style="text-align:center;">Thanks for your purchase!</h3>

                                        <p>Your new words will be added to your account as soon as payment is confirmed, and our editors are ready and waiting to help you write with perfect English!</p>

                                    <?php } ?>

                                </div>

                            </div>

                        </div>

                    </div>

                </div>

            </div>

        </div>

    </div>

    <?php

}

?>

<?php

if (isset($_REQUEST['add_paypal']) || isset($_REQUEST['add_paypal1']) || isset($_REQUEST['paypal_id'])) {

    //print_r($_REQUEST);exit;

    if (isset($_REQUEST['paypal_id'])) {

        $paypal_id = $_REQUEST['paypal_id'];

        $paypal_pay_amount = round($_REQUEST['paypal_pay_amount'], 2);

        $paypal_extra_words = $_REQUEST['paypal_extra_words'];

    } else {

        $paypal_id = $_REQUEST['paypal_id1'];

        $paypal_pay_amount = round($_REQUEST['paypal_pay_amount1'], 2);

        $paypal_extra_words = $_REQUEST['paypal_extra_words1'];

    }



    $paypal_discount_amount = $_REQUEST['paypal_discount_amount'];

    

    $date = date('Y-m-d h:i:s');

    $descr = "Extra charges for " . $paypal_extra_words . " words";

    $wpdb->insert('wp_price_per_extra_words', array('fk_customer_id' => $user_id, 'payment_date' => $date, 'price' => $paypal_pay_amount, 'descriptions' => $descr, 'payment_source' => 'PayPal'));

    $ins_id = $wpdb->insert_id;



    $wpdb->update("tbl_customer_general_info", array('paypal_id' => $paypal_id), array('fk_customer_id' => $user_id));



    $business_email = get_option('pmpro_gateway_email');

    $gateway_environment = get_option('pmpro_gateway_environment');

    if ($gateway_environment == 'sandbox') {

        $URL = "https://www.sandbox.paypal.com/cgi-bin/webscr";

    } elseif ($gateway_environment == 'live') {

        $URL = "https://www.paypal.com/cgi-bin/webscr";

    }



    $currency_code = get_option('pmpro_currency');



    $notify = get_theme_root_uri() . '/' . get_template() . '/notify-paypal.php'; //get_site_url()."/notify-paypal.php";

    $success = get_site_url() . "/customer-dashboard/?token=xs00u8c9cxe&level=success&ins_id=$ins_id";

    $cancel = get_site_url() . "/customer-dashboard/?token=xd0eu8c9cxe&level=cancel";

    $i = 1;

    ?>

    <br />

    <br />

    <br />

    <br />

    <br />

    <br />

    <br />

    <div class="wrapper">

        <div style="text-align:center; margin-top:30px; font-size:24px;">Please wait...Loading PayPal...<br/><br/>Please don't refresh the page.</div>

        <div style="text-align:center; margin-top:20px;">

        </div>



        <form action="<?php echo $URL; ?>" method="post" name="ckout" id="ckout">

            <input type="hidden" name="cmd" value="_cart">

            <input type="hidden" name="upload" value="1">

            <input type="hidden" name="business" value="<?php echo $business_email; ?>">

            <input type="hidden" name="item_number_<?php echo $i; ?>" value="<?php echo $ins_id; ?>">

            <input type="hidden" name="item_name_<?php echo $i; ?>" value="<?php echo $descr; ?>">

            <input type="hidden" name="quantity_<?php echo $i; ?>" value="1">

            <input type="hidden" name="amount_<?php echo $i; ?>" value="<?php echo $paypal_pay_amount; ?>">

            <input type="hidden" name="shipping_1" value="0">

            <input type="hidden" name="discount_amount_cart" value="<?php echo $paypal_discount_amount; ?>">

            <input type="hidden" name="currency_code" value="<?php echo $currency_code; ?>">

            <input type="hidden" name="custom" value="<?php echo $ins_id; ?>">

            <input type="hidden" name="return" value="<?php echo $success; ?>">

            <input type="hidden" name="cancel_return" value="<?php echo $cancel; ?>">

        </form>

        <script type="text/javascript">

            document.ckout.submit();

        </script>

        <?php

    }



    include 'stripe/Stripe.php';



    Stripe::setApiKey(pmpro_getOption("stripe_secretkey"));



    if (isset($_POST['stripeToken'])) {

        $user_id = get_current_user_id();

        $cardno = encrypt_string($_REQUEST['cardno']);

        $expdate = $_REQUEST['expdate'];

        $expyear = $_REQUEST['expyear'];

        $securitycode = encrypt_string($_REQUEST['securitycode']);



        $country = $_REQUEST['country'];

        $address = $_REQUEST['address'];

        $address1 = $_REQUEST['address1'];

        $zipcode = $_REQUEST['zipcode'];

        $state = $_REQUEST['state'];

        

        $prefix = $wpdb->prefix;

        $table_name = $prefix . 'creditdebit_card_details';

        $user = $wpdb->get_results("SELECT * FROM $table_name WHERE customer_id = $user_id");

        $user_count = count($user);

        if ($user_count > 0) {

            $insert = $wpdb->update(

                    $table_name, array(

                'cardnumber' => $cardno,

                'expirymonth' => $expdate,

                'expyear' => $expyear,

                'securitycode' => $securitycode,

                'country' => $country,

                'address' => $address,

                'address1' => $address1,

                'zipcode' => $zipcode,

                'state' => $state

                    ), array('customer_id' => $user_id)

            );

        } else {

            $insert = $wpdb->insert(

                    $table_name, array(

                'customer_id' => $user_id,

                'cardnumber' => $cardno,

                'expirymonth' => $expdate,

                'expyear' => $expyear,

                'securitycode' => $securitycode,

                'country' => $country,

                'address' => $address,

                'address1' => $address1,

                'zipcode' => $zipcode,

                'state' => $state,

                'createddate' => date('Y-m-d')

                    )

            );

        }



        $amount_cents = str_replace(".", "", $_POST['card_price']);

        $price = $_POST['card_price'];

        $txt_extra_words = $_POST['txt_extra_words'];



        try {

            $date = date('Y-m-d h:i:s');

            $description = "Extra charges for " . $txt_extra_words . " words";

            $wpdb->insert('wp_price_per_extra_words', array('fk_customer_id' => $user_id, 'stripe_reference' => '', 'payment_date' => $date, 'price' => $price, 'descriptions' => $description, 'payment_source' => 'Stripe'));

            $ins_id = $wpdb->insert_id;

            

            

            $table_name = $wpdb->prefix . 'creditdebit_card_details';

            $user_card_details = $wpdb->get_results("SELECT * FROM $table_name WHERE customer_id = $user_id");



            $result_countries = $wpdb->get_results("SELECT * from countries");



            foreach ($result_countries as $value) {

                if($value->id == $user_card_details[0]->country) {

                    $country = $value->name;



                    $result_states = $wpdb->get_results("SELECT * from states where country_id=" . $value->id . "");



                    foreach ($result_states as $state_val) {

                        if( $state_val->id == $user_card_details[0]->state ) {

                            $state = $state_val->name;

                            break;

                        }

                    }



                    break;

                }

            }



            $metadata = array();



            if( $country ) {

                $metadata['country'] = $country;

            }

            if( $user_card_details[0]->address ) {

                $metadata['address'] = $user_card_details[0]->address;

            }

            if( $user_card_details[0]->address1 ) {

                $metadata['address1'] = $user_card_details[0]->address1;

            }

            if( $state ) {

                $metadata['state'] = $state;

            }

            if( $user_card_details[0]->zipcode ) {

                $metadata['zipcode'] = $user_card_details[0]->zipcode;

            }



            $userdata = get_userdata( $user_id );

            $exist_customer = Stripe_Customer::all(array("email" => $userdata->user_email));

            

            if (isset($_REQUEST['stripeDiscount'])) {

                $discount = Stripe_Coupon::create(array(

                    'duration' => "once",

                    "currency" => "usd",

                    'id' => "discount-coupon-" . $_POST['stripeToken'],

                    'amount_off' => str_replace(".", "", $_POST['stripeDiscount'])

                ));

            }



            if( !empty($exist_customer['data']) ) {

                $stripe_user_id = $exist_customer['data'][0]['id'];



                $cus_retrieve = Stripe_Customer::retrieve($stripe_user_id);

                $cus_retrieve->source = $_POST['stripeToken'];

                $cus_retrieve->metadata = $metadata;

                

                if (isset($_REQUEST['stripeDiscount'])) {

                    $cus_retrieve->coupon = "discount-coupon-" . $_POST['stripeToken'];

                }



                $cus_retrieve->save();



            } else {

                if (isset($_REQUEST['stripeDiscount'])) {                   

                    $customer = Stripe_Customer::create(array(

                        "email" => $userdata->user_email,

                        "source" => $_POST['stripeToken'],

                        "metadata" => $metadata,

                        "coupon" => "discount-coupon-" . $_POST['stripeToken']

                    ));

                } else {

                    $customer = Stripe_Customer::create(array(

                        "email" => $userdata->user_email,

                        "source" => $_POST['stripeToken'],

                        "metadata" => $metadata

                    ));

                }

                $stripe_user_id = $customer['id'];

            }

            

            if (isset($_REQUEST['stripeDiscount'])) {

                $charge_metadata = array(

                    "coupon_code" => "discount-coupon-" . $_POST['stripeToken'],

                    "coupon_discount" => str_replace(".", "", $_POST['stripeDiscount'])

                );

            } else {

                $charge_metadata = array();

            }

            

            $charge = Stripe_Charge::create(array(

                "amount" => $amount_cents,

                "currency" => "usd",

                "customer" => $stripe_user_id,

                "description" => $description,

                "metadata" => $charge_metadata

                )

            );



            $chargeArray = $charge->__toArray(true);

            $stripe_reference = $chargeArray['id'];



            $wpdb->update("wp_price_per_extra_words", array('stripe_reference' => $stripe_reference), array('id' => $ins_id));



            echo "<script type='text/javascript'>window.location=document.location.href = '?token=xs00u8c9cxe&level=success&ins_id=$ins_id';</script>";

        } catch (Stripe_CardError $e) {



            echo "<script type='text/javascript'>

            window.location=document.location.href = '?token=xd0eu8c9cxe&level=declined';

            </script>";



            //$error = $e->getMessage();

        } catch (Stripe_InvalidRequestError $e) {



            //print_r($e->getMessage());exit;

            echo "<script type='text/javascript'>

            window.location=document.location.href = '?token=xd0eu8c9cxe&level=declined';

            </script>";

        } catch (Stripe_AuthenticationError $e) {



            //print_r($e->getMessage());exit;

            echo "<script type='text/javascript'>

            window.location=document.location.href = '?token=xd0eu8c9cxe&level=declined';

            </script>";

        } catch (Stripe_ApiConnectionError $e) {



            //print_r($e->getMessage());exit;

            echo "<script type='text/javascript'>

            window.location=document.location.href = '?token=xd0eu8c9cxe&level=declined';

            </script>";

        } catch (Stripe_Error $e) {



            //print_r($e->getMessage());exit;

            echo "<script type='text/javascript'>

            window.location=document.location.href = '?token=xd0eu8c9cxe&level=declined';

            </script>";

        } catch (Exception $e) {

            

        }

    }

    ?>   



    <!--POPUP-->

    <div id="edit_free" class="pop_overlay open start_tests ready" style="display: none;">

        <div class="pop_main">

            <div class="pop_head">

                <a href="javascript:void(0);"><i class="fa fa-remove" aria-hidden="true"></i></a>

            </div>

            <div class="pop_body">

                <div class="load_overlay" id="pop_loding">

                    <img src="<?php echo get_template_directory_uri(); ?>/images/39.gif"/>

                </div>

                <div class="page_title">

                    <h2>Are you ready </h2>

                </div>

                <div class="pop_content">

                    <div class="first_test save_doc">

                        <p>Are you ready to have your document proofread? You will not be able to access your submission again until our proofread is complete.</p>                       

                        <button class="btn_sky btnyes">Yes</button>

                        <button class="btn_sky btnno orange pop_btn">No</button>                       

                    </div>

                    <div class="doc_msg"></div>

                </div>

            </div>

        </div>

    </div>





    <!--POPUP NEW-->

    <div id="popup_for_non_subs" class="pop_overlay open start_tests ready" style="display: none;">

        <div class="pop_main">

            <div class="pop_head">

                <a href="javascript:void(0);" class="pricemodal"><i class="fa fa-remove" aria-hidden="true"></i></a>

            </div>

            <div class="pop_body">

                <div class="load_overlay" id="pop_loding">

                    <img src="<?php echo get_template_directory_uri(); ?>/images/39.gif"/>

                </div>

                <div class="pop_content">

                    <div class="first_test save_doc" style="padding-top:0;">

                        <p>Your document is <span id="lbl_pending_words">0</span> words more than your currently available credits. To have this document proofread, click below. Or if you're a frequent writer, check out our subscription plans for our best available rates</p> 

                        <a href="javascript:void(0);" class="btn_sky btnno pop_btn" style="margin-top:15px;" id="id_payment_option"><span style="font-size:34px;">$&nbsp;</span><span id="lbl_priceperwords" style="font-size:34px;"></span><br />Have this paper proofread now</a>

                        <?php if ($user_is_subscribed == 'no') { ?>

                            Frequent Writer? <a href="<?php echo get_site_url(); ?>/plan/" style="margin-top:22px;">Check out our subscriptions plans</a>

                        <?php } ?>

                        <p><a href="javascript:void(0);" id="applyDiscount" style="margin-top: 15px;">Apply Discount Code</a></p>

                        <p class="discounts">

                            <input type="text" autocomplete="off" class="contact_block" id="discountCode" name="discountCode" placeholder="Enter Discount Code" style="display: none;width: 90%;margin-bottom: 5px !important;">

                            <span class="edit_link">

                                <a href="javascript:void(0);" id="btnSaveDiscount" class="save_pro" style="display: none;"></a>

                                <a href="javascript:void(0);" id="btnCancelDiscount" class="cancel_pro" style="display: none;"></a>

                            </span>

                        </p>

                    </div>

                    <div class="doc_msg"></div>

                </div>

            </div>

        </div>

    </div>



    <!--Billing Method-->

    <div id="payment_option_modal" class="pop_overlay" style="display: none;">

        <div class="pop_main">

            <div class="pop_head">

                <a href="javascript:void(0);" data-dismiss="modal" type="button" class="ppmodal" ><i class="fa fa-remove"  aria-hidden="true"></i></a>

            </div>

            <div class="pop_body">

                <div class="setting_right">

                    <div class="field_title">

                        <h4>Billing Methods</h4>

                    </div>

                    <div class="">

                        <div class="billing_table_responsive">

                            <div class="billing_table">

                                <div class="customer_billing_methods">

                                    <div class="billing_image">

                                        <img src="<?php echo get_template_directory_uri() ?>/images/paypal.png" alt="paypal">

                                    </div>

                                    <div class="billing_type">

                                        <p>Paypal</p>

                                    </div>

                                    <div class="bill_popup" style="width:50%;">

                                        <a href="#" role="button" data-toggle="modal" data-target="#paypal_modal" class="paypal_pop  btn_sky">Set up</a>

                                    </div>

                                </div>

                                <div class="customer_billing_methods">

                                    <div class="billing_image">

                                        <img src="<?php echo get_template_directory_uri() ?>/images/credit_debit.png" alt="paypal">

                                    </div>

                                    <div class="billing_type">

                                        <p>Credit or debit card</p>

                                    </div>

                                    <div class="bill_popup">

                                        <a href="javascript:void(0);" class="stripe_pop btn_sky pop_btn">Set up</a>

                                    </div>

                                </div>

                            </div>

                        </div>

                    </div>

                </div>

            </div>

        </div>

    </div>



    <!--Password-->

    <div id="password_modal" class="pop_overlay" style="display: none;">

        <div class="pop_main">

            <div class="pop_head">

                <a href="javascript:void(0);" data-dismiss="modal" type="button" ><i class="fa fa-remove"  aria-hidden="true"></i></a>

            </div>

            <div class="pop_body">

                <div class="page_title">

                    <h4>Enter your password to confirm this purchase</h4>

                </div>

                <div class="pop_content">

                    <div class="row">

                        <div class="col-sm-offset-2 col-sm-8">

                            <input type="password" class="contact_block" id="password" value="" maxlength="100" ><!--Kishor@2017!!-->

                        </div>

                        <div class="col-sm-offset-2 col-sm-4">

                            <input type="button" value="Submit" class="btn_sky" onclick="validatePassword();" >

                        </div>

                        <div class="col-sm-offset-2 col-sm-8">

                            <div style="color:#F00;"><h3 id="err_pass_msg"></h3></div>

                        </div>



                    </div>

                </div>

            </div>

        </div>

    </div>



    <?php

    $paypal_result = $wpdb->get_results("SELECT * FROM tbl_customer_general_info WHERE fk_customer_id = " . $user_id . " LIMIT 1 ");

    ?>

    <div id="paypal_modal" class="pop_overlay" style="display: none;">

        <div class="pop_main">

            <div class="pop_head">

                <a href="javascript:void(0);" data-dismiss="modal" class="ppmodal" type="button" ><i class="fa fa-remove"  aria-hidden="true"></i></a>

            </div>

            <div class="pop_body">

                <div class="page_title">

                    <h2>Add a Paypal Account</h2>


                    <h4>Payment information</h4>

                </div>

                <div class="pop_content">



                    <form method="post" action="" id="cust_paypal" autocomplete="off">

                        <div class="row">

                            <div class="col-sm-offset-2 col-sm-8">

                                <input type="email" class="contact_block" name="paypal_id" id="paypal_id" value="<?php echo $paypal_result[0]->paypal_id; ?>" maxlength="100" placeholder="Paypal Id*" required>

                            </div>

                            <div class="buttons col-sm-12">

                                <input type="submit" name="add_paypal" id="add_paypal" value="Make Payment" class="btn_sky" style="float:none;">

                                <input data-dismiss="modal" type="button" id="close_paypal_model" value="Cancel" class="btn_sky">

                                <input type="hidden" name="paypal_extra_words" id="paypal_extra_words" />

                                <input type="hidden" name="paypal_pay_amount" id="paypal_pay_amount" />

                                <input type="hidden" value="0" name="paypal_discount_amount" id="paypal_discount_amount" />

                            </div>

                            <div class="paypal_msg"></div>

                        </div>
                        
                    <h5 style="color: red;font-size: 18px">Remember to hit ‘return to merchant’ after making your payment to ensure your document is processed</h5>


                    </form>



                </div>

            </div>

        </div>

    </div>

    <div id="Paypal" class="pop_overlay open" style="display: none;">

        <?php

        $user_id = get_current_user_id();



        $prefix = $wpdb->prefix;

        $table_name = $prefix . 'creditdebit_card_details';

        $user_infio = $wpdb->get_row("SELECT * FROM $table_name WHERE customer_id = $user_id");

        

        $result_countries = $wpdb->get_results("SELECT * from countries");

        $credit_result = $wpdb->get_results("SELECT * FROM wp_creditdebit_card_details WHERE customer_id = $user_id LIMIT 1 ");

        $credit_result = $credit_result[0];

        ?>

        <div class="pop_main">

            <div class="pop_head">

                <a href="javascript:void(0);"><i class="fa fa-remove" aria-hidden="true"></i></a>

            </div>

            <div class="pop_body">

                <div class="page_title">

                    <h2>Add a credit or debit card</h2>

                    <h4>Payment information</h4>

                </div>

                <div class="pop_content">               

                    <form action="" method="POST" id="payment-form" autocomplete="off">



                        <div class="row">

                            <div class="col-sm-12 text-center">

                                <span class="payment-errors" style="color:#F00;"></span>

                            </div>

                            <div class="col-sm-12">

                                <input type="text" value="<?php echo decrypt_string($user_infio->cardnumber); ?>" name="cardno" id="cardno" placeholder="Card number" value="" data-stripe="number" class="only_num contact_block" maxlength="16">

                                <div><span id="errorcardno" style="color:red;"></span></div>

                            </div>

                            <div class="col-sm-6">

                                <select class="contact_block" id="expdate" name="expdate" data-stripe="exp_month">

                                    <option>Expiration Month</option>

                                    <option value="01" <?php echo ($user_infio->expirymonth == '01') ? "selected" : ''; ?>>01</option>

                                    <option value="02" <?php echo ($user_infio->expirymonth == '02' ) ? "selected" : ''; ?>>02</option>

                                    <option value="03" <?php echo ($user_infio->expirymonth == '03') ? "selected" : ''; ?>>03</option>

                                    <option value="04" <?php echo ($user_infio->expirymonth == '04' ) ? "selected" : ''; ?>>04</option>

                                    <option value="05" <?php echo ($user_infio->expirymonth == '05') ? "selected" : ''; ?>>05</option>

                                    <option value="06" <?php echo ($user_infio->expirymonth == '06' ) ? "selected" : ''; ?>>06</option>

                                    <option value="07" <?php echo ($user_infio->expirymonth == '07') ? "selected" : ''; ?>>07</option>

                                    <option value="08" <?php echo ($user_infio->expirymonth == '08' ) ? "selected" : ''; ?>>08</option>

                                    <option value="09" <?php echo ($user_infio->expirymonth == '09') ? "selected" : ''; ?>>09</option>

                                    <option value="10" <?php echo ($user_infio->expirymonth == '10' ) ? "selected" : ''; ?>>10</option>

                                    <option value="11" <?php echo ($user_infio->expirymonth == '11') ? "selected" : ''; ?>>11</option>

                                    <option value="12" <?php echo ($user_infio->expirymonth == '12' ) ? "selected" : ''; ?>>12</option>

                                </select>

                                <div><span id="errorexpdate" style="color:red;"></span></div> 

                            </div>

                            <div class="col-sm-6">

                                <select class="contact_block" id="expyear" name="expyear" data-stripe="exp_year">

                                    <option>Year</option>

                                    <?php

                                    $next_yr = date('Y') + 12;

                                    for ($i = date('Y'); $i < $next_yr; $i++) {

                                        ?>

                                        <option value="<?php echo $i; ?>" <?php echo ($user_infio->expyear == $i) ? "selected" : ''; ?>><?php echo $i; ?></option>

                                    <?php } ?>

                                </select>

                                <div><span id="errorexpyear" style="color:red;"></span></div> 

                            </div>

                            <div class="col-sm-6">

                                <input type="text" value="<?php echo $user_infio->securitycode; ?>" maxlength="3" placeholder="Security Code*" id="securitycode" name="securitycode" class="only_num contact_block" data-stripe="cvc" />

                                <div><span id="errorsecuritycode" style="color:red;"></span></div> 

                            </div>

                            <div class="col-sm-6">

                                <label class="contact_block">What's this?</label>

                            </div>

                            

                            <div class="col-sm-12">

                                <select class="contact_block" id="country" name="country">

                                    <option  value="0">Select Country</option>

                                    <?php 

                                        foreach ($result_countries as $value): ?>

                                            <?php if ($value->name != "") : ?>

                                                    <option <?php echo ($credit_result->country == $value->id) ? "selected" : ''; ?> value="<?php echo $value->id ?>"> <?php echo $value->name ?> </option>

                                            <?php endif; ?>

                                        <?php endforeach; ?>

                                </select>

                            </div>

                            

                            <div class="col-sm-12">

                                <input  value="<?php echo $credit_result->address; ?>"  type="text" placeholder="Address" class="contact_block" id="address" name="address" maxlength="200"/>

                            </div>



                            <div class="col-sm-12">

                                <input  value="<?php echo $credit_result->address1; ?>"  type="text" placeholder="Address (optional)" class="contact_block" id="address1" name="address1" maxlength="200"/>

                            </div>



                            <div class="col-sm-6">

                                <input  value="<?php echo $credit_result->zipcode; ?>"  maxlength="6" type="text" placeholder="Zip Code*" class="only_num contact_block" id="zipcode" name="zipcode"/>

                                <div><span id="errorzipcode" style="color:red;"></span></div>

                            </div>



                            <div class="col-sm-6">

                                <select class="contact_block" id="state" name="state">

                                    <option value="0">Select State</option>



                                    <?php

                                        $country_id = $credit_result->country;

                                        if ($country_id):

                                            $state_id = $credit_result->state;

                                            $result_states = $wpdb->get_results("SELECT * from states where country_id=" . $country_id . "");



                                            foreach ($result_states as $value) {

                                                if ($value->name != "") : ?>

                                                    <option <?php echo ($state_id == $value->id) ? "selected" : ''; ?> value="<?php echo $value->id; ?>"> <?php echo $value->name; ?> </option>

                                                    <?php

                                                endif;

                                            }

                                        endif;

                                    ?>



                                </select>

                            </div>

                            

                        </div>

                        

                        <div class="buttons">

                            <input type="submit" value="Make Payment" class="btn_sky" id="btnAddCart"  />

                            <input type="reset" value="Cancel" id="close_paypal"  data-dismiss="modal" class="btn_sky" />

                        </div>

                        <div class="card_msg"></div>

                    </form>

                </div>

            </div>

        </div>

    </div>



    <?php

    $cdate = date('Y-m-d');

    $str = "SELECT l.* FROM wp_pmpro_discount_codes c,wp_pmpro_discount_codes_levels l WHERE c.id=l.code_id And l.level_id = " . $plan_id . " And ('$cdate' BETWEEN c.starts And c.expires)";

    $res_discount = $wpdb->get_results($str);

    ?>



    <div id="plan_modal" class="pop_overlay open" style="display: none;">

        <style>

        #txt_area_upload_doc strong{

            font-family:Arial;

        }

            .pop-container{width:750px; margin:auto; background:#03b4d7; padding:0px 50px 50px 50px;}

            .head-text{ font-weight:300; font-size:30px;color:#fff;}

            .box{min-height:318px; background:#fff;-webkit-box-shadow: 6px 2px 5px 2px rgba(0,0,0,0.1);-moz-box-shadow: 6px 2px 5px 2px rgba(0,0,0,0.1);box-shadow: 6px 2px 5px 2px rgba(0,0,0,0.1);padding:20px 10px 20px 10px;text-align:center;}

            .box h1{ font-size:20px; font-weight:700; padding-top:0px; margin-top:0px;} 

            .box span{display:block;}

            .col-green{color:#87bb36 !important;}

            .col-blue{color:#14acb4 !important;}

            .col-yellow{color:#fd962a !important;}

            .pr{ font-size:31px; font-weight:700; display:block;}

            .price-text{font-weight:400; color:#aaaaaa; font-size:11px;}

            .popular{float:left;display: inline-block; margin-top:20px;}

            .off{float: right; display: inline-block; margin-top:10px;}

            .f-size-12{font-size:12px !important;}

            .f-size-15{font-size:15px !important;}

            .bdr{border:solid 6px #fd962a;}

            .mar-bot-5{ margin-bottom:5px;}

            .mar-top-35{margin-top:35px;}

            .mar-bot-60{margin-bottom:60px;}

            .btnn1{position: absolute; left: 54px; bottom: 25px;}

            .stan{display: inline-block;  word-break: break-all; width: 120px; margin-top:20px !important;}

            .privacy .changeable p{padding:0 !important;color:inherit !important; font-size:inherit !important;line-height: inherit !important}

        </style>

        <div class="pop_main" style="padding:0; height:396px;">

            <div class="pop_head" style="min-height:0;">

                <a href="javascript:void(0);" data-dismiss="modal" type="button" ><i class="fa fa-remove"  aria-hidden="true"></i></a>

            </div>

            <div class="pop-container">



                <div class="row">

                    <p class="text-center head-text">Save with one of our subscription packages</p>

                    <div class="col-md-4">

                        <?php

// If paypal is available

                        if ($paypal_result[0]->paypal_id != "") {

                            ?>



                            <?php

                        } else {

                            $per_words_price = $wpdb->get_results("SELECT * FROM wp_price_per_words WHERE id = 1");

                            $price_per_words1 = $per_words_price[0]->price_per_words;

                            ?>

                            <div class="box">

                                <h1 class="col-green" >Pay for Single Document</h1>

                                <span class="pr col-green" id="lbl_priceperwords2"></span>

                                <br />

                                <span class="f-size-15 mar-bot-5" id="lbl_pending_words11">For  extra words</span>

                                <span class="price-text" >Price per additional word: $<?php echo $price_per_words1; ?></span>

                                <span class="f-size-15 mar-bot-5" style=" line-height:92px;">&nbsp;</span>

                                <a href="javascript:void(0);" id="btn_pay_btn">

                                    <img src="<?php echo get_template_directory_uri() ?>/images/pay-btn.jpg">

                                </a>

                            </div>

                        <?php } ?>

                    </div>

                    <?php

                    $pmpro_levels = pmpro_getAllLevels(false, true);



                    $count = 0;

                    foreach ($pmpro_levels as $level) {

                        if (isset($current_user->membership_level->ID))

                            $current_level = ($current_user->membership_level->ID == $level->id);

                        else

                            $current_level = false;

                        if ($level->most_popular == 1)

                            $class = "most_popular";

                        else

                            $class = "";



                        $cl = 'blue';

                        $img = 'purchase-btn';

                        if ($count > 0) {

                            $cl = 'yellow';

                            $img = 'purchase-btn2';

                        }



                        $price_per_words = $level->price_per_additional_word;



                        $str = "SELECT l.* FROM wp_pmpro_discount_codes c,wp_pmpro_discount_codes_levels l WHERE c.id=l.code_id And l.level_id = " . $level->id . " And ('$cdate' BETWEEN c.starts And c.expires)";

                        $res_discount = $wpdb->get_results($str);

                        ?>

                        <div class="col-md-4">

                            <div class="box <?php if ($count > 0) { ?> bdr <?php } ?>" <?php if ($count > 0) { ?> style="padding: 0px;" <?php } ?>>





                                <?php if ($count == 0) { ?>

                                    <h1 class="col-<?php echo $cl; ?> <?php if ($count > 0) { ?> stan <?php } ?>" ><?php echo $level->name; ?></h1>

                                <?php } else { ?>

                                    <div style="float:left;">

                                        <h1 class="col-<?php echo $cl; ?> <?php if ($count > 0) { ?> stan <?php } ?>" ><?php echo $level->name; ?></h1>

                                    </div>

                                <?php } ?>



                                <span class="pr col-<?php echo $cl; ?>">

                                    <?php

                                    if (pmpro_isLevelFree($level))

                                        $cost_text = "<strong>" . __("Free", "pmpro") . "</strong>";

                                    else

                                        $cost_text = pmpro_getLevelCost($level, true, true);

                                    $expiration_text = pmpro_getLevelExpiration($level);

                                    if (!empty($cost_text))

                                        echo $cost_text;

                                    elseif (!empty($expiration_text))

                                        echo $expiration_text;

                                    ?>

                                </span>



                                <span class="price-text f-size-12 col-<?php echo $cl; ?> mar-bot-5" id="lbl_other_price_this_doc<?php echo $count; ?>"><!--$0.03 for this document--></span>

                                <span class="f-size-15 mar-bot-5" <?php if ($count > 0) { ?>style="padding-left:6px;" <?php } ?>><?php echo $level->plan_words; ?> words included per month</span>

                                <span class="price-text" >Price per additional word: $<?php echo $price_per_words; ?></span>

                                <?php if (count($res_discount) > 0) { ?>

                                    <span class="f-size-15 mar-bot-5" style="font-size:13px !important; line-height:40px;">With discount ($<?php echo $res_discount[0]->initial_payment; ?> cheaper)</span>

                                    <?php

                                } else {

                                    if ($count == 0) {

                                        ?>

                                        <span class="f-size-15 mar-bot-5" style=" line-height:46px;">&nbsp;</span>

                                    <?php } else { ?>

                                        <span class="f-size-15 mar-bot-5" style=" line-height:62px;">&nbsp;</span>

                                    <?php } ?>

                                <?php } ?>

                                <input type="hidden" id="lbl_other_price_plan_word<?php echo $count; ?>" value="<?php echo $level->plan_words; ?>" />

                                <input type="hidden" id="lbl_other_price_per_word<?php echo $count; ?>" value="<?php echo $price_per_words; ?>" />

                                <a href="<?php echo get_site_url(); ?>/plan/"><img src="<?php echo get_template_directory_uri() ?>/images/<?php echo $img; ?>.jpg"></a>

                            </div>

                        </div>

                        <?php

                        $count++;

                    }

                    ?>



                </div>

            </div>



        </div>



    </div>



    <!-- Thank You Modal-->

    <?php

    if ($user_is_subscribed == 'no') {

        $hght = '400px';

    } else {

        $hght = '380px';

    }

    if (isset($_REQUEST['token']) && $_REQUEST['token'] == "xs00u8c9cxe" && isset($_SESSION['sess_id'])) {

        if (isset($_REQUEST['ins_id'])) {

            $ins_id = $_REQUEST['ins_id'];

            $user_info = $wpdb->get_results("SELECT * FROM tbl_customer_general_info WHERE fk_customer_id = $user_id LIMIT 1");

            $pay_info = $wpdb->get_row("SELECT * FROM wp_price_per_extra_words WHERE id = $ins_id");

            if ($pay_info->status == 0) {

                $wpdb->update("wp_price_per_extra_words", array('status' => 1), array('id' => $ins_id));

            }

        }

        $sess_id = $_SESSION['sess_id'];

        $tmp = $wpdb->get_results("SELECT * FROM `wp_customer_document_main_temp` WHERE sess_id = '" . $sess_id . "'");

        if (count($tmp) > 0) {

            $totalNoOfWords = $tmp[0]->word_count;

            $doc_number = $tmp[0]->doc_number;

            $doc_title = $tmp[0]->document_title;

            $datetime = date('Y-m-d H:i:s');

            $desc = $tmp[0]->document_desc;

            $status = true;

            $timestamp = strtotime($datetime);

            $order_no = $tmp[0]->order_no;

            

            if($order_no > 0){

                $wpdb->insert("wp_customer_document_main", array('fk_customer_id' => $user_id, 'word_count' => $totalNoOfWords, 'doc_number' => $doc_number, 'document_title' => $doc_title, 'upload_date' => $datetime, 'document_desc' => $desc, 'Status' => $status, 'created_date' => $datetime, 'order_no' => $order_no));



                $pk_doc_main_id = $wpdb->insert_id;

            } else {

                $pk_doc_main_id = 0;

            }



            if ($pk_doc_main_id > 0) {

                $tmp = $wpdb->get_results("SELECT * FROM `wp_customer_document_details_temp` WHERE sess_id = '" . $sess_id . "'");

                if (count($tmp) > 0) {

                    foreach ($tmp as $t) {

                        $wpdb->insert("wp_customer_document_details", array('fk_doc_main_id' => $pk_doc_main_id, 'word_count' => $totalNoOfWords, 'document_desc' => $t->document_desc, 'status' => 'Pending', 'word_start_no' => $t->word_start_no, 'word_end_no' => $t->word_end_no, 'fk_cust_id' => $user_id, 'is_active' => true, 'created_date' => $datetime));

                    }

                }



                $result_general = $wpdb->get_results("SELECT * FROM `tbl_customer_general_info` WHERE fk_customer_id = $user_id LIMIT 1 ");

                $result_doc_main = $wpdb->get_results("SELECT * FROM `wp_customer_document_main` WHERE fk_customer_id = $user_id AND Status=1 ");

                $totaldocs = count($result_doc_main);



                if (count($result_general) > 0) {

                    $remaining_credit = $result_general[0]->remaining_credit_words;

                    $result = $wpdb->update(

                            'tbl_customer_general_info', array

                        (

                        'total_submited_docs' => $totaldocs,

                        'remaining_credit_words' => max(0, $remaining_credit - $totalNoOfWords),

                        "modified_date" => date('Y-m-d H:i:s')

                            ), array('fk_customer_id' => $user_id));

                } else {

                    $result = $wpdb->insert(

                            'tbl_customer_general_info', array(

                        'total_submited_docs' => $totaldocs,

                        'created_date' => date('Y-m-d H:i:s')

                            )

                    );

                }



                $blogusers = get_users(array('role' => 'proofreader', 'meta_query' => array(

                        'relation' => 'AND',

                        array(

                            'key' => 'test_completed',

                            'value' => TRUE,

                            'compare' => '='

                        ),

                        array(

                            'key' => 'test_status',

                            'value' => 'accepted',

                            'compare' => '='

                        ),

                    )

                ));



              /*  foreach ($blogusers as $users) {

                    $proof_id = $users->ID;

                    $assigned_document = $wpdb->get_results("SELECT * FROM wp_assigned_document_details WHERE fk_proofreader_id= $proof_id AND status = 'Pending' OR status='Single Check'" . "");

                    $assigned_count = count($assigned_document);

                    $result_user = $wpdb->get_results("SELECT * FROM wp_proofreader_notification_setting WHERE user_id= $proof_id");

                    $desk_noti = $result_user[0]->desktop_new_doc;



                    $subject = '[' . get_bloginfo('name') . '] A new document has been submitted for proofreading.';

                    $descs = 'A new document has been submitted for proofreading.';

                    send_proof_notification($user_id, $proof_id, $descs, $desk_noti, 1, $subject);''

                } */



                $pkdocid = $pk_doc_main_id;

                $subdocsdeatils = $wpdb->get_results("SELECT * FROM wp_customer_document_details WHERE fk_doc_main_id= $pkdocid AND is_active = 1");



                $cntLine = 1;

                foreach ($subdocsdeatils as $subdc) {

                    $user_id = $user->ID;

                    $str = $subdc->document_desc;

                    $arr = explode("\r\n", $str);

                    $Fk_sub_doc_id = $subdc->pk_doc_details_id;

                    foreach ($arr as $key => $value) {

                        global $wpdb;

                        $table_name = 'tbl_doc_line_details';

                        $wpdb->insert(

                                $table_name, array(

                            'Fk_main_doc_id' => $pkdocid,

                            'Fk_sub_doc_id' => $Fk_sub_doc_id,

                            'Fk_line_id' => $cntLine,

                            'Line_details' => $value,

                            'Fk_user_id' => $user_id

                                )

                        );

                        ++$cntLine;

                    }

                }

                

                //Discount Codes

                $disTemp = $wpdb->get_results("SELECT * FROM `tbl_discount_codes_uses_temp` WHERE sess_id = '" . $sess_id . "'");

                $disc = $wpdb->insert(

                    'tbl_discount_codes_uses', 

                    array(

                        'code_id' => $disTemp[0]->code_id,

                        'user_id' => $disTemp[0]->user_id,

                        'words' => $disTemp[0]->words,

                        'order_no' => $order_no

                    ),

                    array(

                        '%s',

                        '%d',

                        '%d',

                        '%d'

                    ));

                if($disc){

                    $wpdb->delete('tbl_discount_codes_uses_temp', array('sess_id' => $sess_id));

                }

                    

                $wpdb->delete('wp_customer_document_main_temp', array('sess_id' => $sess_id));

                $wpdb->delete('wp_customer_document_details_temp', array('sess_id' => $sess_id));

                unset($_SESSION['sess_id']);

            }

        }

        if ($user_is_subscribed == 'yes') {

            ?>

            <div id="thank_modal" class="pop_overlay open" style="display: none1;">

                <div class="pop_main" style="height:<?php echo $hght; ?>">

                    <div class="pop_head">

                        <a href="javascript:void(0);" data-dismiss="modal" type="button" ><i class="fa fa-remove"  aria-hidden="true"></i></a>

                    </div>

                    <div class="pop_body">

                        <div class="confirmation_thank_you registration_thank_you">

                            <div class="row">

                                <div class="col-md-offset-2 col-sm-8">

                                    <div class="">

                                        <div class="thank_msg">

                                            <div class="thank_img">

                                                <img src="<?php echo get_template_directory_uri() ?>/images/check_thanku.png" alt="images">

                                            </div>

                                            <h3 style="text-align:center;">Thanks for your submission!</h3>

                                            <p>Your document has been received, and will be edited within 24 hours!





                                            </p>

                                        </div>

                                    </div>

                                </div>

                            </div>

                        </div>

                    </div>

                </div>

            </div>

            <?php

        } else {

            ?>

            <style>

                .pop-container{width:750px; margin:auto; background:#03b4d7; padding:0px 50px 50px 50px;}

                .head-text{ font-weight:300; font-size:30px;color:#fff;}

                .box{min-height:274px; background:#fff; -webkit-box-shadow: 6px 2px 5px 2px rgba(0,0,0,0.1);-moz-box-shadow: 6px 2px 5px 2px rgba(0,0,0,0.1);box-shadow: 6px 2px 5px 2px rgba(0,0,0,0.1);

                     padding:20px 10px 20px 10px;text-align:center; }

                .box h1{ font-size:16px; font-weight:700; padding-top:0px; margin-top:0px;} 

                .box span{display:block;}

                .col-green{color:#87bb36 !important;}

                .col-blue{color:#14acb4 !important;}

                .col-yellow{color:#fd962a !important;}

                .pr{ font-size:31px; font-weight:700; display:block;}

                .price-text{font-weight:400; color:#aaaaaa; font-size:11px;}

                .popular{position: absolute;  left: 15px;}

                .off{   position: absolute;   right: 22px;   top: 10px;}

                .f-size-12{font-size:12px !important;}

                .f-size-15{font-size:15px !important;}

                .bdr{border:solid 6px #fd962a;}

                .mar-bot-5{ margin-bottom:5px;}

                .mar-top-35{margin-top:35px;}

                .mar-bot-60{margin-bottom:60px;}

            </style>

            <div id="thank_modal" class="pop_overlay open" style="display: none1;">

                <div class="pop_main" style="padding:0; height:<?php echo $hght; ?>">

                    <div class="pop_head" style="min-height:0;">

                        <a href="javascript:void(0);" data-dismiss="modal" type="button" ><i class="fa fa-remove"  aria-hidden="true"></i></a>

                    </div>

                    <div class="pop-container">

                        <div class="row">

                            <div class="col-sm-12">

                                <h3 style="text-align:center;">Thanks for your Submission!</h3>

                            </div>

                        </div>

                        <div class="row">

                            <p class="text-center head-text">Save with one of our monthly subscription packages</p>

                            <div class="col-md-2">&nbsp;</div>

                            <?php

                            $pmpro_levels = pmpro_getAllLevels(false, true);



                            $count = 0;

                            foreach ($pmpro_levels as $level) {

                                if (isset($current_user->membership_level->ID))

                                    $current_level = ($current_user->membership_level->ID == $level->id);

                                else

                                    $current_level = false;

                                if ($level->most_popular == 1)

                                    $class = "most_popular";

                                else

                                    $class = "";



                                $cl = 'blue';

                                $img = 'purchase-btn';

                                if ($count > 0) {

                                    $cl = 'yellow';

                                    $img = 'purchase-btn2';

                                }



                                $price_per_words = $level->price_per_additional_word;



                                $str = "SELECT l.* FROM wp_pmpro_discount_codes c,wp_pmpro_discount_codes_levels l WHERE c.id=l.code_id And l.level_id = " . $level->id . " And ('$cdate' BETWEEN c.starts And c.expires)";

                                $res_discount = $wpdb->get_results($str);

                                ?>

                                <div class="col-md-4">

                                    <div class="box">



                                        <h1 class="col-<?php echo $cl; ?>" ><?php echo $level->name; ?></h1>

                                        <span class="pr col-<?php echo $cl; ?>">

                                            <?php

                                            if (pmpro_isLevelFree($level))

                                                $cost_text = "<strong>" . __("Free", "pmpro") . "</strong>";

                                            else

                                                $cost_text = pmpro_getLevelCost($level, true, true);

                                            $expiration_text = pmpro_getLevelExpiration($level);

                                            if (!empty($cost_text))

                                                echo $cost_text;

                                            elseif (!empty($expiration_text))

                                                echo $expiration_text;

                                            ?>

                                        </span>

                                        <span class="price-text f-size-12 col-<?php echo $cl; ?> mar-bot-5"><!--With discount<br>-->



                                        </span>

                                        <span class="f-size-15 mar-bot-5"><?php echo $level->plan_words; ?> words included per month</span>

                                        <span class="price-text" >Price per additional word: $<?php echo $price_per_words; ?></span>

                                        <?php if (count($res_discount) > 0) { ?>

                                            <span class="f-size-15 mar-bot-5" style="font-size:13px !important; line-height:40px;">With discount ($<?php echo $res_discount[0]->initial_payment; ?> cheaper)</span>

                                        <?php } else { ?>

                                            <span class="f-size-15 mar-bot-5" style=" line-height:40px;">&nbsp;</span>

                                        <?php } ?>

                                        <a href="<?php echo get_site_url(); ?>/plan/"><img src="<?php echo get_template_directory_uri() ?>/images/<?php echo $img; ?>.jpg"></a>

                                    </div>

                                </div>

                                <?php

                                $count++;

                            }

                            ?>



                        </div>

                    </div>



                </div>

            </div>

            <?php

        }

    }

    ?>



    <!-- Error Modal-->

    <?php

    if (isset($_REQUEST['token']) && $_REQUEST['token'] == "xd0eu8c9cdf" && isset($_SESSION['sess_id'])) {

        $sess_id = $_SESSION['sess_id'];

        $wpdb->delete('tbl_discount_codes_uses_temp', array('sess_id' => $sess_id));

        unset($_SESSION['sess_id']);

        $order = $wpdb->get_row("SELECT * FROM $wpdb->pmpro_membership_orders WHERE user_id = $user_id AND Status = 'pending' LIMIT 1");

        if($order){

            $sqlQuery = "UPDATE $wpdb->pmpro_membership_orders SET status = '" . esc_sql('error') . "' WHERE id = '" . $order->id . "' LIMIT 1";

            $wpdb->query($sqlQuery);

        }

        

        $order->updateStatus("cancelled");

        ?>

        <div id="error_modal" class="pop_overlay open" style="display: none1;">

            <div class="pop_main" style="height:355px;">

                <div class="pop_head">

                    <a href="javascript:void(0);" data-dismiss="modal" type="button" ><i class="fa fa-remove"  aria-hidden="true"></i></a>

                </div>

                <div class="pop_body">

                    <div class="confirmation_thank_you registration_thank_you">

                        <div class="row">

                            <div class="col-md-offset-2 col-sm-8">

                                <div class="">

                                    <div class="thank_msg" style="border-top:solid 5px #F00;">

                                        <div class="thank_img">

                                            <img src="<?php echo get_template_directory_uri() ?>/images/warning.png" alt="images">

                                        </div>

                                        <h3 style="text-align:center;">Payment Error</h3>

                                        <p style="text-align:center;">Unfortunately we are unable to process your payment at this time. If you paid via a prepaid card, you may have insufficient funds. If not, your bank may have declined the payment. Please try another method of payment, or chat with us and we can help you find a solution.</p>

                                    </div>

                                </div>

                            </div>



                        </div>

                    </div>

                </div>

            </div>

        </div>

    <?php } ?>

    <?php

    if (isset($_REQUEST['token']) && $_REQUEST['token'] == "xd0eu8c9cxe" && isset($_SESSION['sess_id'])) {

        $sess_id = $_SESSION['sess_id'];

        $wpdb->delete('wp_customer_document_main_temp', array('sess_id' => $sess_id));

        $wpdb->delete('wp_customer_document_details_temp', array('sess_id' => $sess_id));

        $wpdb->delete('tbl_discount_codes_uses_temp', array('sess_id' => $sess_id));

        unset($_SESSION['sess_id']);

        ?>

        <div id="error_modal" class="pop_overlay open" style="display: none1;">

            <div class="pop_main" style="height:355px;">

                <div class="pop_head">

                    <a href="javascript:void(0);" data-dismiss="modal" type="button" ><i class="fa fa-remove"  aria-hidden="true"></i></a>

                </div>

                <div class="pop_body">

                    <div class="confirmation_thank_you registration_thank_you">

                        <div class="row">

                            <div class="col-md-offset-2 col-sm-8">

                                <div class="">

                                    <div class="thank_msg" style="border-top:solid 5px #F00;">

                                        <div class="thank_img">

                                            <img src="<?php echo get_template_directory_uri() ?>/images/warning.png" alt="images">

                                        </div>

                                        <h3 style="text-align:center;">Payment Error</h3>

                                        <p style="text-align:center;">Unfortunately we are unable to process your payment at this time. If you paid via a prepaid card, you may have insufficient funds. If not, your bank may have declined the payment. Please try another method of payment, or chat with us and we can help you find a solution.</p>

                                    </div>

                                </div>

                            </div>



                        </div>

                    </div>

                </div>

            </div>

        </div>

    <?php } ?>

    <?php

    $user_id = get_current_user_id();

    $customer_info = $wpdb->get_results(" SELECT * FROM `tbl_customer_general_info` WHERE fk_customer_id = $user_id LIMIT 1 ");

    $remaining_word_credits = 0;

    $total_submited_docs = 0;

    if (count($customer_info) > 0) {

        $remaining_word_credits = $customer_info[0]->remaining_credit_words;

        $total_submited_docs = $customer_info[0]->total_submited_docs;

    }
    // $document_count = $wpdb->get_results("SELECT * FROM `wp_customer_document_main` WHERE fk_customer_id= $user_id AND Status=1 ORDER BY pk_document_id DESC");
    // $document_count = (int)(sizeof($document_count)/2);
    $document_list = $wpdb->get_results("SELECT * FROM `wp_customer_document_main` WHERE fk_customer_id= $user_id AND Status=1 ORDER BY pk_document_id DESC LIMIT 1");

    $document_no = 1;

    if (count($document_list) > 0) {

        $document_no = $document_list[0]->doc_number + 1;

    }

    ?>

    <section>

        <div class="breadcum">

            <div class="container">

                <div class="page_title">

                    <h1>Dashboard</h1>
                    
                </div>

            </div>

        </div>

    </section>

    <section class="uploaded_file_main">

        <div class="clickable">

            <a href="javascript:void(0);" class="openre">View all submitted documents <i class="fa fa-angle-down" aria-hidden="true"></i></a>                

        </div>

        <div class="collapsible_content" style="display: none;">

            <div class="inner_content submitted_docs">

                <div class="container-fluid">

                    <div class="collapsible_slider">

                        <div class="create_new_doc">

                            <button type="button" id="btnCreateNewDoc" onclick="fnCreateNewDoc()">Create new</button>

                        </div>

                        <div class="dashboard_content_slider">

                            <?php if (count($document_list) > 0): ?>

                                <h5>All submitted documents</h5>

                                <div class="dashboard_collapsible_slider">

                                    <h3 id="load_text" style="color: white">Your documents are loading...</h3>

                                    <ul class="slides">

                                    </ul>

                                </div>

                            <?php else: ?>

                                <h5>You have not submitted a document yet.</h5>

                            <?php endif; ?>

                        </div>

                    </div>

                </div>

                <div class="clickable">

                    <a href="javascript:void(0);" class="closer">Close <i class="fa fa-angle-up" aria-hidden="true"></i></a>                

                </div>

            </div>                

        </div>

    </section>



    <section>

        <div class="container">

            <div class="privacy customer">

                <div class="row service">

                    <div class="col-sm-5">

                        <div class="total_ammount credit">

                            <div class="left">

                                <h4><?php echo $remaining_word_credits; ?><span>Words</span></h4>

                                <p>Credits<a href="<?php echo get_page_link(14); ?>">Upgrade Subscription</a></p>                                    

                            </div>

                            <div class="right"></div>

                        </div>

                    </div>

                    <div class="col-sm-5">

                        <div class="total_ammount submitted">

                            <div class="left">

                                <h4><?php echo $total_submited_docs; ?><span>Docs</span></h4>

                                <p>Total submitted<a href="javascript:void(0);" id="OpenAllSubmittedDocuments" >View all</a></p>                                    

                            </div>

                            <div class="right"></div>

                        </div>

                    </div>

                    <div class="col-sm-2">

                        <label class="cloud_file">

                            Upload Your Doc

                            <input type="file"  name="cust_document" id="cust_document"/>

                        </label>

                    </div>

                    <div class="col-sm-12 doc_error">



                    </div>

                </div>

                <div class="doc_name">

                    <h2 id="h2DocTitle"><span>Document <?php echo $document_no; ?> </span><a href="javascript:void(0);" id="btnEditDoc" class="edit_pro"></a>  </h2>

                    <input type="text" maxlength="50" class="contact_block" id="txtDocumentTitle" name="txtDocumentTitle" value="Document <?php echo $document_no; ?>" style="display: none;">

                    <input type="hidden" id="loggedCustomerId" value="<?php echo get_current_user_id(); ?>">

                    <div class="edit_link">



                        <!--<a href="javascript:void(0);" id="btnEditDoc" class="edit_pro"></a>-->

                        <a href="javascript:void(0);" id="btnSaveDoc" class="save_pro" style="display: none;"></a>

                        <a href="javascript:void(0);" id="btnCancelDoc" class="cancel_pro" style="display: none;"></a>

                    </div>

                </div>

                <?php

                //$settings = array( 'media_buttons' => false );

                //wp_editor( '', 'mycustomeditor', $settings );

                ?>

                <div class="main_editor">

                    <div class="editor_top">

                        <div class="editor_inner_top">                            

                            <div class="used_word">

                                <span>Word Count:</span><span class="count">0</span>

                            </div>

                        </div>

                        <div class="hidden_scroll">

                            <div id="progress" class="parentscrollcontents" style="width: 100%; height:300px; display: inline-block;">

                                <div class="changeable" style="padding-right: 0px;">

                                    <textarea placeholder="Type or paste your text here, or upload your document with the box above. Our team of native English speaking proofreaders will have your document back written in perfect, fluent English within 24 hours." id="txt_area_upload_doc" style="min-height: 300px; display: block; width: 100%; border: none;"></textarea>

                                </div>

                                <div class="progress_msg" style="display:none;">

                                    <p style="margin: 0 !important;"><i class="fa fa-lock" aria-hidden="true"></i> Proofreading is in Process and the content cannot be edited</p>

                                </div>

                            </div>

                        </div>

                    </div>

                    <div class="submit_area">

                        <div class="col-sm-12 doc_submit_error">                        

                        </div>

                                            <p>To have our proofreading team edit your document, just click below!</p>





                    

                        <div class="btn_blue">



                            <button class="btn_sky" id="id_SaveProofRead">Have this document proofread!</button>

                            <input type="hidden" id="hdnremaingwords" name="hdnremaingwords" value="<?php echo $remaining_word_credits; ?>">

                            <input type="hidden" id="hdnwordcount" value="0" name="hdnwordcount">

                            <?php

                            foreach ($usersDtl as $value) {

                                echo '<input type="hidden" name="hdnarrayofUsers[]" value="' . $value . '">';

                            }

                            ?>

                        </div>

                    </div>

                </div>

            </div>

        </div>

    </section>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>

    <script type="text/javascript" src="https://js.stripe.com/v2/"></script>

    <script type="text/javascript" src="<?php echo get_template_directory_uri() ?>/js/tinymce/tinymce.min.js"></script>

    <!-- TO DO : Place below JS code in js file and include that JS file -->

    <script type="text/javascript">                             

                                Stripe.setPublishableKey('<?php echo pmpro_getOption("stripe_publishablekey"); ?>');



                                function submitStripe() {

                                    var $form = $('#payment-form');

                                    // Request a token from Stripe:

                                    Stripe.card.createToken($form, stripeResponseHandler);

                                }



                                function stripeResponseHandler(status, response) {

                                    // Grab the form:

                                    var $form = $('#payment-form');



                                    if (response.error) { // Problem!



                                        // Show the errors on the form:

                                        $form.find('.payment-errors').text(response.error.message);

                                        $form.find('#btnAddCart').prop('disabled', false); // Re-enable submission



                                    } else { // Token was created!



                                        // Get the token ID:

                                        var token = response.id;



                                        // Price

                                        var price = $('#lbl_priceperwords').html();

                                        if($('#card_price').length){

                                            //$('#card_price').val(price);

                                        } else {

                                            $form.append($('<input type="hidden" name="card_price" id="card_price">').val(price));

                                        }



                                        var txt_extra_words = $('#lbl_pending_words').html();

                                        $form.append($('<input type="hidden" name="txt_extra_words">').val(txt_extra_words));



                                        // Insert the token ID into the form so it gets submitted to the server:

                                        $form.append($('<input type="hidden" name="stripeToken">').val(token));



                                        // Submit the form:

                                        $form.get(0).submit();

                                    }

                                }

                                ;

    </script>



    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js" type="text/javascript"></script>

    <script src="<?php echo get_template_directory_uri() ?>/js/node_modules/socket.io-client/socket.io.js"></script>

    <script src="<?php echo get_template_directory_uri() ?>/js/nodeClient.js"></script>

    <script>
                            tinymce.init({ 

                                    selector: 'textarea#txt_area_upload_doc',

                                    height: 295,

                                    theme: 'modern',

                                    plugins: 'preview autolink fullscreen image link media table hr pagebreak nonbreaking anchor lists textcolor wordcount paste placeholder',

                                    menubar: false,

                                    statusbar: false,

                                    toolbar: false,

                                    force_p_newlines : true,

                                    force_br_newlines : false,

                                    verify_html : false,

                                    cleanup : true,

                                    paste_auto_cleanup_on_paste: true,

                                    paste_remove_styles: false,

                                    paste_remove_styles_if_webkit: false, 

                                    paste_word_valid_elements: "br,b,strong,i,em,h1,h2,h3,h4,h5,h6,p,div,span,u,ul,li,ol,blockquote,a[href],img[src]",

                                    valid_elements: "br,b,strong,i,em,h1,h2,h3,h4,h5,h6,p,div,span,u,ul,li,ol,blockquote,a[href],img[src]",

                                    init_instance_callback : function(editor) {

                                        $(tinymce.activeEditor.getBody()).find('br').remove();

                                        var words = tinymce.get('txt_area_upload_doc').plugins.wordcount.getCount();

                                        $(".used_word .count").text(words);

                                        $("#hdnwordcount").val(words);

                                    },

                                    setup: function(ed){

                                        ed.on('change keyup', function(e) {

                                            var words = tinymce.get('txt_area_upload_doc').plugins.wordcount.getCount();

                                            $(".used_word .count").text(words);

                                            $("#hdnwordcount").val(words);

                                       });

                                    }

                                });

                                                                

                                function getDiscountPrice() {

                                    var discount_code = $('#discount_code').val().trim();

                                    if (discount_code == "") {

                                        $('#discount_code').focus();

                                        return false;

                                    }

                                    var plan_id = $('#hid_plan_id').val().trim();

                                    $.post("<?php echo get_template_directory_uri(); ?>/ajax.php", {"choice": "chek_coupon", "discount_code": discount_code, "plan_id": plan_id}, function (result) {



                                        var tmp = result.split('~');

                                        if (tmp[0] == "yes") {



                                            var prev_price = $('#paypal_pay_amount1').val();

                                            var disc_price = tmp[1];

                                            var curr_price = parseFloat(prev_price) - parseFloat(disc_price);



                                            $('#paypal_pay_amount1').val(curr_price);

                                            $('#lbl_priceperwords1').html(curr_price.toFixed(2));

                                            $('#lbl_priceperwords2').html('$' + curr_price.toFixed(2));



                                            $('#lbl_dis_success').html('Discount Code applied');

                                            $('#lbl_dis_error').html('');



                                            $('#discount_code').attr('readonly', true);

                                            $('#btnDiscount').attr('onclick', '').unbind('click');

                                        } else {

                                            $('#lbl_dis_success').html('');

                                            $('#lbl_dis_error').html('Wrong Discount Code !!!');



                                            var fade_out = function () {

                                                $("#lbl_dis_error").fadeOut();

                                                $('#discount_code').val('');

                                                $('#discount_code').focus();

                                            }

                                            setTimeout(fade_out, 3000);

                                        }

                                    });

                                }

                                

                                $("#cust_document").change(function ()

                                {

                                    // var desc = tinymce.get('txt_area_upload_doc').getContent();

                                    var fileInput = document.getElementById('cust_document');

                                    var filename = fileInput.files[0].name;

                                    var ext = getFileExtension2(filename);

                                   





                                    var arrayExtensions = ["txt", "doc", "docx", "odt", "html"];



                                    if (arrayExtensions.lastIndexOf(ext) == -1) {

                                        $('div.doc_error').html('<span class="spn_doc_error">Unsupported file type. Upload a file in one of the following formats. Microsoft Word (.doc, docx), OpenOffice (.odt) and TXT.</span>');

                                        setTimeout(function () {

                                            $('.spn_doc_error').fadeOut('slow');

                                        }, 2000);



                                        $("#cust_document").val("");

                                        return false;

                                    }



                                    //Add validation for file size upto 4 MB

                                    var _size = this.files[0].size;

                                    var fSExt = new Array('Bytes', 'KB', 'MB', 'GB'),

                                            i = 0;

                                    while (_size > 900) {

                                        _size /= 1024;

                                        i++;

                                    }

                                    var exactSize = (Math.round(_size * 100) / 100) + ' ' + fSExt[i];

                                    if (fSExt[i] == 'MB' && (Math.round(_size * 100) / 100) > 3) {

                                        $('div.doc_error').html('<span class="spn_doc_error">Unsupported documents larger than 4 MB.</span>');

                                        setTimeout(function () {

                                            $('.spn_doc_error').fadeOut('slow');

                                        }, 2000);



                                        $("#cust_document").val("");

                                        return false;

                                    }



                                    var file_data = $('#cust_document').prop('files')[0];

                                    var form_data = new FormData();

                                    //console.log(file_data);

                                    stop();

                                    form_data.append('file', file_data);

                                    form_data.append('action', 'user_upload_doc');

                                    // var data = $("#frm_docupload").serialize();



                                    $('#loding').show();

                                    $.ajax({

                                        url: '<?php echo admin_url('admin-ajax.php'); ?>',

                                        // dataType: 'html', // what to expect back from the PHP script, if anything

                                        cache: false,

                                        contentType: false,

                                        processData: false,

                                        data: form_data,

                                        type: 'post',

                                        success: function (data) {

                                            tinymce.get('txt_area_upload_doc').setContent(data);

                                            var words = tinymce.get('txt_area_upload_doc').plugins.wordcount.getCount();

                                            

                                            $(".used_word .count").text(words);

                                            $("#hdnwordcount").val(words);

                                            $('#loding').hide();



                                        },

                                        error: function (jqXHR, textStatus, errorThrown) {

                                            alert(jqXHR + " :: " + textStatus + " :: " + errorThrown);

                                            $('#loding').hide();

                                        }

                                    });



                                });



                                function getFileExtension2(filename) {

                                    return filename.split('.').pop().toLowerCase();

                                }



                                $(document).on("click", "#applyDiscount", function () {

                                    $("#applyDiscount").hide();

                                    $("#discountCode").show();

                                    $("#btnSaveDiscount").show();

                                    $("#btnCancelDiscount").show();

                                });

                                

                                $(document).on("click", "#btnSaveDiscount", function () {

                                    if ($("#discountCode").val().trim() == "")

                                    {

                                        $('.discounts').append('<span class="text-danger" style="width: 100%; display: inline-block; margin-bottom: 0px;">Discount Code is required...</span>');

                                        $(".discounts span.text-danger").fadeOut(5000);

                                        return false;

                                    }

                                    

                                    $('#loding').show();

                                    

                                    $.ajax({

                                        url: '<?php echo admin_url('admin-ajax.php'); ?>',

                                        type: 'post',

                                        data: {

                                            'action': 'saveDiscount',

                                            'code': $("#discountCode").val()

                                        },

                                        dataType: 'text',

                                        success: function (data) {

                                            if(data == 1){

                                                $('.discounts').append('<span class="text-success" style="width: 100%; display: inline-block; margin-bottom: 0px;">Discount code successfully saved.</span>');

                                                $(".discounts span.text-success").fadeOut(5000);

                                                $("#discountCode").hide();

                                                $("#btnSaveDiscount").hide();

                                                $("#btnCancelDiscount").hide();

                                                $("#applyDiscount").show();

                                                

                                                var words = parseInt($("#hdnwordcount").val().trim()) - parseInt($("#hdnremaingwords").val().trim());

                                                $.ajax({

                                                    url: '<?php echo admin_url('admin-ajax.php'); ?>',

                                                    type: 'post',

                                                    data: {

                                                        'action': 'checkDiscount',

                                                        'words': words,

                                                        'type': 'onetime'

                                                    },

                                                    dataType: 'text',

                                                    success: function (data) {

                                                        if(data == 0){

                                                            //Discount Applied and there are no words to pay for

                                                            $('.docmsg').remove();

                                                            $('#lbl_priceperwords').html('0.00');



                                                            var desc = tinymce.get('txt_area_upload_doc').getContent();

                                                            //Intercom('trackEvent', 'submit-doc');



                                                            var count = $("#hdnwordcount").val();

                                                            var doctitle = $("#txtDocumentTitle").val().trim();

                                                            var data = new FormData();



                                                            data.append('action', 'save_user_upload_doc');

                                                            data.append('word_desc', desc);

                                                            data.append('word_count', count);

                                                            data.append('doc_title', doctitle);

                                                            data.append('discount', 'yes');



                                                            if (desc != "")

                                                            {

                                                                $.ajax({

                                                                    url: '<?php echo admin_url('admin-ajax.php'); ?>',

                                                                    dataType: 'text',

                                                                    cache: false,

                                                                    contentType: false,

                                                                    processData: false,

                                                                    data: data,

                                                                    type: 'post',

                                                                    success: function (data) {

                                                                        if (data == 0)

                                                                        {

                                                                            $(".doc_msg").html('<span class="text-danger docmsg">Document title in too long.</span>');

                                                                        } else if (data == 'error')

                                                                        {

                                                                            $(".doc_msg").html('<span class="text-danger docmsg">Please enter data</span>');

                                                                        } else if (data == '-1')

                                                                        {

                                                                            $(".doc_msg").html('<span class="text-success docmsg">Discount has been applied and your document has been submitted. It will be ready within 24 hours</span>');

                                                                            window.setTimeout(function () {

                                                                                $(".docmsg").fadeOut();

                                                                                $('#popup_for_non_subs').fadeOut();

                                                                                $('#popup_for_non_subs').removeClass('open');

                                                                                window.location.reload();

                                                                            }, 2500);

                                                                        } else

                                                                        {

                                                                            $(".doc_msg").html('<span class="text-success docmsg">Discount has been applied and your document has been submitted. It will be ready within 24 hours</span>');

                                                                            window.setTimeout(function () {

                                                                                $(".docmsg").fadeOut();

                                                                                $('#popup_for_non_subs').fadeOut();

                                                                                $('#popup_for_non_subs').removeClass('open');

                                                                                window.location.reload();

                                                                            }, 2500);

                                                                        }

                                                                        $('#loding').hide();

                                                                    },

                                                                    error: function (jqXHR, textStatus, errorThrown) {

                                                                        $(".doc_msg").html('<span class="text-success docmsg">Discount has been applied and your document has been submitted. It will be ready within 24 hours</span>');

                                                                        window.setTimeout(function () {

                                                                            $(".docmsg").fadeOut();

                                                                            $('#popup_for_non_subs').fadeOut();

                                                                            $('#popup_for_non_subs').removeClass('open');

                                                                            window.location.reload();

                                                                        }, 2500);

                                                                        $('#loding').hide();

                                                                    }

                                                                });

                                                            } else

                                                            {

                                                                $('div.doc_submit_error').html('<p class="spn_submited_doc_error" style="color:red;"> Document should not be blank </p>');

                                                                setTimeout(function () {

                                                                    $('.spn_submited_doc_error').fadeOut('slow');

                                                                }, 2000);

                                                                $('#loding').hide();

                                                            }

                                                        } else {

                                                            var per_words = $('#hid_price_per_words').val();

                                                            if(data != words){

                                                                //Paypal

                                                                var discount_words = parseInt(words)-parseInt(data)

                                                                var disc_price = parseFloat(parseInt(discount_words) * parseFloat(per_words)).toFixed(2);

                                                                var new_price = parseFloat(parseInt(data) * parseFloat(per_words)).toFixed(2);



                                                                $('#paypal_discount_amount').val(disc_price);

                                                                $('#lbl_priceperwords').html(new_price);

                                                                

                                                                //Stripe

                                                                var $form = $('#payment-form');

                                                                if($("#stripeDiscount").length){

                                                                    $('#stripeDiscount').val(disc_price);

                                                                } else {

                                                                    $form.append($('<input type="hidden" name="stripeDiscount">').val(disc_price));

                                                                }

                                                                

                                                                if($('#card_price').length){

                                                                    $('#card_price').val(new_price);

                                                                } else {

                                                                    $form.append($('<input type="hidden" name="card_price" id="card_price">').val(new_price));

                                                                }

                                                            } else {

                                                                //reset any previous holding inputs

                                                                var lbl_priceperwords = parseInt(words) * parseFloat(per_words);

                                                                $('#lbl_priceperwords').html(lbl_priceperwords.toFixed(2));



                                                                $('#paypal_discount_amount').val('0');

                                                                if($("#stripeDiscount").length){

                                                                    $('#stripeDiscount').remove();

                                                                } 

                                                                

                                                                if($('#card_price').length){

                                                                    $('#card_price').remove();

                                                                }

                                                            }



                                                            $('#loding').hide();

                                                        }

                                                    },

                                                    error: function (jqXHR, textStatus, errorThrown) {

                                                        $('#loding').hide();

                                                        $(".doc_msg").html('<span class="text-danger docmsg">An error has occurred. Try again</span>');

                                                    }

                                                });

                                            } else {

                                                if(data == 2){

                                                    $('.discounts').append('<span class="text-danger" style="width: 100%; display: inline-block; margin-bottom: 0px;">You have alreaady appplied this discount code.</span>');

                                                    $(".discounts span.text-danger").fadeOut(5000);

                                                } else if(data == 3){

                                                    $('.discounts').append('<span class="text-danger" style="width: 100%; display: inline-block; margin-bottom: 0px;">You have already applied another discount code.</span>');

                                                    $(".discounts span.text-danger").fadeOut(5000);

                                                } else {

                                                    $('.discounts').append('<span class="text-danger" style="width: 100%; display: inline-block; margin-bottom: 0px;">Discount code is invalid. Try again.</span>');

                                                    $(".discounts span.text-danger").fadeOut(5000);

                                                }

                                            }

                                            $('#loding').hide();

                                        },

                                        error: function (jqXHR, textStatus, errorThrown) {

                                            $('.discounts').append('<span class="text-danger" style="width: 100%; display: inline-block; margin-bottom: 0px;">An error has occurred.</span>');

                                            $(".discounts span.text-danger").fadeOut(5000);

                                            $('#loding').hide();

                                        }

                                    });

                                });

                                $(document).on("click", "#btnCancelDiscount", function () {

                                    $("#discountCode").hide();

                                    $("#btnSaveDiscount").hide();

                                    $("#btnCancelDiscount").hide();

                                    $("#applyDiscount").show();

                                });



                                $("#id_payment_option").click(function () {

                                    var paypal_email_exist = $('#paypal_id').val().trim();

                                    if (paypal_email_exist == "") {

                                        $('#payment_option_modal').fadeIn();

                                        $('#payment_option_modal').addClass('open');

                                        $('#payment_option_modal').css('display', 'block');

                                    } else {

                                        $('#payment_option_modal').fadeIn();

                                        $('#payment_option_modal').addClass('open');

                                        $('#payment_option_modal').css('display', 'block');

                                    }

                                    return false;

                                });

                                

                                $("#btn_pay_btn").click(function () {



                                    $('#password_modal').css('display', 'none');



                                    $('#plan_modal').fadeOut();

                                    $('#plan_modal').removeClass('open');



                                    $('#payment_option_modal').fadeIn();

                                    $('#payment_option_modal').addClass('open');

                                    $('#payment_option_modal').css('display', 'block');

                                    return false;

                                });



                                // new code

                                $("#cust_paypal").submit(function(){

                                    $("#add_paypal").prop('disabled', true);

                                    saveDocs('paypal');

                                    return false;

                                });

                                

                                $("#payment-form").submit(function(){

                                    $("#btnAddCart").prop('disabled', true);

                                    saveDocs('card');

                                    return false;

                                });

                                

                                function saveDocs(type) {

                                    $('#pop_loding').show();

                                    $('div.paypal_msg, div.card_msg').html('');

                                    var desc = tinymce.get('txt_area_upload_doc').getContent(data, {format: 'html'});



                                    var count = $("#hdnwordcount").val();

                                    var doctitle = $("#txtDocumentTitle").val().trim();

                                    var data = new FormData();



                                    data.append('action', 'save_user_upload_doc_temp');

                                    data.append('word_desc', desc);

                                    data.append('word_count', count);

                                    data.append('doc_title', doctitle);



                                    if (desc != "") {

                                        $('#loding').show();

                                        $.ajax({

                                            url: '<?php echo admin_url('admin-ajax.php'); ?>',

                                            dataType: 'text', // what to expect back from the PHP script, if anything

                                            cache: false,

                                            contentType: false,

                                            processData: false,

                                            data: data,

                                            type: 'post',

                                            success: function (data) {

                                                $('#loding').hide();

                                                if(data == 1){

                                                    $('#pop_loding').hide();

                                                    if (type == 'paypal') {

                                                        $('#cust_paypal').submit();

                                                    } else {

                                                        submitStripe();

                                                    }

                                                } else{

                                                    if(type == 'paypal'){

                                                        $('div.paypal_msg').html('<p class="spn_submited_doc_error" style="color:red; text-align: center;"> An error has occurred when saving your document. Try again. </p>');

                                                        $("#add_paypal").prop('disabled', false);

                                                    } else{

                                                        $('div.card_msg').html('<p class="spn_submited_doc_error" style="color:red; text-align: center;"> An error has occurred when saving your document. Try again. </p>');

                                                        $("#btnAddCart").prop('disabled', false);

                                                    }

                                                }

                                            },

                                            error: function (jqXHR, textStatus, errorThrown) {

                                                $('#loding').hide();

                                                if(type == 'paypal'){

                                                    $('div.paypal_msg').html('<p class="spn_submited_doc_error" style="color:red; text-align: center;"> An error has occurred when saving your document. Try again. </p>');

                                                    $("#add_paypal").prop('disabled', false);

                                                } else{

                                                    $('div.card_msg').html('<p class="spn_submited_doc_error" style="color:red; text-align: center;"> An error has occurred when saving your document. Try again. </p>');

                                                    $("#btnAddCart").prop('disabled', false);

                                                }

                                                //$('#pop_loding').hide();

                                                //alert(jqXHR + " :: " + textStatus + " :: " + errorThrown);



                                                //if (type == 'paypal') {

                                                    //$('#cust_paypal').submit();

                                                //}

                                            }

                                        });

                                        //return false;

                                    } else {



                                        $('div.doc_submit_error').html('<p class="spn_submited_doc_error" style="color:red;"> Document should not be blank </p>');

                                        setTimeout(function () {

                                            $('.spn_submited_doc_error').fadeOut('slow');

                                        }, 2000);

                                    }

                                    //return false;

                                    //}

                                }

                                $("#id_SaveProofRead").click(function ()

                                {

                                    var that = $(this);

                                    that.prop('disabled', true);

                                    $('.docmsg').remove();

                                    var desc = tinymce.get('txt_area_upload_doc').getContent({format: 'html'});

                                    // console.log(desc);

                                    var count = $("#hdnwordcount").val();

                                    var doctitle = $("#txtDocumentTitle").val().trim();

                                    var data = new FormData();



                                    data.append('action', 'save_user_upload_doc');

                                    data.append('word_desc', desc);

                                    data.append('word_count', count);

                                    data.append('doc_title', doctitle);



                                    var doc_detail = tinymce.get('txt_area_upload_doc').getContent({format: 'html'});

                                    var words = tinymce.get('txt_area_upload_doc').plugins.wordcount.getCount();



                                    var remaingwords = $("#hdnremaingwords").val().trim();

                                    if (words > remaingwords) {

                                        that.prop('disabled', false);



                                        var lbl_pending_words = parseInt(words) - parseInt(remaingwords);

                                        $('#lbl_pending_words').html(lbl_pending_words);

                                        $('#lbl_pending_words1').html(lbl_pending_words);

                                        $('#paypal_extra_words').val(lbl_pending_words);

                                        $('#paypal_extra_words1').val(lbl_pending_words);

                                        $('#lbl_pending_words11').html(lbl_pending_words + '-word document');



                                        //Basic

                                        var lbl_other_price_per_word = $('#lbl_other_price_per_word0').val();

                                        var lbl_other_price_this_doc = parseInt(lbl_pending_words) * parseFloat(lbl_other_price_per_word);

                                        $('#lbl_other_price_this_doc0').html('$' + lbl_other_price_this_doc.toFixed(2) + ' for this document');



                                        // Standard

                                        var lbl_other_price_per_word = $('#lbl_other_price_per_word1').val();

                                        var lbl_other_price_this_doc = parseInt(lbl_pending_words) * parseFloat(lbl_other_price_per_word);

                                        $('#lbl_other_price_this_doc1').html('$' + lbl_other_price_this_doc.toFixed(2) + ' for this document');





                                        var per_words = $('#hid_price_per_words').val();

                                        //alert(per_words)

                                        var lbl_priceperwords = parseInt(lbl_pending_words) * parseFloat(per_words);

                                        $('#lbl_priceperwords').html(lbl_priceperwords.toFixed(2));

                                        $('#lbl_priceperwords1').html('$' + lbl_priceperwords.toFixed(2));

                                        $('#lbl_priceperwords2').html('$' + lbl_priceperwords.toFixed(2));



                                        $('#paypal_pay_amount').val(lbl_priceperwords);

                                        $('#paypal_pay_amount1').val(lbl_priceperwords);



                                        $('#popup_for_non_subs').fadeIn();

                                        dataLayer.push({

                                            'event': 'Virtual Doc Submit Payment View',

                                            'virtualPageURL': '/customer-dashboard/pay',

                                            'virtualPageTitle': 'Modal pay for document extra words',

                                            'price_of_doc': lbl_priceperwords.toFixed(2)

                                        });

                                        $('#popup_for_non_subs').addClass('open');

                                        return false;

                                    }



                                    if (words > remaingwords) {

                                        $('div.doc_submit_error').html('<p class="spn_submited_doc_error" style="color:red;"> you have only ' + remaingwords + ' words credit </p>');

                                        setTimeout(function () {

                                            $('.spn_submited_doc_error').fadeOut('slow');

                                        }, 3000);

                                        that.prop('disabled', false);

                                        return false;

                                    } else if (!$('#btnEditDoc').is(':visible')) {

                                        $('div.doc_submit_error').html('<p class="spn_submited_doc_error" style="color:red;">Save document title...</p>');

                                        setTimeout(function () {

                                            $('.spn_submited_doc_error').fadeOut('slow');

                                        }, 2000);

                                        that.prop('disabled', false);

                                        return false;

                                    } else if ($("#txtDocumentTitle").val().trim() == "") {

                                        $('div.doc_submit_error').html('<p class="spn_submited_doc_error" style="color:red;">Document title is required...</p>');

                                        setTimeout(function () {

                                            $('.spn_submited_doc_error').fadeOut('slow');

                                        }, 2000);

                                        that.prop('disabled', false);

                                        return false;

                                    } else {

                                        var DocumentTitle = $("#txtDocumentTitle").val().trim();

                                        $.ajax({

                                            url: "<?php echo admin_url('admin-ajax.php'); ?>",

                                            type: "POST",

                                            data: {

                                                action: 'check_doc_name',

                                                DocumentTitle: DocumentTitle

                                            },

                                            success: function (data) {

                                                if (data == 0) {

                                                    if (desc != "") {

                                                        //console.log(desc);

                                                        /*

                                                         desc contains user's document.

                                                         */

                                                        $('#edit_free').fadeIn();

                                                        $('#edit_free').addClass('open');

                                                       that.prop('disabled', false);

                                                        return false;

                                                    } else {



                                                        $('div.doc_submit_error').html('<p class="spn_submited_doc_error" style="color:red;"> Document should not be blank </p>');

                                                        setTimeout(function () {

                                                            $('.spn_submited_doc_error').fadeOut('slow');

                                                        }, 2000);

                                                        that.prop('disabled', false);

                                                    }

                                                } else {

                                                    $('div.doc_submit_error').html('<p class="spn_submited_doc_error" style="color:red;">Your document title already exits. Please choose a different document title.</p>');

                                                    setTimeout(function () {

                                                        $('.spn_submited_doc_error').fadeOut('slow');

                                                    }, 2000);

                                                    that.prop('disabled', false);

                                                    return false;

                                                }

                                                that.prop('disabled', false);

                                                window.localStorage.removeItem("proof_doc");

                                            },

                                            error: function (jqXHR, textStatus, errorThrown) {

                                                console.log(jqXHR + " :: " + textStatus + " :: " + errorThrown);

                                            }

                                        });



                                    }

                                });





                                function fnCreateNewDoc()

                                {

                                    var dashboard_page_link = '<?php echo get_page_link(762); ?>';

                                    window.location.href = dashboard_page_link;

                                }



                                $('#OpenAllSubmittedDocuments').click(function (e) {

                                    e.preventDefault();

                                    e.stopPropagation();

                                    if (!$('.openre').hasClass('open')) {

                                        $("a.openre").addClass('open');

                                        $('.collapsible_content').slideDown('slow');

                                    }

                                });
            //                     $(document).ready(function(){
            //                         console.log('aaaaaaaaaa');

            //                    function get_sel_doc() {
            //                         console.log('bbbbbbbbbb');
            //                         var main_doc_id = $(this).attr("data-id");

            //                         var doc_status = $(this).attr("data-status");



            //                         if (doc_status.trim().toLowerCase() == 'completed')

            //                         {

            //                             var url = '<?php echo get_page_link(924); ?>?doc=' + main_doc_id;

            //                             window.location.href = url;

            //                             return false;

            //                         }



            //                         var data = new FormData();

            //                         data.append('action', 'get_selected_doc_data');

            //                         data.append('main_doc_id', main_doc_id);



            //                         if (main_doc_id != "")

            //                         {

            //                             $.ajax({

            //                                 url: '<?php echo admin_url('admin-ajax.php'); ?>',

            //                                 dataType: "json",

            //                                 cache: false,

            //                                 contentType: false,

            //                                 processData: false,

            //                                 data: data,

            //                                 type: 'post',

            //                                 success: function (data) {



            //                                     var first = data[0];

            //                                     //var desc = $(first).html().trim();

            //                                     //first = desc.replace(/<br>/g, "\n");

            //                                     //desc = desc.replace(/<div>/g, "");

            //                                     //first = desc.replace(/<\/div>/g, "\n");

            //                                     var second = data[1];

            //                                     $("div.doc_name h2").text(second);

            //                                     $(".progress_msg").show();

            //                                     $("#progress").addClass("progres");

            //                                     $(".submit_area").remove();

            //                                     $(".used_word").hide();

            //                                     $("div.hidden_scroll").addClass("in_progress");

                                                // tinymce.get('txt_area_upload_doc').setContent(first);

            //                                     $('html, body').animate({scrollTop: 500}, "slow");



            //                                     $("label.cloud_file").remove()

            //                                     tinymce.get('txt_area_upload_doc').getBody().setAttribute('contenteditable', false);



            //                                     if (data == 'error')

            //                                     {

            //                                         alert("Please enter data");

            //                                     } else

            //                                     {

            //                                         //alert("success  = " + data);

            //                                     }



            //                                     var data1 = new FormData();

            //                                     data1.append('action', 'getProofreaderByCustomerId');

            //                                     data1.append('main_doc_id', main_doc_id);



            //                                     $.ajax({

            //                                         url: '<?php echo admin_url('admin-ajax.php'); ?>',

            //                                         dataType: "json",

            //                                         cache: false,

            //                                         contentType: false,

            //                                         processData: false,

            //                                         data: data1,

            //                                         type: 'post',

            //                                         success: function (data2) {

            //                                             socket.emit('adduser', data2[0]);

            //                                             socket.emit('sendDocDtl', data2[0]);

            //                                         },

            //                                         error: function (jqXHR, textStatus, errorThrown) {

            //                                             alert("ERROR=" + jqXHR + " :: " + textStatus + " :: " + errorThrown);

            //                                         }

            //                                     });



            //                                 },

            //                                 error: function (jqXHR, textStatus, errorThrown) {

            //                                     alert("ERROR=" + jqXHR + " :: " + textStatus + " :: " + errorThrown);

            //                                 }

            //                             });

            //                         } else

            //                         {

            //                             alert('Error');

            //                         }


            //                     };
            //                     });


                                $(document).on("click", "#btnEditDoc", function () {

                                    $("#h2DocTitle").hide();

                                    $("#txtDocumentTitle").show();

                                    $("#btnEditDoc").hide();

                                    $("#btnSaveDoc").show();

                                    $("#btnCancelDoc").show();



                                });

                                $(document).on("click", "#btnSaveDoc", function () {

                                    if ($("#txtDocumentTitle").val().trim() == "")

                                    {

                                        $('.doc_name').append('<span class="text-danger" style="width: 100%; display: inline-block; margin-bottom: 10px;">Document Title is required...</span>');

                                        $(".doc_name span.text-danger").fadeOut(5000);

                                        return false;

                                    }

                                    $("#h2DocTitle span").text($("#txtDocumentTitle").val());

                                    $("#h2DocTitle").show();

                                    $("#txtDocumentTitle").hide();

                                    $("#btnEditDoc").show();

                                    $("#btnSaveDoc").hide();

                                    $("#btnCancelDoc").hide();

                                });

                                $(document).on("click", "#btnCancelDoc", function () {

                                    //alert("cancel");

                                    $("#h2DocTitle").show();

                                    $("#txtDocumentTitle").val($("#h2DocTitle span").text());

                                    $("#txtDocumentTitle").hide();

                                    $("#btnEditDoc").show();

                                    $("#btnSaveDoc").hide();

                                    $("#btnCancelDoc").hide();

                                });



                                



                                $('#txt_area_upload_doc').bind("DOMNodeInserted", function () {

                                    if ($(this).val().trim() == "")

                                    {

                                        $(".used_word .count").text("0");

                                        $("#hdnwordcount").val(0);

                                    } else

                                    {

                                        var words = tinymce.get('txt_area_upload_doc').plugins.wordcount.getCount();

                                        $(".used_word .count").text(words);

                                        $("#hdnwordcount").val(words);

                                    }

                                });



                                $('#txt_area_upload_doc').keyup(function () {



                                    if ($(this).val().trim() == "")

                                    {

                                        $(".used_word .count").text("0");

                                        $("#hdnwordcount").val(0);

                                    } else

                                    {

                                        var words = tinymce.get('txt_area_upload_doc').plugins.wordcount.getCount();

                                        $(".used_word .count").text(words);

                                        $("#hdnwordcount").val(words);

                                    }

                                });



                                $('.pop_head a .fa-remove,#edit_free .save_doc .btnno').click(function (e) {

                                    e.stopPropagation();

                                    $('#edit_free').fadeOut();

                                    $('#edit_free').removeClass('open');

                                });

                                $('a.pricemodal .fa-remove').click(function (e) {

                                    e.stopPropagation();

                                    $('#popup_for_non_subs').fadeOut();

                                    $('#popup_for_non_subs').removeClass('open');

                                });

                                $('#paypal_modal a.ppmodal  .fa-remove').click(function (e) {

                                    e.stopPropagation();

                                    $('#paypal_modal').fadeOut();

                                    $('#paypal_modal').removeClass('open');

                                });

                                $('#payment_option_modal a.ppmodal .fa-remove').click(function (e) {

                                    e.stopPropagation();

                                    $('#payment_option_modal').fadeOut();

                                    $('#payment_option_modal').removeClass('open');

                                });



                                $('#close_paypal').click(function (e) {

                                    e.stopPropagation();

                                    $('#Paypal').fadeOut();

                                    $('#Paypal').removeClass('open');

                                });



                                $('#plan_modal a .fa-remove').click(function (e) {

                                    e.stopPropagation();

                                    $('#plan_modal').fadeOut();

                                    $('#plan_modal').removeClass('open');

                                });



                                $('#thank_modal a .fa-remove').click(function (e) {

                                    $('#thank_modal').fadeIn();

                                    $('#thank_modal').removeClass('open');

                                    $('#thank_modal').css('display', 'none');



                                    $('#plan_modal').css('display', 'none');

                                });

                                $('#password_modal a .fa-remove').click(function (e) {



                                    $('#password_modal').fadeIn();

                                    $('#password_modal').removeClass('open');

                                    $('#password_modal').css('display', 'none');

                                });



                                $('#error_modal a .fa-remove').click(function (e) {



                                    $('#error_modal').fadeIn();

                                    $('#error_modal').removeClass('open');

                                    $('#error_modal').css('display', 'none');

                                });



                                $('#edit_free .save_doc .btnyes').click(function (e) {

                                    var that = $(this);

                                    that.prop('disabled', true);

                                    var desc = tinymce.get('txt_area_upload_doc').getContent();


                                   // Intercom('trackEvent', 'submit-doc');



                                    var count = $("#hdnwordcount").val();

                                    var doctitle = $("#txtDocumentTitle").val().trim();

                                    var data = new FormData();



                                    data.append('action', 'save_user_upload_doc');

                                    data.append('word_desc', desc);

                                    data.append('word_count', count);

                                    data.append('doc_title', doctitle);



                                    if (desc != "")

                                    {

                                        $('#loding').show();

                                        $.ajax({

                                            url: '<?php echo admin_url('admin-ajax.php'); ?>',

                                            dataType: 'text', // what to expect back from the PHP script, if anything

                                            cache: false,

                                            contentType: false,

                                            processData: false,

                                            data: data,

                                            type: 'post',

                                            success: function (data) {

                                                $('#loding').hide();

                                                if (data == 0)

                                                {

                                                    $(".doc_msg").html('<span class="text-danger docmsg">Document title in too long.</span>');

                                                    that.prop('disabled', false);

                                                } else if (data == 'error')

                                                {

                                                    $(".doc_msg").html('<span class="text-danger docmsg">Please enter data</span>');

                                                    that.prop('disabled', false);

                                                } else if (data == '-1')

                                                {

                                                    $(".doc_msg").html('<span class="text-success docmsg">Your document has already been submitted, and will be ready within 24 hours</span>');

                                                    that.prop('disabled', false);

                                                    window.setTimeout(function () {

                                                        $(".docmsg").fadeOut();

                                                        $('#edit_free').fadeOut();

                                                        $('#edit_free').removeClass('open');

                                                        window.location.reload();

                                                    }, 2500);

                                                } else

                                                {

                                                    $(".doc_msg").html('<span class="text-success docmsg">' + data + '</span>');

                                                    that.prop('disabled', true);

                                                    window.setTimeout(function () {

                                                        $(".docmsg").fadeOut();

                                                        $('#edit_free').fadeOut();

                                                        $('#edit_free').removeClass('open');

                                                        window.location.reload();

                                                    }, 2500);

                                                }

                                            },

                                            error: function (jqXHR, textStatus, errorThrown) {

                                                $(".doc_msg").html('<span class="text-success docmsg">Your document has been submitted, and will be ready within 24 hours</span>');

                                                that.prop('disabled', true);

                                                $('#loding').hide();

                                                window.setTimeout(function () {

                                                    $(".docmsg").fadeOut();

                                                    $('#edit_free').fadeOut();

                                                    $('#edit_free').removeClass('open');

                                                    window.location.reload();

                                                }, 2500);

                                            }

                                        });

                                    } else

                                    {

                                        that.prop('disabled', false);

                                        $('div.doc_submit_error').html('<p class="spn_submited_doc_error" style="color:red;"> Document should not be blank </p>');

                                        setTimeout(function () {

                                            $('.spn_submited_doc_error').fadeOut('slow');

                                        }, 2000);

                                    }

                                });



                                function validateEmail(email) {

                                    var filter = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;

                                    if (filter.test(email)) {

                                        return true;

                                    } else {

                                        return false;

                                    }

                                }







                                // new code

                                jQuery(document).ready(function ($) {



                                    $('#openBtn1').click(function () {



                                        $('#plan_modal').fadeIn();

                                        $('#plan_modal').removeClass('open');



                                        $('#myModal1').fadeOut();

                                        $('#myModal1').addClass('open');

                                        $('#myModal1').css('display', 'block');



                                        return false;

                                    });



                                    $('#openBtn2').click(function () {



                                        $('#plan_modal').fadeIn();

                                        $('#plan_modal').removeClass('open');



                                        return false;

                                    });



                                    $("#country").change(function () {

                                        $('#state').find('option:not(:first)').remove();

                                        $.ajax({

                                            url: '<?php echo admin_url('admin-ajax.php'); ?>',

                                            data: {

                                                'action': 'getStates',

                                                'countryId': $("#country").val()



                                            },

                                            dataType: 'text',

                                            success: function (data) {

                                                // This outputs the result of the ajax request

                                                $("#state").append(data);

                                            },

                                            error: function (errorThrown) {

                                                console.log(errorThrown);

                                            }

                                        });

                                    });



                                    $("#btnAddCart").click(function () {



                                        if ($("#cardno").val() == "") {

                                            $("#errorcardno").html("Please enter card number");

                                            $("#cardno").focus();

                                            return false;

                                        } else {

                                            $("#errorcardno").html("");

                                            if ($("#cardno").val().length < 16) {

                                                $("#errorcardno").html("Please enter valid card number");

                                                $("#cardno").focus();

                                                return false;

                                            }

                                        }

                                        if ($("#expdate").val() == "Expiration Month") {

                                            $("#errorexpdate").html("Please select expiry month");

                                            $("#expdate").focus();

                                            return false;

                                        } else {

                                            $("#errorexpdate").html("");

                                        }

                                        if ($("#expyear").val() == "Year") {

                                            $("#errorexpyear").html("Please select expiry year");

                                            $("#expyear").focus();

                                            return false;

                                        } else {

                                            $("#errorexpyear").html("");

                                        }

                                        if ($("#securitycode").val() == "Year") {

                                            $("#errorsecuritycode").html("Please enter cvv no");

                                            $("#securitycode").focus();

                                            return false;

                                        }







                                    });



                                });



                                function validatePassword() {



                                    var pass = $('#password').val().trim();

                                    if (pass == "") {

                                        $('#password').focus();

                                        return false;

                                    }

                                    $.post("<?php echo get_template_directory_uri(); ?>/ajax.php", {"choice": "chek_user_password", "password": pass}, function (result) {

                                        if (result.trim() == "yes") {



                                            $('#cust_paypal').submit();

                                            //$('#err_pass_msg').html('')

                                            //$('#plan_modal').fadeIn();

                                            //$('#plan_modal').addClass('open');

                                        } else {

                                            $('#err_pass_msg').html('You have entered an invalid password!');

                                            var fade_out = function () {

                                                $("#err_pass_msg").fadeOut();

                                                $('#password').val('');

                                                $('#password').focus();

                                            }

                                            setTimeout(fade_out, 3000);

                                        }

                                    });

                                }


    </script>   



    <?php get_footer(); ?>