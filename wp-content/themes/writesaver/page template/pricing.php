<?php
/*
 * Template Name: Pricing Page
 */

get_header();

if (is_user_logged_in()) {
    $current_user = wp_get_current_user();
    $user_roles_array = $current_user->roles;
    $user_role = array_shift($user_roles_array);
    if ($user_role != "customer") {
        echo '<script>window.location.href="' . get_site_url() . '"</script>';
        exit;
    }
} else {
    $current_user->ID = '';
}
?>

<section>
    <div class="breadcum">
        <div class="container">
            <div class="page_title">
                <?php the_title('<h1>', '</h1>'); ?>
            </div>
        </div>
    </div>
</section>
<input type="hidden" value="<?php echo $current_user->ID; ?>" id="current_user_ID" />
<input type="hidden" value="<?php echo get_the_permalink(14); ?>" id="plan_url" />
<?php
if (have_posts()) :
    while (have_posts()) : the_post();
        the_content();
    endwhile;
endif;
?>
 <script>
     $("#myModal1").remove();
     document.getElementById("openBtn1").onclick = function() {myFunction()};
     function myFunction() {

         document.getElementById("Paypal").style.display = "block";
      //   document.getElementsByClassName("pop_overlay open").removeAttr("id");
 myga('Credit card');
 
     }
 </script>
<?php get_footer(); ?>


<?php
if (isset($_REQUEST['plan'])) {
    ?>
    <script>
        var level_id =<?php echo $_REQUEST['plan']; ?>;
        if (level_id != 'custom') {
            $('#hid_mamu_level_id').val(level_id);
            var url = '<?php echo home_url(); ?>/paypal-process/?level=' + level_id;
            $('#lbl_paypal_submit_link').attr("href", url);
            if ($('#current_user_ID').val() == '') {
                open_loginpop();
            } else {
                $('#payment_option_modal .paypal_billing').hide();
                $('#payment_option_modal').fadeIn();
                $('#payment_option_modal').addClass('open');

            }
        } else {
            $('#payment_option_modal .paypal_billing').show();
        }
    </script>
    <?php
}
?>