<?php

/*
 * Template Name: Test
 */
get_header();
?>

<section>
    <div class="breadcum">
        <div class="container">
            <div class="page_title">
                <h1>Dashboard Test</h1>
            </div>
        </div>
    </div>
</section>

<section>
    <div class="container">
        <div class="privacy customer proofreader">
            <div class="doc_name">
                <h2>Document 1</h2>
            </div>
            <div class="main_editor">
                <div class="editor_top">
                    <div class="editor_inner_top">                            
                        <div class="delete_track">

                            <div class="ios_checkbox ios_checkbox_sm">                                    
                                <input class="ios8-switch" id="track_chk" type="checkbox">                                    
                                <label for="track_chk">Track Changes</label>
                            </div>
                        </div>
                        <div class="used_word" id="maindocword">
                            <span>Word Count:</span><span class="count">568</span>
                        </div>                            
                    </div>
                    <div class="hidden_scroll">
                        <div class="changeable" id="txt_area_upload_doc_" data-id="" style="height:500px; width: 100%; display: inline-block;    white-space: pre-line;" contenteditable="false">

                            <div class="doc_desc">
                                <div class="track_result" style="">
                                    <div class="hover_eff"><ins class="diff">Bartcon</ins><del class="diff">Barton</del></div>
                                    <ins class="diff">waijkted</ins><del class="diff">waited</del> twenty always repair in within we do. An delighted offending curiosity my is dashwoods at. Boy prosperous increasing <del class="diff">surrounded </del>companions her nor advantages sufficient put. John on time down give meet help as of. Him waiting and correct believe now cottage she <ins class="diff">hj</ins><del class="diff">another</del>. Vexed <ins class="diff">sihjx</ins><del class="diff">six</del> shy yet along learn maids her tiled. Through studied shyness evening bed him winding present. Become excuse hardly on my thirty it wanted. Apartments simplicity or understood do it we. Song such eyes had and off. Removed winding ask explain delight out few behaved lasting. Letters old hastily ham sending not sex chamber because present. Oh is indeed twenty entire figure. Occasional diminution announcing <del class="diff">new </del>now literature terminated. Really regard excuse off ten pulled. Lady am room head so <del class="diff">lady </del>four or eyes an. He do of consulted sometimes concluded mr. An household behaviour if pretended. Ham followed now ecstatic use speaking exercise may repeated. Himself he evident oh greatly my on inhabit general concern. It earnest <ins class="diff">amhongst</ins><del class="diff">amongst</del> he showing females so improve in picture. Mrs can hundred its greater account. Distrusts daughters certainly suspected convinced our perpetual him yet. Words did noise taken right state are since. Chapter too parties its letters nor. Cheerful but whatever ladyship disposed yet judgment. Lasted answer oppose to ye months no esteem. Branched is on an ecstatic directly it. Put off continue you denoting returned juvenile. Looked person sister result mr to. Replied demands charmed do viewing ye colonel to so. Decisively inquietude he advantages insensible at oh continuing unaffected of. For norland produce age wishing. To figure on it spring season up. Her provision acuteness had excellent two why intention. As called mr needed praise at. Assistance imprudence yet sentiments unpleasant expression met surrounded not. Be at talked ye though secure nearer. Possession her thoroughly remarkably terminated man continuing. Removed greater to do ability. You shy shall while but wrote marry. Call why sake has sing pure. Gay six set polite nature worthy. So matter be me we wisdom should basket moment merely. Me burst ample wrong which would mr he could. Visit arise my point timed drawn no. Can friendly laughter goodness man him appetite carriage. Any widen see gay forth alone fruit bed. Fat new smallness few supposing suspicion two. Course sir people worthy horses add entire suffer. How one dull get busy dare far. At principle perfectly by sweetness do. As mr started arrival subject by believe. Strictly numerous outlived kindness whatever on we no on addition. Enjoyed minutes related as at on on. Is fanny dried as often me. Goodness as reserved raptures to mistaken steepest oh screened he. Gravity he mr sixteen esteems. Mile home its new way with high told said. Finished no horrible blessing landlord dwelling dissuade if. Rent fond am he in on read. Anxious cordial demands settled entered in do to colonel. Yourself off its pleasant ecstatic now law. Ye their mirth seems of songs. Prospect out bed contempt separate. Her inquietude our shy yet sentiments collecting. Cottage fat beloved himself arrived old. Grave widow hours among him no you led. Power had these met least nor young. Yet match drift wrong his our. Able an hope of body. Any nay shyness article matters own removal nothing his forming. Gay own additions education satisfied the perpetual. If he cause manor happy. Without farther she exposed saw man led. Along on happy could cease green oh.<ins class="diff"> hj hj</ins></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</section>
<script>
    jQuery('.track_result .diff').mouseenter(function (){
        var pos = jQuery(this).position();
        jQuery('.hover_eff').css({
            "display": "block",
            "left": pos.left ,
            "top": pos.top + 30
        });
    });
    jQuery('.diff').mouseleave(function (){
        jQuery('.hover_eff').css('display','none');
    });
</script>
<?php get_footer(); ?>