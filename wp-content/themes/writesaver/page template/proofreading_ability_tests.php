<?php
/*
 * Template Name:Proofreading Ability Test
 */
get_header();
global $wpdb;
$current_user = wp_get_current_user();
$user_roles_array = $current_user->roles;
$user_role = array_shift($user_roles_array);
if (!is_user_logged_in() || $user_role != "proofreader") {
    echo '<script>window.location.href="' . get_site_url() . '"</script>';
    exit;
}

$user_id = get_current_user_id();
$test_result = $wpdb->get_results("SELECT max(test_id) as test_id FROM tbl_proofreader_test WHERE fk_proofreader_id = $user_id LIMIT 1 ");

$current_testid = $test_result[0]->test_id;
if ($current_testid == 5) {
    echo '<script>window.location.href="' . get_the_permalink(778) . '"</script>';
    exit;
}
if ($current_testid)
    $current_testid = $current_testid + 1;
else
    $current_testid = 1;
get_header();
?>
<section>
    <div class="breadcum">
        <div class="container">
            <div class="page_title">
                <h1>Proofreading ability</h1>
            </div>
        </div>
    </div>
</section>        
<section>
    <div class="blog_category_sticky basic_information">
        <div class="container">   
            <div class="blog_category_sticky_left">
                <div class="desktop_catagory">
                    <div class="category_full_list">
                        <div class="category_btn">
                            <div class="category_btn_icon">
                                <img src="<?php echo get_template_directory_uri() ?>/images/cat_icon.png" class="img-responsive">
                            </div>
                            <div class="category_btn_txt">
                                <span>Informations</span>
                            </div>
                        </div>
                        <ul id="links">
                            <li onclick=" $(this).unbind('click');
                                    $('#links').unbind('click');"><a href="javascript:void(0);" > Basic information  </a></li>
                            <li  class="active"><a  href="javascript:void(0);">  Proofreading ability </a></li>                                                                  
                        </ul>
                    </div>
                </div>
            </div> 
        </div>
    </div>  
</section>
<section>
    <div class="basic_info" id="vertical">
        <div class="container">
            <div class="row basic_info_inner tests">
                <div class="col-sm-2"> 
                    <div class="blog_category_sticky_left">
                        <div class="category_full_listing">
                            <?php
                            $args = array(
                                'post_type' => 'testexam',
                                'order' => ASC
                            );
                            $the_query = new WP_Query($args);
                            echo '<ul id="all_link">';
                            $count = 1;
                            if ($the_query->have_posts()) :

                                while ($the_query->have_posts()) :
                                    $the_query->the_post();
                                    echo '<li id="note_' . $count . '"  class="' . $class . '">';
                                    $count++;
                                    echo the_title();
                                    echo'</li>';
                                endwhile;
                            endif;
                            echo'</ul>';
                            ?>
                        </div>
                    </div>
                </div>
                <div class="col-sm-10"> 
                    <div class="blog_category_main" id="notContent">

                        <?php
                        $count = 1;
                        if ($the_query->have_posts()) :

                            while ($the_query->have_posts()) :

                                $the_query->the_post();
                                $id = get_the_ID();

                                $content_post = get_post($id);
                                $content = $content_post->post_content;
                                $content = apply_filters('the_content', $content);
                                $content = str_replace(']]>', ']]&gt;', $content);
                                ?>
                                <div id="notContent_<?php echo $count; ?>" class="blog_listt"> 

                                    <div class="test_content"style=" display:none;"><?php echo $content; ?></div> 
                                    <div class="post_id"  style=" display:none;"><?php echo $id; ?></div> 
                                    <div class="test_id"  style=" display:none;"><?php echo $count; ?></div> 
                                    <h4><?php echo get_the_title(); ?></h4>
                                    <div class="test_contents">
                                        <div class="main_editor">
                                            <div class="editor_top">
                                                <div class="editor_inner_top">                            
                                                    <div class="used_word">
                                                        <span>Word Count:</span><span class="count">0</span>
                                                    </div>
                                                </div>
                                                <div class="hidden_scroll">
                                                    <div id="progress" class="">    
                                                        <div class="changeable text_area_test" contenteditable="true"  style="height:500px; width: 100%; display: inline-block; white-space: pre-line;">
                                                            <?php echo $content; ?>
                                                        </div>                                                                
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="test_stop_content">
                                            <p>After finished test, Clicking "<span>STOP</span>" and continue to next test</p>
                                            <div class="timer" id="timer"></div>
                                            <a href="javascript:void(0);" id="pouse" class="btn_sky pop_btn btn_stop" data-id="<?php echo $count; ?>">Stop</a>  
                                        </div>
                                    </div>                          
                                </div>
                                <?php
                                $count++;
                            endwhile;
                            ?>
                            <input type="hidden" id="hdntotaltest" name="hdntotaltest" value="<?php echo $count ?>" />
                            <div id="pop_start" class="pop_overlay open start_tests" style="display: none;">
                                <div class="pop_main">
                                    <div class="pop_head">
                                        <a href="javascript:void(0);"><i class="fa fa-remove" aria-hidden="true"></i></a>
                                    </div>
                                    <div class="pop_body">
                                        <div class="page_title">
                                            <h2 id="popuptitle"></h2>
                                        </div>
                                        <div class="pop_content">
                                            <div class="first_test">
                                                <p id="popupdesc">You will be prompted to move on to the next test.</p>
                                                <a href="#" id="startpopupid" class="btn_sky pop_btn startpopupid" >Start now</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php endif;
                        ?>   
                    </div>
                </div>
            </div>
        </div>
    </div>
</section> 
<?php get_footer(); ?>
<script>
    $(document).click(function (e) {
        e.stopPropagation();
        $('.user a').removeClass('open');
        $('.user .notify_content').slideUp('slow');
        $('#pop_start').fadeOut();
    });

    $(document).ready(function () {

        $('.text_area_test').keydown(function (e) {
            if (e.keyCode == 13) {
                document.execCommand('insertHTML', false, ' ');
            }
        });

        $('#note_<?php echo $current_testid; ?>').addClass('active');
        $('.blog_listt').css('display', 'none');
        $('#notContent_<?php echo $current_testid; ?>').css('display', 'block');


//        text_area_test


        var doc_detail = $('.text_area_test:visible').text().trim();
        var words = get_doc_word_count(doc_detail);
        $(".used_word .count").text(words);

        $('.text_area_test').keyup(function () {
            var doc_detail = $(this).text().trim();
            var words = get_doc_word_count(doc_detail);
            $(".used_word .count").text(words);
        });

        $('.text_area_test').bind("DOMNodeInserted", function () {
            if ($(this).text().trim() == "")
            {
                $(".used_word .count").text("0");
            } else
            {
                var doc_detail = $(this).text().trim();
                var words = get_doc_word_count(doc_detail);
                $(".used_word .count").text(words);
            }
        });


        $(".btn_sky.pop_btn.btn_stop").click(function ()
        {
            $('#loding').show();
            $('.test_err').remove();
            var test = ($(this).attr("data-id"));
            var id = parseInt(test) + 1;
            if (test == 1) {
                $("#popuptitle").text("Your First Test is Complete");

            } else if (test == 2) {
                $("#popuptitle").text("Your Second Test is Complete");
            } else if (test == 3) {
                $("#popuptitle").text("Your Third Test is Complete");
            } else if (test == 4) {
                $("#popuptitle").text("Your Fourth Test is Complete. Just one more!");
            }
            $("#startpopupid").attr("data-id", id);
            var desc = $(this).closest('div.blog_listt').find(".text_area_test").text().trim();
            var question = $(this).closest('div.blog_listt').find(".test_content").text().trim();
            var post_index = $(this).closest('div.blog_listt').find('.post_id').text().trim();
            var timer = $('#timer').text().trim();
            var test_index = $(this).closest('div.blog_listt').find('.test_id').text().trim()
            var data = new FormData();
            data.append('action', 'save_proofreader_test');
            data.append('word_desc', desc);
            data.append('test_question', question);
            data.append('post_id', post_index);
            data.append('test_timer', timer);
            data.append('test_id', test_index);
            if (desc != "")
            {
                $.ajax({
                    url: '<?php echo admin_url('admin-ajax.php'); ?>',
                    dataType: 'text',
                    cache: false,
                    contentType: false,
                    processData: false,
                    data: data,
                    type: 'post',
                    success: function (data) {
                        if (data == 1) {
                            if (test != 5) {
                                $('#pop_start').fadeIn();
                                $('#pop_start').addClass('open');
                            } else {
                                window.location.href = "<?php echo get_page_link(778); ?>";
                            }
                        } else
                        {
                            $('.test_stop_content').after('<span class="text-danger test_err">Try again later..</span>')
                        }
                        $('#loding').hide();
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        $('#loding').hide();
                        console.log(jqXHR + " :: " + textStatus + " :: " + errorThrown);
                    }
                });
            } else
            {
                $('.test_stop_content').after('<span class="text-danger test_err">No Data Found. Try again later..</span>')
            }
        });
        $("#startpopupid").click(function () {
            $('.test_err').remove();
            var newid = $(this).attr("data-id");
            var oldid = parseInt(newid) - 1;
            $("#pop_start").hide();
            $("#notContent_" + oldid).hide();
            $('.timer').timer('reset');
            $("#notContent_" + newid).show();
            $("#all_link li").removeClass("active");
            $("#all_link li:eq(" + oldid + ")").addClass("active");
            var doc_detail = $('.text_area_test:visible').text().trim();
            var words = get_doc_word_count(doc_detail);
            $(".used_word .count").text(words);
        });
    })
    //Popup
    $('.pop_main').click(function (e) {
        $()
        e.stopPropagation();
    });

    $('.pop_head a .fa-remove').click(function (e) {
        $('.test_err').remove();
        e.stopPropagation();
        $('#pop_start').fadeOut();
        $('#pop_start').removeClass('open');
    });
    (function () {
        $('.timer').timer({format: '%H:%M:%S'});
        $('#pouse').on('click', function () {
            $('.timer').timer('pause');
        });
    })();

    $(window).load(function () {

        $('#note_<?php echo $current_testid; ?>').addClass('active');
        $('.blog_listt').css('display', 'none');
        $('#notContent_<?php echo $current_testid; ?>').css('display', 'block');
    });

</script>