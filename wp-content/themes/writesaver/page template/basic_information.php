<?php
/*
 * Template Name: Basic information
 */

get_header();
$current_user = wp_get_current_user();
$user_roles_array = $current_user->roles;
$user_role = array_shift($user_roles_array);
if (!is_user_logged_in() || $user_role != "proofreader") {
    echo '<script>window.location.href="' . get_site_url() . '"</script>';
    exit;
}
$user_id = get_current_user_id();
?>
<section>
    <div class="breadcum">
        <div class="container">
            <div class="page_title">
                <h1><?php the_title(); ?></h1>
            </div>
        </div>
    </div>
</section>
<section>
    <div class="blog_category_sticky basic_information">
        <div class="container">
            <div class="blog_category_sticky_left">
                <div class="desktop_catagory">
                    <div class="all_category">
                        <div class="category_btn">
                            <div class="category_btn_icon">
                                <img src="<?php echo get_template_directory_uri() ?>/images/cat_icon.png" class="img-responsive">
                            </div>
                            <div class="category_btn_txt">
                                <span>Informations</span>
                            </div>
                        </div>
                        <div class="category_full_list">
                            <ul id="links">
                                <li class="active"><a href="javascript:void(0);"> Basic information  </a></li>
                                <li onclick=" $(this).unbind('click'); $('#links').unbind('click');"><a href="javascript:void(0);" onclick="return  false;">  Proofreading ability </a></li>                                                                     
                            </ul>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section>
    <div class="basic_info" id="vertical">
        <div class="container">
            <div class="row basic_info_inner">
                <div class="col-sm-2">
                    <div class="blog_category_sticky_left">
                        <div class="">
                            <ul id="all_link">
                                <li id="note_1"  class="active" onclick=" $('#all_link').unbind('click');">Personal info</li>
                                <li id="note_2"  onclick=" $('#all_link').unbind('click');">Education & University</li>
                                <li id="note_3"  onclick=" $('#all_link').unbind('click');">Country info</li>
                                <li id="note_4"  onclick=" $('#all_link').unbind('click');">Job History</li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-sm-10">

                    <div class="blog_category_main" id="notContent">
                        <div id="notContent_1" class="blog_listt">
                            <h4>Personal info </h4>
                            <div class="row">
                                <form method="post" id="proofreader_profile">
                                    <div class="col-sm-6">
                                        <?php
                                        if (is_user_logged_in()):
                                            $user_id = get_current_user_id();
                                            global $wpdb;
                                            $prefix = $wpdb->prefix;
                                            $usermeta = $prefix . 'usermeta';
                                            $result = $wpdb->get_results($wpdb->prepare("SELECT * FROM $usermeta WHERE user_id = %d ", $user_id));
                                            ?>

                                            <input type="text"  value="<?php echo $current_user->user_firstname; ?>"  maxlength="50"  class="contact_block only_alpha" name="fname" value="<?php echo $result[1]->meta_value ?>" id="fname" placeholder="First Name*">
                                        </div>
                                        <div class="col-sm-6">
                                            <input type="text"  value="<?php echo $current_user->user_lastname; ?>"  maxlength="50" class="contact_block only_alpha" name="lname" value="<?php echo $result[2]->meta_value ?>" id="lname" placeholder="Last Name*">
                                        </div>
                                        <div class="col-sm-6">
                                            <input type="text" maxlength="500" value="<?php echo get_usermeta($user_id, 'address1', TRUE) ?>"  class="contact_block" name="add1" id="add1" placeholder="Address line1*">
                                        </div>
                                        <div class="col-sm-6">
                                            <input type="text" maxlength="500" value="<?php echo get_usermeta($user_id, 'address2', TRUE) ?>"  class="contact_block" name="add2" id="add2" placeholder="Address line2">
                                        </div>
                                        <div class="col-sm-6">
                                            <input type="text" value="<?php echo get_usermeta($user_id, 'zip_code', TRUE) ?>"  class="contact_block" maxlength="20" name="zip_code" id="zip_code" placeholder="Zip Code*">
                                        </div>
                                        <div class="col-sm-6">
                                            <input type="text" maxlength="50"  class="contact_block only_alpha" name="P_state" id="P_state" placeholder="State" value="<?php echo get_usermeta($user_id, 'state', TRUE) ?>">
                                        </div>
                                        <div class="col-sm-12">
                                            <input type="submit" value="Continue" id="personal_button" class="btn_sky" />
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="profile_msg"></div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                            <div id="notContent_2" class="blog_listt">
                                <h4>Education & University</h4>
                                <?php $total_edu = get_user_meta($user_id, 'total_edu', TRUE); ?>
                                <div class="row">
                                    <form method="post" id="university_detail">
                                        <input type="hidden" id="edu_count" name="edu_count" value="<?php
                                        if ($total_edu > 0)
                                            echo $total_edu;
                                        else
                                            echo '1';
                                        ?>">
                                        <div class=" edu_parent">
                                            <?php
                                            if ($total_edu > 0):
                                                for ($i = 1; $i <= $total_edu; $i++) {
                                                    ?>
                                                    <div class="edu_info" id="uni_<?php echo $i; ?>">
                                                        <div class="col-sm-6">
                                                            <input type="text" data-degree="<?php echo get_usermeta($user_id, 'degree' . $i, TRUE) ?>" placeholder="Degree of*" class="university_info edu_degree contact_block" id='degree<?php echo $i; ?>' name='degree<?php echo $i; ?>' value="<?php echo get_usermeta($user_id, 'degree' . $i, TRUE) ?>"  >
                                                        </div>
                                                        <div class="col-sm-5">
                                                            <input type="text" data-uni="<?php echo get_usermeta($user_id, 'university' . $i, TRUE) ?>" placeholder="University*" class="university_info edu_uni contact_block" id='uni<?php echo $i; ?>' name='uni<?php echo $i; ?>' value="<?php echo get_usermeta($user_id, 'university' . $i, TRUE) ?>">
                                                        </div>
                                                        <?php if ($i != 1): ?>
                                                            <div class="col-sm-1">
                                                                <a  class="del_edu old_edu" href="javascript:void(0);" data-edu="<?php echo $i; ?>"><i class="fa fa-trash-o" aria-hidden="true"></i></a>
                                                            </div>
                                                        <?php endif; ?>
                                                    </div>
                                                <?php } else: ?>
                                                <div class="edu_info" id="uni_1">
                                                    <div class="col-sm-6">
                                                        <input type="text" data-degree="" placeholder="Degree of*" class="university_info edu_degree contact_block" id='degree1' name='degree1'   >
                                                    </div>
                                                    <div class="col-sm-5">
                                                        <input type="text" data-uni="" placeholder="University*" class="university_info edu_uni contact_block" id='uni1' name='uni1' >
                                                    </div>

                                                </div>
                                            <?php endif; ?>
                                        </div>
                                        <div class="col-sm-12">
                                            <a href="javascript:void(0);" class="add_edu" >Add new</a>
                                        </div>
                                        <div class="col-sm-12">
                                            <input type="submit" value="Continue" id="degree_uni_button" class="btn_sky" />
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="university_msg"></div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                            <div id="notContent_3" class="blog_listt">
                                <h4>Country info</h4>
                                <div class="row">
                                    <form method="post" id="country_detail">
                                        <div class="col-sm-6">
                                            <input class="contact_block only_alpha"  type="text" id='country_citizenship' name='country_citizenship' value="<?php echo get_usermeta($user_id, 'country_citizenship', TRUE) ?>" placeholder="Country of Citizenship*" >
                                        </div>
                                        <div class="col-sm-6">
                                            <input class="contact_block only_alpha" type="text" name='country_cresidence' id='country_cresidence' value="<?php echo get_usermeta($user_id, 'country_cresidence', TRUE) ?>" placeholder="Country of Residence*" >
                                        </div>


                                        <div class="col-sm-12">
                                            <input type="submit" id="save_country" value="Continue" class="btn_sky" />
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="country_msg" ></div>
                                        </div>
                                    </form>

                                </div>
                            </div>
                            <div id="notContent_4" class="blog_listt" >
                                <h4>Job History</h4>
                                <?php
                                $jobs = $wpdb->get_results("SELECT * FROM job_history WHERE proof_id = $user_id");
                                $total_job = count($jobs);
                                ?>
                                <div class="row">
                                    <form method="post" id="job_detail">
                                        <input type="hidden" id="job_count" name="job_count" value="<?php
                                        if ($total_job > 0)
                                            echo $total_job;
                                        else
                                            echo '1';
                                        ?>">
                                        <input type="hidden" value="" id="del_job" name="del_job" />
                                        <input type="hidden" value="" id="rem_resume" name="rem_resume" />
                                        <div class="job_parent">
                                            <?php
                                            if ($total_job > 0):
                                                $count = 0;
                                                foreach ($jobs as $job) {
                                                    $count++;
                                                    ?>
                                                    <div class="job_history" id="job_<?php echo $count; ?>">

                                                        <div class="col-sm-6">
                                                            <input value="<?php echo $job->company_name; ?>"  type="text" data-cname="<?php echo $job->company_name; ?>" placeholder="Company Name*" class="job_info job_cname contact_block" id='cname<?php echo $count; ?>' name='cname[]'   >
                                                        </div>
                                                        <div class="col-sm-5">
                                                            <input  value="<?php echo $job->designation; ?>" type="text" data-designation="<?php echo $job->designation; ?>" placeholder="Designation*" class="job_info job_des contact_block" id='designation<?php echo $count; ?>' name='designation[]' >
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <input  value="<?php echo date("d-m-Y", strtotime($job->start_date)); ?>"   type="text" data-start_date="<?php echo date("d-m-Y", strtotime($job->start_date)); ?>" placeholder="Start Date*" class="job_date job_info job_sdate contact_block" id='sdate<?php echo $count; ?>' name='sdate[]'   >
                                                        </div>
                                                        <div class="col-sm-5">
                                                            <input  value="<?php echo date("d-m-Y", strtotime($job->end_date)); ?>"  type="text" data-end_date="<?php echo date("d-m-Y", strtotime($job->end_date)); ?>" placeholder="End Date*" class="job_date job_info job_edate contact_block" id='edate<?php echo $count; ?>' name='edate[]'   >
                                                        </div>
                                                        <div class="col-sm-1">
                                                            <a  class="del_job old_job delete" href="javascript:void(0);" data-job="<?php echo $count; ?>"><i class="fa fa-trash-o"></i></a>
                                                            <input type="hidden" class="job_id" name="job_id[]" value="<?php echo $job->job_id; ?>"/>
                                                        </div>
                                                    </div>
                                                <?php } else: ?>
                                                <div class="job_history" id="job_1">
                                                    <div class="col-sm-6">
                                                        <input type="text" data-cname="" placeholder="Company Name*" class="job_info job_cname contact_block" id='cname1' name='cname[]'   >
                                                    </div>
                                                    <div class="col-sm-5">
                                                        <input type="text" data-designation="" placeholder="Designation*" class="job_info job_des contact_block" id='designation1' name='designation[]' >
                                                    </div>
                                                    <div class="col-sm-6">
                                                        <input type="text" data-start_date="" placeholder="Start Date*" class="job_date job_info job_sdate contact_block" id='sdate1' name='sdate[]'   >
                                                    </div>
                                                    <div class="col-sm-5">
                                                        <input type="text" data-end_date="" placeholder="End Date*" class="job_date job_info job_edate contact_block" id='edate1' name='edate[]'   >
                                                    </div>
                                                    <div class="col-sm-1">
                                                        <a  class="del_job old_job delete" href="javascript:void(0);" data-job="1"><i class="fa fa-trash-o"></i></a>
                                                        <input type="hidden" class="job_id" name="job_id[]" value=""/>
                                                    </div>
                                                </div>
                                            <?php endif; ?>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="fileinput fileinput-exists" data-provides="fileinput">
                                                <span class="btn btn-default btn-file"><span>Upload Resume</span>
                                                    <input type="hidden" value="" name="">                                                        
                                                    <input type="file"  name="upload_resume" id="upload_resume"  class="fileinput_info job_info" onchange="validateresumeFile(this.value, 'upload_resume')"/>
                                                </span>
                                                <span class="fileinput-filename">No file chosen</span>
                                                <span class="fileinput-new">No file chosen</span>
                                                <div class="pic_msg"></div>
                                            </div>
                                            <?php
                                            if (get_user_meta($user_id, 'job_resume', true)):
                                                echo '<div class="resume_div">';
                                                $url = get_user_meta($user_id, 'job_resume', true);
                                                echo '<a href="' . $url . '" target="_blank">' . $name = basename($url) . '</a>';
                                                echo '<a href="javascript:void(0);" class="remove_resume"><i class="fa fa-times"></i>Remove</a>';
                                                echo '</div>';
                                            endif;
                                            ?>

                                        </div>
                                        <div class="col-sm-12">
                                            <a href="javascript:void(0);" class="add_job" >Add new</a>
                                        </div>
                                        <div class="col-sm-12">
                                            <input type="submit" value="Continue" id="job_history_button" class="btn_sky" />
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="job_msg"></div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    <?php endif;
                    ?>
                    </form>       
                </div>
            </div>
        </div>
    </div>
    <input type="hidden" value="<?php echo get_the_permalink(776) ?>" id="review_url"/>
</section>

<script>
    function validateresumeFile(file, resume_name) {
        debugger;
        $('.pic_msg1').remove();
        var ext = file.split(".");
        ext = ext[ext.length - 1].toLowerCase();
        var arrayExtensions = ["pdf", "doc", "docx"];
        if (arrayExtensions.lastIndexOf(ext) == -1) {
            $(".pic_msg").html('<span class="text-danger job_err ">Wrong extension type.Please upload valid file</span>');
            $(".job_err").fadeOut(5000);
            $("#upload_resume").val('');
            return false;
        } else {
            return true;
        }
    }
//Compare dates 
    function compareDate(start_date, end_date) {
        var date = start_date.substring(0, 2);
        var month = start_date.substring(3, 5);
        var year = start_date.substring(6, 10);
        var start_date = new Date(year, month - 1, date);
        var currentDate = new Date();
        if (end_date != '') {
            var edate = end_date.substring(0, 2);
            var emonth = end_date.substring(3, 5);
            var eyear = end_date.substring(6, 10);
            var end_date = new Date(eyear, emonth - 1, edate);
            if (end_date > currentDate) {
                return false;
            } else if (end_date < start_date) {
                return false;
            } else {
                return true;
            }
        } else {
            if (start_date > currentDate) {
                return false;
            } else {
                return true;
            }
        }
    }

    jQuery(document).ready(function ($) {
        
        //Make previous link clickable
        $('.basic_info_inner #all_link ').on('click', 'li', function () {
            debugger;
            var active_index = $('.basic_info_inner #all_link li.active').index();
            var next_index = $(this).index();
            if (next_index < active_index)
            {
                $('.blog_category_main .blog_listt:eq(' + eval(active_index) + ')').hide();
                $('.blog_category_main .blog_listt:eq(' + eval(next_index) + ')').show();
                $('#all_link  li.active').removeClass('active');
                $(this).addClass('active');
            } else {
                return false;
            }
        });

        /***datepicker***/
        jQuery('.job_date').datepicker({
            format: 'dd-mm-yyyy'
        });

        $(".remove_resume").live('click', function (e) {

            $('.resume_div').remove();
            $('#rem_resume').val(1);
        });


        $(".new_job").live('click', function (e) {

            var id = $(this).attr("data-history");
            var job_count = $('#job_count').val();
            job_count--;
            $("#job_" + id).remove();
            $('#job_count').val(job_count);
            var total_flag = 0;
            $(".job_history:visible").each(function (i) {

                var id = this.id;
                if ($('#' + id + ':visible')) {
                    total_flag = total_flag + 1;
                    $(this).attr('id', 'job_' + total_flag);
                    $('#job_' + total_flag + ' a.del_job').attr('data-history', total_flag);
                }

            });
            $('#job_count').val(total_flag);
        });
        $('.add_job').click(function (event) {
            event.preventDefault();
            var job_count = parseInt($('#job_count').val()) + 1;
            $('#job_count').val(job_count);
            $('.job_parent').append('<div class="job_history" id="job_' + job_count + '">' +
                    '<div class="col-sm-6">' +
                    '<input type="text" data-cname="" placeholder="Company Name*" class="job_info job_cname contact_block" id="cname' + job_count + '" name="cname[]" >' +
                    '</div><div class="col-sm-5">' +
                    '<input class="job_info job_des contact_block" data-designation placeholder="Designation*" type="text" id="designation' + job_count + '" name="designation[]" >' +
                    '</div>' +
                    '<div class="col-sm-6">' +
                    '<input type="text" data-start_date="" placeholder="Start Date*" class="job_date job_info job_sdate contact_block" id="sdate' + job_count + '" name="sdate[]" >' +
                    '</div><div class="col-sm-5">' +
                    '<input type="text" data-end_date="" placeholder="End Date*" class="job_date job_info job_edate contact_block" id="edate' + job_count + '" name="edate[]" >' +
                    '</div>' +
                    '<div class="col-sm-1"><a data-history="' + job_count + '" class="del_job new_job delete"  href="javascript:void(0);"><i class="fa fa-trash-o"></i></a></div></div>');
            jQuery('.job_date').datepicker({format: 'dd-mm-yyyy'});
            return false;
        });
        $(".new_edu").live('click', function (e) {

            var id = $(this).attr("data-edu");
            var edu_count = $('#edu_count').val();
            edu_count--;
            $("#uni_" + id).remove();
            $('#edu_count').val(edu_count);
            var total_flag = 0;
            $(".edu_info").each(function (i) {
                var id = this.id;
                if ($('#' + id + ':visible')) {

                    total_flag = total_flag + 1;
                    $(this).attr('id', 'uni_' + total_flag);
                    $('#uni_' + total_flag + ' a.del_edu').attr('data-edu', total_flag);
                }
                $('#edu_count').val(total_flag);
            });
        });
        $(".old_edu").live('click', function (e) {

            var id = $(this).attr("data-edu");
            $("#uni_" + id).hide();
        });
        $(".old_job").live('click', function (e) {
            var del_job = $('#del_job').val();
            var job_id = $(this).next('.job_id').val();
            var id = $(this).attr("data-job");
            if (del_job)
                var deleted_ids = del_job + ',' + job_id;
            else
                var deleted_ids = job_id;
            $("#job_" + id).hide();
            $('#del_job').val(deleted_ids);
        });
        $('.add_edu').click(function (event) {
            event.preventDefault();
            var counter = parseInt($('#edu_count').val()) + 1;
            $('#edu_count').val(counter);
            $('.edu_parent').append('<div class="edu_info" id="uni_' + counter + '"><div class="col-sm-6">' +
                    '<input type="text" data-degree="" placeholder="Degree of*" class="contact_block university_info edu_degree" id="degree' + counter + '" name="degree' + counter + '" >' +
                    '</div><div class="col-sm-5">' +
                    '<input class="university_info edu_uni contact_block" data-uni="" placeholder=" University*" type="text" id="uni' + counter + '" name="uni' + counter + '" >' +
                    '</div>' +
                    '<div class="col-sm-1"><a data-edu="' + counter + '" class="del_edu new_edu"  href="javascript:void(0);"><i class="fa fa-trash-o" aria-hidden="true"></i></a></div></div>');
            return false;
        });
        $('#personal_button').click(function () {

            var button = $(this);
            $('.pro_err').remove();
            var fname = $('#fname').val();
            var lname = $('#lname').val();
            var add1 = $('#add1').val();
            var add2 = $('#add2').val();
            var zip_code = $('#zip_code').val();
            var P_state = $('#P_state').val();
            var error = false;
            if (fname == '')
            {
                $('#fname').after('<span class="text-danger pro_err">First name is required.</span>');
                error = true;
            }
            if (lname == '')
            {
                $('#lname').after('<span class="text-danger pro_err">Last name is required.</span>');
                error = true;
            }
            if (add1 == '')
            {
                $('#add1').after('<span class="text-danger pro_err">Address is required.</span>');
                error = true;
            }

            if (zip_code == '')
            {
                $('#zip_code').after('<span class="text-danger pro_err">Zip code is required.</span>');
                error = 1;
            }
            
            if (error == false) {
                $('#loding').show();
                $.ajax({
                    type: "POST",
                    url: "<?php echo admin_url('admin-ajax.php'); ?>",
                    data: $('#proofreader_profile').serialize() + "&action=save_basic",
                    dataType: "json",
                    success: function (data) {
                        $('.user_fname').text(fname);
                        $(".profile_msg").html(data['message']);
                        $(".pic_msg1").fadeOut(5000);
                        $('#all_link  li.active').removeClass('active');
                        $('.blog_category_sticky_left #all_link li:eq(' + eval(button.closest('div.blog_listt').index()) + ')').removeClass('active').next().addClass('active');
                        $('.blog_category_main .blog_listt:eq(' + eval(button.closest('div.blog_listt').index()) + ')').hide().next().show();
                        $('#loding').hide();
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        console.log(jqXHR + " :: " + textStatus + " :: " + errorThrown);
                        $('#loding').hide();
                    }
                });
            }
            return false;
        });
        $('#job_history_button').click(function () {
            debugger;
            var button = $(this);
            $('.job_err').remove();
            var statusflag = true;
            $('.job_cname:visible').each(function () {
                if ($(this).val() == "") {
                    statusflag = false;
                    $(this).after('<span class="text-danger job_err">Company name is required.</span>');
                }
            });
            $('.job_des:visible').each(function () {
                if ($(this).val() == "") {
                    statusflag = false;
                    $(this).after('<span class="text-danger job_err">Designation is required.</span>');
                }
            });
            $('.job_sdate:visible').each(function () {
                if ($(this).val() == "") {
                    statusflag = false;
                    $(this).after('<span class="text-danger job_err">Start date is required.</span>');
                } else {

                    var start_date = $(this).val();
                    var sdate_flag = compareDate(start_date, '');
                    if (sdate_flag == false) {
                        statusflag = false;
                        $(this).after('<span class="text-danger job_err">Enter valid start date.</span>');
                    }
                }
            });
            $('.job_edate:visible').each(function () {
                if ($(this).val() == "") {
                    statusflag = false;
                    $(this).after('<span class="text-danger job_err">End date is required.</span>');
                } else {
                    var end_date = $(this).val();
                    var start_date = $(this).parent().prev().find('.job_sdate').val();
                    var edate_flag = compareDate(start_date, end_date);
                    if (edate_flag == false) {
                        statusflag = false;
                        $(this).after('<span class="text-danger job_err">Enter valid end date.</span>');
                    }
                }
            });
            if (statusflag == true) {
                debugger;
                var form = $("#job_detail")[0];
                var formdata = new FormData(form);
                formdata.append('action', 'save_jobhistory');
                formdata.append('info', 1);
                $('#loding').show();
                $.ajax({
                    type: "POST",
                    url: "<?php echo admin_url('admin-ajax.php'); ?>",
                    data: formdata,
                    contentType: false,
                    processData: false,
                    success: function (data) {
                        console.log(data);
                        if (data == 1) {
                            $(".job_msg").html('<span class="text-success job_err">Updated successfully...</span>');
                            setTimeout(function () {
                                window.location.href = jQuery('#review_url').val();
                            }, 1000);
                        } else
                            $(".job_msg").html('<span class="text-danger job_err">Not updated successfully...</span>');
                        $(".job_err").fadeOut(5000);
                        $('#loding').hide();
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        console.log(jqXHR + " :: " + textStatus + " :: " + errorThrown);
                        $('#loding').hide();
                    }
                });
            }
            return false;
        });
        $('#degree_uni_button').click(function () {
            debugger;
            var button = $(this);
            $('.university_err').remove();
            var statusflag = true;
            $('.edu_degree:visible').each(function () {
                if ($(this).val() == "") {
                    statusflag = false;
                    $(this).after('<span class="text-danger university_err">Degree is required.</span>');
                }
            });
            $('.edu_uni:visible').each(function () {
                if ($(this).val() == "") {
                    statusflag = false;
                    $(this).after('<span class="text-danger university_err">University is required.</span>');
                }
            });
            if (statusflag == true) {
                var education = [];
                $(".edu_info:visible").each(function (i) {
                    var id = this.id;
                    var edu_degree = $('#' + id + '  .edu_degree').val();
                    var edu_uni = $('#' + id + ' .edu_uni').val();
                    education[i] = '{"edu_degree":"' + edu_degree + '","edu_uni":"' + edu_uni + '"}';
                });
                $('#loding').show();
                $.ajax({
                    type: "POST",
                    url: "<?php echo admin_url('admin-ajax.php'); ?>",
                    data: {
                        action: 'save_university',
                        education: education
                    },
                    success: function (data) {

                        if (data == 1) {
                            $(".university_msg").html('<span class="text-success university_err">Updated successfully...</span>');
                            $('#all_link  li.active').removeClass('active');
                            $('.blog_category_sticky_left #all_link li:eq(' + eval(button.closest('div.blog_listt').index()) + ')').removeClass('active').next().addClass('active');
                            $('.blog_category_main .blog_listt:eq(' + eval(button.closest('div.blog_listt').index()) + ')').hide().next().show();
                        } else
                            $(".university_msg").html('<span class="text-danger university_err">Not Updated successfully...</span>');
                        $(".university_err").fadeOut(5000);
                        $('#loding').hide();
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        console.log(jqXHR + " :: " + textStatus + " :: " + errorThrown);
                        $('#loding').hide();
                    }
                });
            }
            return false;
        });
        $('#save_country').click(function () {
            var button = $(this);
            $('.country_err').remove();
            var country_citizenship = $('#country_citizenship').val();
            var country_cresidence = $('#country_cresidence').val();
            var error = false;
            if (country_citizenship == '')
            {
                $('#country_citizenship').after('<span class="text-danger country_err">Country of citizenship is required</span>');
                error = true;
            }
            if (country_cresidence == '')
            {
                $('#country_cresidence').after('<span class="text-danger country_err">Country of residence is required.</span>');
                error = true;
            }

            if (error == false) {
                $('#loding').show();
                $.ajax({
                    type: "POST",
                    url: "<?php echo admin_url('admin-ajax.php'); ?>",
                    data: $('#country_detail').serialize() + "&action=save_country&info=1",
                    success: function (data) {

                        if (data == 1) {
                            $(".country_msg").html('<span class="text-success country_err">Updated successfully...</span>');
                            $('#all_link  li.active').removeClass('active');
                            $('.blog_category_sticky_left #all_link li:eq(' + eval(button.closest('div.blog_listt').index()) + ')').removeClass('active').next().addClass('active');
                            $('.blog_category_main .blog_listt:eq(' + eval(button.closest('div.blog_listt').index()) + ')').hide().next().show();
                            setTimeout(function () {
                                //  jQuery('.country_err').fadeOut('slow');
                                // window.location.href = jQuery('#review_url').val();
                            }, 1500);
                        } else
                            $(".country_msg").html('<span class="text-danger country_err">Not updated successfully...</span>');
                        $(".country_err").fadeOut(5000);
                        $('#loding').hide();
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        console.log(jqXHR + " :: " + textStatus + " :: " + errorThrown);
                        $('#loding').hide();
                    }
                });
            }
            return false;
        });
    });
</script>

<?php get_footer(); ?>

