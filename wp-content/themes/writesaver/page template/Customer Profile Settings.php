<?php
/*
 * Template Name: customer_profile_settings
 */

get_header();
global $wpdb;
$current_user = wp_get_current_user();
$user_roles_array = $current_user->roles;
$user_role = array_shift($user_roles_array);
if (!is_user_logged_in() || $user_role != "customer") {
    echo '<script>window.location.href="' . get_site_url() . '"</script>';
    exit;
}



$result_countries = $wpdb->get_results("SELECT * from countries");



$prefix = $wpdb->prefix;
$notification_table = $prefix . 'notification_settings';

$user_id = get_current_user_id();
$result = $wpdb->get_results($wpdb->prepare("SELECT * FROM $notification_table WHERE user_id = %d ", $user_id));
$credit_result = $wpdb->get_results("SELECT * FROM wp_creditdebit_card_details WHERE customer_id = $user_id LIMIT 1 ");
$paypal_result = $wpdb->get_results("SELECT * FROM tbl_customer_general_info WHERE fk_customer_id = $user_id LIMIT 1 ");


$dash_doc_started = "";
$dash_doc_completed = "";
$receive_doc_started = "";
$receive_doc_completed = "";
$receive_stories = "";


if (count($result) > 0) {
    $dash_doc_started = ($result[0]->dash_doc_started == 1 ? "checked" : "");
    $dash_doc_completed = ($result[0]->dash_doc_completed == 1 ? "checked" : "");
    $receive_doc_started = ($result[0]->receive_doc_started == 1 ? "checked" : "");
    $receive_doc_completed = ($result[0]->receive_doc_completed == 1 ? "checked" : "");
    $receive_stories = ($result[0]->receive_stories == 1 ? "checked" : "");
}

$old_profile_pic = get_user_meta($user_id, 'profile_pic_url', true);
$dummy_image = get_template_directory_uri() . '/images/customer.png';
if ($old_profile_pic) {
    $profile_pic = $old_profile_pic;
    $display = 'block';
} else {
    $profile_pic = $dummy_image;
    $display = 'none';
}

global $current_user;
$user_info = $wpdb->get_results("SELECT * FROM tbl_customer_general_info WHERE fk_customer_id = $current_user->ID LIMIT 1");
?>
<section>
    <div class="breadcum">
        <div class="container">
            <div class="page_title">
                <h1>Settings</h1>
            </div>
        </div>
    </div>
</section>
<section>
    <div id="main_blog" class="profile">
        <div class="blog_category_sticky">
            <div class="container">
                <div class="blog_category_sticky_left">
                    <div class="desktop_catagory">
                        <div class="category_full_list">
                            <div class="category_btn">
                                <div class="category_btn_icon">
                                    <img src="<?php echo get_template_directory_uri() ?>/images/cat_icon.png" class="img-responsive">
                                </div>
                                <div class="category_btn_txt">
                                    <span>settings</span>
                                </div>
                            </div>
                            <ul id="links">
                                <li id="note_1" class="active"> Profile settings </li>
                                <li id="note_2" > Subscription settings</li>
                                <li id="note_3"> Billing settings</li>
                                <li id="note_4"> Notification settings</li>
                            </ul>
                        </div>

                    </div>
                </div>
            </div>
        </div>
        <div class="blog_category_main" id="notContent">
            <div class="container">
                <div id="notContent_1" class="blog_listt">
                    <form id="updatecustomerprofile" method="post" action="#">
                        <div class="row">
                            <div class="col-sm-2">
                                <div class="main_profile_img_block">
                                    <div class="main_profile_img">
                                        <img src="<?php echo $profile_pic; ?>" alt="" id="profile_pic" class="image-responsive output" />
                                        <input type="hidden" id="old_pic" value="<?php echo $profile_pic; ?>" />
                                        <input type="hidden" id="dummy_pic" value="<?php echo $dummy_image; ?>" />

                                    </div>

                                    <div class="main_profile_remove" style="display: none">
                                        <a href="javascript:void(0);" class="red_link remove_old_img" style="display: <?php echo $display; ?>">Remove</a>
                                        <a href="javascript:void(0);" class="red_link remove_img" style="display: none">Remove</a>

                                        <input type="hidden" name="remove_image" id="remove_image" value="" />
                                        <div class="fileinput fileinput-exists" data-provides="fileinput">
                                            <span class="btn btn-default btn-file"><span>Upload Profile</span>
                                                <input type="hidden" value="" name="">                                                        
                                                <input type="file" name="profile_pic" id="profile_picture"  class="fileinput_info" onchange="allvalidateimageFile(this.value, 'profile_img')"/>
                                            </span>
                                            <span class="fileinput-filename">No file chosen</span>
                                            <span class="fileinput-new">No file chosen</span>
                                            <div class="pic_msg"></div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <div class="col-sm-10">
                                <div class="setting_right">
                                    <div class="field_title">
                                        <h4>Personal info </h4>
                                        <div class="edit_link">
                                            <a href="javascript:void(0);" class="edit_pro" id='edit_info'></a>      </div>
                                    </div>
                                    <div class="all_fields pro_msg">
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <input type="text" data-fname="<?php echo $current_user->user_firstname; ?>" class="pro_info only_alpha" placeholder="First Name*" id="fname" name="fname" value="<?php echo $current_user->user_firstname; ?>" disabled="">
                                            </div>
                                            <div class="col-sm-6">
                                                <input type="text"  data-lname="<?php echo $current_user->user_lastname; ?>"  class="pro_info only_alpha"  id="lname" placeholder="Last Name*" name="lname" value="<?php echo $current_user->user_lastname; ?>" disabled="true">
                                            </div>
                                        </div>
                                        <div class="profile_msg"></div>
                                    </div>
                                    <div class="field_title">
                                        <h4>Email</h4>
                                    </div>
                                    <div class="all_fields ">
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <input type="text"  value="<?php echo $current_user->user_email; ?>" disabled>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="field_title">
                                        <h4>Change password</h4>
                                        <div class="edit_link">
                                            <a href="#" class="chg_pass edit_pro_chg"></a>
                                        </div>
                                    </div>
                                    <div class="main_profile_delete">
                                        <a href="javascript:void(0);" class="red_link " onclick="delete_acc(<?php echo $user_id; ?>)" >Delete account</a>
                                        <div class="del_msg"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <div id="notContent_2" class="blog_listt">
                    <?php
                    if (is_user_logged_in() && function_exists('pmpro_hasMembershipLevel') && pmpro_hasMembershipLevel()) {

                        $current_user->membership_level = pmpro_getMembershipLevelForUser($current_user->ID);
                    }
                    ?> 
                    <div class="row">
                        <div class="col-sm-12">
                            <h4>Current plan</h4>
                            <div class="total_ammount credit current <?php echo (empty($current_user->membership_level)) ? "not_found" : ""; ?>">
                                <?php if (!empty($current_user->membership_level)): ?>
                                    <div class="left">
                                        <h4><?php echo $current_user->membership_level->plan_words; ?><span>Words<?php echo ($current_user->membership_level->expiration_number > 1) ? '/' . $current_user->membership_level->expiration_number : ''; ?><?php echo $current_user->membership_level->expiration_period; ?></span></h4>
                                    </div>
                                    <div class="right_cont">
                                        <h4>$<?php echo $current_user->membership_level->billing_amount; ?></h4>
                                        <p> You're currently a '<?php echo $current_user->membership_level->name; ?>' subscriber. Upgrade now to get more words per month and save!</p>
                                    </div>
                                    <a href="<?php echo pmpro_url("checkout", "?level=" . $current_user->membership_level->id, "https") ?>" class="btn_sky">Upgrade Plan</a>
                                    <?php
                                else:
                                    echo "<p>You have no plan purchased yet..</p>";
                                endif;
                                ?>
                            </div>
                        </div>
                        <div class="col-sm-6 privacy customer">
                            <h4>Remaining words</h4>
                            <div class="total_ammount credit">
                                <div class="left">
                                    <h4><?php echo ($user_info[0]->remaining_credit_words) ? $user_info[0]->remaining_credit_words : '0'; ?><span>Words</span></h4>
                                    <p>Remaining credit</p>
                                </div>
                                <div class="right"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="notContent_3" class="blog_listt">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="setting_right">
                                <div class="field_title">
                                    <h4>Billing Methods</h4>
                                </div>
                                <div class="">
                                    <div class="billing_table_responsive">
                                        <div class="billing_table">
                                            <div class="customer_billing_methods">
                                                <div class="billing_image">
                                                    <img src="<?php echo get_template_directory_uri() ?>/images/paypal.png" alt="paypal">
                                                </div>
                                                <div class="billing_type">
                                                    <p>Paypal</p>
                                                </div>
                                                <div class="bill_popup">
                                                    <a href="#" role="button" data-toggle="modal" data-target="#paypal_modal" class="paypal_pop  btn_sky">Set up</a>                                                       
                                                </div>
                                            </div>
                                            <div class="customer_billing_methods">
                                                <div class="billing_image">
                                                    <img src="<?php echo get_template_directory_uri() ?>/images/credit_debit.png" alt="paypal">
                                                </div>
                                                <div class="billing_type">
                                                    <p>Credit or debit card</p>
                                                </div>
                                                <div class="bill_popup">
                                                    <a href="javascript:void(0);" class="btn_sky pop_btn">Set up</a>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="notContent_4" class="blog_listt">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="setting_right">
                                <div class="field_title">
                                    <h4>Dashboard notification</h4>
                                </div>
                                <div class="setting_checkbox">
                                    <div class="s_checkbox">
                                        <input id="checkbox-1"  <?php echo $dash_doc_started ?> class="checkbox-custom" name="checkbox-1" type="checkbox" data-id="dash_doc_started">
                                        <label for="checkbox-1" class="checkbox-custom-label">A document you submitted has been started</label>
                                    </div>
                                    <div class="s_checkbox">
                                        <input id="checkbox-2"   <?php echo $dash_doc_completed; ?> class="checkbox-custom " name="checkbox-2" type="checkbox" data-id="dash_doc_completed">
                                        <label for="checkbox-2" class="checkbox-custom-label">A document you submitted has been completed</label>
                                    </div>
                                </div>
                                <div class="field_title">
                                    <h4>Receive notification via email</h4>
                                </div>
                                <div class="setting_checkbox last_field_checkbox">
                                    <div class="s_checkbox">
                                        <input id="checkbox-3"  <?php echo $receive_doc_started ?>  class="checkbox-custom" name="checkbox-3" type="checkbox" data-id="receive_doc_started">
                                        <label for="checkbox-3" class="checkbox-custom-label">A document you submitted has been started </label>
                                    </div>
                                    <div class="s_checkbox">
                                        <input id="checkbox-4"  <?php echo $receive_doc_completed ?> class="checkbox-custom chng_setting" name="checkbox-4" type="checkbox" data-id="receive_doc_completed">
                                        <label for="checkbox-4" class="checkbox-custom-label">A document you submitted has been completed</label>
                                    </div>
                                    <div class="s_checkbox">
                                        <input id="checkbox-5"   <?php echo $receive_stories ?> class="checkbox-custom" name="checkbox-5" type="checkbox" data-id="receive_stories">
                                        <label for="checkbox-5" class="checkbox-custom-label">We publish stories relevant to your interests</label>
                                    </div>
                                </div>
                                <div class="noti_msg"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>  
<div id="paypal_modal" class="pop_overlay" style="display: none;">
    <div class="pop_main">
        <div class="pop_head">
            <a href="javascript:void(0);" data-dismiss="modal" type="button" ><i class="fa fa-remove"  aria-hidden="true"></i></a>
        </div>
        <div class="pop_body">
            <div class="page_title">
                <h2>Add a Paypal</h2>
                <h4>Payment information</h4>
            </div>
            <div class="pop_content">
                <form method="post" id="cust_paypal" autocomplete="off">
                    <div class="row">
                        <div class="col-sm-offset-2 col-sm-8">
                            <input value="<?php echo $paypal_result[0]->paypal_id; ?>" type="email" class="contact_block" name="paypal_id" id="paypal_id"  maxlength="100"  placeholder="Paypal Id*">
                        </div>
                        <div class="buttons col-sm-12">
                            <input type="button" id="add_paypal" value="Add Id" class="btn_sky">
                            <input data-dismiss="modal" type="button" id="close_paypal_model" value="Cancel" class="btn_sky">
                        </div>
                        <div class="paypal_msg"></div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<div id="Paypal" class="pop_overlay open" style="display: none;">
    <div class="pop_main">
        <div class="pop_head">
            <a href="javascript:void(0);"><i class="fa fa-remove" aria-hidden="true"></i></a>
        </div>
        <div class="pop_body">
            <div class="page_title">
                <h2>Add a credit or debit card</h2>
                <h4>Payment information</h4>
            </div>
            <div class="pop_content">
                <form id="CreditDebitCardDetails" name="CreditDebitCardDetails">

                    <div class="row">

                        <div class="col-sm-6">

                            <input value="<?php echo $credit_result[0]->firstname; ?>" type="text" name="firstname" id="firstname" placeholder="First Name*" class="only_alpha contact_block" maxlength="200" />
                            <div><span id="errorfirstname" style="color:red;"></span></div>
                        </div>

                        <div class="col-sm-6">
                            <input  type="text" value="<?php echo $credit_result[0]->lastname; ?>" id="lastname" name="lastname" placeholder="Last Name*" class="only_alpha contact_block" maxlength="200"/>
                            <div><span id="errorlastname" style="color:red;"></span></div> 
                        </div>

                        <div class="col-sm-12">

                            <input type="text" value="<?php echo decrypt_string($credit_result[0]->cardnumber); ?>" id="cardno" name="cardno" placeholder="Card Number*" class="only_num contact_block" maxlength="16" />
                            <div><span id="errorcardno" style="color:red;"></span></div> 
                        </div>

                        <div class="col-sm-6">

                            <select class="contact_block" id="expdate" name="expdate">

                                <option>Expiration Month</option>

                                <option <?php echo ($credit_result[0]->expirymonth == 1) ? "selected" : ''; ?>>01</option>

                                <option <?php echo ($credit_result[0]->expirymonth == 2 ) ? "selected" : ''; ?>>02</option>

                                <option <?php echo ($credit_result[0]->expirymonth == 3 ) ? "selected" : ''; ?>>03</option>

                                <option <?php echo ($credit_result[0]->expirymonth == 4 ) ? "selected" : ''; ?>>04</option>

                                <option <?php echo ($credit_result[0]->expirymonth == 5 ) ? "selected" : ''; ?>>05</option>

                                <option <?php echo ($credit_result[0]->expirymonth == 6 ) ? "selected" : ''; ?>>06</option>

                                <option <?php echo ($credit_result[0]->expirymonth == 7 ) ? "selected" : ''; ?>>07</option>

                                <option <?php echo ($credit_result[0]->expirymonth == 8 ) ? "selected" : ''; ?>>08</option>

                                <option <?php echo ($credit_result[0]->expirymonth == 9 ) ? "selected" : ''; ?>>09</option>

                                <option <?php echo ($credit_result[0]->expirymonth == 10 ) ? "selected" : ''; ?>>10</option>

                                <option <?php echo ($credit_result[0]->expirymonth == 11 ) ? "selected" : ''; ?>>11</option>

                                <option <?php echo ($credit_result[0]->expirymonth == 12) ? "selected" : ''; ?>>12</option>

                            </select>
                            <div><span id="errorexpdate" style="color:red;"></span></div> 
                        </div>

                        <div class="col-sm-6">

                            <select class="contact_block" id="expyear" name="expyear">

                                <option>Year</option>

                                <option <?php echo ($credit_result[0]->expyear == 2017) ? "selected" : ''; ?>>2017</option>

                                <option <?php echo ($credit_result[0]->expyear == 2018) ? "selected" : ''; ?>>2018</option>

                                <option <?php echo ($credit_result[0]->expyear == 2019) ? "selected" : ''; ?>>2019</option>

                                <option <?php echo ($credit_result[0]->expyear == 2020) ? "selected" : ''; ?>>2020</option>

                                <option <?php echo ($credit_result[0]->expyear == 2021) ? "selected" : ''; ?>>2021</option>

                                <option <?php echo ($credit_result[0]->expyear == 2022) ? "selected" : ''; ?>>2022</option>

                                <option <?php echo ($credit_result[0]->expyear == 2023) ? "selected" : ''; ?>>2023</option>

                                <option <?php echo ($credit_result[0]->expyear == 2024) ? "selected" : ''; ?>>2024</option>

                                <option <?php echo ($credit_result[0]->expyear == 2025) ? "selected" : ''; ?>>2025</option>

                                <option <?php echo ($credit_result[0]->expyear == 2026) ? "selected" : ''; ?>>2026</option>

                                <option <?php echo ($credit_result[0]->expyear == 2027) ? "selected" : ''; ?>>2027</option>

                                <option <?php echo ($credit_result[0]->expyear == 2028) ? "selected" : ''; ?>>2028</option>

                            </select>
                            <div><span id="errorexpyear" style="color:red;"></span></div> 
                        </div>

                        <div class="col-sm-6">

                            <input  value="<?php echo $credit_result[0]->securitycode; ?>"  type="text" maxlength="3" placeholder="Security Code*" id="securitycode" name="securitycode" class="only_num contact_block" />
                            <div><span id="errorsecuritycode" style="color:red;"></span></div> 
                        </div>

                        <div class="col-sm-6">

                            <label class="contact_block">What’s this?</label>

                        </div>

                        <div class="col-sm-12">

                            <select class="contact_block" id="country" name="country">

                                <option  value="0">Select Country</option>
                                <?php foreach ($result_countries as $value) { ?>
                                    <?php if ($value->name != "") : ?>
                                        <option <?php echo ($credit_result[0]->country == $value->id) ? "selected" : ''; ?> value="<?php echo $value->id ?>"> <?php echo $value->name ?> </option>
                                    <?php endif; ?>
                                <?php } ?>

                            </select>
                            <div><span id="errorcountry" style="color:red;"></span></div>
                        </div>

                        <div class="col-sm-12">

                            <input  value="<?php echo $credit_result[0]->address; ?>"  type="text" placeholder="Address*" class="contact_block" id="address" name="address" maxlength="200"/>
                            <div><span id="erroraddress" style="color:red;"></span></div>
                        </div>

                        <div class="col-sm-12">

                            <input  value="<?php echo $credit_result[0]->address1; ?>"  type="text" placeholder="Address (optional)" class="contact_block" id="address1" name="address1" maxlength="200"/>

                        </div>

                        <div class="col-sm-6">

                            <input type="text"  value="<?php echo $credit_result[0]->city; ?>"  placeholder="City*" class="only_alpha contact_block" id="city" name="city" maxlength="50" />
                            <div><span id="errorcity" style="color:red;"></span></div>
                        </div>

                        <div class="col-sm-6">

                            <select class="contact_block" id="state" name="state">
                                <option value="0">Select State</option>
                                <?php
                                $country_id = $credit_result[0]->country;
                                if ($country_id):
                                    $state_id = $credit_result[0]->state;
                                    $result_states = $wpdb->get_results("SELECT * from states where country_id=" . $country_id . "");



                                    foreach ($result_states as $value) {

                                        if ($value->name != "") :
                                            ?>
                                            <option <?php echo ($state_id == $value->id) ? "selected" : ''; ?> value="' . $value->id . '"> <?php echo $value->name; ?> </option>';
                                            <?php
                                        endif;
                                    }
                                endif;
                                ?>
                            </select>
                            <div><span id="errorstate" style="color:red;"></span></div>
                        </div>

                        <div class="col-sm-6">

                            <input  value="<?php echo $credit_result[0]->zipcode; ?>"  maxlength="6" type="text" placeholder="Zip Code*" class="only_num contact_block" id="zipcode" name="zipcode"/>
                            <div><span id="errorzipcode" style="color:red;"></span></div>
                        </div>

                        <div class="col-sm-6">

                            <input  value="<?php echo $credit_result[0]->phone; ?>"  type="text" maxlength="10" placeholder="Phone*" class="only_num contact_block" id="phone" name="phone" />
                            <div><span id="errorphone" style="color:red;"></span></div>
                        </div>

                        <div class="buttons">

                            <input type="button" value="Add card" class="btn_sky" id="btnAddCart" name="btnAddCart" />

                            <input type="reset" value="Cancel" id="close_paypal"  data-dismiss="modal" class="btn_sky" />

                        </div>
                        <div class="card_msg"></div>


                    </div>

                </form>
            </div>
        </div>
    </div>
</div>
<div id="pass_modal" class="pop_overlay" style="display: none;">
    <div class="pop_main">
        <div class="pop_head">
            <a href="javascript:void(0);" class="remove"><i class="fa fa-remove" aria-hidden="true"></i></a>
        </div>
        <div class="pop_body">
            <div class="page_title">
                <h2>Change Password</h2>
            </div>
            <div class="pop_content">
                <form method="post" id="profile_pass" autocomplete="off">
                    <div class="row">
                        <div class="col-sm-offset-2 col-sm-8">
                            <input type="password" class="txt_box contact_block" name="pass" id="pass"  maxlength="30"  placeholder="Current Password*">
                        </div>
                        <div class="col-sm-offset-2 col-sm-8">
                            <input type="password" class="txt_box contact_block" name="new_pass" id="new_pass"  maxlength="30"  placeholder="New Password*">
                        </div>
                        <div class="col-sm-offset-2 col-sm-8">
                            <input type="password" class="txt_box contact_block" name="conf_pass" id="conf_pass"  maxlength="30" placeholder="Confirm New Password*">
                        </div>
                        <div class="col-sm-offset-2 col-sm-8">
                            <input type="submit" name="text" class="submit btn_sky" value="Change Password">                                    
                        </div>
                        <div class="content pass_msg"></div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- END  Change Password Popup --> 

<!--POPUP-->

<div id="change_settings" class="pop_overlay open start_tests ready" style="display: none;">
    <div class="pop_main">
        <div class="pop_head">
            <a href="javascript:void(0);"><i class="fa fa-remove" aria-hidden="true"></i></a>
        </div>
        <div class="pop_body">
            <div class="load_overlay" id="pop_loding">
                <img src="<?php echo get_template_directory_uri(); ?>/images/39.gif"/>
            </div>
            <div class="page_title">
                <h2>Are you sure about this? </h2>
            </div>
            <div class="pop_content">
                <div class="first_test save_doc">
                    <p>This will disable all emails we send letting you know your documents are complete, meaning you'll have to check your dashboard yourself to see when we've finished proofreading you document.</p>                       
                    <a href="#" class="btn_sky btnyes">Yes, disable emails when documents are complete</a>
                    <a href="#" class="btn_sky btnno  orange  pop_btn">No, I still want to know when my documents are finished</a>                       
                </div>
                <div class="doc_msg"></div>
            </div>
        </div>
    </div>
</div>

<script>

    //Email Validation  
    function validateEmail(email) {
        var filter = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
        if (filter.test(email)) {
            return true;
        } else {
            return false;
        }
    }
    function delete_acc(user_id) {
        var txt;
        var r = confirm("Are you sure you want to delete your account?");
        if (r == true) {
            $.ajax({
                url: "<?php echo admin_url('admin-ajax.php'); ?>",
                type: "POST",
                data: {
                    action: 'delete_acc',
                    user_id: user_id,
                },
                success: function (data) {
                    $('#loding').hide();
                    if (data == 1)
                        $(".del_msg").html('<span class="text-success delmsg">Account deleted sucessfully... </span>');
                    else
                        $(".del_msg").html('<span class="text-danger delmsg">Not deleted sucessfully...</span>');
                    $(".delmsg").fadeOut(5000);
                    window.setTimeout(function () {
                        location.reload();
                    }, 1000);
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    $('#loding').hide();
                    console.log(jqXHR + " :: " + textStatus + " :: " + errorThrown);
                }
            });
        } else {
            $(r).dialog("close");

        }
        return false;
    }

    //---------------Check Image type---------//
    function allvalidateimageFile(file, imageName) {
        debugger;
        var ext = file.split(".");
        ext = ext[ext.length - 1].toLowerCase();
        var arrayExtensions = ["jpg", "jpeg", "png"];
        if (arrayExtensions.lastIndexOf(ext) == -1) {

            $('#profile_pic').attr('src', $('#old_pic').val());
            $(".pic_msg").html('<span class="text-danger pic_msg1 ">Wrong extension type.Please upload valid file</span>');
            $(".pic_msg1").fadeOut(5000);
            $("#profile_picture").val('');
            return false;
        } else if ($('#profile_picture')[0].files[0].size > 5242880) {

            $('#profile_pic').attr('src', $('#old_pic').val());
            $(".pic_msg").html('<span class="text-danger pic_msg1 ">Please upload max 5MB size file.</span>');
            $(".pic_msg1").fadeOut(5000);
            $("#profile_picture").val('');
            return false;
        } else {
            $('.remove_img').show();
            $('.remove_old_img').hide();
            $('#remove_image').val(0);
            var reader = new FileReader();
            reader.onload = function () {
                var output = document.getElementById('profile_pic');
                output.src = reader.result;
            };
            reader.readAsDataURL($('#profile_picture')[0].files[0]);
            return true;
        }
    }
    jQuery(document).ready(function ($) {

        $("#country").change(function () {
            $('#state').find('option:not(:first)').remove();
            $.ajax({
                url: '<?php echo admin_url('admin-ajax.php'); ?>',
                data: {
                    'action': 'getStates',
                    'countryId': $("#country").val()

                },
                dataType: 'text',
                success: function (data) {
                    // This outputs the result of the ajax request
                    $("#state").append(data);
                },
                error: function (errorThrown) {
                    console.log(errorThrown);
                }
            });
        });

        $(".bill_popup .paypal_pop").click(function () {
            $('.paypal_err').remove();
        });


        $(".bill_popup .pop_btn").click(function () {

            $("#errorfirstname").html("");
            $("#errorlastname").html("");
            $("#errorcardno").html("");
            $("#errorexpdate").html("");
            $("#errorexpyear").html("");
            $("#errorcountry").html("");
            $("#erroraddress").html("");
            $("#errorcity").html("");
            $("#errorstate").html("");
            $("#errorzipcode").html("");
            $("#errorphone").html("");
        });
        $("#btnAddCart").click(function () {

            var error = "";

            if ($("#firstname").val() == "")
            {
                $("#errorfirstname").html("Please enter first name");
                error = "false";
            } else {
                $("#errorfirstname").html("");
            }

            if ($("#lastname").val() == "")
            {
                $("#errorlastname").html("Please enter last name");
                error = "false";
            } else {
                $("#errorlastname").html("");
            }
            if ($("#cardno").val() == "") {
                $("#errorcardno").html("Please enter card number");
                error = "false";
            } else
            {
                $("#errorcardno").html("");
                if ($("#cardno").val().length < 16)
                {
                    error = "false";
                    $("#errorcardno").html("Please enter valid card number");
                }
            }
            if ($("#expdate").val() == "Expiration Month")
            {
                error = "false";
                $("#errorexpdate").html("Please select expiry month");

            } else {
                $("#errorexpdate").html("");
            }
            if ($("#expyear").val() == "Year")
            {
                error = "false";
                $("#errorexpyear").html("Please select expiry year");
            } else {
                $("#errorexpyear").html("");
            }
            if ($("#country").val() == 0)
            {
                error = "false";
                $("#errorcountry").html("Please select country");

            } else
            {
                $("#errorcountry").html("");
            }
            if ($("#address").val() == "")
            {
                error = "false";
                $("#erroraddress").html("Please enter address");

            } else
            {
                $("#erroraddress").html("");
            }
            if ($("#city").val() == "")
            {
                error = "false";
                $("#errorcity").html("Please select city");
            } else {
                $("#errorcity").html("");
            }
            if ($("#state").val() == 0)
            {
                error = "false";
                $("#errorstate").html("Please select state");
            } else
            {
                $("#errorstate").html("");
            }

            if ($("#zipcode").val() == "")
            {
                error = "false";
                $("#errorzipcode").html("Please enter zipcode");
            } else {
                $("#errorzipcode").html("");
            }
            if ($("#phone").val() == "")
            {
                error = "false";
                $("#errorphone").html("Please enter phone");
            } else {
                $("#errorphone").html("");
            }
            if (error != "") {
                $("#error").html(error);
                error = "";
                return false;
            } else {
                error = "";
                $("#errorfirstname").html("");
                $("#errorlastname").html("");
                $("#errorcardno").html("");
                $("#errorexpdate").html("");
                $("#errorexpyear").html("");
                $("#errorcountry").html("");
                $("#erroraddress").html("");
                $("#errorcity").html("");
                $("#errorstate").html("");
                $("#errorzipcode").html("");
                $("#errorphone").html("");
                $("#error").html("");
                $.ajax({
                    url: '<?php echo admin_url('admin-ajax.php'); ?>',
                    data: {
                        'action': 'user_cart_details_store',
                        'firstname': $("#firstname").val(),
                        'lastname': $("#lastname").val(),
                        'cardno': $("#cardno").val(),
                        'expdate': $("#expdate").val(),
                        'expyear': $("#expyear").val(),
                        'securitycode': $("#securitycode").val(),
                        'country': $("#country").val(),
                        'address': $("#address").val(),
                        'address1': $("#address1").val(),
                        'city': $("#city").val(),
                        'state': $("#state").val(),
                        'zipcode': $("#zipcode").val(),
                        'phone': $("#phone").val()
                    },
                    dataType: 'text',
                    success: function (data) {

                        $('#loding').hide();
                        if (data == 0)
                            $(".card_msg").html('<span class="text-danger paypalmsg">Not updated successfully...</span>');
                        else {
                            // debugger;
                            $(".card_msg").html('<span class="text-success paypalmsg">Updated successfully...</span>');
                            $(".paypalmsg").fadeOut(5000);

                            window.setTimeout(function () {

                                $('#Paypal').fadeOut();
                                $('#Paypal').removeClass('open');
                            }, 2000);
                        }
                    },
                    error: function (errorThrown) {
                        console.log(errorThrown);
                    }
                });
                return true;
            }

        });


        $('#add_paypal').click(function () {
            $('.paypal_err').remove();
            var paid_id = $('#paypal_id').val();
            if (paid_id == '') {
                $('#paypal_id').after('<span class="text-danger paypal_err">Paypal id is required.</span>');
                return false;
            } else if (!validateEmail(paid_id)) {
                jQuery('#paypal_id').after('<span class="text-danger paypal_err">Please enter valid paypal id.</span>');
                return false;
            } else {
                $('#loding').show();
                $.ajax({
                    url: "<?php echo admin_url('admin-ajax.php'); ?>",
                    type: "POST",
                    data: {
                        action: 'save_cust_paypal',
                        paid_id: paid_id
                    },
                    success: function (data) {
                        $('#loding').hide();
                        if (data == 0)
                            $(".paypal_msg").html('<span class="text-danger paypalmsg">Not updated successfully...</span>');
                        else
                            $(".paypal_msg").html('<span class="text-success paypalmsg">Updated successfully...</span>');
                        $(".paypalmsg").fadeOut(5000);

                        window.setTimeout(function () {

                            $('#paypal_modal').fadeOut();
                            $('#paypal_modal').removeClass('open');
                        }, 1000);
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        $('#loding').hide();
                        console.log(jqXHR + " :: " + textStatus + " :: " + errorThrown);
                    }
                });
            }
        });


        $('.bill_popup .pop_btn').click(function (e) {
            e.stopPropagation();
            $('#Paypal').fadeIn();
            $('#Paypal').addClass('open');
        });
        $('#Paypal .pop_head a .fa-remove, #close_paypal').click(function (e) {
            e.stopPropagation();
            $('#Paypal').fadeOut();
            $('#Paypal').removeClass('open');
        });

//        $('#paypal_modal .pop_head a .fa-remove, #close_paypal_model').click(function (e) {
//            e.stopPropagation();
//            $('#paypal_modal').fadeOut();
//            $('#paypal_modal').removeClass('open');
//        });

        $('.remove_img').click(function () {
            $('#profile_pic').attr('src', $('#old_pic').val());
            $("#profile_picture").val('');
            $('.remove_old_img').show();
            $(this).hide();
            $('#remove_image').val(1);
        });
        $('.remove_old_img').click(function () {
            $('#profile_pic').attr('src', $('#dummy_pic').val());
            $('#old_pic').val($('#dummy_pic').val());
            $("#profile_picture").val('');
            $('#remove_image').val(1);
            $(this).hide();
        });
        //Edit Profile Info
        $('#edit_info').click(function () {
            $('.pro_info').prop("disabled", false);
            $(this).after('<a href="javascript:void(0);" id="save_info" class="save_pro"></a><a href="javascript:void(0);" id="cancel_info" class="cancel_pro"></a>');
            $(this).hide();
            $('.remove_image').show();
            $('.main_profile_remove').show();
        });
        $("#save_info").live('click', function (e) {

            $('.main_profile_remove').hide();
            $('.pro_err').remove();
            var fname = $('#fname').val();
            var lname = $('#lname').val();

            var error = false;
            if (fname == '')
            {
                $('#fname').after('<span class="text-danger pro_err">First name is required.</span>');
                error = true;
            }
            if (lname == '')
            {
                $('#lname').after('<span class="text-danger pro_err">Last name is required.</span>');
                error = true;
            }

            if (error == false) {

                $('#loding').show();
                var alldata = $('#updatecustomerprofile').serialize();
                var old_pic = $('#old_pic').val();
                var form = $("#updatecustomerprofile")[0];
                var formdata = new FormData(form);
                formdata.append('alldata', alldata);
                formdata.append('action', 'save_basic');
                $.ajax({
                    type: "POST",
                    url: "<?php echo admin_url('admin-ajax.php'); ?>",
                    data: formdata,
                    dataType: "json",
                    contentType: false,
                    processData: false,
                    success: function (data) {
                        if (data['image'] == '') {
                            $('.remove_img').hide();
                            $('.remove_old_img').hide();
                            $('#profile_pic').attr('src', old_pic);
                            $('.user_info img').attr('src', old_pic);
                            $('.user img').attr('src', old_pic);
                        } else {
                            $('.remove_old_img').show();
                            $('#profile_pic').attr('src', data['image']);
                            $('#old_pic').val(data['image']);
                            $('.remove_img').hide();
                            $('.remove_old_img').show();
                            $('.user img').attr('src', data['image']);
                        }
                        $('#fname').attr("data-fname", fname);
                        $('#lname').attr("data-lname", lname);


                        $('.user_fname').text(fname);
                        $(".profile_msg").html(data['message']);
                        $(".pic_msg1").fadeOut(5000);
                        $('#edit_info').show();
                        $('.pro_info').prop("disabled", true);
                        $('#save_info').remove();
                        $('#cancel_info').remove();
                        $(".pro_err").fadeOut(5000);
                        $("#profile_picture").val('');
                        $('#loding').hide();
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        console.log(jqXHR + " :: " + textStatus + " :: " + errorThrown);
                        $('#loding').hide();
                    }
                });
            }
            return false;
        });
        $("#cancel_info").live('click', function (e) {
            $('#loding').show();
            $('#fname').val($('#fname').attr("data-fname"));
            $('#lname').val($('#lname').attr("data-lname"));
            $('#profile_pic').attr('src', $('#old_pic').val());
            $('.main_profile_remove').hide();
            $('.pro_err').remove();
            $('#edit_info').show();
            $('.pro_info').prop("disabled", true);
            $('#save_info').remove();
            $('#cancel_info').remove();
            $("#profile_picture").val('');
            $('#loding').hide();
        });
        $('#remove_pic').click(function () {
            $('#img_preview').attr('src', $('#img_preview1').attr('src'));
            $('#remove-image').val('true');
            $('#hdnremoveimage').val('1');
            $('.fileinput_info').show();

        });


        // NOTIFICATION SETTINGS
        $('.checkbox-custom').click(function () {
            $('.notify_msg').remove();
            var fieldname1 = $(this).attr("data-id");
            if (fieldname1 == "receive_doc_completed") {
                if (!$(this).is(':checked')) {
                    $('#change_settings').fadeIn();
                    $('#change_settings').addClass('open');
                    return false;
                }
            }
            $('.notify_msg').remove();
            $('#loding').show();

            var fieldvalue1 = "false";
            if ($(this).is(":checked")) {
                fieldvalue1 = "true";
            } else
            {
                fieldvalue1 = "false";
            }

            $.ajax({
                url: "<?php echo admin_url('admin-ajax.php'); ?>",
                type: "POST",
                data: {
                    action: "fnupdatecustomernotification",
                    fieldvalue: fieldvalue1,
                    fieldname: fieldname1,
                },
                dataType: "html",
                success: function (data) {
                    $(".noti_msg").html('<span class="text-success notify_msg ">Notification settings saved successfully..</span>');
                    $('#loding').hide();
                    $(".notify_msg").fadeOut(3000);
                },
                error: function (err) {
                    $(".noti_msg").html('<span class="text-danger notify_msg ">Notification settings not saved successfully...</span>');
                    $('#loding').hide();
                    $(".notify_msg").fadeOut(3000);
                }
            });
        });

        $('.pop_head a .fa-remove,#change_settings .save_doc .btnno').click(function (e) {
            e.stopPropagation();
            $('#change_settings').fadeOut();
            $('#change_settings').removeClass('open');
        });

        $('#change_settings .save_doc .btnyes').click(function (e) {
            $('#pop_loding').show();
            $('.notify_msg').remove();
            var fieldname1 = $('.chng_setting').attr("data-id");
            var fieldvalue1 = "false";

            $.ajax({
                url: "<?php echo admin_url('admin-ajax.php'); ?>",
                type: "POST",
                data: {
                    action: "fnupdatecustomernotification",
                    fieldvalue: fieldvalue1,
                    fieldname: fieldname1,
                },
                dataType: "html",
                success: function (data) {
                    $('.chng_setting').attr('checked', false);
                    $(".doc_msg").html('<span class="text-success notify_msg ">Notification settings saved successfully..</span>');
                    $('#pop_loding').hide();
                    window.setTimeout(function () {
                        $(".docmsg").fadeOut();
                        $('#change_settings').fadeOut();
                        $('#change_settings').removeClass('open');
                    }, 2000);
                },
                error: function (err) {
                    $(".doc_msg").html('<span class="text-danger notify_msg ">Notification settings not saved successfully...</span>');
                    $('#pop_loding').hide();
                    window.setTimeout(function () {
                        $(".docmsg").fadeOut();
                        $('#change_settings').fadeOut();
                        $('#change_settings').removeClass('open');
                    }, 2000);
                }
            });

        });

    });
</script>

<?php get_footer(); ?>