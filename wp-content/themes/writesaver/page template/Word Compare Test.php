<?php
/*
 * Template Name: Word Compare Test
 */

get_header();
?>
<style>

    b.testcursorsample {
        background: red;
        font-weight: normal;
        width: 1px;
        display: inline-block;
        cursor: progress;
        -webkit-animation: 1s blink step-end infinite;
        -moz-animation: 1s blink step-end infinite;
        -ms-animation: 1s blink step-end infinite;
        -o-animation: 1s blink step-end infinite;
        animation: 1s blink step-end infinite;
    }
</style>
<section>

    <div class="container">
        <h1> Word Compare Test Page - Binal </h1>
        <div class="changeable" contenteditable="true" id="txt_area_upload_doc11" style="border: solid 1px black;height:500px; width: 100%; display: inline-block;position: relative;white-space: pre-line;">The tiger are the largesr cat species, most recognisable for their pattern of darkr 
            vertical stripes on reddish-orange fur withd a lighter underside. The species is classified in the genus Panthera with the liond, leopard, jaguar and snowd leopard.
        </div>
    </div>
    <div id="original" style="white-space: pre-line;position: relative;display:none;">The tiger are the largesr cat species, most recognisable for their pattern of darkr 
        vertical stripes on reddish-orange fur withd a lighter underside. The species is classified in the genus Panthera with the liond, leopard, jaguar and snowd leopard.
    </div>
    <div id="temdoc" style="white-space: pre-line;position: relative;display:none;">The tiger are the largesr cat species, most recognisable for their pattern of darkr 
        vertical stripes on reddish-orange fur withd a lighter underside. The species is classified in the genus Panthera with the liond, leopard, jaguar and snowd leopard.        
    </div>

    <div id="testcursorsample" contenteditable="true" style="border: solid 1px black;height:500px; width: 100%; display: inline-block;position: relative;white-space: pre-line;">The tiger are the largesr cat species, most recognisable for their pattern of darkr 
        vertical stripes on reddish-orange fur withd a <b class="testcursorsample">&nbsp;</b>lighter underside. The species is classified in the genus Panthera with the liond, leopard, jaguar and snowd leopard.        
    </div>
    <div id="deletedwords"></div>
    <input type="hidden" id="hdndocidpartsid" name="hdndocidpartsid" value="1" />
    <input type="hidden" id="fk_proofreader_id" name="fk_proofreader_id" value="1" />
    <a href="javascript:void(0);" id="lnksave">Get LIST Save</a>
</section>
<script>


    var lstedited = [];
    var lstoriginal = [];
    var listofwordschanged = [];
    function getlines()
    {
        debugger;
        var enteredoriginalText = $("#original").html();
        var numberOfLineBreaksoriginal = (enteredoriginalText.match(/\n/g) || []).length;
        var orginal_numbers_line = numberOfLineBreaksoriginal;
        var lines = $("#original").html().split("\n");
        // var lines = $("#original").html();
        $.each(lines, function (n, elem) {
            if (elem != "" && elem != null)
            {
                lstoriginal.push(elem.toString().trim());

            }
        });


        var entered_editedText = $("#txt_area_upload_doc11").html();
        var numberOfLineBreaks_edited = (entered_editedText.match(/\n/g) || []).length;
        var edited_numbers_line = numberOfLineBreaks_edited;

        //var lines = $("#txt_area_upload_doc11").html();
        var lines = $("#txt_area_upload_doc11").html().split("\n");
        $.each(lines, function (n, elem) {
            if (elem != "" && elem != null)
            {
                lstedited.push(elem.toString().trim());
            }
        });

        $.each(lstoriginal, function (n, elem) {
            if (elem != "")
            {
                console.log("orginal :- " + elem);
            }
        });
        $.each(lstedited, function (n, elem) {
            if (elem != "")
            {
                console.log("edited :- " + elem);
            }
        });

        //comparedocs();
    }
    $(document).ready(function () {


        var temparrybackspacechnaged = [];
        $('#txt_area_upload_doc11').keydown(function (e) {
            var updatednew = "";
            var userSelection;
            if (window.getSelection) {
                userSelection = window.getSelection();
            }
            var start = userSelection.anchorOffset;
            var end = userSelection.focusOffset;
            var position = [start, end];
            var val = $(this).text();

            //new logic for old words get

            var oldstringnewlogic;
            if (e.keyCode != 46) {

                debugger;
                var startpostions = start;
                var endpostions = end;

                var temptext = $("#original").text();

                while (temptext != " ")
                {
                    startpostions = parseInt(startpostions) - 1;
                    temptext = val.substring(startpostions, start);
                    temptext = temptext.substr(0, 1);
                }
                temptext = $("#original").text();

                while (temptext != " ")
                {
                    endpostions = parseInt(endpostions) + 1;
                    temptext = val.substring(end, endpostions);
                    temptext = temptext.substr(temptext.length - 1);
                }

                //alert(val.substring(startpostions, endpostions));
                oldstringnewlogic = val.substring(startpostions, endpostions);
                if (start >= startpostions && end <= endpostions)
                {
                    if (temparrybackspacechnaged.length == 0)
                    {
                        temparrybackspacechnaged.push([startpostions, endpostions, oldstringnewlogic]);
                    } else
                    {
                        for (var kc = 0; kc <= temparrybackspacechnaged.length - 1; kc++)
                        {
                            if (startpostions > temparrybackspacechnaged[kc][0] && endpostions < temparrybackspacechnaged[kc][1])
                            {
                                temparrybackspacechnaged.push([startpostions, endpostions, oldstringnewlogic]);
                                break;
                            }
                        }
                    }
                }


            }
            //end for new logic
            var length = parseInt(end) - parseInt(start);
            if (length == 0)
            {
                length = 1;

            }
            //var getoffsetandstring = GetOffset(start, length, val.substring(position[0], position[1])); //commented

            var offset = start; //chnaged
            var oldstring = ""; //changed

            var deleted = '';
            if (e.keyCode == 8) {

                if (position[0] == position[1]) {
                    if (position[0] == 0)
                    {
                        deleted = '';
                        offset = offset;
                        length = length;
                        oldstring = deleted;
                    } else
                    {
                        deleted = val.substr(parseInt(position[0]) - 1, 1);
                        offset = parseInt(position[0]) - 1;

                        oldstring = deleted;
                    }
                } else {
                    deleted = val.substring(position[0], position[1]);
                    offset = position[0];
                }

                updatednew = deleted;
                console.log("backspace call" + offset + " " + length + " " + oldstring + " " + updatednew + " " + "Delete BackSpace");
                listofwordschanged.push([offset, length, oldstring, updatednew, "Delete BackSpace"]);
                //comparedocs();

            } else if (e.keyCode == 46) {

                var val = $(this).text();
                if (position[0] == position[1]) {

                    if (position[0] === val.length)
                        deleted = '';
                    else
                        deleted = val.substr(position[0], 1);
                } else {
                    deleted = val.substring(position[0], position[1]);
                }
                console.log("deleted words call");
                updatednew = deleted;
                oldstring = deleted;
                offset = position[0];
                alert(deleted);
                listofwordschanged.push([offset, length, oldstring, updatednew, "Delete"]);
                //comparedocs();

            } else if (e.which !== 0) {
                var c = String.fromCharCode(e.which);
                var isWordCharacter = c.match(/\w/);
                if ((isWordCharacter)) {
                    var val = $(this).text();


                    updatednew = String.fromCharCode(e.which).toLowerCase();
                    oldstring = oldstring;
                    length = 1;
                    offset = position[0];


                    var tmpstartpostions = parseInt(position[0]) - 1;
                    var tmpstartstring = val.highLightAt(tmpstartpostions);

                    var removestart = 0;
                    var preveiousstrings = "";
                    while (tmpstartstring.trim().length > 0)
                    {
                        if (tmpstartstring.trim().length != 0)
                        {
                            tmpstartpostions = parseInt(tmpstartpostions) - 1;
                            preveiousstrings += tmpstartstring;
                        }
                        removestart++;
                        tmpstartstring = val.highLightAt(tmpstartpostions);


                    }


                    oldstring = "";
                    updatednew = "";
                    var tmpendpostions = parseInt(position[0]) - removestart;
                    offset = tmpendpostions - 1;
                    var tmpendstring = val.highLightAt(tmpendpostions);

                    preveiousstrings += tmpendstring;
                    while (tmpendstring.trim().length > 0)
                    {
                        tmpendstring = val.highLightAt(tmpendpostions);
                        // alert("end" + (tmpendstring));

                        tmpendpostions = parseInt(tmpendpostions) + 1;
                        if (tmpendpostions != position[0])
                        {
                            oldstring += tmpendstring;
                            updatednew += tmpendstring;
                        } else {
                            oldstring += tmpendstring;
                            updatednew += tmpendstring + String.fromCharCode(e.which).toLowerCase();
                        }


                    }
                    var endoffset = parseInt(tmpendpostions);
                    // alert(oldstring.length);
                    length = updatednew.length - 1;
                    for (var kc = 0; kc <= temparrybackspacechnaged.length - 1; kc++)
                    {
                        if (start >= temparrybackspacechnaged[kc][0] && end <= temparrybackspacechnaged[kc][1])
                        {
                            oldstringnewlogic = temparrybackspacechnaged[kc][2];
                            break;
                        } else if (start == end)
                        {
                            oldstringnewlogic = temparrybackspacechnaged[kc][2];
                            break;
                        }
                    }
                    console.log(start + " end " + end + " changed:" + " " + offset + " " + length + " " + oldstringnewlogic + " " + updatednew + " " + "Changed or Added");
                    listofwordschanged.push([offset, length, oldstringnewlogic, updatednew, "Changed or Added"]);
                    temparrybackspacechnaged = [];
                } else {
                    //up and down and other key pressed so don't need to track
                    //alert("notword");
                }
            }
            $("#deletedwords").text(deleted);


        });

//below is old function
//        $('#txt_area_upload_doc11').keydown(function (e) {
//            var updatednew = "";
//            var userSelection;
//            if (window.getSelection) {
//                userSelection = window.getSelection();
//            }
//            var start = userSelection.anchorOffset;
//            var end = userSelection.focusOffset;
//            var position = [start, end];
//            var val = $(this).text();
//            debugger;
//            //new logic for old words get
//            if (e.keyCode != 46) {
//                var startpostions = start;
//                var endpostions = end;
//
//                var temptext = $("#original").text();
//
//                while (temptext != " ")
//                {
//                    startpostions = parseInt(startpostions) - 1;
//                    temptext = val.substring(startpostions, start);
//                    temptext = temptext.substr(0, 1);
//                }
//                temptext = $("#original").text();
//
//                while (temptext != " ")
//                {
//                    endpostions = parseInt(endpostions) + 1;
//                    temptext = val.substring(end, endpostions);
//                    temptext = temptext.substr(temptext.length - 1);
//                }
//
//                alert(val.substring(startpostions, endpostions));
//
//            }
//            //end for new logic
//            var length = parseInt(end) - parseInt(start);
//            if (length == 0)
//            {
//                length = 1;
//
//            }
//            var getoffsetandstring = GetOffset(start, length, val.substring(position[0], position[1]));
//
//            var offset = getoffsetandstring.split('_')[0];
//            var oldstring = getoffsetandstring.split('_')[1];
//
//            var deleted = '';
//            if (e.keyCode == 8) {
//
//                if (position[0] == position[1]) {
//                    if (position[0] == 0)
//                    {
//                        deleted = '';
//                        offset = offset;
//                        length = length;
//                        oldstring = deleted;
//                    }
//                    else
//                    {
//                        deleted = val.substr(parseInt(position[0]) - 1, 1);
//                        offset = parseInt(position[0]) - 1;
//
//                        oldstring = deleted;
//                    }
//                }
//                else {
//                    deleted = val.substring(position[0], position[1]);
//                    offset = position[0];
//                }
//
//                updatednew = deleted;
//                console.log("backspace call" + offset + " " + length + " " + oldstring + " " + updatednew + " " + "Delete BackSpace");
//                listofwordschanged.push([offset, length, oldstring, updatednew, "Delete BackSpace"]);
//                //comparedocs();
//
//            }
//            else if (e.keyCode == 46) {
//                debugger;
//                var val = $(this).text();
//                if (position[0] == position[1]) {
//
//                    if (position[0] === val.length)
//                        deleted = '';
//                    else
//                        deleted = val.substr(position[0], 1);
//                }
//                else {
//                    deleted = val.substring(position[0], position[1]);
//                }
//                console.log("deleted words call");
//                updatednew = deleted;
//                oldstring = deleted;
//                alert(deleted);
//                listofwordschanged.push([offset, length, oldstring, updatednew, "Delete"]);
//                //comparedocs();
//
//            } else if (e.which !== 0) {
//                var c = String.fromCharCode(e.which);
//                var isWordCharacter = c.match(/\w/);
//                if ((isWordCharacter)) {
//                    var val = $(this).text();
//                    console.log("chnaged words call");
//                    // comparedocs();
////                updatednew = val.substring(position[0], position[1]);
//
//                    updatednew = String.fromCharCode(e.which).toLowerCase();
//                    oldstring = oldstring;
//                    length = 1;
//                    offset = position[0];
//
//
//                    var tmpstartpostions = parseInt(position[0]) - 1;
//                    var tmpstartstring = val.highLightAt(tmpstartpostions);
//                    //alert(tmpstartstring);
//                    var removestart = 0;
//                    var preveiousstrings = "";
//                    while (tmpstartstring.trim().length > 0)
//                    {
//                        if (tmpstartstring.trim().length != 0)
//                        {
//                            tmpstartpostions = parseInt(tmpstartpostions) - 1;
//                            preveiousstrings += tmpstartstring;
//                        }
//                        removestart++;
//                        tmpstartstring = val.highLightAt(tmpstartpostions);
//
//
//                    }
//                    // alert(removestart + preveiousstrings);
////                offset = tmpstartpostions;
//                    debugger;
//                    oldstring = "";
//                    updatednew = "";
//                    var tmpendpostions = parseInt(position[0]) - removestart;
//                    offset = tmpendpostions - 1;
//                    var tmpendstring = val.highLightAt(tmpendpostions);
//                    //oldstring=tmpendstring;
//                    //    alert("default:-"+tmpendstring);
//                    preveiousstrings += tmpendstring;
//                    while (tmpendstring.trim().length > 0)
//                    {
//                        tmpendstring = val.highLightAt(tmpendpostions);
//                        // alert("end" + (tmpendstring));
//
//                        tmpendpostions = parseInt(tmpendpostions) + 1;
//                        if (tmpendpostions != position[0])
//                        {
//                            oldstring += tmpendstring;
//                            updatednew += tmpendstring;
//                        } else {
//                            oldstring += tmpendstring;
//                            updatednew += tmpendstring + String.fromCharCode(e.which).toLowerCase();
//                        }
//
//
//                    }
//                    var endoffset = parseInt(tmpendpostions);
//                    // alert(oldstring.length);
//                    length = updatednew.length - 1;
//                    console.log("changed:" + " " + offset + " " + length + " " + oldstring + " " + updatednew + " " + "Changed or Added");
//                    listofwordschanged.push([offset, length, oldstring, updatednew, "Changed or Added"]);
//                } else {
//                    //up and down and other key pressed so don't need to track
//                    //alert("notword");
//                }
//            }
//            $("#deletedwords").text(deleted);
//
//
//        });
        $("#lnksave").click(function () {
            $("#deletedwords").html("");
            var listofwordschangedFinal = [];

            var temparray = [];
            listofwordschanged.sort(function (a, b) {
                var aVal = parseInt(a[0]) + parseInt(a[1]),
                        bVal = parseInt(b[0]) + parseInt(b[1]);
                return bVal - aVal;
            });
            for (var k = 0; k <= listofwordschanged.length - 1; k++)
            {
                var sourceId = listofwordschanged[k][0];
                var item = temparray.filter(function (collect) {
                    return collect[0] == sourceId;
                });
                if (item.length == 0)
                {

                    if (listofwordschanged[k][4].toString().trim() != "Delete BackSpace")
                    {
                        temparray.push([listofwordschanged[k][0], listofwordschanged[k][1], listofwordschanged[k][2], listofwordschanged[k][3], listofwordschanged[k][4]]);
                    }
                }
            }

            for (var t = 0; t <= temparray.length - 1; t++)
            {
                //addeed logic for old string get words
                var oldwordArray = [];

                oldwordArray = $.grep(listofwordschanged, function (n, i)
                {
                    return (parseInt(temparray[t][0]) <= parseInt(listofwordschanged[i][0]) && parseInt(temparray[t][1]) > parseInt(listofwordschanged[i][1]));//(listofwordschanged[i][3] == "Delete" || listofwordschanged[i][3] == "Deleted") &&
                });

                oldwordArray.sort(function (a, b) {
                    var aVal = parseInt(a[0]) + parseInt(a[1]),
                            bVal = parseInt(b[0]) + parseInt(b[1]);
                    return  aVal - bVal;
                });
                debugger;
                var oldstring = "";
                for (var i = 0; i <= oldwordArray.length - 1; i++)
                {
                    oldstring = oldwordArray[i][2];
                    break;
                }

                //end logic for old string get words and display variable oldstring below in  line
                $("#deletedwords").append("<br/>\n" + " offset " + temparray[t][0] + " length " + temparray[t][1] + " oldstring " + oldstring + " new string " + temparray[t][3] + " Action " + temparray[t][4]);

            }

            var userid = $("#fk_proofreader_id").val();
            var docid = $("#hdndocidpartsid").val();
            var essay = $("#txt_area_upload_doc11").text();
//            $.ajax({
//                type: 'POST',
//                url: '<?php echo admin_url('admin-ajax.php'); ?>',
//                method: "post",
//                data: {
//                    action: 'save_Word_Changes',
//                    doc_id: docid,
//                    userid: userid,
//                    data2: temparray,
//                    essaydesc:essay
//                },
//                success: function (data) {
//                    //listofwordschanged = [];
//                    //temparray = [];
//                    alert(data);
//                    console.log((data));
//                }
//                ,
//                error: function (xhr, status, error) {
//                    var err = xhr.responseText;
//                    alert("error occurs while saving data." + err);
//                }
//
//            });
        });

    });




    String.prototype.highLightAt = function (index) {
        return  this.substr(index, 1);
    }

    function comparedocs()
    {
        debugger;
        lstedited = [];
        lstoriginal = [];
        getlines();

        if (lstedited.length >= lstoriginal.length)
        {
            for (var i = 0; i <= lstoriginal.length - 1; i++)
            {

                if (lstoriginal[i] != "")
                {
                    //check line exists in edit or not
                    var orginalwordsary = lstoriginal[i].toString().split(' ');
                    var editedwordsary = lstedited[i].toString().split(' ');
                    console.log("<br/>\n total original words :- " + orginalwordsary.length);
                    console.log("<br/>\n total edited words :- " + editedwordsary.length);
                    for (var j = 0; j <= orginalwordsary.length - 1; j++)
                    {

                        if (typeof editedwordsary[j] === 'undefined') {
                            // index does not exist
                            console.log("<br/>\n index does not exist index is" + j + " and word is :- " + orginalwordsary[j]);
                        } else
                        {
                            // does exist
                            var index1 = jQuery.inArray(orginalwordsary[j].trim(), editedwordsary);
                            var index = jQuery.inArray(orginalwordsary[j].trim(), editedwordsary, j);
                            if (index == -1 && index1 == -1)
                            {
                                if (index == -1)
                                {
                                    console.log("<br/>\n chnaged1 :- " + orginalwordsary[j] + " new word " + editedwordsary[j]);
                                }
                            } else if (index1 == -1)
                            {
                                console.log("<br/>\n chnaged :- " + orginalwordsary[j] + " new word " + editedwordsary[j]);
                            } else
                            {
                                if (orginalwordsary[j] == editedwordsary[j])
                                {
                                    console.log("<br/>\n matched  :- " + orginalwordsary[j] + " old word " + editedwordsary[j]);
                                } else {
                                    console.log("<br/>\n not matched  :- " + orginalwordsary[j] + " old word " + editedwordsary[j]);
                                }
                            }
                        }

                    }
                }

            }
        }

    }





    function GetOffset(offset, length, currenttext)
    {
        var strcontents = CleardivForNewline($("#temdoc").html().trim());

        if (strcontents.substr(offset, length).indexOf(" ") != -1) {
            var contents = strcontents.substr(offset, length);
            var lastchar = contents.substr(contents.length - 1);
            var firstchar = contents.substr(0, 1);
            if (firstchar == " ") {
                if (length != 1) {
                    offset = parseInt(offset) + 1;
                    newstring = strcontents.substr(offset, length);
                } else {
                    offset = offset;
                    newstring = strcontents.substr(offset, length);
                }
            } else if (lastchar == " ") {
                if (length != 1) {
                    offset = parseInt(offset) - 1;
                    newstring = strcontents.substr(offset, length);
                } else {
                    offset = offset;
                    newstring = strcontents.substr(offset, length);
                }
            } else {
                var newstring = strcontents.substr(offset, length);
                while (currenttext.trim() != newstring.trim()) {
                    offset = parseInt(offset) - 1;
                    newstring = strcontents.substr(offset, length);
                }
            }
        } else {
            var newstring = strcontents.substr(offset, length);

            while (currenttext.trim() != newstring.trim()) {
                offset = parseInt(offset) + 1;
                newstring = strcontents.substr(offset, length);
            }

        }
        $("#temdoc").html($('#txt_area_upload_doc11').html());
        return  offset + "_" + newstring;
    }
    function CleardivForNewline(originalhtml) {
        //  $("#final_span").html($("#erater2_results").html().trim());
        var str = originalhtml;
        //var stralllines = $("#final_span").html().split("\n")
        var stralllines = str.split("\n")
        var finalvalues = "";
        for (var i = 0; i < stralllines.length; i++) {
            if (finalvalues == "") {
                finalvalues = stralllines[i];
            } else {
                if (stralllines[i] == "" || stralllines[i] == null) {
                    finalvalues = finalvalues + "\n " + stralllines[i];
                } else {
                    finalvalues = finalvalues + "\n " + stralllines[i];
                }
            }
        }
        return finalvalues;
    }

</script>
<?php get_footer(); ?>
