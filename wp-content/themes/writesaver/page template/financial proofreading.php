<?php
/*
 * Template Name: Financial proofreading
 */

$current_user = wp_get_current_user();
$user_roles_array = $current_user->roles;
$user_role = array_shift($user_roles_array);
if (is_user_logged_in()):
    if ($user_role == "customer") {
        echo '<script>window.location.href="' . get_the_permalink(762) . '"</script>';
        exit;
    } elseif ($user_role == "proofreader") {
        echo '<script>window.location.href="' . get_the_permalink(810) . '"</script>';
        exit;
    } elseif ($user_role == "administrator") {
       // echo '<script>window.location.href="' . site_url() . '/wp-admin"</script>';
       // exit;
    } else {
        
    }

endif;


get_header();
?>
	


<?php/*
<section class="top_section section" id="section0">            
    <div class="top_section_main">
        <div class="top_contant">
            <div class="container">
                <div class="wrap">
                    <div class="type-wrap">
                        <div id="typed-strings">                                
                            <p>Perfect your English writing today!</p>
                            
                        </div>
                        <span id="typed"></span>
                    </div>                        
                </div>

                <div class="main_editor home">
                    <div class="editor_top">
                        <div class="editor_inner_top">                            
                            <div class="used_word">
                                <span>Word Count:</span><span class="count">0</span>
                            </div>
                        </div>
                        <div class="hidden_scroll">
                            <div class="parentscrollcontents" style="width: 100%; height:300px; display: inline-block;">
                                <div class="changeable 11" placeholder="Type or paste your text here to get it corrected by our team of native English speaking proofreaders." contenteditable="true" id="txt_area_upload_doc1" style="min-height: 300px; display: inline-block; "></div>
                            </div>
                        </div>
                    </div>
                    <div class="submit_area">
                        <p>Click below to have your writing edited by our proofreading team of native English speakers. Your first <?php echo of_get_option('free_words_for_customer') ?> words is free!</p>
                        <div class="btn_blue1">
                            
                            <?php
                            $login_url = "";
                            if (!is_user_logged_in()) {
                                $login_url = get_page_link(540);
                            } else {
                                $current_user = wp_get_current_user();
                                $user_roles_array = $current_user->roles;
                                $user_role = array_shift($user_roles_array);
                                if ($user_role == 'customer') {
                                    $login_url = get_page_link(762);
                                } else if ($user_role == 'proofreader') {
                                    $login_url = get_page_link(810);
                                } else {
                                    $login_url = get_page_link(6);
                                }
                            }
                            ?>
                            <a href="javascript:void(0);" class="btn_sky proof_doc" >Have this paper proofread!</a>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section> 

if (have_posts()) :
    while (have_posts()) : the_post();
        the_content();
    endwhile;
endif;*/
?>

  <main class="content" role="content">
        <section class="proofreading">
            <div class="proofreading__inner">
                <div class="proofreading__content">
                    <div class="proofreading__heading-wrap">
                        <h1 class="proofreading__heading">Financial Document Editing Service</h1>
                    </div>
                    <!-- /END proofreading__heading-wrap-->

                    <div class="proofreading__content-inner">
                        <div class="proofreading__form-wrap">
                            <form class="proofreading__form">
							<?/* <div class="proofreading__form-text" placeholder="Type or paste your text here to get it corrected by our team of native English speaking proofreaders." contenteditable="true" id="txt_area_upload_doc"></div>
          */?>
		   <textarea class="proofreading__form-text" name="message"  id="txt_area_upload_doc"  placeholder="Type or paste your text here"></textarea>
                            </form>
                            <div class="proofreading__line"></div>
                        </div>
                        <!--/END proofreading__form-wrap-->
                        <div class="proofreading__count-wrap">
                            <div class="proofreading__count">
                                <div class="proofreading__count-left">
										<?/*?>
								  <p class="proofreading__count-text">Drop file(s) or browse </p>
                                    <a href="" class="proofreading__count-link">link</a>
									<?*/?>
                                </div>
								
                                <div class="proofreading__count-right">
                                    <p class="proofreading__count-text">Word Count:</p>
                                    <span class="proofreading__count-val count">0</span>
                                </div>
                            </div>
                        </div>
                        <!--/END proofreading__count-wrap-->
                    </div>
                </div>
                <!-- /END proofreading__content-->


                <div class="proofreading__content-order">
                    <div class="proofreading__heading-wrap">
                        <h2 class="proofreading__heading">Your order details</h2>
                    </div>

                    <div class="proofreading__content-order-inner">
                        <div class="proofreading__content-order-list-wrap">
                            <div class="proofreading__close-btn-wrap">
                                <a href="#" class="proofreading__close-btn">link</a>
                            </div>
                            <ul class="proofreading__content-order-list">
                                <li class="proofreading__content-order-item">
                                    <div class="proofreading__content-order-item-inner">
                                        <h3 class="proofreading__content-order-caption">Your document</h3>

                                        <div data-price='<?=priceWord()?>' data-word='<?=of_get_option('free_words_for_customer')?>' class="proofreading__content-order-doc-val">
                                            <img src="<?=get_template_directory_uri() ?>/img/proofreading/doc_icon.png" />
                                        </div>
                                    </div>
                                </li>
                                <li class="proofreading__content-order-item">
                                    <div class="proofreading__content-order-item-inner">
                                        <h3 class="proofreading__content-order-caption">Word Count:</h3>
                                        <span class="proofreading__content-order-word-val">0</span>
                                    </div>
                                </li>
                                <li class="proofreading__content-order-item">
                                    <div class="proofreading__content-order-item-inner">
                                        <h3 class="proofreading__content-order-caption">Delivery:</h3>
                                        <span class="proofreading__content-order-deliv-val">less than 24 hours</span>
                                    </div>
                                </li>
                                <li class="proofreading__content-order-item">
                                    <div class="proofreading__content-order-item-inner">
                                        <h3 class="proofreading__content-order-caption">Cost:</h3>
                                        <span class="proofreading__content-order-cost-val-1"><strong id='pr'>Free</strong>  with your free trial</span>
									
                                        <span class="proofreading__content-order-cost-val">$0.00</span>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <!-- /END proofreading__content-order-inner-->
                </div>
                <!-- /END proofreading__content-order-->


                <div class="proofreading__bot-wrap">
                    <p class="proofreading__bot-text">Your first 500 words are free</p>
                    <a href="javascript:void(0);" class="proofreading__btn-link proof_doc">Have this paper proofread</a>
				
                </div>
            </div>
        </section>
		
		<!--/END proofreading-->

						<section class="about">
						<div class="about__inner">
						<div class="about__heading-wrap">
						<h2 class="about__heading">Financial Proofreading with Writesaver</h2>
						</div>
						<div class="about__description-wrap">
						<div class="about__description">
						<p class="about__description-text">Writesaver is the fastest, most affordable professional English financial proofreading service online. Unlike other grammar proofreading services, Writesaver uses real, educated native English speakers to edit your financial documents and make sure you sound just like a native English speaker. If you want to boost your professional credibility, impress clients and colleagues, and bring yourself better personal and business opportunities in English, there's no better proofreading service online than Writesaver.</p>

						</div>
						</div>
						</div>
						</section><!--/END about-->
								
								<section class="key-facts">
						<div class="key-facts__inner">
						<div class="key-facts__heading-wrap">
						<h2 class="key-facts__heading">Key facts</h2>
						</div>
						<!--END key-facts__heading-wrap-->
						<div class="key-facts__list-wrap">
						<ul class="key-facts__list">
							<li class="key-facts__item">
						<div class="key-facts__item-inner">
						<div class="key-facts__img-wrap"><div class="price_icon"></div></div>
						<h3 class="key-facts__caption">Affordability</h3>
						<p class="key-facts__tetx">Proofreading costs as low as $7.50 per 1,000 words</p>

						</div></li>
							<li class="key-facts__item">
						<div class="key-facts__item-inner">
						<div class="key-facts__img-wrap"><div class="experise_icon"></div></div>
						<h3 class="key-facts__caption">Professionalism</h3>
						<p class="key-facts__tetx">Native English speaking editors from top universities
						</p>

						</div></li>
							<li class="key-facts__item">
						<div class="key-facts__item-inner">
						<div class="key-facts__img-wrap"><div class="agility_icon"></div></div>
						<h3 class="key-facts__caption">Speed</h3>
						<p class="key-facts__tetx">The average document is proofread in just 12 hours</p>

						</div></li>
							<li class="key-facts__item">
						<div class="key-facts__item-inner">
						<div class="key-facts__img-wrap"><div class="industries_icon"></div></div>
						<h3 class="key-facts__caption">Expertise</h3>
						<p class="key-facts__tetx key-facts__tetx--small">Professional editing for over 25 industries</p>

						</div></li>
						</ul>
						<!--END key-facts__list-->

						</div>
						<!--/END key-facts__list-wrap-->

						</div>
						<!--END key-facts__inner-->

						</section>


		<?
		if (have_posts()) :
    while (have_posts()) : the_post();
        the_content();
    endwhile;
endif;
		
		//echo '<pre>'; print_r($Coments); echo '</pre>';	
		?>
		
        <section class="clients">
            <div class="clients__inner">
                <div class="clients__heading-wrap">
                    <h2 class="clients__heading">What our clients say about us</h2>
                </div>

                <div class="clients__slider-wrap">
                    <div class="clients__slider">
                        <div class="clients__slid">
                            <div class="clients__slid-inner">
                                <div class="clients__slid-left">
                                    <div class="clients__slid-img-wrap">
                                        <img src="<?=get_template_directory_uri() ?>/img/clients/caroline.png" >
                                    </div>
                                </div>
                                <div class="clients__slid-right">
                                    <div class="clients__slid-text-wrap">
                                        <p class="clients__slid-text">
                                            “I love working with Writesaver. My emails and requests are always answered the same day that I write them and the customer service is just brilliant. They are always very respectful and understanding. The texts that I need proofread are always delivered within a day. The quality of their work is impecable. They have given my business peace of mind knowing that our writing is flawless thanks to their proofreading work.”
                                        </p>
                                    </div>
                                    <div class="clients__description-wrap">
                                        <div class="clients__line"></div>
                                        <p class="clients__about">Carolina, Startups with Souls, Consulting, Panama</p>

                                        <div class="clients__from-wrap">
                                            <a href="www.startupswithsouls.com" class="clients__from-link">
                                                <img src="<?=get_template_directory_uri() ?>/img/clients/startupswithsouls-logo.png" >
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--/END clients__slid-->
                        <div class="clients__slid">
                            <div class="clients__slid-inner">
                                <div class="clients__slid-left">
                                    <div class="clients__slid-img-wrap">
                                        <img src="<?=get_template_directory_uri() ?>/img/clients/olya.png" alt="/">
                                    </div>
                                </div>
                                <div class="clients__slid-right">
                                    <div class="clients__slid-text-wrap">
                                        <p class="clients__slid-text">
                                            “We used Writesaver to correct all of the text on our web development agency's website. They finished our entire website in less than a day, and managed to keep the intent, meaning, and tone of our original writing, while putting it into clear, fluent English. I'll definitely be using their services again!”
                                        </p>
                                    </div>
                                    <div class="clients__description-wrap">
                                        <div class="clients__line"></div>
                                        <p class="clients__about">Olya, Nordwhale, Software Development, Ukraine</p>

                                        <div class="clients__from-wrap">
                                            <a href="http://nordwhale.com" class="clients__from-link">
                                                <img src="<?=get_template_directory_uri() ?>/img/clients/company/Russia-for-Me-Logo.png" alt="/">
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--/END clients__slid-->
                        <div class="clients__slid">
                            <div class="clients__slid-inner">
                                <div class="clients__slid-left">
                                    <div class="clients__slid-img-wrap">
                                        <img src="<?=get_template_directory_uri() ?>/img/clients/sergey.png" alt="/">
                                    </div>
                                </div>
                                <div class="clients__slid-right">
                                    <div class="clients__slid-text-wrap">
                                        <p class="clients__slid-text">
                                            “Our proofreading task was completed in a timely and diligent fashion, despite the significant size of the file. Now we can pursue our launch deadlines without worrying about the quality of our English texts. We are totally satisfied with the edits and the overall smooth and pleasant experience. My partners and I have decided to use Writesaver for all future proofreading jobs. Thanks and keep up the good work!.”
                                        </p>
                                    </div>
                                    <div class="clients__description-wrap">
                                        <div class="clients__line"></div>
                                        <p class="clients__about">Sergey, UBI Agency, Business Services, Ukraine</p>

                                        <div class="clients__from-wrap">
                                            <a href="http://ubi.agency" class="clients__from-link">
                                                <img src="<?=get_template_directory_uri() ?>/img/clients/company/ubi-logo-0123-no-tagline.png" alt="/">
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--/END clients__slid-->
                        <div class="clients__slid">
                            <div class="clients__slid-inner">
                                <div class="clients__slid-left">
                                    <div class="clients__slid-img-wrap">
                                        <img src="<?=get_template_directory_uri() ?>/img/clients/shimin.png" alt="/">
                                    </div>
                                </div>
                                <div class="clients__slid-right">
                                    <div class="clients__slid-text-wrap">
                                        <p class="clients__slid-text">
                                            “I recommend Writesaver especially because of the easy communication between the customers and the Writesaver team, which makes the whole process transparent, efficient and pleasant. Their edits are careful and diligent, and their customer service team takes care of problems even outside of their job description. I had all of my Master’s application documents revised here, since I can tell that every revised document is fully reviewed and of high qualify. Many thanks to Writesaver for helping me get admitted to my ideal MA program in the end!”
                                        </p>
                                    </div>
                                    <div class="clients__description-wrap">
                                        <div class="clients__line"></div>
                                        <p class="clients__about">Shimin, Masters Student, China</p>

                                        <div class="clients__from-wrap">
                                           
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--/END clients__slid-->

                        <div class="clients__slid">
                            <div class="clients__slid-inner">
                                <div class="clients__slid-left">
                                    <div class="clients__slid-img-wrap">
                                
                                    </div>
                                </div>
                                <div class="clients__slid-right">
                                    <div class="clients__slid-text-wrap">
                                        <p class="clients__slid-text">
                                            “It was a pleasure to work with Writesaver. They offer a high quality and affordable proofreading service. Customer support is world class; very responsive and friendly, so working on even a large text went smoothly. I'd recommend every SaaS business try working with them to make their content more legible and credible.”
                                        </p>
                                    </div>
                                    <div class="clients__description-wrap">
                                        <div class="clients__line"></div>
                                        <p class="clients__about">Wojtek, Presspad, Mobile App for Publishers, Poland</p>

                                        <div class="clients__from-wrap">
                                            <a href="http://presspadapp.com" class="clients__from-link">
                                                <img src="<?=get_template_directory_uri() ?>/img/clients/company/PressPad_LOGO_main_horizontal_gradient.png" alt="/">
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--/END clients__slid-->
						          <div class="clients__slid">
                            <div class="clients__slid-inner">
                                <div class="clients__slid-left">
                                    <div class="clients__slid-img-wrap">
                                
                                    </div>
                                </div>
                                <div class="clients__slid-right">
                                    <div class="clients__slid-text-wrap">
                                        <p class="clients__slid-text">
                                            “All of our website texts are proofread by Writesaver. What else can I say? The service is really effective, and it is a pleasure to use Writesaver. When paired with its affordable pricing, all of this makes your service the best I have ever used.”
                                        </p>
                                    </div>
                                    <div class="clients__description-wrap">
                                        <div class="clients__line"></div>
                                        <p class="clients__about">Alexey, Russia for Me, Travel Agency, Russia</p>

                                        <div class="clients__from-wrap">
                                            <a href="http://russiaforme.com" class="clients__from-link">
                                                <img src="<?=get_template_directory_uri() ?>/img/clients/company/Russia-for-Me-Logo.png" alt="/">
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--/END clients__slid-->
						
                    </div>
                    <!--/END clients__slider-->

                    <div class="clients__icon-slider-wrap">
                        <div class="clients__icon-slider">
                            <div class="clients__icon-slid">
                                <div class="clients__icon-slid-inner">
                                    <img src="<?=get_template_directory_uri() ?>/img/clients/caroline.png" alt="/">
                                </div>
                            </div>
                            <div class="clients__icon-slid">
                                <div class="clients__icon-slid-inner">
                                    <img src="<?=get_template_directory_uri() ?>/img/clients/olya.png" alt="/">
                                </div>
                            </div>
                            <div class="clients__icon-slid">
                                <div class="clients__icon-slid-inner">
                                    <img src="<?=get_template_directory_uri() ?>/img/clients/sergey.png" alt="/">
                                </div>
                            </div>
                            <div class="clients__icon-slid">
                                <div class="clients__icon-slid-inner">
                                    <img src="<?=get_template_directory_uri() ?>/img/clients/shimin.png" alt="/">
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--/END clients__icon-slider-wrap-->
                </div>
                <!--/END clients__slider-wrap-->

                <div class="clients__company-slider-wrap">
                    <div class="clients__company-slider">
                        <div class="clients__company-slid">
                            <div class="clients__company-slid-inner">
                                <a class="clients__company-link" href="www.startupswithsouls.com">
                                    <img src="<?=get_template_directory_uri() ?>/img/clients/company/startupswithsouls-logo1.png" alt="/">
                                </a>
                            </div>
                        </div>
                        <div class="clients__company-slid">
                            <div class="clients__company-slid-inner">
                                <a class="clients__company-link" href="http://russiaforme.com">
                                    <img src="<?=get_template_directory_uri() ?>/img/clients/company/Russia-for-Me-Logo.png" alt="/">
                                </a>
                            </div>
                        </div>
                        <div class="clients__company-slid">
                            <div class="clients__company-slid-inner">
                                <a class="clients__company-link" href="http://nordwhale.com">
                                    <img src="<?=get_template_directory_uri() ?>/img/clients/company/Nordwhale_logo.png" alt="/">
                                </a>
                            </div>
                        </div>
                        <div class="clients__company-slid">
                            <div class="clients__company-slid-inner">
                                <a class="clients__company-link" href="http://ubi.agency">
                                    <img src="<?=get_template_directory_uri() ?>/img/clients/company/ubi-logo-0123-no-tagline.png" alt="/">
                                </a>
                            </div>
                        </div>
                        <div class="clients__company-slid">
                            <div class="clients__company-slid-inner">
                                <a class="clients__company-link" href="http://presspadapp.com">
                                    <img src="<?=get_template_directory_uri() ?>/img/clients/company/PressPad_LOGO_main_horizontal_gradient.png" alt="/">
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                <!--/END clients__company-slider-wrap-->
            </div>
        </section>
        <!--/END clients-->
        <section class="protect">
            <div class="protect__inner">
                <div class="protect__heading-wrap">
                    <h2 class="protect__heading">Confidentiality Guaranteed</h2>

                    <p class="protect__text">All of our proofreaders agree to strict confidentiality agreements, and we hold all of our editors to the highest standards of confidentiality and trust. </p>
                </div>
            </div>
            <!--/END privacy__inner-->
        </section>
        <!--/END privacy-->
		
		
		<?php
			$query = new WP_Query(array( 'cat' => 1, 'posts_per_page' => 4 ) );
	
	        if ($query->have_posts()) :
		?>
        <section class="articles">
            <div class="articles__inner">
                <div class="articles__heading-wrap">
                    <h2 class="articles__heading">Popular articles</h2>
                </div>
                <div class="articles__list-wrap">
                    <ul class="articles__list">
					<?
				   while ($query->have_posts()) { $query->the_post();?>
                        <li class="articles__item">
                            <div class="articles__item-inner">
                                <div class="articles__img-wrap">
                                    <img src="<?php the_post_thumbnail_url(array(1140, 478)); ?>" class="img-responsive" alt="<?//titleImg()?>" title="<?//titleImg()?>" >
                                </div>
                                <div class="articles__text-wrap">
                                    <p class="articles__text">  <?php
                                                        $len = strlen(get_the_title()); 
                                                        if ($len > 80):
                                                            echo substr(get_the_title(), 0, 80) . "...";
                                                        else:
                                                            echo get_the_title();
                                                        endif;
                                                        ?></p>
                                    <a href="<?php the_permalink(); ?>" class="articles__btn-link"></a>
                                </div>
                            </div>
                        </li>
                     
				<?}?>
                    </ul>
                </div>
            </div>
            <!--/END articles__inner-->
        </section>
		<? endif;?>
        <!--/END articles-->
    </main>



<script>
    $(document).ready(function () {
        $("body").addClass("home");
    });
// Get Word count
    function get_doc_word_count(val) {
      
        var wom = val.match(/\S+/g);
        return wom ? wom.length : 0;
    }

    $('#txt_area_upload_doc').bind("DOMNodeInserted", function () {
	
        if ($(this).text().trim() == "")
        { 
            $(".count").text("0");
        } else
        {
          //  var doc_detail = $('#txt_area_upload_doc').text().trim();
		  var doc_detail = $('#txt_area_upload_doc').val();
            //doc_detail = doc_detail.replace(/\s+/g, ' ');
            var words = get_doc_word_count(doc_detail);
            $(".count").text(words);
            $("#txt_area_upload_doc span").removeAttr("style");
        }
    });

    $('#txt_area_upload_doc').keydown(function (e) {
        if (e.keyCode == 13) {
            document.execCommand('insertHTML', false, ' ');
        }
    });

    $('#txt_area_upload_doc').keyup(function () {
		
       // if ($(this).text().trim() == "")
		   if ($(this).val() == "")
        {
            $(".count").text("0");
        } else
        {
           // var doc_detail = $('#txt_area_upload_doc').text().trim();
		    var doc_detail = $('#txt_area_upload_doc').val();
            //doc_detail = doc_detail.replace(/\s+/g, ' ');
            var words = get_doc_word_count(doc_detail);
            $(".count").text(words);
            $("#txt_area_upload_doc span").removeAttr("style");
        }
    });

    $('.proof_doc').click(function () {
        $('.spn_submited_doc_error').remove();
       // var desc = $.trim($("#txt_area_upload_doc").html());
	    var desc = $("#txt_area_upload_doc").val();
        if (desc == '') {
            $('div.submit_area').append('<p class="spn_submited_doc_error" style="color:red;"> Document should not be blank </p>');
            setTimeout(function () {
                $('.spn_submited_doc_error').fadeOut('slow');
            }, 2000);
        } else {
            localStorage.setItem('proof_doc', desc);
            if ($('header').hasClass('with_login')) {
                window.location.href = "<?php echo $login_url; ?>";
            } else {
                open_loginpop('register');
            }
        }
    });

    function checkTestWordCount()
    {
       // var words = $('#txt_area_upload_doc').text().replace(/^[\s,.;]+/, "").replace(/[\s,.;]+$/, "").split(/[\s,.;]+/).length;
		var words = $('#txt_area_upload_doc').val().replace(/^[\s,.;]+/, "").replace(/[\s,.;]+$/, "").split(/[\s,.;]+/).length;
		
      //  var desc = $.trim($("#txt_area_upload_doc").text());
		var desc = $("#txt_area_upload_doc").val();
		
        var data = new FormData();
        data.append('action', 'save_user_upload_doc123');
        data.append('word_desc', desc);

        if (desc != "")
        {
            $.ajax({
                url: '<?php echo admin_url('admin-ajax.php'); ?>',
                dataType: 'text', // what to expect back from the PHP script, if anything
                cache: false,
                contentType: false,
                processData: false,
                data: data,
                type: 'post',
                success: function (data) {

                    if (data == 'error')
                    {
                        // alert("Please enter data");
                    } else
                    {
                        // alert(data);
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    alert("Error");
                }
            });
        } else
        {
            alert('no data found');
        }
    }
</script>
<?php get_footer(); ?>  
