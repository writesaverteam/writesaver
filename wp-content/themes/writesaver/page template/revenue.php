<?php
/*
 * Template Name: Revenue
 */

get_header();
global $wpdb;
$current_user = wp_get_current_user();
$user_roles_array = $current_user->roles;
$user_role = array_shift($user_roles_array);
if (!is_user_logged_in() || $user_role != "proofreader") {
    echo '<script>window.location.href="' . get_site_url() . '"</script>';
    exit;
}

$user_id = get_current_user_id();
?>

<section>
    <div class="breadcum">
        <div class="container">
            <div class="page_title">
                <h1>Earnings</h1>
            </div>
        </div>
    </div>
</section>        
<section>
    <div class="container">
        <div class="privacy notification">
            <?php
            $total_amt = 0;
            $paid_amt = 0;
            $remaining_amt = 0;
            $pending_count = 0;
            $revenues = $wpdb->get_results("SELECT * FROM `tbl_proofreader_revenue` WHERE fk_proofreader_id= $user_id ORDER BY pk_proofreader_revenue_id ASC");
            if (count($revenues) > 0) {
                foreach ($revenues as $revenue) {
                    $total_amt = $total_amt + $revenue->requested_amount;
                    $status = $revenue->status;
                    if ($status == 'Pending')
                        $pending_count++;
                    if ($status == 'Pending' || $status == 'Process') {
                        $remaining_amt = $remaining_amt + $revenue->requested_amount;
                    }
                    if ($status == 'Completed') {
                        $paid_amt = $paid_amt + $revenue->requested_amount;
                    }
                }
            }
            ?>
            <div class="row service">
                <div class="col-sm-4">
                    <div class="total_ammount">
                        <div class="left">
                            <h4>$<?php echo $total_amt; ?></h4>
                            <p>Total amount</p>
                        </div>
                        <div class="right"></div>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="total_ammount paid">
                        <div class="left">
                            <h4>$<?php echo $paid_amt; ?></h4>
                            <p>Paid amount</p>
                        </div>
                        <div class="right"></div>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="total_ammount remaining">
                        <div class="left">
                            <h4>$<?php echo $remaining_amt; ?></h4>
                            <p>Remaining amount</p>
                        </div>
                        <div class="right"></div>
                    </div>
                </div>
            </div>
            <div class="proofer_section_title">
                <h4>Paid amount</h4>
                <?php
                if ($pending_count > 0) {
                    ?>
                    <a href="javascript:void(0);" class="btn_sky" id="request_payment">Request payment</a>
                <?php } ?>
            </div>
            <div class="history_table_main">
                <div class="history_table">

                    <div class="history_row">
                        <div class="history_col"></div>
                        <div class="history_col">Requested date</div>
                        <div class="history_col">Approval date</div>
                        <div class="history_col">Requested amount</div>
                        <div class="history_col">Status</div>
                    </div>
                    <?php
                    $paged = ( get_query_var('paged') ) ? get_query_var('paged') : 1;
                    $total = count($revenues);
                    $limit = 10;
                    $offset = ( $paged - 1 ) * $limit;
                    $num_of_pages = ceil($total / $limit);
                    $revenue_list = $wpdb->get_results("SELECT * FROM `tbl_proofreader_revenue` WHERE fk_proofreader_id= $user_id ORDER BY pk_proofreader_revenue_id DESC LIMIT $offset, $limit");
                    $ct = $offset + 1;
                    if (count($revenue_list) > 0) {
                        foreach ($revenue_list as $revenue) {
                            ?>
                            <div class="history_row">
                                <div class="history_col"><?php echo $ct++; ?></div>
                                <div class="history_col"><?php echo ($revenue->requested_date) ? date('m/d/Y h:i:s A', strtotime($revenue->requested_date)) : ''; ?></div>
                                <div class="history_col"><?php echo ($revenue->approval_date) ? date('m/d/Y h:i:s A', strtotime($revenue->approval_date)) : ''; ?></div>
                                <div class="history_col"><?php echo "$" . $revenue->requested_amount; ?></div>
                                <div class="history_col"><?php echo $revenue->status; ?></div>
                            </div> 
                            <?php
                        }
                    }
                    ?>
                </div>
                <?php
                if (count($revenue_list) <= 0) {
                    _e(' <div class="history_row"><p>You have no revenue yet..</p></div>');
                }
                ?>
            </div>
            <?php
            if (count($revenue_list) > 0) {
                ?>
                <div class="pagination_contain">
                    <p>Showing <?php
                        $entry = $limit + $offset;
                        if ($offset == 0) {
                            echo '1';
                        } else {
                            echo $offset + 1;
                        }
                        ?> to <?php
                        if ($entry > $total) {
                            echo $total;
                        } else {
                            echo $entry;
                        }
                        ?> of <?php echo $total; ?> entries</p>
                    <div class="pagination_main">
                        <?php
                        if (function_exists(custom_pagination)) {
                            custom_pagination($num_of_pages, "", $paged);
                        }
                        ?>
                    </div>
                </div>
            <?php }
            ?>
            <div class="revenue_msg"></div>
        </div>
    </div>
</section>
<script>
    jQuery(document).ready(function ($) {
        $("#request_payment").click(function () {
            $('.revenuemsg').remove();
            $('#loding').show();
            $.ajax({
                url: '<?php echo admin_url('admin-ajax.php'); ?>',
                data: {
                    'action': 'proofreader_request_payment'
                },
                dataType: 'text',
                success: function (data) {
                    if (data > 0) {
                        $(".revenue_msg").html('<span class="text-success revenuemsg">Payment request send successfully...</span>');
                        window.setTimeout(function () {
                            $('.revenuemsg').fadeOut();
                            location.reload();
                        }, 2000);
                    } else {
                        debugger;
                        $(".revenue_msg").html('<span class="text-danger revenuemsg">payment request not send successfully...</span>');
                        $(".revenuemsg").fadeOut(5000);
                    }
                    $('#loding').hide();
                },
                error: function (errorThrown) {
                    console.log(errorThrown);
                }
            });
            return false;
        });
    });
</script>
<?php
get_footer();
