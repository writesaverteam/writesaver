<?php
/*
 * Template Name: White Paper Edit
 */

$current_user = wp_get_current_user();
$user_roles_array = $current_user->roles;
$user_role = array_shift($user_roles_array);
$logged = is_user_logged_in();
if (is_user_logged_in()){
    if ($user_role == "customer") {
        echo '<script>window.location.href="' . get_the_permalink(762) . '"</script>';
        exit;
    } elseif ($user_role == "proofreader") {
        echo '<script>window.location.href="' . get_the_permalink(810) . '"</script>';
        exit;
    } elseif ($user_role == "administrator") {
       // echo '<script>window.location.href="' . site_url() . '/wp-admin"</script>';
       // exit;
    } else {
        
    }
}


get_header();
?>
  <main class="content" role="content">
        <section class="proofreading">
            <div class="proofreading__inner">
                <div class="proofreading__content" style="background-color: transparent;">
                    <div class="proofreading__heading-wrap">
                        <h1 class="proofreading__heading">ICO Whitepaper Proofreading by Native <br/>English Speakers</h1>
                    </div>
                    <!-- /END proofreading__heading-wrap-->

                    <div class="proofreading__content-inner">
                    </div>
                </div>
                <!-- /END proofreading__content-->


                <div class="proofreading__bot-wrap">
                    <a href="javascript:void(0);" class="proofreading__btn-link">Submit your Free Sample Document</a>
				
                </div>
            </div>
        </section>
		
		<!--/END proofreading-->

						<section class="about">
						<div class="about__inner">
						<div class="about__heading-wrap">
						<h2 class="about__heading">Best ICO Whitepaper Proofreading Service</h2>
						</div>
						<div class="about__description-wrap">
						<div class="about__description">
						<p class="about__description-text">Writesaver is the fastest, most affordable whitepaper proofreading service online. Unlike other services, Writesaver uses real, educated native English speakers to edit your whitepaper and make sure it is in fluent, clear English. <br/><br/>If you want a successful ICO, your whitepaper needs to be flawless. There's no better way to ensure your whitepaper is error free than with Writesaver! </p>

						</div>
						</div>
						</div>
						</section><!--/END about-->
								
								<section class="key-facts">
						<div class="key-facts__inner">
						<div class="key-facts__heading-wrap">
						<h2 class="key-facts__heading">Key facts</h2>
						</div>
						<!--END key-facts__heading-wrap-->
						<div class="key-facts__list-wrap">
						<ul class="key-facts__list">
							<li class="key-facts__item">
						<div class="key-facts__item-inner">
						<div class="key-facts__img-wrap"><div class="price_icon"></div></div>
						<h3 class="key-facts__caption">Affordability</h3>
						<p class="key-facts__tetx">Proofreading costs as low as $0.75 per 100 words</p>

						</div></li>
							<li class="key-facts__item">
						<div class="key-facts__item-inner">
						<div class="key-facts__img-wrap"><div class="experise_icon"></div></div>
						<h3 class="key-facts__caption">Professionalism</h3>
						<p class="key-facts__tetx">Native English speaking editors from top universities
						</p>

						</div></li>
							<li class="key-facts__item">
						<div class="key-facts__item-inner">
						<div class="key-facts__img-wrap"><div class="agility_icon"></div></div>
						<h3 class="key-facts__caption">Speed</h3>
						<p class="key-facts__tetx">The average document is proofread in just 4 hours</p>

						</div></li>
							<li class="key-facts__item">
						<div class="key-facts__item-inner">
						<div class="key-facts__img-wrap"><div class="industries_icon"></div></div>
						<h3 class="key-facts__caption">Expertise</h3>
						<p class="key-facts__tetx key-facts__tetx--small">Professional Whitepaper editing for ICOs</p>

						</div></li>
						</ul>
						<!--END key-facts__list-->

						</div>
						<!--/END key-facts__list-wrap-->

						</div>
						<!--END key-facts__inner-->

						</section>


		<?
		if (have_posts()) :
    while (have_posts()) : the_post();
        the_content();
    endwhile;
endif;
		
		//echo '<pre>'; print_r($Coments); echo '</pre>';	
		?>
		
        <section id="clients" class="clients">
            <div class="clients__inner">
                <div class="clients__heading-wrap">
                    <h2 class="clients__heading">What our clients say about us</h2>
                </div>

                <div class="clients__slider-wrap">
                    <div class="clients__slider">
                        <div class="clients__slid">
                            <div class="clients__slid-inner">
                                <div class="clients__slid-left">
                                    <div class="clients__slid-img-wrap">
                                        <img src="<?=get_template_directory_uri() ?>/img/clients/caroline.png" >
                                    </div>
                                </div>
                                <div class="clients__slid-right">
                                    <div class="clients__slid-text-wrap">
                                        <p class="clients__slid-text">
                                            “I love working with Writesaver. My emails and requests are always answered the same day that I write them and the customer service is just brilliant. They are always very respectful and understanding. The texts that I need proofread are always delivered within a day. The quality of their work is impecable. They have given my business peace of mind knowing that our writing is flawless thanks to their proofreading work.”
                                        </p>
                                    </div>
                                    <div class="clients__description-wrap">
                                        <div class="clients__line"></div>
                                           <p class="clients__about">Carolina, Startups with Souls, Consulting, Panama</p>
                                        <div class="clients__from-wrap">
                                            <a href="www.startupswithsouls.com" class="clients__from-link">
                                                <img src="<?=get_template_directory_uri() ?>/img/clients/startupswithsouls-logo.png" >
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--/END clients__slid-->
                        <div class="clients__slid">
                            <div class="clients__slid-inner">
                                <div class="clients__slid-left">
                                    <div class="clients__slid-img-wrap">
                                        <img src="<?=get_template_directory_uri() ?>/img/clients/olya.png" alt="/">
                                    </div>
                                </div>
                                <div class="clients__slid-right">
                                    <div class="clients__slid-text-wrap">
                                        <p class="clients__slid-text">
                                            “We used Writesaver to correct all of the text on our web development agency's website. They finished our entire website in less than a day, and managed to keep the intent, meaning, and tone of our original writing, while putting it into clear, fluent English. I'll definitely be using their services again!”
                                        </p>
                                    </div>
                                    <div class="clients__description-wrap">
                                        <div class="clients__line"></div>
                                          <p class="clients__about">Olya, Nordwhale, Software Development, Ukraine</p>
                                        <div class="clients__from-wrap">
                                            <a href="http://nordwhale.com" class="clients__from-link">
                                                <img src="<?=get_template_directory_uri() ?>/img/clients/company/Nordwhale_logo.png" alt="/">
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--/END clients__slid-->
                        <div class="clients__slid">
                            <div class="clients__slid-inner">
                                <div class="clients__slid-left">
                                    <div class="clients__slid-img-wrap">
                                        <img src="<?=get_template_directory_uri() ?>/img/clients/sergey.png" alt="/">
                                    </div>
                                </div>
                                <div class="clients__slid-right">
                                    <div class="clients__slid-text-wrap">
                                        <p class="clients__slid-text">
                                            “Our proofreading task was completed in a timely and diligent fashion, despite the significant size of the file. Now we can pursue our launch deadlines without worrying about the quality of our English texts. We are totally satisfied with the edits and the overall smooth and pleasant experience. My partners and I have decided to use Writesaver for all future proofreading jobs. Thanks and keep up the good work!.”
                                        </p>
                                    </div>
                                    <div class="clients__description-wrap">
                                        <div class="clients__line"></div>
                                                     <p class="clients__about">Sergey, UBI Agency, Business Services, Ukraine</p>

                                        <div class="clients__from-wrap">
                                            <a href="http://ubi.agency" class="clients__from-link">
                                                <img src="<?=get_template_directory_uri() ?>/img/clients/company/ubi-logo-0123-no-tagline.png" alt="/">
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--/END clients__slid-->
                        <div class="clients__slid">
                            <div class="clients__slid-inner">
                                <div class="clients__slid-left">
                                    <div class="clients__slid-img-wrap">
                                        <img src="<?=get_template_directory_uri() ?>/img/clients/shimin.png" alt="/">
                                    </div>
                                </div>
                                <div class="clients__slid-right">
                                    <div class="clients__slid-text-wrap">
                                        <p class="clients__slid-text">
                                            “I recommend Writesaver especially because of the easy communication between the customers and the Writesaver team, which makes the whole process transparent, efficient and pleasant. Their edits are careful and diligent, and their customer service team takes care of problems even outside of their job description. I had all of my Master’s application documents revised here, since I can tell that every revised document is fully reviewed and of high qualify. Many thanks to Writesaver for helping me get admitted to my ideal MA program in the end!”
                                        </p>
                                    </div>
                                    <div class="clients__description-wrap">
                                        <div class="clients__line"></div>
                                        <p class="clients__about">Shimin, Masters Student, China</p>

                                        <div class="clients__from-wrap">
                                           
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--/END clients__slid-->

                        <div class="clients__slid">
                            <div class="clients__slid-inner">
                                <div class="clients__slid-left">
                                    <div class="clients__slid-img-wrap">
                                
                                    </div>
                                </div>
                                <div class="clients__slid-right">
                                    <div class="clients__slid-text-wrap">
                                        <p class="clients__slid-text">
                                            “It was a pleasure to work with Writesaver. They offer a high quality and affordable proofreading service. Customer support is world class; very responsive and friendly, so working on even a large text went smoothly. I'd recommend every SaaS business try working with them to make their content more legible and credible.”
                                        </p>
                                    </div>
                                    <div class="clients__description-wrap">
                                        <div class="clients__line"></div>
                                       <p class="clients__about">Wojtek, Presspad, Mobile App for Publishers, Poland</p>

                                        <div class="clients__from-wrap">
                                            <a href="http://presspadapp.com" class="clients__from-link">
                                                <img src="<?=get_template_directory_uri() ?>/img/clients/company/PressPad_LOGO_main_horizontal_gradient.png" alt="/">
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--/END clients__slid-->
						          <div class="clients__slid">
                            <div class="clients__slid-inner">
                                <div class="clients__slid-left">
                                    <div class="clients__slid-img-wrap">
                                
                                    </div>
                                </div>
                                <div class="clients__slid-right">
                                    <div class="clients__slid-text-wrap">
                                        <p class="clients__slid-text">
                                            “All of our website texts are proofread by Writesaver. What else can I say? The service is really effective, and it is a pleasure to use Writesaver. When paired with its affordable pricing, all of this makes your service the best I have ever used.”
                                        </p>
                                    </div>
                                    <div class="clients__description-wrap">
                                        <div class="clients__line"></div>
                                          <p class="clients__about">Alexey, Russia for Me, Travel Agency, Russia</p>

                                        <div class="clients__from-wrap">
                                            <a href="http://russiaforme.com" class="clients__from-link">
                                                <img src="<?=get_template_directory_uri() ?>/img/clients/company/Russia-for-Me-Logo.png" alt="/">
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--/END clients__slid-->
						
                    </div>
                    <!--/END clients__slider-->

                    <div class="clients__icon-slider-wrap">
                        <div class="clients__icon-slider">
                            <div class="clients__icon-slid">
                                <div class="clients__icon-slid-inner">
                                    <img src="<?=get_template_directory_uri() ?>/img/clients/caroline.png" alt="/">
                                </div>
                            </div>
                            <div class="clients__icon-slid">
                                <div class="clients__icon-slid-inner">
                                    <img src="<?=get_template_directory_uri() ?>/img/clients/olya.png" alt="/">
                                </div>
                            </div>
                            <div class="clients__icon-slid">
                                <div class="clients__icon-slid-inner">
                                    <img src="<?=get_template_directory_uri() ?>/img/clients/sergey.png" alt="/">
                                </div>
                            </div>
                            <div class="clients__icon-slid">
                                <div class="clients__icon-slid-inner">
                                    <img src="<?=get_template_directory_uri() ?>/img/clients/shimin.png" alt="/">
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--/END clients__icon-slider-wrap-->
                </div>
                <!--/END clients__slider-wrap-->

                <div class="clients__company-slider-wrap">
                    <div class="clients__company-slider">
                        <div class="clients__company-slid">
                            <div class="clients__company-slid-inner">
                                <a class="clients__company-link" href="www.startupswithsouls.com">
                                    <img src="<?=get_template_directory_uri() ?>/img/clients/company/startupswithsouls-logo1.png" alt="/">
                                </a>
                            </div>
                        </div>
                        <div class="clients__company-slid">
                            <div class="clients__company-slid-inner">
                                <a class="clients__company-link" href="http://russiaforme.com">
                                    <img src="<?=get_template_directory_uri() ?>/img/clients/company/Russia-for-Me-Logo.png" alt="/">
                                </a>
                            </div>
                        </div>
                        <div class="clients__company-slid">
                            <div class="clients__company-slid-inner">
                                <a class="clients__company-link" href="http://nordwhale.com">
                                    <img src="<?=get_template_directory_uri() ?>/img/clients/company/Nordwhale_logo.png" alt="/">
                                </a>
                            </div>
                        </div>
                        <div class="clients__company-slid">
                            <div class="clients__company-slid-inner">
                                <a class="clients__company-link" href="http://ubi.agency">
                                    <img src="<?=get_template_directory_uri() ?>/img/clients/company/ubi-logo-0123-no-tagline.png" alt="/">
                                </a>
                            </div>
                        </div>
                        <div class="clients__company-slid">
                            <div class="clients__company-slid-inner">
                                <a class="clients__company-link" href="http://presspadapp.com">
                                    <img src="<?=get_template_directory_uri() ?>/img/clients/company/PressPad_LOGO_main_horizontal_gradient.png" alt="/">
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                <!--/END clients__company-slider-wrap-->
            </div>
        </section>
        <!--/END clients-->
        <section class="protect">
            <div class="protect__inner">
                <div class="protect__heading-wrap">
                                 <h2 class="protect__heading">Confidentiality Guaranteed</h2>

                    <p class="protect__text">All of our proofreaders agree to strict confidentiality agreements, and we hold all of our editors to the highest standards of confidentiality and trust. </p>
				</div>
            </div>
            <!--/END privacy__inner-->
        </section>
        <!--/END privacy-->
		
		
		<?php
			$query = new WP_Query(array( 'cat' => 1, 'posts_per_page' => 4 ) );
	
	        if ($query->have_posts()) :
		?>
        <section class="articles">
            <div class="articles__inner">
                <div class="articles__heading-wrap">
                    <h2 class="articles__heading">Popular articles</h2>
                </div>
                <div class="articles__list-wrap">
                    <ul class="articles__list">
					<?
				   while ($query->have_posts()) { $query->the_post();?>
                        <li class="articles__item">
                            <div class="articles__item-inner">
                                <div class="articles__img-wrap">
                                    <img src="<?php the_post_thumbnail_url(array(1140, 478)); ?>" class="img-responsive" alt="<?//titleImg()?>" title="<?//titleImg()?>" >
                                </div>
                                <div class="articles__text-wrap">
                                    <p class="articles__text">  <?php
                                                        $len = strlen(get_the_title()); 
                                                        if ($len > 80):
                                                            echo substr(get_the_title(), 0, 80) . "...";
                                                        else:
                                                            echo get_the_title();
                                                        endif;
                                                        ?></p>
                                    <a href="<?php the_permalink(); ?>" class="articles__btn-link"></a>
                                </div>
                            </div>
                        </li>
                     
				<?}?>
                    </ul>
                </div>
            </div>
            <!--/END articles__inner-->
        </section>
		<? endif;?>
        <!--/END articles-->
    </main>
<script>	
    $(document).ready(function () {
        $("body").addClass("home noneditor");
				$(".proofreading__btn-link").on("click", function (e) { open_loginpop('register'); });
		<?php //if($logged == false){ echo "open_loginpop('login');"; }?>
    });
</script>
<style>
.noneditor .proofreading__content.close{
	opacity: 1;
    display: block;
    text-shadow: none;
    float: none;
    font-size: inherit;
    font-weight: inherit;
    line-height: normal;
}
</style>
    });
</script>
<?php get_footer(); ?>  
