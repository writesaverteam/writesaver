<?php
/*
 * Template Name: User Redirect
 */
get_header();
$google = 0;
$facebook = 0;

if (is_user_logged_in()){
    $referer = wp_get_referer();
	$userId = get_current_user_id();
	global $wpdb;
    if (strpos($referer, 'proofreader') !== false) {
        $role = "proofreader";
    } else {
        $role = "customer";
    }
		
	if ($role == 'customer'){
		$customer = $wpdb->get_results("SELECT * FROM tbl_customer_general_info WHERE fk_customer_id= $userId LIMIT 1 ");
		$proofreader = $wpdb->get_results("SELECT * FROM wp_proofreader_notification_setting WHERE user_id = $userId LIMIT 1 ");		
		if (count($customer) == 0 && count($proofreader) == 0) {
			$free_words = of_get_option('free_words_for_customer');
			$wpdb->insert(
				'tbl_customer_general_info', 
				array(
					'fk_customer_id' => $userId,
					'free_words' => $free_words,
					'total_submited_docs' => 0,
					'remaining_credit_words' => $free_words,
					'created_date' => date('Y-m-d H:i:s')
				)
			);


			$wpdb->insert(
				'wp_notification_settings', 
				array(
					'user_id' => $userId,
					'dash_doc_started' => 1,
					'dash_doc_completed' => 1,
					'receive_doc_started' => 1,
					'receive_doc_completed' => 1,
					'receive_stories' => 1,
					'createddate' => date('Y-m-d H:i:s')
				)
			);
			
			wp_update_user(array('ID' => $userId, 'role' => $role));
			
			$provider = get_user_meta($userId, 'thechamp_provider', true);
			
			if($provider == 'facebook'){
				$facebook = 1;
			} else if($provider == 'google'){
				$google = 1;
			}
		}
		
		if (strpos($referer, 'plan') !== false) {
			$url = get_the_permalink(14);
		} else {
			$url = get_the_permalink(762);
		}
	} elseif ($role == 'proofreader'){
		$customer = $wpdb->get_results("SELECT * FROM tbl_customer_general_info WHERE fk_customer_id= $userId LIMIT 1 ");
		$proofreader = $wpdb->get_results("SELECT * FROM wp_proofreader_notification_setting WHERE user_id = $userId LIMIT 1 ");		
		if (count($customer) == 0 && count($proofreader) == 0) {
			$wpdb->insert(
				'wp_proofreader_notification_setting',
				array(
					'user_id' => $userId,
					'desktop_new_doc' => 1,
					'desktop_other_notification' => 1,
					'email_info_about_availability' => 1,
					'email_relevant_stories' => 1,
					'created_date' => date('Y-m-d H:i:s')
				)
			);
			
			wp_update_user(array('ID' => $userId, 'role' => $role));
		}
		
		$info = get_user_meta($userId, 'info_completed', true);
		$test = get_user_meta($userId, 'test_completed', true);
		if ($info == 1) {
			if ($test == 1) {
				$url = get_the_permalink(810);
			} else {
				$url = get_the_permalink(774);
			}
		} else {
			$url = get_the_permalink(770);
		}
	} else {
		$url = home_url();
	}
} else {
	$url = home_url();
}

//wp_redirect($url);
?>
<section class="proofreading">
    <div class="proofreading__inner">
        <div class="proofreading__content">
            <div class="proofreading__heading-wrap" style="min-height: 287px;">
                <h1 class="proofreading__heading">Please wait......redirecting.</h1>
                <h2 class="proofreading__heading">If you're not immediately redirected, please click <a href="https://writesaver.co/customer-dashboard">here</a>.</h2>
            </div>
        </div>
    </div>
</section>
<?php get_footer(); ?>
<script type="text/javascript" id="">(function(a,e,f,g,b,c,d){a.GoogleAnalyticsObject=b;a[b]=a[b]||function(){(a[b].q=a[b].q||[]).push(arguments)};a[b].l=1*new Date;c=e.createElement(f);d=e.getElementsByTagName(f)[0];c.async=1;c.src=g;d.parentNode.insertBefore(c,d)})(window,document,"script","https://www.google-analytics.com/analytics.js","ga");ga("create","UA-80475559-1","auto");ga("send","pageview");</script>
<script type='text/javascript'>
	var google = <?php echo $google; ?>;
	var facebook = <?php echo $facebook; ?>;
	$(function(){
		if(google == 1){
			fbq('track', 'CompleteRegistration');
			ga('send', 'event', 'Register Submit', 'Google Register', {
				hitCallback: function() {
				  location.href = '<?php echo $url; ?>';
				}
			});
		} else if(facebook == 1){
			fbq('track', 'CompleteRegistration');
			ga('send', 'event', 'Register Submit', 'Facebook Register', {
				hitCallback: function() {
				  location.href = '<?php echo $url; ?>';
				}
			});
		} else {
			location.href = '<?php echo $url; ?>';
		}
	});
</script>