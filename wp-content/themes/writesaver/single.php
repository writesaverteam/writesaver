<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
add_filter('wp_head', 'rel_link_wp_head_new', 10, 0 );
get_header();
global $post;
$categories = get_the_category( $post->ID );
$catidlist = '';
foreach( $categories as $category) {
    $catidlist .= $category->cat_ID . ",";
}
?>

<section>
    <div class="breadcum">
        <div class="container">
            <div class="page_title">
                <p>Blog</p>
            </div>
        </div>
    </div>
</section>
<section>
    <div id="main_blog" >
        <div class="blog_category_sticky">
            <div class="container">
                <div class="blog_category_sticky_right">
                    <div class="search_box">
                        <?php get_search_form(); ?>
                    </div>
                </div>
                <div class="blog_category_sticky_left">
                    <div class="desktop_catagory">
                        <div class="category_full_list">
                            <div class="category_btn">
                                <div class="category_btn_icon">
                                    <img src="<?php echo get_template_directory_uri() ?>/images/cat_icon.png" class="img-responsive">
                                </div>
                                <div class="category_btn_txt">
                                    <span>Categories</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div> 
            </div>
        </div>
        <div class="blog_category_main blog_details">
            <div class="container">
				<div class="row">
					<?php $imgIter=1; while (have_posts()) : the_post(); ?>
						<article class="col-xs-12 col-sm-12 col-md-8 col-lg-8 mainArticle">
							<div class="blog_block">
								<h5><?php the_date(); ?> 
								<?php
									$category_detail = get_the_category(get_the_ID()); //$post->ID
									if ($category_detail):
										$cat_count = count($category_detail);
										$count = 0;
										echo ' <span>';
										foreach ($category_detail as $cd) {
											$count ++;
											echo $cd->cat_name;
											if ($count != $cat_count)
												echo ', ';
										}
										echo '</span>';
									endif;
								?>    
								</h5>
								<?php the_title('<h1>', '</h1>'); ?>
								<div class="row">
								  <div class="col-xs-12 col-sm-6 col-md-9 authLeft">
									<?php echo get_avatar( get_the_author_meta( 'ID' ), 60, '', '', array('class' => 'img-circle img-responsive') ); ?>
									<h6><?php the_author(); ?><br /> 
										<span>
											<?php 
												$title = get_user_meta( get_the_author_meta( 'ID' ), '_blog_title', true );
												$authorData = get_userdata( get_the_author_meta( 'ID' ) ); 
												if($title){
													echo $title;
												} else {
													echo 'Brand ' . ucfirst(array_shift($authorData->roles));
												}  
											?>
									   </span>
									</h6>
								  </div>
								  <div class="col-xs-12 col-sm-6 col-md-3">
									<ul class="authRight pull-right">
									  <?php
										$fb = get_user_meta( get_the_author_meta( 'ID' ), '_fb_link', true );
										if($fb){
											echo '<li class="facebook"><a target="_blank" href="' . esc_url($fb) . '"><i class="fa fa-facebook"></i></a></li>';
										}
										
										$tw = get_user_meta( get_the_author_meta( 'ID' ), '_tw_link', true );
										if($tw){
											echo '<li class="twitter"><a target="_blank" href="' . esc_url($tw) . '"><i class="fa fa-twitter"></i></a></li>';
										}
										
										$linkedin = get_user_meta( get_the_author_meta( 'ID' ), '_pn_link', true );
										if($linkedin){
											echo '<li class="linkedin"><a target="_blank" href="' . esc_url($linkedin) . '"><i class="fa fa-linkedin"></i></a></li>';
										}
									  ?>
									</ul>
								  </div>
								</div>
								<?php if (has_post_thumbnail()) : ?>
									<img src="<?php the_post_thumbnail_url(array(1140, 478)); ?>" class="articleTopImg img-responsive" title="<?titleImg('Blog'); echo $imgIter;?>" alt="<?titleImg('Blog'); echo $imgIter;?>">
								<?php endif; ?>
								<?php the_content(); ?>
								<div class="articleQuiz">
								  <div class="articleQuizLeft">
									<h4>Was this post helpful to you? Share it:</h4>
									<div class="articleQuiZUnder"></div>
								  </div>
								  <div class="quizSocial text-right">
									<?php echo do_shortcode('[TheChamp-Sharing]'); ?>
								  </div>
								</div>
							</div>
						</article>  
					<?php $imgIter++; endwhile; wp_reset_query();?>
					<aside class="col-xs-12 col-sm-12 col-md-4 col-lg-4 sideBar">
						<div class="row">
							<div class="col-xs-12 asideTopHolder">
								<div class="asideTop">
								  <?php echo do_shortcode('[contact-form-7 id="13260" title="Subscribe Form"]');?>
								</div>
							</div>
						</div>
						<div class="col-xs-12 asideMidHolder">
							<div class="row">
							  <div class="col-xs-12 text-center asideMidHead">
								<h3>TODAY'S MUST READS</h3>
								<div class="asideMidUnder"></div>
							  </div>
							  <?php
								$must_reads_meta = get_post_meta( $post->ID, 'must-reads', true );
								if($must_reads_meta){
									$must_reads_posts = explode(',', $must_reads_meta);
										$args = array(
										'posts_per_page' => 5,
										'post__in'  => $must_reads_posts,
									);
								} else {
									$args = array(
										'posts_per_page' => 5,
									);
								}
								
								$featured = new WP_Query($args);
								if ($featured->have_posts()): while($featured->have_posts()): $featured->the_post(); ?>
								  <div class="col-xs-12 col-sm-6 col-md-12 col-lg-12 asideMidCont">
									<a href="<?php the_permalink(); ?>">
									  <div class="mindContInn <?php if (!has_post_thumbnail()) : echo 'nothumbnail'; endif; ?>">
										<?php if (has_post_thumbnail()) : ?>
											<img src="<?php the_post_thumbnail_url(array(360, 360)); ?>" alt="" class="img-responsive">
										<?php endif; ?>
										<h4><?php the_title(); ?></h4>
									  </div>
									</a>
								  </div>
								<?php 
									endwhile;
									wp_reset_query();
									endif;
								?>
							  <div class="col-xs-12 col-sm-6 col-md-12 col-lg-12 asideMidCont">
								  <div class="mindContInnSPec text-center">
									<div class="mindContInnSPecTop">
									  <div class="mySpec"></div>
										<h1>Want Perfect English Writing?</h1>
									</div>
									<div class="myspecInn">
										<p>Write like a native English speaker with Writesaver's incredible editing team</p>
									</div>
									<a class="btn btn-info trialBtn" href="<?php echo esc_url(home_url('/register')); ?>">Take Your Free Trial</a>
								  </div>
							  </div>
							</div>
						</div>
					</aside>
				</div>
            </div>
        </div>
    </div>
</section>
<?php
	$custom_query_args = array( 
		'posts_per_page' => 3, // Number of related posts to display
		'post__not_in' => array($post->ID), // Ensure that the current post is not displayed
		'orderby' => 'rand', // Randomize the results
		'cat' => $catidlist, // Select posts in the same categories as the current post
	);
	// Initiate the custom query
	$related = new WP_Query( $custom_query_args );
	if ($related->have_posts()):
?>
	<section class="relatedPosts">
		<div class="container">
			<div class="row">
			  <div class="col-xs-12 text-center relatedPostsHead">
				<h1>related posts</h1>
			  </div>
			  <div class="col-xs-12">
				<div class="row">
				<?php while($related->have_posts()): $related->the_post(); ?>
				  <div class="col-xs-12 col-sm-4 col-md-4 rPosts">
					  <div class="relatedPostsInner">
						<a href="<?php the_permalink(); ?>">
							<?php if (has_post_thumbnail()) : ?>
								<img src="<?php the_post_thumbnail_url('full'); ?>" alt="" class="relatedPostImg img-responsive">
							<?php endif; ?>
							<div class="postConts">
							  <h3><?php the_title(); ?></h3>
							  <h6><?php echo get_the_date(); ?></h6>
							  <div class="quizSocial relatedPostSocial">
								<?php echo do_shortcode('[TheChamp-Sharing]'); ?>
							  </div>
							</div>
							<div class="clearfix"></div>
						</a>
					  </div>
				  </div>
				<?php endwhile; wp_reset_query(); ?>
				</div>
			  </div>
			</div>
		</div>
	</section>
<?php endif; ?>
<?php if ( comments_open() || get_comments_number() ) {
	comments_template();
} ?>
<?php get_footer(); ?>
<script type="text/javascript" src="//downloads.mailchimp.com/js/signup-forms/popup/unique-methods/embed.js" data-dojo-config="usePlainJson: true, isDebug: false"></script><script type="text/javascript">window.dojoRequire(["mojo/signup-forms/Loader"], function(L) { L.start({"baseUrl":"mc.us14.list-manage.com","uuid":"0caa87439d59f169fb4621c50","lid":"417b6aae18","uniqueMethods":true}) })</script>
