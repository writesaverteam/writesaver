<?php

/*
 * Template Name: Stripe Webhook
 */
http_response_code(200);

global $wpdb;
global $logstr;
$logstr = "";

require_once 'stripe/Stripe.php';

$stripe_secretkey = pmpro_getOption("stripe_secretkey");
Stripe::setApiKey($stripe_secretkey);

$body = @file_get_contents('php://input');
$event_json = json_decode($body);

file_put_contents(dirname( __FILE__ ) . "/../../logs/stripe_request_" . date('d-m-Y') . ".log", print_r($body, true) . PHP_EOL, FILE_APPEND | LOCK_EX);
//real event?
if(!empty($event_json->id)){
	if ($event_json->type == 'invoice.payment_succeeded') {
		$order = getOrderFromInvoiceEvent($event_json);
		
		$subscription_transaction_id = $event_json->data->object->subscription;
		$code = $event_json->data->object->lines->data[0]->plan->id;
		$price = $event_json->data->object->lines->data[0]->amount / 100;
		$stripe_reference = $event_json->data->object->subscription;
		$invoice = $event_json->data->object;
		
		if(empty($order->id))
		{				
			//last order for this subscription //getOldOrderFromInvoiceEvent($event_json);
			$old_order = new MemberOrder();
			$old_order->getLastMemberOrderBySubscriptionTransactionID($subscription_transaction_id);
			
			//lookup by customer id
			if(empty($old_order) || empty($old_order->id))
			{
				$old_order->getLastMemberOrderBySubscriptionTransactionID($event_json->data->object->customer);
			}
			
			//still can't find the order
			if(empty($old_order) || empty($old_order->id))
			{
				$logstr .= "Couldn't find the original subscription. Subscription ID = " . $subscription_transaction_id . ".";
				custom_pmpro_stripeWebhookExit();
			}

			$user_id = $old_order->user_id;
			$user = get_userdata($user_id);

			if(empty($user))
			{
				$logstr .= "Couldn't find the old order's user. Order ID = " . $old_order->id . ".";
				custom_pmpro_stripeWebhookExit();
			}
			
			$pmpro_level = pmpro_getLevel($old_order->membership_id);
			
			if($old_order->status == "success"){
				//alright. create a new order/invoice
				$morder = new MemberOrder();
				$morder->user_id = $old_order->user_id;
				$morder->membership_id = $old_order->membership_id;

				if(isset($invoice->amount))
				{
					$morder->subtotal = number_format((float) $invoice->subtotal / 100, 2, '.', '');
					$morder->total = number_format((float) $invoice->subtotal / 100, 2, '.', '');				
				}
				elseif(isset($invoice->subtotal))
				{
					$morder->tax = 0.00;
					$morder->subtotal = number_format((float) $invoice->subtotal / 100, 2, '.', '');
					$morder->total = number_format((float) $invoice->subtotal / 100, 2, '.', '');
				}
				
				$level_id = $morder->membership_id;
				$level_detail = pmpro_getLevel($level_id);
				
				if ( (float) $level_detail->billing_amount != (float) $morder->total ) {
					$discount = (float) $morder->total - (float) $level_detail->billing_amount;
					$morder->notes = 'Discount Amount = ' . $discount;
					$morder->total = $amount;
				}

				$morder->payment_transaction_id = $invoice->id;
				$morder->subscription_transaction_id = $invoice->subscription;

				$morder->gateway = $old_order->gateway;
				$morder->gateway_environment = $old_order->gateway_environment;
				$morder->payment_type = 'Stripe';

				$morder->FirstName = $old_order->FirstName;
				$morder->LastName = $old_order->LastName;
				$morder->Email = $wpdb->get_var("SELECT user_email FROM $wpdb->users WHERE ID = '" . $old_order->user_id . "' LIMIT 1");
				$morder->Address1 = $old_order->Address1;
				$morder->City = $old_order->billing->city;
				$morder->State = $old_order->billing->state;
				$morder->Zip = $old_order->billing->zip;
				$morder->PhoneNumber = $old_order->billing->phone;

				$morder->billing = new stdClass();
				
				$morder->billing->name = $morder->FirstName . " " . $morder->LastName;
				$morder->billing->street = $old_order->billing->street;
				$morder->billing->city = $old_order->billing->city;
				$morder->billing->state = $old_order->billing->state;
				$morder->billing->zip = $old_order->billing->zip;
				$morder->billing->country = $old_order->billing->country;
				$morder->billing->phone = $old_order->billing->phone;

				//get CC info that is on file
				$morder->cardtype = '';
				$table_name = $wpdb->prefix . 'creditdebit_card_details';
				$card = $wpdb->get_row("SELECT * FROM $table_name WHERE customer_id = $user_id");
				$morder->accountnumber = $card->cardnumber;
				$morder->expirationmonth = $card->expirymonth;
				$morder->expirationyear = $card->expyear;

				//save
				$morder->status = "success";
				$morder->saveOrder();
				$logstr .= "Created new order with ID #" . $morder->id . ". Event ID #" . $event_json->id . ".";
			}
			
			if($old_order->status == "pending"){
				$morder = new MemberOrder($old_order->id);
				
				//get some more order info
				$morder->getMembershipLevel();
				$morder->getUser();
				
				$level_id = $morder->membership_id;
				$level_detail = pmpro_getLevel($level_id);
				
				//update membership
				if ( ipnChangeMembershipLevel( $invoice->id, $morder ) ) {
					$logstr .= "Updated order with ID #" . $morder->id . ". Event ID #" . $event_json->id . ".";
				} else {
					$logstr .= "ERROR: Couldn't change level for order (" . $morder->code . ").";
					custom_pmpro_stripeWebhookExit();
				}
			}

			//$morder->getMemberOrderByID($morder->id);
			
			//add words
			$plan_words = $level_detail->plan_words;

			$user_info = $wpdb->get_results("SELECT * FROM tbl_customer_general_info WHERE fk_customer_id = $user_id LIMIT 1");

			if (!empty($user_info)) {
				$remaining_credit_words = $user_info[0]->remaining_credit_words;
				$total_worls = $remaining_credit_words + $plan_words;
				$info_id = $user_info[0]->pk_customer_general_id;
				$wpdb->update(
						'tbl_customer_general_info', array('remaining_credit_words' => $total_worls), array('pk_customer_general_id' => $info_id), array('%d'), array('%d')
				);

				$date = date('Y-m-d h:i:s');
				$description = "Subscriptions charges for " . $level_detail->name;
				$wpdb->insert('wp_price_per_extra_words', array('fk_customer_id' => $user_id, 'stripe_reference' => $invoice->id, 'payment_date' => $date, 'price' => $price, 'descriptions' => $description, 'payment_source' => 'Stripe', 'words' => $plan_words, 'status' => 1));
				
			} else {
				$total_worls = $order_detail->plan_words;
			}
			
			$subject = 'You have new words on Writesaver.';
			$descs = 'You have new words in your Writesaver account! Our proofreaders are standing by and are ready to help you write with perfect, native English on your emails, papers, documents, proposals, and any other writing you can think of. If you have any questions about your account, feel free to shoot us an email at contact@writesaver.co, we\'ll get back to you as soon as we can, and we\'re always happy to help.


			To your success in writing,
			The Writesaver Team';
			$noti = 'You have new words available.';
			$descs .= '
			<p>Below is some more information about your order:</p>
			<p>Membership Level:' . $level_detail->name . '</p>
			<p>Membership Fee: $' . $level_detail->initial_payment . '</p>
			<p>Words added: ' . $level_detail->plan_words . '</p>
			<p>Price Per Additional Word: ' . $level_detail->price_per_additional_word . '</p>
			  <p>Total Words in your Account: ' . $total_worls . '</p>';

			send_cust_notification($user_id, '', $descs, 1, 1, $subject, $noti);
			
			//update membership
			$startdate = current_time("mysql");
			$sqlQuery = "UPDATE $wpdb->pmpro_memberships_users
							SET billing_amount = '" . esc_sql($level_detail->billing_amount) . "',
								initial_payment = '" . esc_sql($level_detail->billing_amount) . "',
								startdate = '" . esc_sql($startdate) . "',
								cycle_number = '" . esc_sql($level_detail->cycle_number) . "',
								cycle_period = '" . esc_sql($level_detail->cycle_period) . "'
							WHERE user_id = '" . esc_sql($user_id) . "'
								AND membership_id = '" . esc_sql($order->membership_id) . "'
								AND status = 'active'
							LIMIT 1";

			$wpdb->query($sqlQuery);

			custom_pmpro_stripeWebhookExit();
		
		} else {
			$logstr .= "We've already processed this order with ID #" . $order->id . ". Event ID #" . $pmpro_stripe_event->id . ".";
			custom_pmpro_stripeWebhookExit();
		}
	}

	if ($event_json->type == 'customer.subscription.deleted') {		
		//for one of our users? if they still have a membership for the same level, cancel it
		$old_order = getOldOrderFromInvoiceEvent($event_json);

		if(!empty($old_order)) {
			$user_id = $old_order->user_id;
			$user = get_userdata($user_id);
			if(!empty($user->ID)) {
				//$attempt_count = $event_json->data->object->attempt_count;
				//if ($attempt_count > 0) {
					do_action("pmpro_stripe_subscription_deleted", $user->ID);

					if ( $old_order->status == "cancelled" ) {
						$logstr .= "We've already processed this cancellation. Probably originated from WP/PMPro. (Order #" . $old_order->id . ", Subscription Transaction ID #" . $old_order->subscription_transaction_id . ")";
					} elseif ( ! pmpro_hasMembershipLevel( $old_order->membership_id, $user->ID ) ) {
						$logstr .= "This user has a different level than the one associated with this order. Their membership was probably changed by an admin or through an upgrade/downgrade. (Order #" . $old_order->id . ", Subscription Transaction ID #" . $old_order->subscription_transaction_id . ")";
					} else {
						//if the initial payment failed, cancel with status error instead of cancelled					
						pmpro_cancelMembershipLevel( $old_order->membership_id, $old_order->user_id, 'cancelled' );

						$logstr .= "Cancelled membership for user with id = " . $old_order->user_id . ". Subscription transaction id = " . $old_order->subscription_transaction_id . ".";

						//send an email to the member
						$myemail = new PMProEmail();
						$myemail->sendCancelEmail( $user, $old_order->membership_id );

						//send an email to the admin
						$myemail = new PMProEmail();
						$myemail->sendCancelAdminEmail( $user, $old_order->membership_id );
					}

					$logstr .= "Subscription deleted for user ID #" . $user->ID . ". Event ID #" . $event_json->id . ".";
					custom_pmpro_stripeWebhookExit();
				//}
			} else {
				$logstr .= "Stripe tells us a subscription is deleted, but we could not find a user here for that subscription. Could be a subscription managed by a different app or plugin. Event ID #" . $event_json->id . ".";
				custom_pmpro_stripeWebhookExit();
			}
		} else {
			$logstr .= "Stripe tells us a subscription is deleted, but we could not find the order for that subscription. Could be a subscription managed by a different app or plugin. Event ID #" . $event_json->id . ".";
			custom_pmpro_stripeWebhookExit();
		}
	}
}

function custom_pmpro_stripeWebhookExit(){
	global $logstr;

	//for log
	if($logstr)
	{
		$logstr = "Logged On: " . date_i18n( "m/d/Y H:i:s" ) . "\n" . $logstr . "\n-------------\n";
		echo $logstr;
		
		//file
		$file = dirname( __FILE__ ) . "/../../logs/stripe_ipn_" . date('d-m-Y') . ".log";
		$loghandle = (file_exists($file)) ? fopen($file,'a') : fopen($file,'w');
		fwrite($loghandle, $logstr);
		fclose($loghandle);
	}
	exit;
}

function getOrderFromInvoiceEvent($event_json){
	//pause here to give PMPro a chance to finish checkout
	sleep(5);
	
	$invoice_id = $event_json->data->object->id;

	//get order by invoice id
	$order = new MemberOrder();
	$order->getMemberOrderByPaymentTransactionID($invoice_id);		
	
	if(!empty($order->id))
		return $order;
	else
		return false;
}

function getOldOrderFromInvoiceEvent($event_json){
	//pause here to give PMPro a chance to finish checkout
	sleep(5);
	
	global $wpdb;

	$customer_id = $event_json->data->object->customer;
	$subscription_id = $event_json->data->object->id;


	// okay, add an invoice. first lookup the user_id from the subscription id passed
	$old_order_id = $wpdb->get_var("SELECT id FROM $wpdb->pmpro_membership_orders WHERE (subscription_transaction_id = '" . $customer_id . "' OR subscription_transaction_id = '"  . esc_sql($subscription_id) . "') AND gateway = 'stripe' ORDER BY timestamp DESC LIMIT 1");

	if (!empty($old_order_id)) {

		$old_order = new MemberOrder( $old_order_id );

		if(isset($old_order->id) && ! empty($old_order->id))
			return $old_order;
	}

	return false;
}

/*
	Change the membership level. We also update the membership order to include filtered valus.
*/
function ipnChangeMembershipLevel( $txn_id, &$morder ) {

	global $wpdb;

	//set the start date to current_time('timestamp')
	$startdate = current_time( 'mysql' );

	//fix expiration date
	if ( ! empty( $morder->membership_level->expiration_number ) ) {
		$enddate = "'" . date_i18n( "Y-m-d", strtotime( "+ " . $morder->membership_level->expiration_number . " " . $morder->membership_level->expiration_period, current_time( "timestamp" ) ) ) . "'";
	} else {
		$enddate = "NULL";
	}

	//custom level to change user to
	$custom_level = array(
		'user_id'         => $morder->user_id,
		'membership_id'   => $morder->membership_level->id,
		'code_id'         => $discount_code_id,
		'initial_payment' => $morder->membership_level->initial_payment,
		'billing_amount'  => $morder->membership_level->billing_amount,
		'cycle_number'    => $morder->membership_level->cycle_number,
		'cycle_period'    => $morder->membership_level->cycle_period,
		'billing_limit'   => $morder->membership_level->billing_limit,
		'trial_amount'    => $morder->membership_level->trial_amount,
		'trial_limit'     => $morder->membership_level->trial_limit,
		'startdate'       => $startdate,
		'enddate'         => $enddate
	);
	
	//change level and continue "checkout"
	if ( pmpro_changeMembershipLevel( $custom_level, $morder->user_id ) !== false ) {
		//update order status and transaction ids
		
		$level_id = $morder->membership_id;
		$level_detail = pmpro_getLevel($level_id);
		
		if ( (float) $level_detail->billing_amount != (float) $morder->total ) {
			$discount = (float) $morder->total - (float) $level_detail->billing_amount;
			$morder->notes = 'Discount Amount = ' . $discount;
			$morder->total = $amount;
		}
		
		$morder->status                 = "success";
		$morder->payment_transaction_id = $txn_id;
		$morder->saveOrder();

		return true;
	} else {
		return false;
	}
}
?>
