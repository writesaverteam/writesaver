<?php ob_start();
require_once('../../../wp-load.php');
header("Content-type: application/json");
error_reporting(0);
if(sanitize_text_field($_POST['message'] == 'yes')){
	global $wpdb;
	
	$check = $wpdb->get_results(
				$wpdb->prepare(
					"SELECT * FROM course_subscribers WHERE course_id = %d AND email = %s", sanitize_text_field($_POST['course_id']), sanitize_email($_POST['email'])
					)
				);
	if($check){
		$data['success'] = false;
		$data['message'] = 'Sorry, your email address already exists.';
	} else {
		$result = $wpdb->insert(
					'course_subscribers',
					array(
						'firstname' => sanitize_text_field($_POST['firstname']),
						'lastname'  => sanitize_text_field($_POST['lastname']),
						'email'     => sanitize_email($_POST['email']),
						'course'    => sanitize_text_field($_POST['course']),
						'course_id' => sanitize_text_field($_POST['course_id']),
					),
					array(
						'%s',
						'%s',
						'%s',
						'%s',
						'%d'
					)
				);

		if($result) {
			$resultset = array();
			$results = $wpdb->get_col($wpdb->prepare("SELECT course FROM course_subscribers WHERE email = %s", sanitize_email($_POST['email'])));

			$courses = implode(", ", $results);
			
			$data['success'] = true;
			$data['message'] = 'Thanks for signing up for the course! You\'ll be receiving an email with your first lesson soon!';
			$data['courses'] = $courses;
		} 
		else {
			$data['success'] = false;
			$data['message'] = 'We experienced an error with your registration. Please try again later.';
		}
	}
} else {
	$data['success'] = false;
   	$data['message'] = 'There was an error!';
}
echo json_encode($data);
ob_end_flush(); 
?>