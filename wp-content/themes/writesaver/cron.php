<?php
require_once(dirname( __FILE__ ) . '/../../../wp-load.php');

global $wpdb;
global $log;
$log = "";

$options = $wpdb->get_row("SELECT * FROM wp_price_per_words");
$docs = $wpdb->get_results( $wpdb->prepare("SELECT * FROM wp_assigned_document_details a INNER JOIN wp_customer_document_details d ON a.fk_doc_details_id = d.pk_doc_details_id WHERE a.fk_doc_details_id NOT IN (SELECT fk_doc_details_id FROM wp_skipped_document_details ) AND a.status = %s", "In Process"));
$datetime = date('Y-m-d H:i:s');

foreach ( $docs as $doc ) 
{
	$word_count = (int)$doc->word_end_no - (int)$doc->word_start_no;
	$duration = ceil($options->time_per_words*$word_count) + ceil($options->atime_per_words*$word_count) + ceil($options->timeout*60);
	$assigned = $doc->assign_date;
	
	if(strtotime('now') > strtotime("$assigned +$duration sec")){
		$result = $wpdb->delete(
			'wp_assigned_document_details', array(
			'fk_doc_details_id' => $doc->pk_doc_details_id,
			'fk_proofreader_id' => $doc->fk_proofreader_id,
			)
		);
		
		$wpdb->update(
			'wp_customer_document_details', array(
				'status' => 'Pending',
				'modified_date' => $datetime
			), 
			array(
				'pk_doc_details_id' => $doc->pk_doc_details_id
			)
		);
		
		$log .= "Document ID - " . $doc->pk_doc_details_id . ", Proofreader ID - " . $doc->fk_proofreader_id . " assigned on " . $assigned . " (duration: " . $duration . "seconds) unassigned on " . $datetime;
	} else {
		$log .= "Document ID - " . $doc->pk_doc_details_id . ", Proofreader ID - " . $doc->fk_proofreader_id . " assigned on " . $assigned . " still in progress.";
	}
	cronExit();
}

function cronExit(){
	global $log;

	//for log
	if($log)
	{
		$log = "Logged On: " . date_i18n( "m/d/Y H:i:s" ) . "\n" . $log . "\n-------------\n";
		echo $log;
		
		//file
		$file = dirname( __FILE__ ) . "/../../logs/cron_job_" . date('d-m-Y') . ".log";
		$loghandle = (file_exists($file)) ? fopen($file,'a') : fopen($file,'w');
		fwrite($loghandle, $log);
		fclose($loghandle);
	}
	exit;
}