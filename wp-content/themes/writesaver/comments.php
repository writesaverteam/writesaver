<?php
/**
 * The template for displaying comments
 *
 * This is the template that displays the area of the page that contains both the current comments
 * and the comment form.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage writesaver
 * @since 1.0
 * @version 1.0
 */

/*
 * If the current post is protected by a password and
 * the visitor has not yet entered the password we will
 * return early without loading the comments.
 */
if ( post_password_required() ) {
	return;
}

global $allowedtags;
$allowed = '';
foreach ( (array) $allowedtags as $tag => $attributes ) {
	if($tag == 'a'){
		$allowed .= '<button type="button" class="btnSpec text-center"><i class="fa fa-link"></i></button>';
	}
	if($tag == 'b'){
		$allowed .= '<button type="button" class="btnSpec text-center"><i class="fa fa-bold"></i></button>';
	}
	if($tag == 'i'){
		$allowed .= '<button type="button" class="btnSpec text-center"><i class="fa fa-italic"></i></button>';
	}
	if($tag == 'strike'){
		$allowed .= '<button type="button" class="btnSpec text-center"><i class="fa fa-strikethrough"></i></button>';
	}
}

$args = array(
  'id_form'           => 'commentform',
  'class_form'        => 'comment-form',
  'id_submit'         => 'submit',
  'class_submit'      => 'btn btn-info sendBtn pull-right',
  'name_submit'       => 'submit',
  'title_reply'       => __( '' ),
  'title_reply_to'    => __( '' ),
  'cancel_reply_link' => __( '' ),
  'label_submit'      => __( 'SEND' ),
  'format'            => 'xhtml',
  'comment_field' =>  '<textarea id="comment" name="comment" cols="80" rows="5" aria-required="true" class="form-control inputBox" placeholder="Comment here..." maxlength="65525" required="required">' .
	'</textarea>',
  'must_log_in' => '<p class="must-log-in">' .
	sprintf(
	  __( 'You must be <a href="%s">logged in</a> to post a comment.' ),
	  wp_login_url( apply_filters( 'the_permalink', get_permalink() ) )
	) . '</p>',
  'logged_in_as' => '',
  'comment_notes_before' => '',
  'comment_notes_after' => '<div class="buttonsHolder"><div class="btn-group">' . $allowed . '</div></div>',
  'fields' => apply_filters( 'comment_form_default_fields', $fields ),
);
?>
<section class="comments">
	<div class="container">
		<div class="row">
			<div class="col-xs-12 commentsHead">
				<h3>
					<?php
					$comments_number = get_comments_number();
					printf(
						_nx(
							'%1$s Comment',
							'%1$s Comments',
							$comments_number,
							'comments title',
							'writesaver'
						),
						number_format_i18n( $comments_number )
					);
					?>
				</h3>
				<?php if ( ! comments_open() && get_comments_number() && post_type_supports( get_post_type(), 'comments' ) ) : ?>
					<h3><?php _e( 'Comments are closed.', 'writesaver' ); ?></h3>
				<?php endif; ?>
				<div class="comentsUnder"></div>
			</div>
			<div class="col-xs-12 commentBoxOuter comment-respond" id="respond">
				<?php echo get_avatar( get_current_user_id(), 60, '', '', array('class' => 'img-circle img-responsive') ); ?>
				<div class="commentBoxInn">
					<h2>
						<?php
							$current_user = wp_get_current_user();
							echo $current_user->display_name;
						?>
					</h2>
					<?php comment_form($args); ?>
				</div>
			</div>
			<?php if ( have_comments() ) : ?>
			<div class="col-xs-12">
				<ul class="commentsList">
					<?php
						wp_list_comments( array(
							'avatar_size' => 80,
							'style'       => 'ul',
							'short_ping'  => true,
							'type'        => 'comment',
							'callback'    => 'comment_callback'
						) );
					?>
				</ul>
			</div>
			<?php endif; ?>
		</div>
	</div>
</section>