<?php

/*
 * Template Name: paypal-process
 */

$plan_id = $_REQUEST['level'];
$datetime = date('Y-m-d H:i:s');
$timestamp = strtotime($datetime);

if(isset($_SESSION['sess_id'])){
	$sess_id = $_SESSION['sess_id'];
} else {
	$_SESSION['sess_id'] = $timestamp;
	$sess_id = $timestamp;
}

$success = home_url() . "/paypal-process/";
$cancel = get_the_permalink(762) . "/?token=xd0eu8c9cxd&level=cancel";

$plan_dtls = $wpdb->get_results("SELECT * FROM wp_pmpro_membership_levels WHERE id = " . $plan_id);
$item = $plan_dtls[0]->name . ' at Writesaver';
$amount = round($plan_dtls[0]->billing_amount, 2);

//no of month,days or years
$period = $plan_dtls[0]->cycle_number;
global $period_length;
//M-month,D-Day,Y-year
if ($plan_dtls[0]->cycle_period == 'Month') {
    $period_length = 'M';
} else if ($plan_dtls[0]->cycle_period == 'Day') {
    $period_length = 'D';
} else if ($plan_dtls[0]->cycle_period == 'Year') {
    $period_length = 'Y';
}

//store card data
global $wpdb;

$user_id = get_current_user_id();
$user_info = get_userdata($user_id);
$prefix = $wpdb->prefix;
$table_name = $prefix . 'creditdebit_card_details';
$user_infio = $wpdb->get_row("SELECT * FROM $table_name WHERE customer_id = $user_id");
$ExpMonth = $user_infio->expirymonth;
$CCNumber = decrypt_string($user_infio->cardnumber);
$ExpYear = $user_infio->expyear;
$CVNumber = $user_infio->securitycode;

$gen_info = $wpdb->get_row("SELECT * FROM  tbl_customer_general_info WHERE fk_customer_id = $user_id");



$CardType = "Visa";

$postalcode = "751010";

######################################################################
######################## PAYPAL ######################################
######################################################################
require_once('paypal.class.php');  // include the class file	

$p = new paypal_class;             // initiate an instance of the class
$business_emails = $gen_info->paypal_id;

$business_email = get_option('pmpro_gateway_email');
//$business_email = 'suresk_1314104870_biz@yahoo.com';

$gateway_environment = get_option('pmpro_gateway_environment');
if ($gateway_environment == 'sandbox') {
    $p->paypal_url = "https://www.sandbox.paypal.com/cgi-bin/webscr";
} elseif ($gateway_environment == 'live') {
    $p->paypal_url = "https://www.paypal.com/cgi-bin/webscr";
}

$currency_code = get_option('pmpro_currency');

//$p->paypal_url = 'https://www.sandbox.paypal.com/cgi-bin/webscr'; // Testing paypal url
// if there is not action variable, set the default action of 'process'
if (empty($_GET['action']))
    $_GET['action'] = 'process';

switch ($_GET['action']) {
    case 'process':      // Process and order...	
        //card details variables
        $p->add_field('first_name', $user_info->first_name);
        $p->add_field('last_name', $user_info->last_name);
        $p->add_field('credit_card_type', $CardType);
        $p->add_field('cc_number', $CCNumber);
        $p->add_field('cvv2_number', $CVNumber);
        $p->add_field('expdate_month', $ExpMonth);
        $p->add_field('expdate_year', $ExpYear);
        $p->add_field('email', $business_emails); //
        $p->add_field('login_email', $business_emails);

        $p->add_field('zip', $postalcode);
        //paypal process variables
        $p->add_field('business', $business_email); //
        $p->add_field('cmd', '_xclick-subscriptions');
        $p->add_field('return', $success . '?action=success');
        $p->add_field('cancel_return', $success . '?action=cancel');
        $p->add_field('currency_code', $currency_code);
        $p->add_field('item_name', $item);
		
		//create order
		$pmpro_level = pmpro_getLevel($plan_id);
		$morder = new MemberOrder();
		$morder->code = $morder->getRandomCode();
		$morder->user_id = $user_id;
		$morder->membership_id = $pmpro_level->id;

		$morder->subtotal = $amount;
		$morder->tax = 0;
		$morder->total = $amount;

		$morder->payment_transaction_id = '';
		$morder->subscription_transaction_id = '';
		$morder->payment_type = 'PayPal Express';
		$morder->paypal_token = '';

		$morder->gateway = 'paypalexpress';
		$morder->gateway_environment = get_option('pmpro_gateway_environment');

		//save
		$morder->status = "review";
		$morder->saveOrder();
        $p->add_field('item_number', $morder->code);
		
		//Check Discount
		$codes = $wpdb->get_row($wpdb->prepare("SELECT * FROM tbl_discount_codes c, tbl_discount_codes_applications a WHERE a.user_id = %d AND c.code = a.code_id AND a.sess_id = %d AND c.subscription = %d AND c.type = 2 LIMIT 1", $user_id, $sess_id, $plan_id));
		if($codes){	
			$uses = $wpdb->get_row($wpdb->prepare("SELECT COUNT(*) as usedcounts FROM tbl_discount_codes_uses WHERE code_id = %s AND user_id = %d", $codes->code_id, $user_id));
			
			$discounted_words_amount = (float)$plan_dtls[0]->price_per_additional_word*(int)$codes->discounted_words;
			$tdiscount = (float)$amount - (float)$discounted_words_amount;
			$discount = round((float) $tdiscount, 2);
			if($discount < 1){
				$discount = 0;
			}
									
			if(strtotime($codes->expires) > strtotime('-1 day') && ($codes->uses > $uses->usedcounts || $codes->uses == 0)) {
				$p->add_field('a1', $discount);
				$p->add_field('p1', $period);
				$p->add_field('t1', $period_length);
				
				if($codes->uses == 2){
					$p->add_field('a2', $discount);
					$p->add_field('p2', $period);
					$p->add_field('t2', $period_length);
				}
			}
		}

        //$p->add_field('custom', $user_id);		 
        //subscription variables
        $p->add_field('a3', $amount);
        $p->add_field('p3', $period);
        $p->add_field('t3', $period_length);
        $p->add_field("src", 1); //Subscribe will start
        $p->add_field("sra", 1); //Reattemp to pay if failure	
        $p->add_field('no_note', '1');
        $p->submit_paypal_post(); // submit the fields to paypal
        //$p->dump_fields();      // for debugging, output a table of all the fields
        break;
    case 'success':      // Order was successful...		
		if($_REQUEST['subscr_id']){
			$morder = new MemberOrder();
			$morder->getMemberOrderByCode($_REQUEST['item_number']);
			$user_id = get_current_user_id();
			$pmpro_level = pmpro_getLevel($morder->membership_id);
			
			if($morder->status == "review"){
				$startdate = current_time("mysql");
				//calculate the end date
				if (!empty($pmpro_level->expiration_number)) {
					$enddate = date_i18n("Y-m-d", strtotime("+ " . $pmpro_level->expiration_number . " " . $pmpro_level->expiration_period, current_time("timestamp")));
				} else {
					$enddate = "NULL";
				}
				
				$morder->user_id = $user_id;
				
				$morder->payment_transaction_id = '';
				$morder->subscription_transaction_id = '';
				$morder->paypal_token = $_REQUEST['payer_id'];
				
				//save
				$morder->status = "pending";
				$morder->saveOrder();
			}			
			
			//update the current user
			global $current_user;
			if (!$current_user->ID && $user->ID) {
				$current_user = $user;
			} //in case the user just signed up
				pmpro_set_current_user();
			//}
			//$_GET['level'] = $pmpro_level->id;
			/* Adding words and sending email per add_words in functions.php */
			//add_words();
			//Check Discount
			$codes = $wpdb->get_row($wpdb->prepare("SELECT * FROM tbl_discount_codes c, tbl_discount_codes_applications a WHERE a.user_id = %d AND c.code = a.code_id AND a.sess_id = %d AND c.subscription = %d AND c.type = 2 LIMIT 1", $user_id, $sess_id, $pmpro_level->id));
			if($codes){	
				$uses = $wpdb->get_row($wpdb->prepare("SELECT COUNT(*) as usedcounts FROM tbl_discount_codes_uses WHERE code_id = %s AND user_id = %d", $codes->code_id, $user_id));
							
				if(strtotime($codes->expires) > strtotime('-1 day') && ($codes->uses > $uses->usedcounts || $codes->uses == 0)) {
					header("location:" . get_the_permalink(762) . "?token=xs00u8c9frd&level=success&discount=yes");
				} else {
					header("location:" . get_the_permalink(762) . "?token=xs00u8c9frd&level=success");
				}
			} else {
				header("location:" . get_the_permalink(762) . "?token=xs00u8c9frd&level=success");
			}
		} else {
			header("location:" . get_the_permalink(762) . "?token=xd0eu8c9cdf&level=cancel");
		}        
        /* $custom=array();
          //Write into the file
          $fp=fopen("hellotest.txt","w");
          foreach($_POST as $key => $value){
          fwrite($fp,$key.'===='.$value."\n");
          } */
        // This is where you would probably want to thank the user for their order
        // or what have you.  The order information at this point is in POST 
        // variables.  However, you don't want to "process" the order until you
        // get validation from the IPN.  That's where you would have the code to
        // email an admin, update the database with payment status, activate a
        // membership, etc.  	 
        //header("location:thanks_payment.php");

        exit;

        // You could also simply re-direct them to another page, or your own 
        // order status page which presents the user with the status of their
        // order based on a database (which can be modified with the IPN code 
        // below).
        break;
    case 'cancel':       // Order was canceled...	
        // The order was canceled before being completed.			
        //header("location:index.php");
        header("location:" . $cancel);
        break;
    case 'ipn':          // Paypal is calling page for IPN validation...	   
        // It's important to remember that paypal calling this script.  There
        // is no output here.  This is where you validate the IPN data and if it's
        // valid, update your database to signify that the user has payed.  If
        // you try and use an echo or printf function here it's not going to do you
        // a bit of good.  This is on the "backend".  That is why, by default, the
        // class logs all IPN data to a text file.		  
        if ($p->validate_ipn()) {

            extract($_POST);
            if ($txn_id <> '') {
                $custom = array();
                //Write into the file
                $fp = fopen("hellotest.txt", "w");
                foreach ($_POST as $key => $value) {
                    fwrite($fp, $key . '====' . $value . "\n");
                }
                //$custom=explode("::",$_POST[custom]);
            }
        }
        break;
}
?>
        