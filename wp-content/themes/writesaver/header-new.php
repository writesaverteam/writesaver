<?php
/*
  Theme Name: writesaver
  Theme URI: 192.168.0.87/wp_content/themes/writesaver
  Description: A brief description.
  Version: 1.0
  Author: Admin
  Author URI: http://192.168.0.87/writesaver
 */
?>
<!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title><?php { wp_title(''); } ?></title>
	<link rel="icon" type="image/x-icon" href="<?php echo of_get_option('favicon_logo'); ?>">
    <link href="<?php echo get_template_directory_uri() ?>/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
	<link href="<?php echo get_template_directory_uri() ?>/css/font-awesome.css" rel="stylesheet" type="text/css"/>
	<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/landing.css" />
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script> 
	<script src="<?php echo get_template_directory_uri() ?>/js/bootstrap.min.js" type="text/javascript"></script>
	<?php wp_head(); ?>
</head>
<body>
<div class="wrapper">
